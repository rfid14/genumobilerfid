package com.genuwin.mobile.Service


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Matrix
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import com.bixolon.labelprinter.BixolonLabelPrinter
import com.genuwin.android.base.CallBackListener
import com.zebra.sdk.comm.BluetoothConnection
import com.zebra.sdk.comm.Connection
import com.zebra.sdk.comm.ConnectionException
import com.zebra.sdk.graphics.internal.ZebraImageAndroid
import com.zebra.sdk.printer.PrinterStatus
import com.zebra.sdk.printer.ZebraPrinter
import com.zebra.sdk.printer.ZebraPrinterFactory
import com.zebra.sdk.printer.ZebraPrinterLinkOs


class PrinterService( var context: Context, var printer_option: PRINTER_BAND ,var  printer_address: String ,var printer_value: String ,var callBackListener: CallBackListener) {

    enum class PRINTER_BAND(val value: Int) {
        BIXOLON_LAN(1),
        ZEBRA_BLUETOOTH(2);

    }

    private var printer_name: String = ""

    var isPrinterConnected = false



    private lateinit var mZebraLabelPrinter: Connection
    private lateinit var mBixolonLabelPrinter: BixolonLabelPrinter


    private val mHandlerBixolon: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                BixolonLabelPrinter.MESSAGE_STATE_CHANGE -> when (msg.arg1) {
                    BixolonLabelPrinter.STATE_CONNECTED -> {
                        isPrinterConnected = true
                    }
                    BixolonLabelPrinter.STATE_CONNECTING -> {
                        callBackListener.callBack("Connecting To Print")
                        isPrinterConnected = false
                    }
                    BixolonLabelPrinter.STATE_NONE -> {
                        callBackListener.callBack("Error $printer_name")
                        isPrinterConnected = false
                    }

                }
                BixolonLabelPrinter.MESSAGE_READ -> {
                }
                BixolonLabelPrinter.MESSAGE_DEVICE_NAME -> {
                    this@PrinterService.printer_name =  msg.data.getString(BixolonLabelPrinter.DEVICE_NAME).toString()
                    isPrinterConnected = if (printer_name.contains(".")) { //where ip
                         callBackListener.callBack("Connected To Printer: $printer_name")
                        true
                    } else {
                          callBackListener.callBack("Error $printer_name")
                        false
                    }
                }
                BixolonLabelPrinter.MESSAGE_USB_DEVICE_SET -> {
                    if (msg.obj == null) {
                          callBackListener.callBack("No connectable to printer:$printer_name")
                        isPrinterConnected = false
                    }
                }
                BixolonLabelPrinter.MESSAGE_NETWORK_DEVICE_SET -> {
                    isPrinterConnected = if (msg.obj == null) {
                         callBackListener.callBack("No connectable to printer. Please check your network")
                        false
                    } else {
                        mBixolonLabelPrinter.connect(
                            printer_address,
                            9100,
                            10000
                        )
                        true
                    }
                }
            }
        }
    }

    fun checkConnect() {

        when (printer_option) {

            PRINTER_BAND.BIXOLON_LAN -> {
                mBixolonLabelPrinter = BixolonLabelPrinter(context, mHandlerBixolon, null)
                mBixolonLabelPrinter.findNetworkPrinters(2000)
            }
            PRINTER_BAND.ZEBRA_BLUETOOTH -> try {
                mZebraLabelPrinter = BluetoothConnection(printer_address)
                (mZebraLabelPrinter as BluetoothConnection).open()
                val printer: ZebraPrinter = ZebraPrinterFactory.getInstance(mZebraLabelPrinter)
                val linkOsPrinter: ZebraPrinterLinkOs = ZebraPrinterFactory.createLinkOsPrinter(printer)
                val printerStatus: PrinterStatus = linkOsPrinter.currentStatus
                isPrinterConnected = if (printerStatus.isReadyToPrint) {
                    callBackListener.callBack("Connected To Printer with MAC: $printer_address")
                    true
                } else {
                    callBackListener.callBack("Can Not Connected To Printer with MAC: $printer_address")
                    false
                }
            } catch (ex: Exception) {
                Log.e("Check status: ", ex.message.toString())
                 callBackListener.callBack("Can Not Connected, Please Check P4T Printer")
                isPrinterConnected = false
            } finally {

            }
        }
    }

    fun printer(
        bitmap: Bitmap,
        width: Int,
        hor: Int,
        ver: Int
    ) {
        when (printer_option) {
            PRINTER_BAND.BIXOLON_LAN -> {
                var levelPrintB = Integer.valueOf(printer_value)
                if (levelPrintB > 80) levelPrintB = 80
                if (levelPrintB < 20) levelPrintB = 20

                 callBackListener.requestProgressDialog("","Printing Barcode, Please Wait...")

                val finalLevelPrintB = levelPrintB
                object : Thread() {
                    override fun run() { //  mBixolonLabelPrinter.drawBitmap(bitmap, 148,2,938, finalLevelPrintB);//fix size report 3 so,1 la dua trai qua phai,2 ladua page xuong,3 khung giay
                        mBixolonLabelPrinter.drawBitmap(
                            bitmap,
                            hor,
                            ver,
                            width,
                            finalLevelPrintB
                        ) //fix size report 3 so,1 la dua trai qua phai,2 ladua page xuong,3 khung giay
                        mBixolonLabelPrinter.print(1,1)  //tab s== drawBitmap(bitmap, 100, 136,1050, levelPrintB);//fix
                        try {
                            sleep(1000)
                        } catch (e: InterruptedException) {
                            e.printStackTrace()
                        }
                         callBackListener.requestProgressDialogDismiss()
                    }
                }.start()
            }
            PRINTER_BAND.ZEBRA_BLUETOOTH -> {
                val bitmap1 = getRotatedBitmap(bitmap, 180)
               callBackListener.requestProgressDialog("","Printing Barcode, Please Wait...")
                object : Thread() {
                    override fun run() {
                        try {
                            if (Looper.myLooper() == null) {
                                Looper.prepare()
                            }
                            //        mZebraLabelPrinter = getZebraPrinterConn();
                            //         mZebraLabelPrinter.open();
                            val printer: ZebraPrinter =
                                ZebraPrinterFactory.getInstance(mZebraLabelPrinter)
                            val linkOsPrinter: ZebraPrinterLinkOs =
                                ZebraPrinterFactory.createLinkOsPrinter(printer)
                            val printerStatus: PrinterStatus =  linkOsPrinter.currentStatus

                            if (printerStatus.isReadyToPrint) {
                                try {
                                    ///// TODO: 4/23/2016 View MD9
                                    //helper.showLoadingDialog("Printer Ready \nProcessing to print");
                                    //alertToastLongTop("Printer Ready \nProcessing to print");
                                    //       printer.sendCommand("^MD9");
                                    printer.printImage(
                                        ZebraImageAndroid(bitmap1),
                                        35,
                                        0,
                                        0,
                                        0,
                                        false
                                    )
                                } catch (e: Exception) { //helper.showErrorDialogOnGuiThread(e.getMessage());
                                    callBackListener.callBack("Exception: " + e.message.toString())
                                }
                            } else if (printerStatus.isHeadOpen) {

                                callBackListener.callBack("Head Open Please Close Printer Head to Print.")

                            } else if (printerStatus.isPaused) {

                                callBackListener.callBack("Printer Paused")
                            } else if (printerStatus.isPaperOut) {

                                callBackListener.callBack("Error: Media Out Please Load Media to Print.")
                            } else {
                                callBackListener.callBack("Please check the Connection of the Printer.")
                            }
                            //       mZebraLabelPrinter.close();
                        } catch (e: ConnectionException) { //helper.showErrorDialogOnGuiThread(e.getMessage());
                            callBackListener.callBack("ConnectionException: " + e.message.toString())
                        }

                        /*catch (ZebraPrinterLanguageUnknownException e) {
                        //helper.showErrorDialogOnGuiThread(e.getMessage());
                        Log.e("UnknownException: ", e.getMessage());*/

                        catch (e: Exception) { //helper.showErrorDialogOnGuiThread(e.getMessage());
                            callBackListener.callBack("ConnectionException: " + e.message.toString())
                        } finally {
                            bitmap.recycle()
                            Looper.myLooper()!!.quit()
                        }
                         callBackListener.requestProgressDialogDismiss()
                    }
                }.start()
            }
        }
    }

    private fun getRotatedBitmap(bitmap: Bitmap, rotation: Int): Bitmap {
        val matrix = Matrix()
        val bitmapResize = Bitmap.createScaledBitmap(
            bitmap, bitmap.width,
            (bitmap.height * 0.89).toInt(), true
        )
        matrix.postRotate(
            rotation.toFloat(), (bitmapResize.width / 2.0).toFloat(),
            (bitmapResize.height / 2.0).toFloat()
        )
        return Bitmap.createBitmap(
            bitmapResize, 0, 0, bitmapResize.width,
            bitmapResize.height, matrix, true
        )
    }

    fun close() { //TODO CODE HERE
    }


}
