package com.genuwin.mobile.Service;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class WebService {

    public static String URL = "";
    public static String dbName = "";
    public static String dbPwd = "";
    public static String dbUser = "";
    private static String NAMESPACE = "http://tempuri.org/";


    private static String OnExcute_MethodName = "OnExcute";

    public WebService(String url, String dbName, String dbUser, String dbPwd) {
        this("http://tempuri.org/", url, dbName, dbUser, dbPwd);
    }

    public WebService(String namespace, String url, String dbName, String dbUser, String dbPwd) {
        NAMESPACE = namespace;
        URL = url;
        WebService.dbName = dbName;
        WebService.dbUser = dbUser;
        WebService.dbPwd = dbPwd;
    }


    public int GetMax(SoapObject a) {
        int max = 0;
        for (int row = 0; row < a.getPropertyCount(); row++)
            if (max < ((SoapObject) a.getProperty(row)).getPropertyCount())
                max = ((SoapObject) a.getProperty(row)).getPropertyCount();
        return max;
    }

    public Result GetDataTableArg(String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
    {
        String GetDataTableArg_MethodName = "Arg";
        Result result = new Result();

        result.data = new String[0][0];

        String SOAP_ACTION = NAMESPACE + GetDataTableArg_MethodName;
        SoapObject request = new SoapObject(NAMESPACE, GetDataTableArg_MethodName);
        //add parameter for request
        request.addProperty("Procedure", Procedure);
        request.addProperty("para", para);
        request.addProperty("dbName", dbName);
        request.addProperty("dbUser", dbUser);
        request.addProperty("dbPwd", dbPwd);

        //Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        ////////////////////////   //////////////////////////////////////////////////////
        SoapObject table = null;
        SoapObject tableRow = null;
        SoapObject responseBody = null;
        try {
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);

            //Log.e("requestDump:  ", androidHttpTransport.requestDump);
            //Log.e("responseDump:  ", androidHttpTransport.responseDump);

            SoapObject response = (SoapObject) envelope.getResponse();// This step: get file XML
            responseBody = (SoapObject) response.getProperty(1);//remove information XML,only retrieved results that returned

            if (responseBody.getPropertyCount() > 0) {
                table = (SoapObject) responseBody.getProperty(0);//get information XMl of tables that is returned

                int numberColumn = 1;
                if (table.getPropertyCount() > 0) {//check data response
                    numberColumn = GetMax(table);//GET NUMBER COLUMNS

                    Log.e("Total rows: ", String.valueOf(table.getPropertyCount()));
                    Log.e("Total columns: ", String.valueOf(numberColumn));

                    result.data = new String[table.getPropertyCount()][numberColumn];
                    for (int row = 0; row < table.getPropertyCount(); row++) {
                        tableRow = (SoapObject) table.getProperty(row);
                        for (int col = 0; col < ((SoapObject) table.getProperty(row)).getPropertyCount(); col++)
                            result.data[row][col] = (tableRow.getProperty(col) == null) ? "-" : tableRow.getProperty(col).toString();
                        if (((SoapObject) table.getProperty(row)).getPropertyCount() < numberColumn)
                            for (int col = ((SoapObject) table.getProperty(row)).getPropertyCount(); col < numberColumn; col++)
                                result.data[row][col] = "-";
                    }
                }

                if (table.getPropertyCount() == 1 && numberColumn == 1) {
                    result.error = true;
                    result.message = result.data[0][0];
                }
            }

        } catch (SoapFault e) {

            Log.e("\n\n SoapFault -- error", "", e.getCause());

            result.error = true;
            result.message = e.getMessage();
        } catch (IOException e) {
            Log.e("\n\n IOException -- error", "", e);
            result.error = true;
            result.message = e.getMessage();

        } catch (XmlPullParserException e) {
            //  e.printStackTrace();


            result.error = true;
            result.message = e.getMessage();

        } catch (Exception e) {
            Log.e("Exception", "exception: " + e.toString());
            result.error = true;
            result.message = e.getMessage() == null ? e.toString() : e.getMessage();

        }
        return result;

    }

    public ResultListData GetListDataTableArg(String Procedure, String para, int numcurr)//, String dbName, String dbUser, String dbPwd)
    {

        ResultListData result = new ResultListData();
        String GetData_MethodName = "GetDataByJson";

        String SOAP_ACTION = NAMESPACE + GetData_MethodName;
        SoapObject request = new SoapObject(NAMESPACE, GetData_MethodName);
        //add parameter for request
        request.addProperty("Procedure", Procedure);
        request.addProperty("para", para);
        request.addProperty("numcurr", numcurr);
        request.addProperty("dbName", dbName);
        request.addProperty("dbUser", dbUser);
        request.addProperty("dbPwd", dbPwd);

        //Create envelope
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        //Set output SOAP object
        envelope.setOutputSoapObject(request);
        Log.d("vvv", "((((" + URL);
        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        ////////////////////////   //////////////////////////////////////////////////////
        SoapObject table = null;
        SoapObject tableRow = null;
        SoapObject responseBody = null;
        try {
            androidHttpTransport.debug = true;
            androidHttpTransport.call(SOAP_ACTION, envelope);

            //Log.e("requestDump:  ", androidHttpTransport.requestDump);
            //Log.e("responseDump:  ", androidHttpTransport.responseDump);

            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            JResultData jd = new JResultData(response.toString());


            try {
                List<JDataTable> jdt = jd.getListODataTable();
                for (int i = 0; i < jdt.size(); i++) {
                    JDataTable dataTable = jdt.get(i);
                    String[][] data = new String[dataTable.totalrows][dataTable.columns.size()];

                    for (int j = 0; j < dataTable.totalrows; j++) {
                        for (int k = 0; k < dataTable.columns.size(); k++) {
                            String str = dataTable.records.get(j).get(dataTable.columns.get(k)).toString();
                            if (str.isEmpty() || str == null) {
                                data[j][k] = "";
                            } else {
                                data[j][k] = str;
                            }

                        }
                    }
                    result.listDataString.add(data);
                    result.listData.add(dataTable);

                }
            } catch (Exception exx) {
                Log.e("\n\n Format -- error", "", exx.getCause());

                result.error = true;
                result.message = exx.getMessage();
            }

            if( result.listData.size() != numcurr){
                result.error = true;
                result.message = response.toString();
            }


        } catch (SoapFault e) {

            Log.e("\n\n SoapFault -- error", "", e.getCause());

            result.error = true;
            result.message = e.getMessage();
        } catch (IOException e) {
            Log.e("\n\n IOException -- error", "", e);
            result.error = true;
            result.message = e.getMessage();

        } catch (XmlPullParserException e) {
            //  e.printStackTrace();


            result.error = true;
            result.message = e.getMessage();

        } catch (Exception e) {
            Log.e("Exception", "exception: " + e.toString());
            result.error = true;
            result.message = e.getMessage() == null ? e.toString() : e.getMessage();

        }
        return result;

    }

    public ResultString TableReadOpenString (String Procedure, String para)//, String dbName, String dbUser, String dbPwd)
    {

        ResultString result = new ResultString();
        Log.e("**** TableRead***URL: ",URL);
        String SOAP_ACTION = NAMESPACE+"TableReadOpenString";
        SoapObject request = new SoapObject(NAMESPACE, "TableReadOpenString");

        request.addProperty("Procedure",Procedure);
        request.addProperty("para", para);
        request.addProperty("dbName", dbName);
        request.addProperty("dbUser",dbUser);
        request.addProperty("dbPwd",dbPwd);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
        androidHttpTransport.debug=false;//--tan
        try {
            androidHttpTransport.debug=true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            result.data = response.toString();

            if (result.data.startsWith("ORA-")) {
                result.error = true;
                result.message = result.data;
            }

        } catch (Exception e) {
            Log.e("Exception e    ","ERROR WebService TableReadOpenString: "+e.toString());
            e.printStackTrace();
            result.error = true;
            result.message = e.getMessage() == null ? e.toString() : e.getMessage();
        }

        return result;
    }

    public  Result UploadImageAndThumb(String _procedure,
                                        byte[] imagefull,  String para){

        String updateImage = "UploadImage2";

        Result result = new Result();

        result.data = new String[1][1];

        String SOAP_ACTION = NAMESPACE + updateImage;
        SoapObject request = new SoapObject(NAMESPACE, updateImage);


        request.addProperty("dbName", dbName);

        request.addProperty("_dbuser", dbUser);
        request.addProperty("_dbpass",dbPwd);
        request.addProperty("_procedure",_procedure);

        request.addProperty("buff", imagefull);
        request.addProperty("para", para);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        //For 64bit build



        //	androidHttpTransport.debug=false;//--tan

        try {
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            new MarshalBase64().register(envelope);

            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            androidHttpTransport.debug=true;
            androidHttpTransport.call(SOAP_ACTION, envelope);
            SoapPrimitive response = (SoapPrimitive) envelope.getResponse();
            result.data[0][0] = response.toString();

//            if (!result.data[0][0].equals("000")) {
//                result.error = true;
//                result.message = result.data[0][0];
//            }
            if (result.data[0][0].startsWith("ORA-")) {
                result.error = true;
                result.message = result.data[0][0];
            }

        } catch (SoapFault e) {

            Log.e("\n\n SoapFault -- error", "", e.getCause());

            result.error = true;
            result.message = e.getMessage();
        } catch (IOException e) {
            Log.e("\n\n IOException -- error", "", e);
            result.error = true;
            result.message = e.getMessage();

        } catch (XmlPullParserException e) {
            //  e.printStackTrace();


            result.error = true;
            result.message = e.getMessage();

        } catch (Exception e) {
            Log.e("Exception", "exception: " + e.toString());
            result.error = true;
            result.message = e.getMessage() == null ? e.toString() : e.getMessage();

        }
        return result;
    }


    public class Result {


        protected String[][] data;
        protected boolean error = false;
        protected String message = "";

        public String[][] getData() {
            return data;
        }

        public boolean isError() {
            return error;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class ResultString {


        protected String data;
        protected boolean error = false;
        protected String message = "";

        public String getData() {
            return data;
        }

        public boolean isError() {
            return error;
        }

        public String getMessage() {
            return message;
        }
    }

    public static class ResultListData {


        protected List<JDataTable> listData = new ArrayList<JDataTable>() {
        };
        protected List<String[][]> listDataString = new ArrayList<String[][]>();
        protected boolean error = false;
        protected String message = "";

        public JDataTable getData(int index) {
            if (index >= listData.size())
                return null;
            return listData.get(index);
        }

        public String[][] getDataStringArr(int index) {
            if (index >= listDataString.size())
                return null;
            return listDataString.get(index);
        }

        public boolean isError() {
            return error;
        }

        public String getMessage() {
            return message;
        }
    }


    public class JResultData {
        public String json;
        List<HashMap<String, Object>> error = new ArrayList();
        private String results;
        private int totals;
        private List<JDataTable> listODataTable = new ArrayList();
        private List<String> columns = new ArrayList();

        /***
         * Initiate Class
         * @param _json
         */
        public JResultData(String _json) {

            if (_json == null || _json.length() == 0) {
                Log.e("Json is Null: ", json);
                return;
            } else {
                this.json = _json;
            }

            try {
                JSONObject jsonRootObject = new JSONObject(json);
                results = jsonRootObject.optString("results");
                totals = jsonRootObject.optInt("totals");
                JSONArray errorJS = jsonRootObject.optJSONArray("error");

                List<HashMap<String, Object>> lsterrors = new ArrayList();
                if (errorJS != null) {
                    for (int r = 0; r < errorJS.length(); r++) {
                        JSONObject jsonOpject = (JSONObject) errorJS.get(r);
                        lsterrors.add((HashMap<String, Object>) jsonToMap(jsonOpject));
                    }
                }
                error = lsterrors;

                JSONArray _objcurdatasList = jsonRootObject.optJSONArray("objcurdatas");
                if (_objcurdatasList != null)
                    for (int o = 0; o < _objcurdatasList.length(); o++) {
                        JSONObject _object = (JSONObject) _objcurdatasList.get(o);
                        JSONArray _columnJS = _object.optJSONArray("columns");
                        JSONArray _recordsJS = _object.optJSONArray("records");
                        int _totalrowsJS = _object.optInt("totalrows");

                        JDataTable objcurdata = new JDataTable();
                        objcurdata.totalrows = _totalrowsJS;

                        List<String> lstcolumn = new ArrayList();
                        if (_columnJS != null) {
                            for (int c = 0; c < _columnJS.length(); c++) {
                                lstcolumn.add(_columnJS.get(c).toString());
                            }
                        }
                        objcurdata.columns = lstcolumn;

                        List<HashMap<String, Object>> lstRecords = new ArrayList();
                        if (_recordsJS != null) {
                            for (int r = 0; r < _recordsJS.length(); r++) {
                                JSONObject jsonOpject = (JSONObject) _recordsJS.get(r);
                                lstRecords.add((HashMap<String, Object>) jsonToMap(jsonOpject));
                            }
                        }
                        objcurdata.records = lstRecords;

                        listODataTable.add(objcurdata);
                    }
                Log.i("listObjcurdatas.size: ", String.valueOf(listODataTable.size()));

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JSONException", e.getCause().toString());
            }
        }

        /***
         * jsonToMap(JSONObject json)
         * @param json
         * @return
         * @throws JSONException
         */
        public Map<String, Object> jsonToMap(JSONObject json) throws JSONException {
            Map<String, Object> retMap = new HashMap<String, Object>();

            if (json != JSONObject.NULL) {
                retMap = toMap(json);
            }
            return retMap;
        }

        /***
         * toMap(JSONObject object)
         * @param object
         * @return
         * @throws JSONException
         */
        public Map<String, Object> toMap(JSONObject object) throws JSONException {
            Map<String, Object> map = new HashMap<String, Object>();

            Iterator<String> keysItr = object.keys();
            while (keysItr.hasNext()) {
                String key = keysItr.next();
                Object value = object.get(key);

                if (value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                } else if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
                map.put(key, value);
            }
            return map;
        }

        /***
         * toList(JSONArray array)
         * @param array
         * @return
         * @throws JSONException
         */
        public List<Object> toList(JSONArray array) throws JSONException {
            List<Object> list = new ArrayList<Object>();
            for (int i = 0; i < array.length(); i++) {
                Object value = array.get(i);
                if (value instanceof JSONArray) {
                    value = toList((JSONArray) value);
                } else if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
                list.add(value);
            }
            return list;
        }

        /***
         * getColumns()
         * @return
         */
        public List<String> getColumns() {
            return columns;
        }

        /***
         * getErrors()
         * @return
         */
        public List<HashMap<String, Object>> getErrors() {
            return error;
        }

        /***
         * getResults()
         * @return
         */
        public List<JDataTable> getListODataTable() {
            return listODataTable;
        }

        /***
         * getResults()
         * @return
         */
        public String getResults() {
            return results;
        }

        /***
         * setResults(String results)
         * @param results
         */
        protected void setResults(String results) {
            this.results = results;
        }

        /***
         * int getTotals()
         * @return
         */
        public int getTotals() {
            return totals;
        }

        /***
         * setTotals(int totals)
         * @param totals
         */
        protected void setTotals(int totals) {
            this.totals = totals;
        }


    }

    public class JDataTable {
        public List<String> columns = new ArrayList(); // DLL sai cho nay
        public List<HashMap<String, Object>> records = new ArrayList();
        public int totalrows;
    }
}

