package com.genuwin.mobile

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.InputType
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.*
import androidx.preference.PreferenceManager
import com.genuwin.mobile.Service.WebService
import com.genuwin.mobile.base.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*


class LoginActivity : BaseActivity() {

    val customHandler = Handler(Looper.getMainLooper())
    var inLogin: Boolean = false

    lateinit var btnLang: ImageButton
    lateinit var btnSystem: ImageButton
    lateinit var btnLogin: Button
    lateinit var mUserView: AutoCompleteTextView
    lateinit var mPasswordView: EditText
    lateinit var mRememberView: CheckBox
    lateinit var mTypeServer: ToggleButton

    lateinit var myBrowser: WebView


    var pass: String = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        val pInfo: PackageInfo =
            packageManager.getPackageInfo(baseContext.packageName, 0)
        @Suppress("DEPRECATION")
        val versionCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            pInfo.versionName +  " - "+ pInfo.longVersionCode.toInt().toString() // avoid huge version numbers and you will be ok
        } else {
            pInfo.versionName + " - "+ pInfo.versionCode.toString()
        }


         findViewById<TextView>(R.id.tvVersion).text = versionCode


        myBrowser = findViewById(R.id.mybrowser)

        myBrowser.settings.javaScriptEnabled = true
        myBrowser.settings.domStorageEnabled = true



        mybrowser.loadUrl("file:///android_asset/mypage.html")

        mUserView = findViewById(R.id.user)
        mPasswordView = findViewById(R.id.password)
        mRememberView = findViewById(R.id.remember)
        mTypeServer = findViewById(R.id.server_type)

        mPasswordView.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                doSomethingWithText()
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {

            }
        })


        var sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(baseContext)



        mTypeServer.isChecked = sharedPreferences.getBoolean("login_type_sever", false) ;

        var lang = sharedPreferences.getString("lang", "en").toString().trim()

        btnLang = findViewById(R.id.btnLangues)
        btnLang.setOnClickListener(View.OnClickListener {
            showLangDialog()

        })

        if (lang == "vi") {
            btnLang.setBackgroundResource(R.drawable.vn)
        } else if (lang == "kr") {
            btnLang.setBackgroundResource(R.drawable.kr)
        } else {
            btnLang.setBackgroundResource(R.drawable.en)
        }

        var bo = sharedPreferences.getBoolean("login_remmeber", false)

        if (bo) {

            mUserView.setText((sharedPreferences.getString("login_user", "")))
            mPasswordView.setText((sharedPreferences.getString("login_pass", "")))

            mRememberView.isChecked = bo

        }


        btnSystem = findViewById(R.id.btnSetting)
        btnSystem.setOnClickListener(View.OnClickListener { showReqPassDialog() })


        btnLogin = findViewById(R.id.email_sign_in_button)
        btnLogin.setOnClickListener(View.OnClickListener { doLogin() })







        customHandler.postDelayed(Runnable { doSomethingWithText() }, 500)
        customHandler.postDelayed(Runnable { doSomethingWithText() }, 1000)


        doCheckUpdate()

    }

    fun doCheckUpdate(){

    }


    fun doSomethingWithText() {
        pass = ""

        var comment: String = "callBackData('" + mPasswordView.text + "')"

        myBrowser.evaluateJavascript(comment, { value ->
            pass = value.toString().replace('"', ' ').trim()
        })


    }




    fun showLangDialog() {
        val array = arrayOf("English", "Tiếng Việt", "한국어")
         val dialog:Dialog
        val builder = AlertDialog.Builder(this)

        builder.setTitle("")

        builder.setItems(array,
            DialogInterface.OnClickListener { _, item ->

                processChangeLang(item)
            })

        // Create a new AlertDialog using builder object
        dialog = builder.create()

        // Finally, display the alert dialog
        dialog.show()
    }

    fun processChangeLang(index: Int) {

        var sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(baseContext)
        var sharedPreferencesEdit: SharedPreferences.Editor = sharedPreferences.edit()

        var lang: String
        if (index == 2) {
            lang = "kr"

        } else if (index == 1) {
            lang = "vi"

        } else {
            lang = "en"

        }

        sharedPreferencesEdit.putString("lang", lang)
        sharedPreferencesEdit.commit()
        sharedPreferencesEdit.apply()

        language(lang)

        doLoadLanguagePack(lang)



        recreate()
    }


    fun language(language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = resources
        val configuration = resources.configuration
        @Suppress("DEPRECATION")
        configuration.locale = locale
        @Suppress("DEPRECATION")
        resources.updateConfiguration(configuration, resources.displayMetrics)

    }

    fun doLoadLanguagePack(language: String) = Thread(Runnable {
        handler.post(Runnable {
            showProgressBar(getString(R.string.download) + "   " + language)
        })

        val langarr  = getWebService().GetDataTableArg(
            "lg_mposv2_language",
            "$language"
        )

        if(!langarr.isError){
            getSystemSQLiteDatabase().execSQL("delete from PACK_LANG2")
            langarr.data.forEachIndexed { _, it ->

                    try{
                        val PK: Int = it.get(0).toInt()
                        val KEY_TITLE: String = it.get(1).toString()
                        val TEXT_VALUE: String = it.get(2).toString()

                        getSystemSQLiteDatabase().execSQL(
                            "INSERT INTO PACK_LANG2 VALUES('" + PK + "','"
                                    + KEY_TITLE + "','" + TEXT_VALUE+ "');"
                        )

                    }catch (ex:Exception){
                        Log.d("Exception", ex.message.toString())
                    }



            }

            getSystemSQLiteDatabase().close()
        }

        handler.post(Runnable {
            hideProgressBar()
        })



    }).start()




    fun showReqPassDialog() {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.prompt_password)


        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        input.maxLines = 1
        input.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        val content = LinearLayout(this)

        content.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        content.setPadding(30, 0, 30, 0)
        content.addView(input)
        builder.setView(content)


        builder.setPositiveButton(
            R.string.yes,
            DialogInterface.OnClickListener { _, _ ->
                val m_Text = input.text.toString()
                if (m_Text == "vina@123") {
                    val i = Intent(applicationContext, SettingsActivity::class.java)

                    startActivity(i)
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Password is incorrect!!!",
                        Toast.LENGTH_LONG
                    ).show()
                    input.text.clear()
                }
            })
        builder.setNegativeButton(
            R.string.no,
            DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        builder.show()
    }

    fun doLogin() {


        if (inLogin) return

        mUserView.error = null
        mPasswordView.error = null

        val user = mUserView.text.toString()
        val password = mPasswordView.text.toString()

        var cancel = false
        var focusView: View? = null

        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        if (TextUtils.isEmpty(user)) {
            mUserView.error = getString(R.string.error_field_required)
            focusView = mUserView
            cancel = true
        } else if (!isEmailValid(user)) {
            mUserView.error = getString(R.string.error_invalid_user)
            focusView = mUserView
            cancel = true
        }

        if (cancel) {
            focusView!!.requestFocus()
        } else {


//            var pass = ""
//
//            var comment:String  = "callBackData('" + password + "')"
//
//            myBrowser.evaluateJavascript(comment, { value ->
//                pass = value.toString().replace('"',' ').trim();
//            })
//
//
//
//            var icount = 0
//            do {
//                Thread.sleep(1000)
//                icount ++
//            }while (pass.length == 0 && icount < 5)


            if (pass.length == 0) {
                Toast.makeText(baseContext, getString(R.string.login_error), Toast.LENGTH_LONG)
                    .show()
                return
            }
            inLogin = true
            lockOrientation(true)
            Thread(Runnable {


                var sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(baseContext)
                var editor: SharedPreferences.Editor = sharedPreferences.edit()


                //editor.putBoolean("login_remmeber", mRememberView.isChecked)
                // editor.commit()

                val dbServerPublic  = sharedPreferences.getString(
                    "server_address_public",
                    getString(R.string.pref_server_address_default)
                )

                val dbServer = sharedPreferences.getString(
                    "server_address",
                    getString(R.string.pref_server_address_default)
                )



                val dbName = sharedPreferences.getString(
                    "server_company",
                    getString(R.string.pref_server_company_default)
                )
                val dbData = sharedPreferences.getString(
                    "server_database",
                    getString(R.string.pref_server_database_default)
                )
                val dbUser = sharedPreferences.getString(
                    "server_username",
                    getString(R.string.pref_server_username_default)
                )
                val dbPass = dbUser + "2"



//                Log.v("vvv", url)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                val url = if (mTypeServer.isChecked) {
                    "http://$dbServerPublic/$dbName/gwWebservice.asmx"
                }else{
                    "http://$dbServer/$dbName/gwWebservice.asmx"
                }
//                val url ="http://test.gasp.genuwinsolution.com/kolonbd/gwWebservice.asmx";



                val webService = WebService(
                    url,
                    dbData,
                    dbUser,
                    dbPass
                )
                //var pass =  Utils.md5B(password)


                val result = webService.GetDataTableArg(
                    "lg_mposv2_check_login",
                    user + "|" + pass
                )

                lockOrientation(false)
                inLogin = false


                if (result.isError) {


                    handler.post(Runnable {
                        Toast.makeText(baseContext, result.message, Toast.LENGTH_LONG)
                            .show()

                    })
                } else {

                    if (result.data.size == 1) {



                        try {

                            var usercheck = result.data[0][0]

                            if(usercheck == "Y") {
                                var user_pk =  result.data[0][2]
                                var userName = result.data[0][3]
                                var userAdmin = result.data[0][4]
                                var userImage = try{ result.data[0][5] }catch (ex: java.lang.Exception){ "" }




                                if (mRememberView.isChecked) {
                                    editor.putString("login_user", user)
                                    editor.putString("login_user_pk", user_pk)
                                    editor.putString("login_pass", password)
                                    editor.putString("login_username", userName)
                                    editor.putString("login_adminyn", userAdmin)
                                    editor.putString("login_image", userImage)
                                    editor.putBoolean("login_remmeber", mRememberView.isChecked)
                                    editor.putBoolean("login_type_sever", mTypeServer.isChecked)
                                    editor.commit()

                                } else {
                                    editor.putString("login_user", user)
                                    editor.putString("login_user_pk", user_pk)
                                    editor.putString("login_pass", "")
                                    editor.putString("login_username", userName)
                                    editor.putString("login_adminyn", userAdmin)
                                    editor.putString("login_image", userImage)
                                    editor.putBoolean("login_remmeber", mRememberView.isChecked)
                                    editor.putBoolean("login_type_sever", mTypeServer.isChecked)
                                    editor.commit()
                                }

                                handler.post(Runnable {
                                    val i =
                                        Intent(applicationContext, MainActivity::class.java)
                                    startActivity(i)
                                    finish()
                                })
                            }else{
                                handler.post(Runnable {
                                    mPasswordView.error = getString(R.string.error_incorrect_password)
                                    mPasswordView.requestFocus()
                                })
                            }
                        } catch (e: java.lang.Exception) {
                            handler.post(Runnable {
                                Toast.makeText(
                                    baseContext,
                                    e.message,
                                    Toast.LENGTH_LONG
                                )
                                    .show()
                            })
                        }
                    } else {
                        handler.post(Runnable {
                            Toast.makeText(
                                baseContext,
                                getString(R.string.user_dont_exist),
                                Toast.LENGTH_LONG
                            )
                                .show()

                        })
                    }
                }
                //TODO
            }).start()
            //THEART
        }

    }

    private fun isEmailValid(user: String): Boolean {
        return user.length > 4
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 2
    }




}
