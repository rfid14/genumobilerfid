package com.genuwin.mobile.gw

import android.R.bool
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import device.common.DecodeResult
import device.common.DecodeStateCallback
import device.common.ScanConst
import device.sdk.ScanManager

class PMRFInput : BaseFragment() {

 var mScanner: ScanManager? = null
 var mDecodeResult: DecodeResult? = null
 private var mBackupResultType = ScanConst.ResultType.DCD_RESULT_COPYPASTE

 var mScanResultReceiver =
  object : BroadcastReceiver() {
   override fun onReceive(context: Context?, intent: Intent?) {

    if (mScanner != null) {
     try {
      if (intent != null) {
       if (ScanConst.INTENT_USERMSG == intent.action) {
        if (mDecodeResult != null) {
         mScanner!!.aDecodeGetResult(mDecodeResult!!.recycle())
         doProcessBarcode(mDecodeResult.toString())
        }
       } else if (ScanConst.INTENT_EVENT == intent.action) {
        val result =
         intent.getBooleanExtra(
          ScanConst.EXTRA_EVENT_DECODE_RESULT,
          false
         )
        val decodeBytesLength =
         intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_LENGTH, 0)
        val decodeBytesValue =
         intent.getByteArrayExtra(ScanConst.EXTRA_EVENT_DECODE_VALUE)
        val decodeValue = decodeBytesValue!!.toString()
        val decodeLength = decodeValue.length
        val symbolName =
         intent.getStringExtra(ScanConst.EXTRA_EVENT_SYMBOL_NAME)
        val symbolId =
         intent.getByteExtra(ScanConst.EXTRA_EVENT_SYMBOL_ID, 0.toByte())
        val symbolType =
         intent.getIntExtra(ScanConst.EXTRA_EVENT_SYMBOL_TYPE, 0)
        val letter =
         intent.getByteExtra(
          ScanConst.EXTRA_EVENT_DECODE_LETTER,
          0.toByte()
         )
        val modifier =
         intent.getByteExtra(
          ScanConst.EXTRA_EVENT_DECODE_MODIFIER,
          0.toByte()
         )
        val decodingTime =
         intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_TIME, 0)
        val TAG = "ARIC TEST"
        Log.d(TAG, "1. result: $result")
        Log.d(TAG, "2. bytes length: $decodeBytesLength")
        Log.d(TAG, "3. bytes value: $decodeBytesValue")
        Log.d(TAG, "4. decoding length: $decodeLength")
        Log.d(TAG, "5. decoding value: $decodeValue")
        Log.d(TAG, "6. symbol name: $symbolName")
        Log.d(TAG, "7. symbol id: $symbolId")
        Log.d(TAG, "8. symbol type: $symbolType")
        Log.d(TAG, "9. decoding letter: $letter")
        Log.d(TAG, "10.decoding modifier: $modifier")
        Log.d(TAG, "11.decoding time: $decodingTime")
       }
      }
     } catch (e: java.lang.Exception) {
      e.printStackTrace()
     }
    }
   }
  }

 private val mHandler: Handler = Handler(Looper.getMainLooper())
 private val mStateCallback: DecodeStateCallback =
  object : DecodeStateCallback(mHandler) {
   override fun onChangedState(state: Int) {
    when (state) {
     ScanConst.STATE_ON, ScanConst.STATE_TURNING_ON -> {}
     ScanConst.STATE_OFF, ScanConst.STATE_TURNING_OFF -> {}
    }
   }
  }

 private fun initScanner() {
  mScanner = ScanManager()
  mDecodeResult = DecodeResult()
  if (mScanner != null) {
   mScanner!!.aRegisterDecodeStateCallback(mStateCallback)
   mBackupResultType = mScanner!!.aDecodeGetResultType()
   mScanner!!.aDecodeSetResultType(ScanConst.ResultType.DCD_RESULT_USERMSG)
   mScanner?.aDecodeSetTriggerMode(ScanConst.TriggerMode.DCD_TRIGGER_MODE_ONESHOT)

   val filter = IntentFilter()
   filter.addAction(ScanConst.INTENT_USERMSG)
   filter.addAction(ScanConst.INTENT_EVENT)
   BaseActivity().baseContext.registerReceiver(mScanResultReceiver, filter)
  }
 }

 // info LINE
 var wi_pk: String = ""
 var line_pk: String = ""
 var wi_no: String = ""
 var item_pk: String = ""
 var item_code: String = ""
 var item_name: String = ""

 // info SIDE
 var side_barcode: String = ""
 var side_pk: String = ""
 var side_name: String = ""
 var side_row: String = ""
 var side_qty: String = ""
 var side_remark: String = ""
 var slip_no: String = ""

 var isProcess = false
 var onload = false

 lateinit var dataview: DataGridView
 lateinit var btnStart: Button
 lateinit var btnClear: Button
 lateinit var btnNew: Button
 lateinit var btnRemove: Button
 lateinit var btnMapping: Button
 lateinit var cbxVersion: Combobox

 lateinit var txtWI: TextView
 lateinit var txtLink: TextView
 lateinit var wiqty: TextView

 override fun onCreateView(
  inflater: LayoutInflater,
  container: ViewGroup?,
  savedInstanceState: Bundle?
 ): View? {
  var root =
   inflater.inflate(R.layout.fragment_rfid_rfinput, container, false) as LinearLayout

  dataview = root.findViewById(R.id.dataview)

  dataview.BeginInit()
  dataview.addColumn("SLIP_NO", 0)
  dataview.addColumn("LINE_PK", 0)
  dataview.addColumn("SIDE_PK", 0)
  dataview.addColumn("DETAIL_PK", 0)
  dataview.addColumn(getString(R.string.lot_no) + ". + " + getString(R.string.item_code), 6)
  dataview.addColumn(getString(R.string.barcode), 4)
  dataview.widthWithpercent = true
  dataview.EndInit()

  btnStart = root.findViewById(R.id.btnStart)
  btnStart.setOnClickListener { btnStartClick() }

  btnClear = root.findViewById(R.id.btnClear)
  btnClear.setOnClickListener { btnClearClick() }

  btnMapping = root.findViewById(R.id.btnMapping)
  btnMapping.setOnClickListener { btnMappingClick() }

  btnRemove = root.findViewById(R.id.btnRemove)
  btnRemove.setOnClickListener { btnRemoveClick() }

  btnNew = root.findViewById(R.id.btnNew)
  btnNew.setOnClickListener { btnNewClick() }

  txtWI = root.findViewById(R.id.txtWI)
  txtLink = root.findViewById(R.id.txtLink)
  wiqty = root.findViewById(R.id.wiqty)
  cbxVersion = root.findViewById(R.id.cbxVersion)

  cbxVersion.listener =
   object : AdapterView.OnItemSelectedListener {
    override fun onNothingSelected(parent: AdapterView<*>?) {
     TODO("Not yet implemented")
    }

    override fun onItemSelected(
     parent: AdapterView<*>?,
     view: View?,
     position: Int,
     id: Long
    ) {
     cbxVersionChanged()
    }
   }

  initScanner()
  handler.postDelayed(Runnable { openWI() }, 200)
  return root
 }

 var open_wi: Boolean = false
 fun openWI() {
  if (open_wi) return

  open_wi = true
  val intent =
   Intent(BaseActivity().baseContext, DetailActivity::class.java).apply {
    putExtra(BaseDetailFragment.ARG_FRAMENT_NAME, "PMWI")
    putExtra(BaseDetailFragment.ARG_FRAMENT_TITLE, getString(R.string.wi))
    putExtra(BaseDetailFragment.ARG_ITEM_ID_M, "86")
   }
  BaseActivity().startActivityForResult(intent, 997)
 }

 override fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {

  if (requestCode == 997) {

   try {
    if (data == null) {
     wi_pk = ""
     line_pk = ""
     wi_no = ""
     item_pk = ""
     item_code = ""
     item_name = ""

     handler.post { txtWI.text = "" }
    } else {
     wi_pk = data?.getExtras()?.get("PK").toString()
     line_pk = data?.getExtras()?.get("LINE_PK").toString()
     wi_no = data?.getExtras()?.get("WI_NO").toString()
     item_pk = data?.getExtras()?.get("ITEM_PK").toString()
     item_code = data?.getExtras()?.get("ITEM_CODE").toString()
     item_name = data?.getExtras()?.get("ITEM_NAME").toString()
     handler.post { txtWI.text = wi_no }
    }
   } catch (ex: Exception) {}

   open_wi = false
  }
 }

 fun doProcessBarcode(barcode: String) {

  BaseActivity().runOnUiThread {
  if (barcode.equals("READ_FAIL")) {
   return@runOnUiThread
  }

  if (isProcess) return@runOnUiThread
  isProcess = true


  Thread(
   Runnable {
    handler.post(Runnable { BaseActivity().showProgressBar(getString(R.string.loading)) })
    Thread.sleep(1000)

    if (barcode.startsWith("L")) {
     val bc = barcode.substring(1)
     val result =
      BaseActivity()
       .getWebService()
       .GetDataTableArg("lg_sel_rfid_rfinput", line_pk + "|" + bc)

     if (result.isError) {
      handler.post(Runnable { BaseActivity().showToast(result.message) })
     } else {

      if (result.data.size == 1) {

       side_barcode = barcode
       side_pk = bc
       side_name = result.data[0][1]
       side_row = result.data[0][2]
       side_qty = result.data[0][3]
       side_remark = result.data[0][4]

       handler.post {
        txtLink.text = side_name
       cbxVersion.clearData()
       }


       loadHis()

      }
     }
    } else {
      if(side_pk.isNullOrBlank()){
       handler.post(Runnable { BaseActivity().showProgressBar("Please scan barcode in side.") })
       Thread.sleep(1000)

      }else {
       val result =
        BaseActivity()
         .getWebService()
         .GetDataTableArg(
          "lg_sel_rfid_rfinput_3",
          slip_no + "|" + line_pk + "|" + side_pk + "|" + wi_pk + "|" + barcode + "|" + item_pk + "|" + userid
         )

       if (result.isError) {
        handler.post(Runnable { BaseActivity().showToast(result.message) })
       } else {

        var pallets : MutableList<Array<String>> = mutableListOf()
        for (e in result.data){

         if(slip_no.equals(e[0])){
          pallets.add(e)

         }
        }
        handler.post { dataview.setData(pallets.toTypedArray())
         wiqty.text = pallets.size.toString()}


       }
      }
    }

    handler.post(Runnable { BaseActivity().hideProgressBar() })

    isProcess = false
   }
  )
   .start()
  }
 }

 private fun cbxVersionChanged() {
  if(!onload){
   BaseActivity().runOnUiThread {
    dataview.clearAll()
    slip_no = cbxVersion.text()

    loadHistorySide()

   }

  }
 }

 private fun btnRemoveClick() {

  this.BaseActivity()
   .confirmDialog(
    getString(R.string.prompt_ismodified),
    getString(R.string.remove),
    object : BaseActivity.ConfirmDialogCallBack {
     override fun callBack(bool: Boolean) {
      if (bool) {
       doRemove()
      }
     }
    }
   )
 }

 fun doRemove(){
  if (onload) return
  onload = true


  Thread(
   Runnable {
    Thread.sleep(1000)
    handler.post(Runnable { BaseActivity().showProgressBar(getString(R.string.loading)) })


    for ( e in dataview.Rows()){
     if(e.isSelectMode){

      val pk_remove = e.getCell()[3].value
      val result =
       BaseActivity()
        .getWebService()
        .GetDataTableArg(
         "lg_sel_rfid_rfinput_2",
         slip_no + "|" + line_pk + "|" + side_pk + "|" + wi_pk + "|" + pk_remove + "|" + userid
        )

      if (result.isError) {
       handler.post(Runnable { BaseActivity().showToast(result.message) })
      } else {

       var pallets : MutableList<Array<String>> = mutableListOf()
       for (e in result.data){

        if(slip_no.equals(e[0])){
         pallets.add(e)

        }
       }
       handler.post { dataview.setData(pallets.toTypedArray())
        wiqty.text = pallets.size.toString()}


      }
     }
    }


    handler.post(Runnable { BaseActivity().hideProgressBar() })

    onload = false
   }
  )
   .start()




 }

 private fun btnMappingClick() {
  this.BaseActivity()
   .confirmDialog(
    getString(R.string.prompt_ismodified),
    getString(R.string.stockout),
    object : BaseActivity.ConfirmDialogCallBack {
     override fun callBack(bool: Boolean) {
      if (bool) {
       doStockOut()
      }
     }
    }
   )
 }

 fun doStockOut(){
  if (onload) return
  onload = true


  Thread(
   Runnable {
    Thread.sleep(1000)
    handler.post(Runnable { BaseActivity().showProgressBar(getString(R.string.loading)) })

    val result =
     BaseActivity()
      .getWebService()
      .TableReadOpenString(
       "LG_PDA_PRO_ST_OUT_MSLIP",
       "INSERT|" + slip_no + "||" + userid + "|"
      )

    if (result.isError) {
     handler.post(Runnable { BaseActivity().showToast(result.message) })
    } else {

     handler.post { BaseActivity().showToast(result.data) }

     }






    handler.post(Runnable { BaseActivity().hideProgressBar() })

    onload = false
   }
  )
   .start()
 }

 private fun btnNewClick() {
  this.BaseActivity()
   .confirmDialog(
    getString(R.string.prompt_ismodified),
    getString(R.string.newstr),
    object : BaseActivity.ConfirmDialogCallBack {
     override fun callBack(bool: Boolean) {
      if (bool) {
       doNew()
      }
     }
    }
   )
 }
 fun doNew() {
  if (slip_no.isNullOrBlank()) {
   return
  }

  BaseActivity().runOnUiThread {
   slip_no = ""

   dataview.clearAll()
   cbxVersion.clearData()

   wiqty.text = ""
  }
 }

 private fun btnClearClick() {
  this.BaseActivity()
   .confirmDialog(
    getString(R.string.prompt_ismodified),
    getString(R.string.clear),
    object : BaseActivity.ConfirmDialogCallBack {
     override fun callBack(bool: Boolean) {
      if (bool) {
       doClear()
      }
     }
    }
   )
 }
 fun doClear() {
  BaseActivity().runOnUiThread {
   side_barcode = ""
   side_pk = ""
   side_name = ""
   side_row = ""
   side_qty = ""
   side_remark = ""

   cbxVersion.clearData()
   slip_no = ""

   dataview.clearAll()

   txtLink.text = ""
   wiqty.text = ""
  }
 }

 private fun btnStartClick() {
  mScanner?.aDecodeSetTriggerOn(1)
 }

 override fun onHanldEvent(code: Int): Boolean {
  mScanner?.aDecodeSetTriggerOn(1)
  return true
 }

 fun loadHistorySide() {
  if (onload) return
  onload = true


  Thread(
   Runnable {
    Thread.sleep(1000)
    handler.post(Runnable { BaseActivity().showProgressBar(getString(R.string.loading)) })

    loadHis()


    handler.post(Runnable { BaseActivity().hideProgressBar() })

    onload = false
   }
  )
   .start()


 }

 fun loadHis(){
  val result =
   BaseActivity()
    .getWebService()
    .GetDataTableArg(
     "lg_sel_rfid_rfinput_1",
     slip_no + "|" + line_pk + "|" + side_pk + "||" + wi_pk
    )

  if (result.isError) {
   handler.post(Runnable { BaseActivity().showToast(result.message) })
  } else {

   if(cbxVersion.size() == 0) {
    var slip_nos_key : MutableList<String> = mutableListOf()
    var slip_nos : MutableList<Array<String?>> = mutableListOf()
    for (e in result.data){
     var index = slip_nos_key.indexOf(e[0])
     if(index == -1){
      slip_nos_key.add(e[0])
      slip_nos.add(e)
     }
    }

    handler.post { cbxVersion.setData(slip_nos.toTypedArray(), 0, 0) }

    Thread.sleep(1000)
    if(slip_no.isNullOrBlank()){
     try {
      slip_no = slip_nos_key[0]
     }catch (ex:Exception){}

    }

    var pallets : MutableList<Array<String>> = mutableListOf()
    for (e in result.data){

     if(slip_no.equals(e[0])){
      pallets.add(e)

     }
    }
    handler.post { dataview.setData(pallets.toTypedArray())
     wiqty.text = pallets.size.toString()}
   }
//   handler.post { BaseActivity().showToast(slip_no) }


  }
 }
}
