package com.genuwin.mobile.gw

import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.google.android.material.textfield.TextInputEditText
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import java.util.*
import android.content.Intent
import android.util.Log


class PMWI : BaseDetailFragment() {

    lateinit var cbxLineGroup: Combobox
    lateinit var cbxLine: Combobox
    lateinit var btnFromDate: Button
    lateinit var btnToDate: Button
    lateinit var btnFind: Button
    lateinit var edtWI: TextInputEditText
    lateinit var edtNo: TextInputEditText
    lateinit var dataview: DataGridView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_rfid_wi,container,false) as LinearLayout

        cbxLineGroup = root.findViewById(R.id.cbxLineGroup)
        cbxLine = root.findViewById(R.id.cbxLine)
        btnFromDate = root.findViewById(R.id.btnFromDate)
        btnToDate = root.findViewById(R.id.btnToDate)
        btnFind = root.findViewById(R.id.btnFind)
        edtWI = root.findViewById(R.id.edtWI)
        edtNo = root.findViewById(R.id.edtNo)
        dataview = root.findViewById(R.id.dataview)


        dataview.BeginInit()
        dataview.addColumn( "PK", 0)
        dataview.addColumn( getString(R.string.wi), 350)
        dataview.addColumn( getString(R.string.line), 150)
        dataview.addColumn( getString(R.string.item_code), 150)
        dataview.addColumn( getString(R.string.item_name), 150)
        dataview.addColumn( getString(R.string.qty), 100)
        dataview.addColumn( "ITEM_PK", 0)
        dataview.addColumn( "LINE_PK", 0)
        dataview.EndInit()


        dataview.itemClickListener = object : DataGridView.HandlerItemClick() {
            override fun onClickCell(position: Int, col: Int): Boolean {

                //  OnLoadBC(dateview.getDataCell(position,0))
                selectRow(position)
                return false
            }
        }
        cbxLineGroup.listener =  object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
               loadLine()
            }

        }

        val mcurrentTime = Calendar.getInstance()
        val year = mcurrentTime.get(Calendar.YEAR)
        val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)

        var datenow = year.toString() + "/" +  if(month < 9 ) { "0" + (month + 1) }else { ""+ (month +1)} + "/" +  if (day < 10 ){ "0" + (day ) }else  {"" + (day) }

        btnFromDate.text = datenow
        btnToDate.text = datenow


        btnFromDate.setOnClickListener {
            showDateDialog(btnFromDate)
        }
        btnToDate.setOnClickListener {
            showDateDialog(btnToDate)
        }

        btnFind.setOnClickListener {
            findWI()
        }

        doLoadFirst()

        return root
    }

    private fun showDateDialog(btn: Button?) {
        val mcurrentTime = Calendar.getInstance()
        val year = mcurrentTime.get(Calendar.YEAR)
         val month = mcurrentTime.get(Calendar.MONTH)
        val day = mcurrentTime.get(Calendar.DAY_OF_MONTH)

        BaseActivity().showDatePickerDialog(year,month,day, callback = object :
            BaseActivity.callBackDatePickerDialog {
            override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                var date: String  =  year.toString() + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" +  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }   ;
                if (btn != null) {
                    btn.text  =  date
                }
            }
        })

    }

    fun doLoadFirst(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(500)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_SYS_SEL_LINEG_PDA",
                lang + "|" + userid
            )




            if (result.isError){

                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                handler.post {
                    cbxLineGroup.setData(result.getData(),1,0)
                    try{
                        cbxLineGroup.setSelectItem(id_m.toInt())
                    }catch(ex: Exception){

                    }
                }

            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()

    }
    fun  loadLine(){

        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(500)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_SYS_SEL_LINE_PDA",
                lang + "|" + userid + "|" + cbxLineGroup.value()
            )

//            Log.v("vvv", result)




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                handler.post {
                    cbxLine.setData(result.getData(),1,0)

                }

            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()

    }

    fun  findWI(){
//        val intent = Intent()
//        activity?.setResult(RESULT_OK, intent)
//        activity?.finish()
//        ===============================
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(500)


            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_SYS_SEL_WI_PDA",
                btnFromDate.text.toString().replace("/","") + "|" + btnToDate.text.toString().replace("/","") + "|" + edtWI.text + "|" + edtNo.text + "||"+cbxLine.value()+"||-1"
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                handler.post {
                    dataview.setData(result.getData())

                }

            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun selectRow(pos: Int){

        var cells  = dataview.Rows()[pos].getCell()


        val intent = Intent()
        intent.putExtra("PK", cells[0].value )
        intent.putExtra("WI_NO", cells[1].value )
        intent.putExtra("LINE", cells[2].value )
        intent.putExtra("ITEM_CODE", cells[3].value )
        intent.putExtra("ITEM_NAME", cells[4].value )
        intent.putExtra("PRODUCT_CODE", cells[5].value )
        intent.putExtra("ITEM_PK", cells[6].value )
        intent.putExtra("LINE_PK", cells[7].value )
        activity?.setResult(RESULT_OK, intent)
        activity?.finish()
    }
}