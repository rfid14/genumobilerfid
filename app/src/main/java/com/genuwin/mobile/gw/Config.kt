package com.genuwin.mobile.gw

import android.database.sqlite.SQLiteDatabase
import com.genuwin.mobile.base.BaseConfig

class Config (): BaseConfig() {
    override fun doConfig(db: SQLiteDatabase) {
       // super.doConfig(db)
        val CREATE_INV_TR = ("CREATE TABLE IF NOT EXISTS PACK_LANG2 ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "KEY_TITLE TEXT, " + "TEXT_VALUE TEXT "
                +  " )")
        db.execSQL(CREATE_INV_TR)

        val CREATE = ("CREATE TABLE IF NOT EXISTS PROCEDURE_LAZY ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "IMG_URI TEXT,  "
                + "IMG_THUMB TEXT,  "
                + "PROCEDURE TEXT, "
                + "PARA TEXT,  "
                + "TITLE TEXT,  "
                + "DEL_IF INTEGER,  "
                + "PROCESS_YN TEXT "
                +  " )")
        db.execSQL(CREATE)
    }
}