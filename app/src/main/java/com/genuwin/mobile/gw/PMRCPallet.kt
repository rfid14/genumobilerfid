package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.MainActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.google.android.material.textfield.TextInputEditText
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import android.content.ClipData.Item

import android.R.string
import android.content.ClipData
import java.lang.StringBuilder


class PMRCPallet:BaseFragment() {

    lateinit var dataview: DataGridView
    lateinit var btnStart: Button
    lateinit var btnClear: Button
    lateinit var btnMapping: Button
    lateinit var cbxGrade : Combobox
    lateinit var etxWeight : TextInputEditText
    lateinit var etxDoffingNo : TextInputEditText

    lateinit var txtCountBC: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_rfid_rcpallet,container,false) as LinearLayout

        cbxGrade = root.findViewById(R.id.cbxGrade)
        etxWeight = root.findViewById(R.id.etxWeight)
        etxDoffingNo = root.findViewById(R.id.etxDoffingNo)
        dataview = root.findViewById(R.id.dataview)

        dataview.BeginInit()

        dataview.addColumn( getString(R.string.barcode), 6)
        dataview.addColumn( getString(R.string.status), 4)
        dataview.widthWithpercent  = true
        dataview.EndInit()

        btnStart = root.findViewById(R.id.btnStart)
        btnStart.setOnClickListener {
            btnStartClick()
        }

        btnClear = root.findViewById(R.id.btnClear)
        btnClear.setOnClickListener {
            btnClearClick()
        }

        btnMapping = root.findViewById(R.id.btnMapping)
        btnMapping.setOnClickListener {
            btnMappingClick()
        }

        txtCountBC = root.findViewById(R.id.txtCountBC)


        doLoadFirst()


        return root
    }

    fun doLoadFirst(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(500)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_GET_LG_CODE",
                "LGPC0071"
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                handler.post {
                    cbxGrade.setData(result.getData(),1,0)

                }

            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()

    }
    var isProcess = false
    private fun btnMappingClick() {

        if(isProcess) return

        val grade = cbxGrade.text()
        val weight = etxWeight.text.toString()
        val doffing = etxDoffingNo.text.toString()

        if( grade.isNullOrBlank() || weight.isNullOrBlank() || doffing.isNullOrBlank()){
            this.BaseActivity().confirmDialog("",getString(R.string.error_field_required), object :
                BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {}
            })

            return
        }




        this.BaseActivity().confirmDialog(getString(R.string.prompt_ismodified),getString(R.string.prompt_do_you_make_pallet) , object :
            BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
                if (bool){
                    doMakePallet(grade,weight,doffing)

                }
            }
        })
    }

    private fun doMakePallet(grade: String, weight: String, doffing: String) {
        isProcess = true
        Thread(Runnable {
            Thread.sleep(500)
            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.loading))
            })

            var key: String = ""
            val listSB  = arrayOf("","","","","")
            var iCount = 0
            var indexList = 0
            for( e in dataview.Rows()){
                if(e.getCell()[1].value == "-"){
                    listSB[indexList] = listSB[indexList]  +key +e.getCell()[0].value
                    key = ","

                    iCount++
                    if (listSB[indexList].length > 3700)
                    {

                        indexList++

                        key = "";

                    }
                }
            }


            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MSLIP_POP_RFID_V2",
                listSB[0] + "|" +listSB[1] + "|" +listSB[2] + "|" +listSB[3] + "|" +listSB[4] + "|" + grade + "|" + weight + "|" + doffing + "|" + iCount.toString() + "|" + userid
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{

                if (result.getData()[0][0].equals("ITEM_BC")){
                    handler.post {
                        dataview.BeginInit()

                        for (e in dataview.Rows()) {
                            if (e.getCell()[1].value == "-")
                                e.setCellData(1, "Y")


                        }

                        dataview.EndInit()
                        //BaseActivity().showToast("Pallet barcode is " + result.getData()[0][1])

                        this.BaseActivity().confirmDialog("","Pallet barcode is " + result.getData()[0][1], object :
                            BaseActivity.ConfirmDialogCallBack {
                            override fun callBack(bool: Boolean) {
                                if (bool){


                                }
                            }
                        })
                    }
                }




            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })

            isProcess = false
        }).start()




    }

    private fun btnClearClick() {
        handler.post {
            dataview.clearAll()

            txtCountBC.text = "0"
            etxWeight.text?.clear()
            etxDoffingNo.text?.clear()

        }
    }

    private fun btnStartClick() {
        if(isProcess) return
        (BaseActivity() as MainActivity) .RfidControl()
    }

    override fun onDataResult(type: Int, data: String) {
        if( type == 1){
            if(isProcess)
                return



            for( e in dataview.Rows()){
                if( e.getCell()[0].value.equals(data))
                    return

            }
            dataview.addRow( arrayOf(data,"-"))
            txtCountBC.text = dataview.Rows().count().toString()
        }else if(type == 2){
            BaseActivity().runOnUiThread {
                if( data.equals("0")){
                    btnStart.text = getText(R.string.start)
                    btnClear.isEnabled = true
                    btnMapping.isEnabled = true
                }else{
                    btnStart.text = getText(R.string.stop)
                    btnClear.isEnabled = false
                    btnMapping.isEnabled = false
                }
            }

        }


        //super.onDataResult(type, data)
    }
}