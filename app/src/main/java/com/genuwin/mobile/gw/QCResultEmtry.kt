package com.genuwin.mobile.gw


import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.FragmentTransaction
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import java.lang.Exception
import java.util.*


class QCResultEmtry : BaseFragment() {
    //var mDualPane = false
    lateinit var btnLoad: Button
    lateinit var txtLastTime: TextView
    lateinit var txtAllTitle: TextView
    lateinit var txtAllValue: TextView
    lateinit var txtDoneTitle: TextView
    lateinit var txtDoneValue1: TextView
    lateinit var txtDoneValue2: TextView
    lateinit var txtToDoTitle: TextView
    lateinit var txtToDoValue: TextView
    lateinit var btnDate: ImageView
    lateinit var txtDate: TextView
    lateinit var cbxInpection: Combobox
    lateinit var scroll_content: NestedScrollView
    lateinit var dataview: DataGridView
    lateinit var scroll_linear_content: LinearLayout



    var dual_screen = false
    lateinit var cusFragment: BaseFragment

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_qc_result_entry_v3,container,false) as LinearLayout
        btnLoad = root.findViewById(R.id.btnLoad)
        txtLastTime = root.findViewById(R.id.txtLastTime)
        txtAllTitle = root.findViewById(R.id.txtAllTitle)
        txtAllValue = root.findViewById(R.id.txtAllValue)
        txtDoneTitle = root.findViewById(R.id.txtDoneTitle)
        txtDoneValue1 = root.findViewById(R.id.txtDoneValue1)
        txtDoneValue2 = root.findViewById(R.id.txtDoneValue2)
        txtToDoTitle = root.findViewById(R.id.txtToDoTitle)
        txtToDoValue = root.findViewById(R.id.txtToDoValue)
        btnDate = root.findViewById(R.id.btnDate)
        txtDate = root.findViewById(R.id.txtDate)
        txtDate.text = Utils().dateIncline

        cbxInpection = root.findViewById(R.id.cbxInpection)
        scroll_content = root.findViewById(R.id.scroll_content)
        scroll_linear_content = LinearLayout(BaseActivity().baseContext)
        scroll_linear_content.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        scroll_linear_content.orientation = LinearLayout.VERTICAL

        scroll_content.addView(scroll_linear_content)
        // dataview = root.findViewById(R.id.dgvrequest)

        // dataview.setDisplayHeader(false)

        btnDate.setOnClickListener(View.OnClickListener {
            showDateDialog()
        })

        btnLoad.setOnClickListener(View.OnClickListener {
            doRefresh()
        })

        var view = root.findViewById<FrameLayout>(R.id.item_detail_container)
        if(view != null){
            dual_screen = true
        }

        doLoadFirst()
        return root
    }

    fun doLoadFirst(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_qc_inspection",
                lang + "|" + userid
            )

            if (result.isError){
                BaseActivity().showToast(result.message)
            }else{
                handler.post(Runnable {
                    cbxInpection.setData(result.getData(),1,0)
                })
            }



            doLoadLangAnddashboard()
            doLoadAllLocation()


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun doRefresh(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            doLoadLangAnddashboard()
            doLoadAllLocation()


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    /*
    / this fun pls use in Thread
     */
    fun doLoadLangAnddashboard(){

        var yyyymmdd =  Utils().getFormatDDMMYYYY2(txtDate.text.toString())
        var dayOfWeek =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_WEEK_IN_MONTH)
        var dayOfMonth =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_MONTH)

        val result = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_qc_dashboard",
            lang + "|" + cbxInpection.value() + "|" + Utils().getFormatDDMMYYYY2(txtDate.text.toString()) +"|" + dayOfWeek  +"|" + dayOfMonth +"|" + userid
        )

        if (result.isError){
            handler.post(Runnable {
                BaseActivity().showToast(result.message)
            })

        }else{

            if (result.data.size == 1) {
                try{
                    handler.post(Runnable {
                        btnLoad.text =  result.data[0][0]
                        txtLastTime.text =  result.data[0][1]


                        txtAllTitle.text = result.data[0][2]
                        txtAllValue.text = result.data[0][3]
                        txtDoneTitle.text = result.data[0][4]
                        txtDoneValue1.text = result.data[0][5]
                        txtDoneValue2.text = result.data[0][6]
                        txtToDoTitle.text = result.data[0][7]
                        txtToDoValue.text = result.data[0][8]

//                        dataview = DataGridView(BaseActivity().baseContext)
//                        dataview.setHeaderStyle(
//                            ContextCompat.getColor(requireContext(), R.color.white),
//                            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
//                            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
//                        )
//                        dataview.layoutParams = LinearLayout.LayoutParams(
//                            ViewGroup.LayoutParams.MATCH_PARENT,
//                            10000
//                        )
//                        scroll_linear_content.addView(dataview)
                    })

                }catch (e: java.lang.Exception){
                    BaseActivity().showToast(getString(R.string.error_field_required))
                }
            }else{
                handler.post(Runnable {
                    BaseActivity().showToast(getString(R.string.error_field_required))
                })
            }

        }
    }

    /*
   / this fun pls use in Thread
    */
    fun doLoadAllLocation(){

        var yyyymmdd =  Utils().getFormatDDMMYYYY2(txtDate.text.toString())
        var dayOfWeek =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_WEEK)
        var dayOfMonth =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_MONTH)


        val result = BaseActivity().getWebService().GetListDataTableArg(
            "lg_mposv2_qc_all_location",
            lang + "|" + cbxInpection.value() + "|" + Utils().getFormatDDMMYYYY2(txtDate.text.toString()) +"|" + dayOfWeek  +"|" + dayOfMonth +"|" +userid,
            2
        )

        if (result.isError){
            handler.post(Runnable {
                BaseActivity().showToast(result.message)
            })

        }else{
            val location = result.getDataStringArr(0)
            val assets = result.getDataStringArr(1)

            handler.post(Runnable {
                scroll_linear_content.removeAllViews()
            })
            location.forEachIndexed { index, it ->
                var root = LayoutInflater.from(BaseActivity().baseContext).inflate(R.layout.fragment_qc_result_entry_v3_location,null) as LinearLayout
                var  locationName =  root.findViewById<TextView>(R.id.txtLocationName)
                var  locationAll =  root.findViewById<TextView>(R.id.txtLocationAll)
                var  locationDone =  root.findViewById<TextView>(R.id.txtLocationDone)
                var  locationTodo =  root.findViewById<TextView>(R.id.txtLocationToDo)
                var  locationExpand =  root.findViewById<CheckBox>(R.id.cbxLocationExpand)
                locationName.text = it[1].toString()
                locationAll.text = it[2].toString()
                locationDone.text = it[3].toString()
                locationTodo.text = it[4].toString()




                var linearLayout = LinearLayout(BaseActivity().baseContext)
                linearLayout.layoutParams =  LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                linearLayout.orientation = LinearLayout.VERTICAL
                linearLayout.gravity = Gravity.RIGHT
                linearLayout.setPadding(20, 5, 5, 5)

                var linearLayoutContent = LinearLayout(BaseActivity().baseContext)
                linearLayoutContent.layoutParams =  LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
                linearLayoutContent.orientation = LinearLayout.VERTICAL
                linearLayoutContent.gravity = Gravity.RIGHT
                linearLayoutContent.setPadding(2, 2, 2, 2)
                //  linearLayoutContent.setBackgroundColor(Color.parseColor("#B3FFFFFF"))


                //linearLayoutContent.background = BaseActivity().getDrawable(R.drawable.background_gray)


                locationExpand.setOnCheckedChangeListener { buttonView, isChecked ->
                    handler.post(Runnable {
                        if (buttonView.isChecked) {
                            linearLayoutContent.visibility = View.VISIBLE
                        } else {
                            linearLayoutContent.visibility = View.GONE
                        }
                    })

                }


                assets.forEachIndexed { index, it2 ->
                    if(it[0].toString() == it2[0].toString()){
                        var rootChild = if (it2[2] == "2") LayoutInflater.from(BaseActivity().baseContext).inflate(R.layout.fragment_qc_result_entry_v3_location,null) as LinearLayout
                        else
                            LayoutInflater.from(BaseActivity().baseContext).inflate(R.layout.fragment_qc_result_entry_v3_location_v2,null) as LinearLayout
                        var  locationNameChild =  rootChild.findViewById<TextView>(R.id.txtLocationName)
                        var  locationLnlAllChild =  rootChild.findViewById<LinearLayout>(R.id.lnlLocationAll)
                        var  locationAllChild =  rootChild.findViewById<TextView>(R.id.txtLocationAll)
                        var  locationLnlDoneChild =  rootChild.findViewById<LinearLayout>(R.id.lnlLocationDone)
                        var  locationDoneChild =  rootChild.findViewById<TextView>(R.id.txtLocationDone)
                        var  locationLnlTodoChild =  rootChild.findViewById<LinearLayout>(R.id.lnlLocationTodo)
                        var  locationTodoChild =  rootChild.findViewById<TextView>(R.id.txtLocationToDo)
                        var  locationExpandChild =  rootChild.findViewById<CheckBox>(R.id.cbxLocationExpand)


                        locationNameChild.text = it2[3].toString()
                        locationAllChild.text = it2[4].toString()
                        locationDoneChild.text = it2[5].toString()
                        locationTodoChild.text = it2[6].toString()


                        var linearLayoutChild = LinearLayout(BaseActivity().baseContext)
                        linearLayoutChild.layoutParams =  LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        linearLayoutChild.orientation = LinearLayout.VERTICAL
                        linearLayoutChild.gravity = Gravity.RIGHT
                        linearLayoutChild.setPadding(5, 5, 5, 5)


                        locationExpandChild.setOnCheckedChangeListener { buttonView, isChecked ->
                            handler.post(Runnable {
                                if (buttonView.isChecked) {
                                    linearLayoutChild.visibility = View.VISIBLE
                                    if(linearLayoutChild.childCount == 0){
                                        doLoadDetail(
                                            linearLayoutChild,
                                            1,
                                            it2[0],
                                            it2[1],
                                            it2[2],
                                            locationAllChild,
                                            locationDoneChild,
                                            locationTodoChild,
                                            locationAll,
                                            locationDone,
                                            locationTodo
                                        )
                                    }
                                } else {
                                    linearLayoutChild.visibility = View.GONE
                                }
                            })

                        }

//                        locationNameChild.setOnClickListener(View.OnClickListener {
//                            locationExpandChild.isChecked = true
//                            doLoadDetail(linearLayoutChild, 0, it2[0] ,it2[1] ,it2[2] )
//                        })


                        locationLnlAllChild.setOnClickListener(View.OnClickListener {
                            locationExpandChild.isChecked = true
                            doLoadDetail(linearLayoutChild, 1, it2[0] ,it2[1] ,it2[2] ,locationAllChild,locationDoneChild, locationTodoChild ,locationAll,locationDone,locationTodo )
                        })
                        locationLnlDoneChild.setOnClickListener(View.OnClickListener {
                            locationExpandChild.isChecked = true
                            doLoadDetail(linearLayoutChild, 2,it2[0] ,it2[1] ,it2[2],locationAllChild,locationDoneChild, locationTodoChild ,locationAll,locationDone,locationTodo)
                        })
                        locationLnlTodoChild.setOnClickListener(View.OnClickListener {
                            locationExpandChild.isChecked = true
                            doLoadDetail(linearLayoutChild, 3,it2[0] ,it2[1] ,it2[2],locationAllChild,locationDoneChild, locationTodoChild ,locationAll,locationDone,locationTodo)
                        })

                        handler.post(Runnable {

                            linearLayoutContent.addView(rootChild)
                            linearLayoutContent.addView(linearLayoutChild)
                        })

                    }

                }


                handler.post(Runnable {
                    linearLayoutContent.visibility = View.GONE
                    scroll_linear_content.addView(root)
                    linearLayout.addView(linearLayoutContent)
                    scroll_linear_content.addView(linearLayout)
                })


            }


        }
    }


    override fun onCameraResult(requestCode: Int, pathImage: String) {
        if(dual_screen){
            cusFragment.onCameraResult(requestCode,pathImage)
        }
    }

    override fun onChangeStatus() {
        if(dual_screen){
            try {
                doLoadDetailReload(
                    currChildView,
                    currType,
                    currlocation,
                    currasset_pk,
                    currreq_list_pk,
                    currassetAll,
                    currassetDone,
                    currassetTodo,
                    currlocationAll,
                    currlocationDone,
                    currlocationTodo
                )
            }catch (e: Exception){}
        }else{

        }
    }


    fun  doLoadDetailReload(
        view: LinearLayout,
        type: Int,
        location: String,
        asset_pk: String,
        req_list_pk: String,
        assetAll: TextView,
        assetDone: TextView,
        assetTodo: TextView,
        locationAll: TextView,
        locationDone: TextView,
        locationTodo: TextView
    ){



        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            var yyyymmdd =  Utils().getFormatDDMMYYYY2(txtDate.text.toString())
            var dayOfWeek =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_WEEK_IN_MONTH)
            var dayOfMonth =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_MONTH)

            val result = BaseActivity().getWebService().GetListDataTableArg(
                "lg_mposv2_qc_item_reload",
                lang + "|" + cbxInpection.value() + "|" + Utils().getFormatDDMMYYYY2(txtDate.text.toString()) +"|" + dayOfWeek  +"|" + dayOfMonth +"|" + type + "|" + location +"|" + asset_pk  +"|" + req_list_pk+"|" +userid,
                2
            )

            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else {

                val qc_req_d = result.getDataStringArr(0)

                if(qc_req_d.size > 0) {
                    var hov = HorizontalScrollView(BaseActivity().baseContext)
                    hov.layoutParams =  LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                    var linearLayoutChild = LinearLayout(BaseActivity().baseContext)
                    linearLayoutChild.layoutParams =  LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    linearLayoutChild.orientation = LinearLayout.VERTICAL
                    linearLayoutChild.background = resources.getDrawable(R.drawable.border_primary_cells)





                    handler.post(Runnable {
                        view.removeAllViews()
                        hov.addView(linearLayoutChild)
                        view.addView(hov)


                    })

                    qc_req_d.forEachIndexed { index, it ->

                        var rootChild = LayoutInflater.from(BaseActivity().baseContext).inflate(R.layout.fragment_qc_result_entry_v3_item,null) as LinearLayout
                        rootChild.layoutParams =  LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        var  col1 =  rootChild.findViewById<TextView>(R.id.txtCol1)
                        var  col2 =  rootChild.findViewById<TextView>(R.id.txtCol2)
                        var  col3 =  rootChild.findViewById<TextView>(R.id.txtCol3)
                        var  col4 =  rootChild.findViewById<TextView>(R.id.txtCol4)
                        var  col5 =  rootChild.findViewById<TextView>(R.id.txtCol5)
                        var  col6 =  rootChild.findViewById<TextView>(R.id.txtCol6)
                        col1.text =it[0].toString()
                        col2.text = it[1].toString()
                        col6.text = it[2].toString()
                        col3.text = it[5].toString()
                        col4.text = it[6].toString()
                        col5.text = it[7].toString()
                        var master_pk  = it[3].toString()
                        var detail_pk   = it[4].toString()
                        var title   = it[0].toString()

                        if(it[9].toString() == "Y"){
                            rootChild.setBackgroundColor(Color.parseColor("#E0F7FA"))
                        }else    if(it[9].toString() == "N"){
                            rootChild.setBackgroundColor(Color.parseColor("#F44336"))
                        }

                        rootChild.setOnClickListener(View.OnClickListener {

                            onclickItem(asset_pk, master_pk,detail_pk,title,rootChild)
                            //BaseActivity().showProgressBar( master_pk + "/" +  detail_pk + "/" + asset_pk)
                        })
                        handler.post(Runnable {
                            linearLayoutChild.addView(rootChild)
                        })
                    }
                }else{
                    handler.post(Runnable {
                        view.removeAllViews()



                    })
                }


                val data = result.getDataStringArr(1)
                if (data.size == 1){
                    handler.post(Runnable {
                        txtAllValue.text = data[0][0].toString()
                        txtDoneValue1.text = data[0][1].toString()
                        txtDoneValue2.text = data[0][2].toString()
                        txtToDoValue.text = data[0][3].toString()

                        assetAll.text = data[0][4].toString()
                        assetDone.text = data[0][5].toString()
                        assetTodo.text = data[0][6].toString()
                        locationAll.text = data[0][7].toString()
                        locationDone.text = data[0][8].toString()
                        locationTodo.text = data[0][9].toString()
                    })


                }



            }




            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }


    private lateinit var  currChildView : LinearLayout
    private lateinit var  currassetAll : TextView
    private lateinit var  currassetDone : TextView
    private lateinit var  currassetTodo : TextView
    private lateinit var  currlocationAll : TextView
    private lateinit var  currlocationDone : TextView
    private lateinit var  currlocationTodo : TextView
    private  var  currType  = 0
    private  var  currlocation =""
    private  var  currasset_pk =""
    private  var  currreq_list_pk = ""


    fun  doLoadDetail(
        view: LinearLayout,
        type: Int,
        location: String,
        asset_pk: String,
        req_list_pk: String,
        assetAll: TextView,
        assetDone: TextView,
        assetTodo: TextView,
        locationAll: TextView,
        locationDone: TextView,
        locationTodo: TextView
    ){

        currType = type
        currlocation = location
        currasset_pk = asset_pk
        currreq_list_pk = req_list_pk
        currChildView = view
        currassetAll = assetAll
        currassetDone = assetDone
        currassetTodo = assetTodo
        currlocationAll = locationAll
        currlocationDone = locationDone
        currlocationTodo = locationTodo




        if (type == 0){
            handler.post(Runnable {
                view.removeAllViews()
            })
            return
        }

        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            var yyyymmdd =  Utils().getFormatDDMMYYYY2(txtDate.text.toString())
            var dayOfWeek =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_WEEK_IN_MONTH)
            var dayOfMonth =  Utils().getStringToDate2(yyyymmdd).get(Calendar.DAY_OF_MONTH)


            val result = BaseActivity().getWebService().GetListDataTableArg(
                "lg_mposv2_qc_item",
                lang + "|" + cbxInpection.value() + "|" + Utils().getFormatDDMMYYYY2(txtDate.text.toString()) +"|" + dayOfWeek  +"|" + dayOfMonth +"|" + type + "|" + location +"|" + asset_pk  +"|" + req_list_pk+"|" +userid,
                1
            )

            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else {

                val qc_req_d = result.getDataStringArr(0)

                if(qc_req_d.size > 0) {
                    var hov = HorizontalScrollView(BaseActivity().baseContext)
                    hov.layoutParams =  LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )

                    var linearLayoutChild = LinearLayout(BaseActivity().baseContext)
                    linearLayoutChild.layoutParams =  LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    linearLayoutChild.orientation = LinearLayout.VERTICAL
                    linearLayoutChild.background = resources.getDrawable(R.drawable.border_primary_cells)





                    handler.post(Runnable {
                        view.removeAllViews()
                        hov.addView(linearLayoutChild)
                        view.addView(hov)


                    })

                    qc_req_d.forEachIndexed { index, it ->

                        var rootChild = LayoutInflater.from(BaseActivity().baseContext).inflate(R.layout.fragment_qc_result_entry_v3_item,null) as LinearLayout
                        rootChild.layoutParams =  LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        var  col1 =  rootChild.findViewById<TextView>(R.id.txtCol1)
                        var  col2 =  rootChild.findViewById<TextView>(R.id.txtCol2)
                        var  col3 =  rootChild.findViewById<TextView>(R.id.txtCol3)
                        var  col4 =  rootChild.findViewById<TextView>(R.id.txtCol4)
                        var  col5 =  rootChild.findViewById<TextView>(R.id.txtCol5)
                        var  col6 =  rootChild.findViewById<TextView>(R.id.txtCol6)
                        col1.text =it[0].toString()
                        col2.text = it[1].toString()
                        col6.text = it[2].toString()
                        col3.text = it[5].toString()
                        col4.text = it[6].toString()
                        col5.text = it[7].toString()
                        var master_pk  = it[3].toString()
                        var detail_pk   = it[4].toString()
                        var title   = it[0].toString()

                        if(it[9].toString() == "Y"){
                            rootChild.setBackgroundColor(Color.parseColor("#E0F7FA"))
                        }else    if(it[9].toString() == "N"){
                            rootChild.setBackgroundColor(Color.parseColor("#F44336"))
                        }

                        rootChild.setOnClickListener(View.OnClickListener {

                            onclickItem(asset_pk, master_pk,detail_pk,title,rootChild)
                            //BaseActivity().showProgressBar( master_pk + "/" +  detail_pk + "/" + asset_pk)
                        })
                        handler.post(Runnable {
                            linearLayoutChild.addView(rootChild)
                        })
                    }
                }else{
                    handler.post(Runnable {
                        view.removeAllViews()



                    })
                }



            }




            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun onclickItem(
        assetPk: String,
        masterPk: String,
        detailPk: String,
        title: String,
        rootChild: LinearLayout
    ) {

        try{
            rootChild.setBackgroundColor(Color.CYAN)
        }catch (ex: java.lang.Exception){

        }

        if (dual_screen) {
            val fragment = QCResultDetailEmtry().apply {
                arguments = Bundle().apply {
                    putString(BaseDetailFragment.ARG_ITEM_ID_M, masterPk)
                    putString(BaseDetailFragment.ARG_ITEM_ID_D, detailPk)
                    putString(BaseDetailFragment.ARG_ITEM_ID_D2, assetPk)
                    putString(BaseDetailFragment.ARG_ITEM_ID_DATA, Utils().getFormatDDMMYYYY2(txtDate.text.toString()))
                }
            }


            val ft: FragmentTransaction =  BaseActivity().supportFragmentManager.beginTransaction()
            cusFragment = fragment
            ft.replace(R.id.item_detail_container, fragment)
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            ft.commit()

        } else {
            val intent = Intent(BaseActivity().baseContext, DetailActivity::class.java).apply {
                putExtra(BaseDetailFragment.ARG_FRAMENT_NAME, "QCResultDetailEmtry")
                putExtra(BaseDetailFragment.ARG_FRAMENT_TITLE, title)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_M, masterPk)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D, detailPk)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D2, assetPk)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_DATA, Utils().getFormatDDMMYYYY2(txtDate.text.toString()))

            }
            BaseActivity().startActivity(intent)
        }
    }

    fun showDateDialog(){
        val c = Calendar.getInstance()
        val yearc = c.get(Calendar.YEAR)
        val monthc = c.get(Calendar.MONTH)
        val dayc = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this.requireContext(), DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
            var month:String = if(monthOfYear >= 9 ){
                (monthOfYear+1).toString()
            }else{
                "0" + (monthOfYear+1).toString()
            }
            var day :String = if(dayOfMonth >= 10 ){
                dayOfMonth.toString()
            }else{
                "0" + dayOfMonth.toString()
            }







            txtDate.text = "" + day + "/" + month + "/" + year


        }, yearc, monthc, dayc)

        dpd.show()
    }
}