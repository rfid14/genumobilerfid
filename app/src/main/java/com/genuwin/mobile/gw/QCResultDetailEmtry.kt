package com.genuwin.mobile.gw

import android.app.Dialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.core.view.isVisible
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.HorizontaBlockView
import java.io.ByteArrayOutputStream
import java.util.*


class QCResultDetailEmtry : BaseDetailFragment(){
    lateinit var colXTitle: TextView
    lateinit var colX3: TextView
    lateinit var colX4: TextView
    lateinit var colX5: TextView
    lateinit var colX6: TextView
    lateinit var colX7: TextView
    lateinit var colX8: TextView
    lateinit var colX9: TextView
    lateinit var colX10: TextView
    lateinit var colX11: TextView
    lateinit var colX12: TextView
    lateinit var colX13: TextView
    lateinit var colX14: TextView
    lateinit var colX15: TextView

    lateinit var radioPass: RadioButton
    lateinit var radioFail: RadioButton
    lateinit var radioNo: RadioButton
    lateinit var radioYes: RadioButton
    lateinit var btnSave: Button
    lateinit var btnDel: Button


    lateinit var colXTitle3: TextView
    lateinit var colXTitle4: TextView
    lateinit var colXTitle5: TextView
    lateinit var colXTitle6: TextView
    lateinit var colXTitle7: TextView
    lateinit var colXTitleDes: TextView
    lateinit var desVal: TextView




    lateinit var txtValue: TextView
    lateinit var lnaValueYN: RadioGroup
    lateinit var lnaValue: LinearLayout


    lateinit var txtNumImager: TextView
    lateinit var listImage: HorizontaBlockView
    lateinit var btnCamera: ImageButton



    var result_pk = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var root = inflater.inflate(R.layout.fragment_qc_result_entry_v3_detail,container,false) as LinearLayout
        colXTitle = root.findViewById(R.id.colXTitle)
        colX3 = root.findViewById(R.id.colX3)
        colX4 = root.findViewById(R.id.colX4)
        colX5 = root.findViewById(R.id.colX5)
        colX6 = root.findViewById(R.id.colX6)
        colX7 = root.findViewById(R.id.colX7)
        colX8 = root.findViewById(R.id.colX8)
        colX9 = root.findViewById(R.id.colX9)
        colX10 = root.findViewById(R.id.colX10)
        colX11 = root.findViewById(R.id.colX11)
        colX12 = root.findViewById(R.id.colX12)
        colX13 = root.findViewById(R.id.colX13)
        colX14 = root.findViewById(R.id.colX14)
        colX15 = root.findViewById(R.id.colX15)
        colXTitleDes = root.findViewById(R.id.colXTitleDes)
        desVal = root.findViewById(R.id.desVal)
        colXTitle3 = root.findViewById(R.id.colXTitle3)
        colXTitle4 = root.findViewById(R.id.colXTitle4)
        colXTitle5 = root.findViewById(R.id.colXTitle5)
        colXTitle6 = root.findViewById(R.id.colXTitle6)
        colXTitle7 = root.findViewById(R.id.colXTitle7)
        radioPass = root.findViewById(R.id.radioPass)
        radioFail = root.findViewById(R.id.radioFail)
        radioNo = root.findViewById(R.id.radioNo)
        radioYes = root.findViewById(R.id.radioYes)
        btnSave = root.findViewById(R.id.btnSave)
        btnDel = root.findViewById(R.id.btnDel)
        txtValue = root.findViewById(R.id.txtValue)
        lnaValueYN = root.findViewById(R.id.lnaValueYN)
        lnaValue = root.findViewById(R.id.lnaValue)

        txtValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                checkValue()
            }
        })

        txtNumImager = root.findViewById(R.id.txtNumImager)
        listImage = root.findViewById(R.id.listImage)
        listImage.customview = object : HorizontaBlockView.CustomView(){
            override fun getView(position: Int, viewgroup: View): View {

                var rootView = LayoutInflater.from(context).inflate(R.layout.block_image_item,null)

                var imageView = rootView.findViewById<ImageView>(R.id.imageView)
                var data = listImage.get(position)
                if (data!=null) {


                    if(!data.get(1).isNullOrEmpty() ) {
                        val imageBytes = Base64.decode(data.get(1), 0)
                        val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                        imageView.setImageBitmap(image)
                    }else{
                        var uri = Uri.parse(data.get(2))
                        imageView.setImageURI(uri)
                    }
                }
                return rootView
            }
        }

        listImage.handlerClickBlock = object : HorizontaBlockView.HandlerClickBlock( ){
            override fun onSelectBlockIndex(position: Int, status: Boolean): Boolean {

                var data = listImage.get(position)
                if (data!=null) {


                    if(!data.get(1).isNullOrEmpty() ) {
                        val imageBytes = Base64.decode(data.get(1), 0)
                        val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                        showImageDialog(image,"0",data.get(0))
                    }else{
                        var uri = Uri.parse(data.get(2))
                        showImageDialog(uri)
                    }
                }
                return true
            }

            override fun onSelectBlockIndexed(position: Int, status: Boolean) {


            }

        }

        btnCamera = root.findViewById(R.id.btnCamera)
        btnCamera.setOnClickListener {

            BaseActivity().openCamera(9999)
        }

        btnSave.setOnClickListener(View.OnClickListener {
            doSave()
        })

        btnDel.setOnClickListener(View.OnClickListener {
            askdoDelete()
        })
        doLoadFirst()
        return root
    }


    override fun onCameraResult(requestCode: Int, pathImage: String) {
        if (requestCode == 9999 ) {



            var list = listOf<String> (
                "0",
                "",
                pathImage
            )
            listImage.add(list.toTypedArray())

            txtNumImager.text = listImage.size().toString()

        }
    }

    fun doLoadFirst(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            val result = BaseActivity().getWebService().GetListDataTableArg(
                "lg_mposv2_qc_ins_detail",
                lang + "|" + userid  + "|" + id_m + "|" + id_d + "|" + id_d2 + "|" + data ,
                2
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                val lang_val = result.getDataStringArr(0)
                val data_val = result.getDataStringArr(1)
                if (lang_val.size == 1 && data_val.size == 1) {

                    try{
                        handler.post(Runnable {

                            colXTitle3.text =  lang_val[0][0]
                            colXTitle4.text =  lang_val[0][1]
                            colXTitle5.text = lang_val[0][2]
                            colXTitle6.text =  lang_val[0][3]
                            colXTitle7.text =  lang_val[0][4]
                            colXTitleDes.text =  lang_val[0][5]
                            colX10.text =  lang_val[0][6]
                            colX11.text =  lang_val[0][7]
                            colX12.text =  lang_val[0][8]
                            radioPass.text =  lang_val[0][9]
                            radioFail.text =  lang_val[0][10]
                            btnSave.text =  lang_val[0][11]
                            btnDel.text =  lang_val[0][12]

                        })



                    }catch (e: java.lang.Exception){
                        handler.post(Runnable {
                            BaseActivity().showToast(getString(R.string.error_field_required))
                        })
                    }

                    try {
                        result_pk = data_val[0][0].toInt()
                    }catch (er: java.lang.Exception){

                    }

                    try{
                        handler.post(Runnable {
                            //colXTitle.text =  result.data[0][0]

                            colXTitle.text =  data_val[0][1]
                            colX3.text =  data_val[0][2]
                            colX4.text =  data_val[0][3]
                            colX5.text =  data_val[0][4]
                            colX6.text =  data_val[0][5]
                            colX7.text =  data_val[0][6]
                            colX8.text =  data_val[0][7]
                            colX9.text =  data_val[0][8]
                            colX13.text =  data_val[0][9]
                            colX14.text =  data_val[0][10]
                            colX15.text =  data_val[0][11]
                            radioNo.text = data_val[0][9]
                            radioYes.text =data_val[0][10]
                            desVal.text =data_val[0][15]
                            txtValue.text =data_val[0][16]
                            var text_type =data_val[0][12]
                            var text_yn = data_val[0][13]
                            if(data_val[0][14].toString() == "Y"){
                                radioPass.isChecked = true
                            }else{
                                radioFail.isChecked = true
                            }

                            if (text_yn == "Y" && text_type != "1"){
                                lnaValue.visibility = View.GONE
                                lnaValueYN.visibility = View.VISIBLE
                            }else if(text_yn == "Y" && text_type == "1"){
                                // android:inputType="numberDecimal|numberSigned"
                                lnaValue.visibility = View.VISIBLE
                                lnaValueYN.visibility = View.GONE
                                txtValue.inputType = InputType.TYPE_NUMBER_FLAG_DECIMAL
                            }
                            else{
                                lnaValue.visibility = View.VISIBLE
                                lnaValueYN.visibility = View.GONE
                            }

                            if(result_pk > 0){
                                btnDel.visibility = View.VISIBLE
                            }else{
                                btnDel.visibility = View.GONE
                            }

                        })



                    }catch (e: java.lang.Exception){
                        handler.post(Runnable {
                            BaseActivity().showToast(getString(R.string.error_field_required))
                        })
                    }





                    handler.post(Runnable {
                        listImage.clear()
                    })

                    if (result_pk > 0){
                        val resultImage = BaseActivity().getWebService().GetDataTableArg(
                            "lg_mposv2_qc_result_image",
                            lang + "|" + userid + "|" + result_pk
                        )

                        if(resultImage.isError){

                            handler.post(Runnable {
                                Toast.makeText(BaseActivity().baseContext,resultImage.message, Toast.LENGTH_LONG).show()
                            })


                        }else {


                            handler.post(Runnable {
                                listImage.setData(resultImage.data)
                                txtNumImager.text = listImage.size().toString()
                            })


                        }
                    }

                }else{
                    handler.post(Runnable {
                        BaseActivity().showToast(getString(R.string.error_field_required))
                    })
                }




            }







            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()

    }

    fun  doSave(){

        if( lnaValue.visibility == View.VISIBLE && txtValue.text.toString() == "" ){

            BaseActivity().showToast(getString(R.string.error_field_required))
            return
        }


        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            var pass_yn = if( radioPass.isChecked){
                "Y"
            }else{
                "N"
            }

            var value  = if( txtValue.text.toString() == "" ){
                "0"
            }else{
                txtValue.text.toString()
            }

            var value2  = if( radioYes.isChecked ){
                "Y"
            }else{
                "N"
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_qc_ins_save",
                lang + "|" + userid  + "|" + data + "|" + id_m + "|" + id_d + "|" + id_d2 + "|" + value + "|" + value2 + "|" + pass_yn + "|" + desVal.text.toString()
            )

            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else {
                if (result.data.size == 1) {

                    try {
                        result_pk = result.data[0][0].toInt()
                    }catch (eformat:  java.lang.Exception){

                    }

                    try{
                        handler.post(Runnable {


//                            try {
//                                result_pk = result.data[0][0].toInt()
//                            }catch (eformat:  java.lang.Exception){
//
//                            }

                            BaseActivity().showToast( result.data[0][1])
                            if(result_pk > 0){
                                btnDel.visibility = View.VISIBLE
                            }else{
                                btnDel.visibility = View.GONE
                            }



                        })

                    }catch (e: java.lang.Exception){
                        handler.post(Runnable {
                            BaseActivity().showToast(getString(R.string.error_field_required))
                        })
                    }
                }else{
                    handler.post(Runnable {
                        BaseActivity().showToast(getString(R.string.error_field_required))
                    })
                }

                if(result_pk >0){
                    for(i :Int in 1..listImage.size()){
                        val data = listImage.get(i-1)
                        if (data!=null){
                            if(data.get(1).isNullOrEmpty()){
                                var uri = Uri.parse(data.get(2))


                                var  image = MediaStore.Images.Media.getBitmap(
                                    BaseActivity().contentResolver, uri)

                                var byteArrayOutputStream =  ByteArrayOutputStream()
                                image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)

                                var byteArrayFull = byteArrayOutputStream .toByteArray()


                                var imageThumb = Bitmap.createScaledBitmap(image,250,250,false)
                                var byteArrayOutputStream2 =  ByteArrayOutputStream()
                                imageThumb.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream2)
                                var byteArrayThumb = byteArrayOutputStream2 .toByteArray()

                                var key = Utils().dateyyyyMMddhhmmss + Random().nextInt(100).toString()

                                var resultImag = BaseActivity().getWebService().UploadImageAndThumb(
                                    "lg_mposv2_qc_result_image_up",
                                    byteArrayFull ,
                                    key + "|" + "Y|Y" + "|" +  result_pk  + "||" + userid
                                )

                                if (resultImag.isError){
                                    handler.post(Runnable {

                                        Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
                                    })
                                    // return@Runnable
                                }else{

                                }


                                resultImag = BaseActivity().getWebService().UploadImageAndThumb(
                                    "lg_mposv2_qc_result_image_up",
                                    byteArrayThumb,
                                    key + "|" + "N|Y" + "|" +  result_pk  + "||" + userid
                                )

                                if (resultImag.isError){
                                    handler.post(Runnable {

                                        Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
                                    })
                                    // return@Runnable
                                }else{
                                    data[1] = Base64.encodeToString(byteArrayThumb,0)
                                }



                            }
                        }


                    }
                }
            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                BaseActivity().onChangeStatus()
            })
        }).start()
    }

    fun checkValue(){

        //min
        colX13.text
        //max
        colX14.text
        //standard
        colX15.text
        var min = -99999.0
        try{
            min = colX13.text.toString().toDouble()
        }catch(e: java.lang.Exception) {}
        var max = 99999.0
        try{
            max = colX14.text.toString().toDouble()
        }catch(e: java.lang.Exception) {}

        var standard = 0.0

        try{
            standard = colX15.text.toString().toDouble()
        }catch(e: java.lang.Exception) {}

        try{
            var value = txtValue.text.toString().toDouble()

            if(value <= max + standard && value >= min + standard){
                radioPass.isChecked = true
            }else{
                radioFail.isChecked = true
            }
        }catch(e: java.lang.Exception) {}



    }

    fun askdoDelete(){
        BaseActivity().confirmDialog("...", btnDel.text.toString(),object : BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {

                doDelete()

            }
        } )
    }

    fun  doDelete(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(btnDel.text.toString())
        })

        Thread(Runnable {
            Thread.sleep(1000)


            var pass_yn = if( radioPass.isChecked){
                "Y"
            }else{
                "N"
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_qc_ins_del",
                lang + "|" + userid  + "|" + result_pk
            )

            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else {

                result_pk = 0;


                handler.post(Runnable {
                    btnDel.visibility = View.GONE
                    txtValue.text = ""
                    radioFail.isChecked = true
                    listImage.clear()

                    txtNumImager.text = "0"
                })


            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                BaseActivity().onChangeStatus()
            })
        }).start()
    }

    fun showImageDialog(bitmap: Bitmap,pkDetail:String, pkImage:String){
        try {
            val dialog = BaseActivity().getNewDialog()
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


            val input = ImageView(context)

            input.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            input.setImageBitmap(bitmap)

            //input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)

            val content = LinearLayout(context)

            content.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            content.addView(input)
            dialog.setContentView(content)
            dialog.setCanceledOnTouchOutside(true)

            dialog.show()



            doLOadFullSizeImage(input, pkDetail, pkImage)
        }catch (e: Exception){

        }
    }

    fun doLOadFullSizeImage(imageView: ImageView,pkDetail:String, pkImage:String) = Thread(Runnable {



        val resultmaster = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_qc_result_images_at",
            pkDetail + "|" + pkImage
        )



        if (resultmaster.isError){
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
            })

        }else{
            try {
                val imageBytes =
                    Base64.decode(resultmaster.data[0][1], 0)
                val image =
                    BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                handler.post(Runnable {
                    imageView.setImageBitmap(image)
                })
            }catch (e: java.lang.Exception){}
            //TODO
        }






    }).start()



    fun showImageDialog(uri: Uri){




        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)




        val input = ImageView(context)

        input.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        input.setImageURI(uri)

        // input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)


        val content = LinearLayout(context)

        content.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
//        if (input.width > input.height)
//            content.rotation = 90F

        content.addView(input)
        dialog.setContentView(content)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

}
