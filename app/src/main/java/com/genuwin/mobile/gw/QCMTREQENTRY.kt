package com.genuwin.mobile.gw

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import com.lamvuanh.lib.HorizontaBlockView
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.HashMap


class QCMTREQENTRY() : BaseFragment() {

    var  processType :String = ""
    var sourceType :String = ""

    var requestType_CodeGrp :String = "LGPC1140"
    var v_ma_req_m_pk :String = ""
    lateinit var txtStatus: TextView
    lateinit var tvGroup: TextView
    lateinit var lstGroup: Combobox
    lateinit var tvLine: TextView
    lateinit var txtAsset: EditText
    lateinit var tvStatus: TextView
    lateinit var lstStatus: Combobox
    lateinit var tvReqNo_M: TextView
    lateinit var txtReqNo_M: EditText
    lateinit var txtDateFrom: TextView
    lateinit var btnDateFrom: Button
    lateinit var txtDateTo: TextView
    lateinit var btnDateTo: Button
    lateinit var btnSearch: Button
    lateinit var dataGridView :DataGridView
    lateinit var dataGrdTask :DataGridView
    lateinit var dataGrdTool :DataGridView



    lateinit var tvReqNo  : TextView
    lateinit var txtReqNo : EditText
    lateinit var tvReqDate : TextView
    lateinit var txtReqDate : TextView
    lateinit var tvReqAsset : TextView
    lateinit var txtReqAsset : AutoCompleteTextView
    lateinit var tvReqEmp : TextView
    lateinit var txtReqEmp : AutoCompleteTextView
    lateinit var tvProcess : TextView
    lateinit var lstProcess : Combobox
    lateinit var tvReqType : TextView
    lateinit var lstReqType : Combobox
    lateinit var btnReqDate : Button
    lateinit var tvDesc : TextView
    lateinit var txtDesc : EditText

    lateinit var tvMaDate      : TextView
    lateinit var txtMaDate      : TextView
    lateinit var btnMaDate      : Button

    lateinit var tvCharger      : TextView
    lateinit var txtCharger     : AutoCompleteTextView
    lateinit var tvAmount       : TextView
    lateinit var txtAmount      : EditText
    lateinit var tvPartner      : TextView
    lateinit var txtPartner	    : AutoCompleteTextView
    lateinit var tvDesc2        : TextView
    lateinit var txtDesc2       : EditText

    lateinit var btnSave: Button
    lateinit var btnNewRow: Button
    lateinit var btnDelete: Button
    lateinit var btnSubmit: Button
    lateinit var btnCamera: ImageButton
    lateinit var listImage: HorizontaBlockView
//    lateinit var image_view: ImageView
    lateinit var IMAGEFULL :Bitmap
//    lateinit var IMG_BITMAP :Bitmap

    lateinit var txtStartDate: TextView
    lateinit var btnStartDate: Button
    lateinit var txtEndDate: TextView
    lateinit var btnEndDate: Button
    lateinit var txtStartTime: TextView
    lateinit var txtEndTime: TextView



    var AssetList : HashMap<String, String> = HashMap<String, String> ()
    var assetLst :ArrayList<String> = arrayListOf()
    var EmpList : HashMap<String, String> = HashMap<String, String> ()
    var empLst :ArrayList<String> = arrayListOf()
    var empLst2 :ArrayList<String> = arrayListOf()

    var actionMaster :String = ""
    var PATHIMAGE :String = ""

    /*dataGridView*/
    val G_REQ_NO		:Int			= 0
    val G_PK			:Int			= 1
    val G_ASSET_CODE    :Int        	= 2
    val G_ASSET_NAME    :Int        	= 3
    val G_UOM           :Int        	= 4
    val G_STATUS        :Int        	= 5
    val G_MA_REQ_M_PK   :Int        	= 6
//    val G_START_DATE              :Int			= 7
//    val G_START_TIME              :Int			= 8
//    val G_END_DATE              :Int			= 9
//    val G_END_TIME              :Int			= 10


    /*data master*/
    val G_M_REQ_PK				  :Int			= 0
    val G_M_ASSET_PK              :Int			= 1
    val G_M_ASSET_CODE            :Int			= 2
    val G_M_ASSET_NAME            :Int			= 3
    val G_M_STATUS                :Int			= 4
    val G_M_STATUSCODE            :Int			= 5
    val G_M_TLG_MA_PROCESS_PK     :Int			= 6
    val G_M_PROCESS_ID            :Int			= 7
    val G_M_PROCESS_NAME          :Int			= 8
    val G_M_REQ_EMP_PK            :Int			= 9
    val G_M_REQ_EMP_ID            :Int			= 10
    val G_M_REQ_EMP_NAME          :Int			= 11
    val G_M_CHARGER_PK            :Int			= 12
    val G_M_CHARGER_ID            :Int			= 13
    val G_M_CHARGER_NAME          :Int			= 14
    val G_M_REQ_DATE              :Int			= 15
    val G_M_REQ_TYPE              :Int			= 16
    val G_M_REQ_DESC              :Int			= 17
    val G_M_REQ_NO                :Int			= 18
    val G_M_MA_DATE               :Int			= 19
    val G_M_MA_AMOUNT             :Int			= 20
    val G_M_RESPONSE              :Int			= 21
//    val G_M_IMG_PNG               :Int			= 22
//    val G_M_IMG_FULL              :Int			= 23
    val G_M_START_DATE              :Int		= 22
    val G_M_START_TIME              :Int		= 23
    val G_M_END_DATE              :Int			= 24
    val G_M_END_TIME              :Int			= 25



    /*grid task*/
    val G_TASK_PK						:Int  = 0
    val G_TASK_MA_REQ_M_PK              :Int  = 1
    val G_TASK_SEQ                      :Int  = 2
    val G_TASK_TLG_MA_TASK_PK           :Int  = 3
    val G_TASK_MA_TASK_NAME             :Int  = 4
    val G_TASK_TLG_MA_PROCESS_PK        :Int  = 5
    val G_TASK_PROCESS_NAME             :Int  = 6
    val G_TASK_MA_TASK_TYPE             :Int  = 7
    val G_TASK_MA_TASK_TYPE_NAME        :Int  = 8
    val G_TASK_P_DELETE                 :Int  = 9

/*grid tool*/
    val G_TOOL_PK						:Int = 0
    val G_TOOL_MA_REQ_M_PK              :Int = 1
    val G_TOOL_SEQ                      :Int = 2
    val G_TOOL_PART_PK                  :Int = 3
    val G_TOOL_ASSET_CODE               :Int = 4
    val G_TOOL_ASSET_NAME               :Int = 5
    val G_TOOL_QTY                      :Int = 6
    val G_TOOL_TLG_MA_PROCESS_PK        :Int = 7
    val G_TOOL_P_DELETE                 :Int = 8

    var cur_pos: Int = 0
    var FUNC_SEP_ID :String = ""
    var MENU_NAME :String = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_qc_mt_req_entry, container, false)
        txtStatus = root.findViewById(R.id.txtStatus)
        tvGroup = root.findViewById(R.id.tvGroup)
        lstGroup = root.findViewById(R.id.lstGroup)
        tvLine = root.findViewById(R.id.tvLine)
        txtAsset = root.findViewById(R.id.txtAsset)
        tvStatus = root.findViewById(R.id.tvStatus)
        lstStatus = root.findViewById(R.id.lstStatus)
        tvReqNo_M = root.findViewById(R.id.tvReqNo_M)
        txtReqNo_M = root.findViewById(R.id.txtReqNo_M)
        txtDateFrom = root.findViewById(R.id.txtDateFrom)
        btnDateFrom = root.findViewById(R.id.btnDateFrom)
        txtDateTo = root.findViewById(R.id.txtDateTo)
        btnDateTo = root.findViewById(R.id.btnDateTo)
        btnSearch = root.findViewById(R.id.btnSearch)
        dataGridView = root.findViewById(R.id.dataGridView)
        dataGrdTask = root.findViewById(R.id.dataGrdTask)
        dataGrdTool = root.findViewById(R.id.dataGrdTool)
        txtStartDate = root.findViewById(R.id.txtStartDate)
        btnStartDate = root.findViewById(R.id.btnStartDate)
        txtEndDate = root.findViewById(R.id.txtEndDate)
        btnEndDate = root.findViewById(R.id.btnEndDate)
        txtStartTime  = root.findViewById(R.id.txtStartTime)
        txtEndTime    = root.findViewById(R.id.txtEndTime)


        tvReqNo             = root.findViewById(R.id.tvReqNo)
        txtReqNo            = root.findViewById(R.id.txtReqNo)
        tvReqDate           = root.findViewById(R.id.tvReqDate)
        txtReqDate          = root.findViewById(R.id.txtReqDate)
        tvReqAsset          = root.findViewById(R.id.tvReqAsset)
        txtReqAsset         = root.findViewById(R.id.txtReqAsset)
        tvReqEmp            = root.findViewById(R.id.tvReqEmp)
        txtReqEmp           = root.findViewById(R.id.txtReqEmp)
        tvProcess           = root.findViewById(R.id.tvProcess)
        lstProcess          = root.findViewById(R.id.lstProcess)
        tvReqType           = root.findViewById(R.id.tvReqType)
        lstReqType          = root.findViewById(R.id.lstReqType)
        btnReqDate          = root.findViewById(R.id.btnReqDate)
        tvDesc              = root.findViewById(R.id.tvDesc)
        txtDesc             = root.findViewById(R.id.txtDesc)

        tvCharger            = root.findViewById(R.id.tvCharger)
        txtCharger           = root.findViewById(R.id.txtCharger)
        tvAmount             = root.findViewById(R.id.tvAmount)
        txtAmount            = root.findViewById(R.id.txtAmount)
        tvDesc2              = root.findViewById(R.id.tvDesc2)
        txtDesc2             = root.findViewById(R.id.txtDesc2)
        tvMaDate             = root.findViewById(R.id.tvMaDate)
        txtMaDate             = root.findViewById(R.id.txtMaDate)
        btnMaDate             = root.findViewById(R.id.btnMaDate)

        btnSave             = root.findViewById(R.id.btnSave)
        btnNewRow             = root.findViewById(R.id.btnNewRow)
        btnDelete             = root.findViewById(R.id.btnDelete)
        btnSubmit             = root.findViewById(R.id.btnSubmit)
        btnCamera             = root.findViewById(R.id.btnCamera)
        listImage             = root.findViewById(R.id.listImage)
//        image_view             = root.findViewById(R.id.image_viewimage_view)
        FUNC_SEP_ID   = this.BaseActivity().FUNC_SEP_ID.toString()
        MENU_NAME = this.BaseActivity().MENU_NAME.toString()

     if(FUNC_SEP_ID == "MOLD")
     {
        processType  = "35" /*gia tri mac dinh . form nay chi su dung cho loai la 35.Mold Ma */
        sourceType  = MENU_NAME /*gia tri mac dinh . form nay chi su dung cho loai la 35.Mold Ma */
     }
     else
     {
         if(FUNC_SEP_ID == "GENERAL")
         {
             processType  = "30" /*gia tri mac dinh . form nay chi su dung cho loai la 35.Mold Ma */
             sourceType  = MENU_NAME /*gia tri mac dinh . form nay chi su dung cho loai la 35.Mold Ma */
         }
     }


        dataGridView.widthWithpercent = true
        dataGridView.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(50  , requireContext())
        )
        dataGridView.mutilSelect=false /*chi highligh dong dang chon*/
        dataGridView.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                 v_ma_req_m_pk = dataGridView.getDataCell(position,G_MA_REQ_M_PK)
                var v_tlg_ma_process_pk :String = dataGridView.getDataCell(position,G_M_TLG_MA_PROCESS_PK)
                actionMaster = ""
                cur_pos = position
                onSearch_Master(v_ma_req_m_pk)
                onSearch_dataGrdTask(v_tlg_ma_process_pk , v_tlg_ma_process_pk )
                onSearch_dataGrdTool(v_tlg_ma_process_pk , v_tlg_ma_process_pk )
                return true
            }
        }





        dataGrdTask.widthWithpercent = true
        dataGrdTask.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
        )
        dataGrdTask.mutilSelect=false /*chi highligh dong dang chon*/
        dataGrdTask.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                return true
            }
        }

        dataGrdTool.widthWithpercent = true
        dataGrdTool.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
        )
        dataGrdTool.mutilSelect=false /*chi highligh dong dang chon*/
        dataGrdTool.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                return true
            }
        }

        setbtnDate(btnDateFrom , txtDateFrom)
        setbtnDate(btnDateTo , txtDateTo)
        setbtnDate(btnReqDate , txtReqDate)
        setbtnDate(btnMaDate , txtMaDate)
        setbtnDate(btnStartDate , txtStartDate)
        setbtnDate(btnEndDate , txtEndDate)
        setbtnTime( txtStartTime)
        setbtnTime( txtEndTime)



        txtDateFrom.text = SetCurrentDate()
        txtDateTo.text = SetCurrentDate()
        txtReqDate.text = SetCurrentDate()
        txtMaDate.text = SetCurrentDate()

        onLoadList( "lstGroup" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "1|||")
        onLoadList( "lstStatus" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "2|||")
        onLoadList( "AssetList" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "3|||")
        onLoadList( "empLst" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "4|||")
        onLoadList( "lstProcess" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "5|" + processType + "||")
        onLoadList( "lstReqType" ,  "LG_QC_MT_REQ_LST" , "$lang|$userid|" + "6|" + requestType_CodeGrp + "||")
        onLoadList( "dataGridView" ,  "LG_QC_MT_REQ_LAYOUT1" , "$lang|$userid|" + "1")
        onLoadList( "dataGrdTask" ,  "LG_QC_MT_REQ_LAYOUT1" , "$lang|$userid|" + "2")
        onLoadList( "dataGrdTool" ,  "LG_QC_MT_REQ_LAYOUT1" , "$lang|$userid|" + "3")


        /* autocomplete*/
        onLoadAutoCompleteTextView(txtReqAsset , assetLst)
        onLoadAutoCompleteTextView(txtReqEmp , empLst)
        onLoadAutoCompleteTextView(txtCharger , empLst)
        /* autocomplete*/

        btnCamera.setOnClickListener(View.OnClickListener {
            this.BaseActivity().openCamera(9999)
        })



        btnSearch.setOnClickListener(View.OnClickListener {
            onSearch_dataGridView()
        })

        btnNewRow.setOnClickListener(View.OnClickListener {
            v_ma_req_m_pk = ""
            onClearMaster()
        })

        btnSave.setOnClickListener(View.OnClickListener {

            if(v_ma_req_m_pk == "")
            {
                actionMaster = "INSERT"
            }
            else
            {
                actionMaster = "UPDATE"
            }

            onSave_Master(actionMaster)

            actionMaster = ""
            //onSearch_Master(v_ma_req_m_pk)

        })

        btnDelete.setOnClickListener(View.OnClickListener {

            actionMaster = ""
            if(v_ma_req_m_pk != "")
            {
                actionMaster = "DELETE"
            }
            if(actionMaster != "")
            {
                this.BaseActivity().confirmDialog("Warning","Do you want to delete this request no" , object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        if (bool){

                            onSave_Master(actionMaster)

                           //  dataGridView.removeRow(cur_pos)
                           // dataGridView.removeRow(cur_pos)
                            cur_pos = 0
                            actionMaster = ""
                            txtReqNo.setText("")


                            txtReqDate.text = SetCurrentDate()
                            txtReqAsset.setText("")
                            txtReqEmp.setText("")
                            txtDesc.setText("")
                            txtMaDate.text = SetCurrentDate()
                            txtCharger.setText("")
                            txtAmount.setText("")
                            txtDesc2.setText("")
                            txtStatus.text=""
                            listImage.clear()
                            dataGrdTask.clearAll()
                            dataGrdTool.clearAll()
                            txtAsset.clearFocus()
                            txtReqEmp.clearFocus()
                            txtCharger.clearFocus()
                            txtStartDate.text = ""
                            txtStartTime.text = ""
                            txtEndDate.text = ""
                            txtEndTime.text = ""



//                            actionMaster = ""
//                            v_ma_req_m_pk = ""
//                            dataGridView.removeRow(cur_pos)
//                            btnNewRow.callOnClick()
 //                           onClearMaster()
//                            cur_pos = 0
                        }
                    }
                })
            }
        })

        btnSubmit.setOnClickListener(View.OnClickListener {

            actionMaster = ""
            if(v_ma_req_m_pk != "")
            {
                actionMaster = "SUBMIT"
            }
            if(actionMaster != "")
            {
                this.BaseActivity().confirmDialog("Warning","Do you want to SUBMIT this request no" , object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        if (bool){
                            onSave_Master(actionMaster)
                            txtStatus.setText("SUBMIT")
                        }
                    }
                })
            }
        })


        lstProcess.listener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                onSearch_dataGrdTask(v_ma_req_m_pk,lstProcess.value())
                onSearch_dataGrdTool(v_ma_req_m_pk,lstProcess.value())
            }

        }
        listImage.customview = object : HorizontaBlockView.CustomView(){
            override fun getView(position: Int, viewgroup: View): View {

                var rootView = LayoutInflater.from(context).inflate(R.layout.block_image_item,null)

                var imageView = rootView.findViewById<ImageView>(R.id.imageView)
                imageView.maxWidth=400
                imageView.maxHeight=400
                var data = listImage.get(position)
                if (data!=null) {


                    if(!data.get(1).isNullOrEmpty() ) {
                        val imageBytes = Base64.decode(data.get(1), 0)
                        val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                        imageView.setImageBitmap(image)
                    }else{
                        var uri = Uri.parse(data.get(2))
                        imageView.setImageURI(uri)
                    }
                }
                return rootView
            }
        }
        listImage.handlerClickBlock = object : HorizontaBlockView.HandlerClickBlock( ) {
            override fun onSelectBlockIndex(position: Int, status: Boolean): Boolean {
                var data = listImage.get(position)

                if (data != null) {
                    if(data.get(2).startsWith("content://") )
                    {
                        var uri = Uri.parse(data.get(2))
                        showImageDialogURI(uri)
                    }
                    else
                    {
                        BaseActivity().confirmDialogPicMaster("Choise Option","Please choise option" , object :
                            BaseActivity.ConfirmDialogCallBack {
                            override fun callBack(bool: Boolean) {
                                if (!bool){

                                    val imageBytes =
                                        Base64.decode(data.get(2), 0)
                                    val image =
                                        BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                                        showImageDialog(image)
                                }
                                else
                                {
                                    onUpdatePic(PATHIMAGE , "P","LG_QC_MT_REQ_UPD_IMG"
                                        ,"DELETE"
                                        ,"P"
                                        ,v_ma_req_m_pk
                                        ,PATHIMAGE
                                        ,data.get(0)
                                        ,"$userid"
                                    )
                                }
                            }
                        })

                    }

                }
                return true
            }

            override fun onSelectBlockIndexed(position: Int, status: Boolean) {
                TODO("Not yet implemented")
            }
        }
        return root
    }

    fun onUpdatePic(pathImage: String , type :String , procedure :String
                    ,p_0    :String?
                    ,p_1    :String?
                    ,p_2    :String?
                    ,p_3    :String?
                    ,P_4    :String?
                    ,P_5    :String?
    ) {
        if (p_0 != "DELETE")
        {
            if (pathImage.startsWith("content://")){
                var uri = Uri.parse(pathImage.toString())

                var  image = MediaStore.Images.Media.getBitmap(BaseActivity().contentResolver
                    , uri)

                var img :ByteArray? = null;

                if(type == "F")
                {

                    var byteArrayOutputStream =  ByteArrayOutputStream()
                    image.compress(Bitmap.CompressFormat.PNG, 4, byteArrayOutputStream)
                    var byteArrayFull = byteArrayOutputStream .toByteArray()
                    img = byteArrayFull;
                }

                if(type == "P")
                {
                    var imageThumb = Bitmap.createScaledBitmap(image,250,250,false)
                    var byteArrayOutputStream2 =  ByteArrayOutputStream()
                    imageThumb.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream2)
                    var byteArrayThumb = byteArrayOutputStream2 .toByteArray()
                    img = byteArrayThumb
                }

                var key = Utils().dateyyyyMMddhhmmss + Random().nextInt(100).toString()

                var resultImag = BaseActivity().getWebService().UploadImageAndThumb(
                    procedure,
                    img ,
                    p_0 + "|" + p_1 + "|" + p_2 + "|" + p_3 + "|" + P_4 + "|" + P_5
                )
                if (resultImag.isError){
                    handler.post(Runnable {
                        BaseActivity().hideProgressBar()
                        Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
                    })
                }else{

                }
            }
        }
        else
        {
            Thread(Runnable {
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                    BaseActivity().showProgressBar(getString(R.string.loading))
                })
                var img :ByteArray? = null;
                var resultImag = BaseActivity().getWebService().UploadImageAndThumb(
                    procedure,
                    img ,
                    p_0 + "|" + p_1 + "|" + p_2 + "|" + p_3 + "|" + P_4 + "|" + P_5
                )
                if (resultImag.isError){
                    handler.post(Runnable {
                        BaseActivity().hideProgressBar()
                        Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
                    })
                }else{
                    btnSave.callOnClick()
                    btnSearch.callOnClick()
                }
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                })
            }).start()


        }


    }

    override fun onCameraResult(requestCode: Int, pathImage: String) {
        if (requestCode == 9999 ) {
//            setPic(pathImage)
            PATHIMAGE = pathImage


            var list = listOf<String> (
                "0",
                "",
                pathImage
            )
            listImage.add(list.toTypedArray())
        }
    }
    fun onClearMaster()
    {
        actionMaster = ""
        v_ma_req_m_pk = ""
        txtReqNo.setText("")
        txtReqDate.text = SetCurrentDate()
        txtReqAsset.setText("")
        txtReqEmp.setText("")
        txtDesc.setText("")
        txtMaDate.text = SetCurrentDate()
        txtCharger.setText("")
        txtAmount.setText("")
        txtDesc2.setText("")
        txtStatus.text=""
        listImage.clear()
        lstProcess.setSelectItem(0)
        dataGrdTask.clearAll()
        dataGrdTool.clearAll()
        txtAsset.clearFocus()
        txtReqEmp.clearFocus()
        txtCharger.clearFocus()
        txtStartDate.text = ""
        txtStartTime.text = ""
        txtEndDate.text = ""
        txtEndTime.text = ""

    }
    fun setbtnDate(btnId :Button , txtId :TextView)
    {
        btnId.setOnClickListener(View.OnClickListener {

            getDate(txtId)
        })
    }

    fun setbtnTime(txtId :TextView)
    {
        txtId.setOnClickListener(View.OnClickListener {

            getTime(txtId)
        })
    }

    fun getTime(txtId :TextView)
    {
        BaseActivity().showDateTimeDialog(callback = object :
            BaseActivity.callBackTimePickerDialog {
            override fun callBack(hourOfDay: Int, minute: Int) {
                var time: String  =  if (hourOfDay < 10 ){ "0" + (hourOfDay ) }else  {"" + (hourOfDay) }  + ":" +  if(minute < 9 ) { "0" + (minute + 1) }else { ""+ (minute +1)}
                txtId.text = time
            }
        })
    }

    fun onLoadAutoCompleteTextView(textView :AutoCompleteTextView , list :ArrayList<String>)
    {
        val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            android.R.layout.simple_list_item_1, list
        )
        textView.dropDownWidth=600
        textView.setAdapter(adapter)
        textView.setOnFocusChangeListener { v, hasFocus ->
            textView.showDropDown()
        }

//        textView.setOnItemClickListener(OnItemClickListener { arg0, arg1, arg2, arg3 ->
//            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//            imm?.hideSoftInputFromWindow(textView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
//        })

//        textView.onItemClickListener = AdapterView.OnItemClickListener{
//                parent,view,position,id->
//            val selectedItem = parent.getItemAtPosition(position).toString()
//            // Display the clicked item using toast
//            if(selectedItem.toString().split("*").size > 0)
//            {
//                textView.setText(selectedItem.toString().split("*")[0].toString())
//            }
//        }
    }
    fun getDate(txtId :TextView)
    {
                var date =  Utils().getStringToDate(txtDateFrom.text.toString().replace("/",""))
                val yearc = date.get(Calendar.YEAR)
                val monthc = date.get(Calendar.MONTH)
                val dayc = date.get(Calendar.DAY_OF_MONTH)
                BaseActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
                BaseActivity.callBackDatePickerDialog {
                override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                    var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
                    txtId.text = date
                }
            })
    }



    private fun setPic(currentPhotoPath :String) {
        // Get the dimensions of the View
//        val targetW: Int = image_view.width
//        val targetH: Int = image_view.height
        val targetW: Int = listImage.width
        val targetH: Int = listImage.height

        val bmOptions = BitmapFactory.Options().apply {
            // Get the dimensions of the bitmap
            inJustDecodeBounds = true

            val photoW: Int = outWidth
            val photoH: Int = outHeight

            // Determine how much to scale down the image
            val scaleFactor: Int = Math.min(photoW / targetW, photoH / targetH)

            // Decode the image file into a Bitmap sized to fill the View
            inJustDecodeBounds = false
            inSampleSize = scaleFactor
            inPurgeable = true
        }
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)?.also { bitmap ->
//            image_view.setImageBitmap(bitmap)
//            IMG_BITMAP = bitmap
        }
    }

    fun showImageDialog(bitmap: Bitmap){
        try {
            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


            val input = ImageView(context)

            input.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            input.setImageBitmap(bitmap)

            //input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)

            val content = LinearLayout(context)

            content.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            content.addView(input)
            dialog.setContentView(content)
            dialog.setCanceledOnTouchOutside(true)
            dialog.show()

        }catch (e: Exception){
            Log.d("Error " , e.message.toString())
        }
    }

    fun onSearch_dataGridView()
    {
        val v_option :String = "1"
        val v_date_from :String = ConvertYYYYMMDD(txtDateFrom.text.toString())
        val v_date_to :String = ConvertYYYYMMDD(txtDateTo.text.toString())
        val v_ma_process_pk :String = ""

        var param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk + "|" + processType
        onLoadData( "dataGridView_data" ,  "LG_QC_MT_REQ_SEL" , "$lang|$userid|" + param1)


    }

    fun onSearch_Master(p_ma_req_m_pk :String)
    {
        var v_option :String = "2"
        val v_date_from :String = ConvertYYYYMMDD(txtDateFrom.text.toString())
        val v_date_to :String = ConvertYYYYMMDD(txtDateTo.text.toString())
        v_ma_req_m_pk = p_ma_req_m_pk
        val v_ma_process_pk :String = ""

        var param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk+ "|" + processType
        onLoadList( "Master_data" ,  "LG_QC_MT_REQ_SEL" , "$lang|$userid|" + param1)

            v_option = "5"
            param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk+ "|" + processType
        onLoadList( "listImage" ,  "LG_QC_MT_REQ_SEL" , "$lang|$userid|"+ param1 )

    }

    fun onSearch_dataGrdTask(p_ma_req_m_pk :String , p_ma_process_pk :String )
    {

        val v_option :String = "3"
        val v_date_from :String = ConvertYYYYMMDD(txtDateFrom.text.toString())
        val v_date_to :String = ConvertYYYYMMDD(txtDateTo.text.toString())
        v_ma_req_m_pk  = p_ma_req_m_pk
        val v_ma_process_pk :String = p_ma_process_pk

        var param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk+ "|" + processType
        onLoadData( "dataGrdTask_data" ,  "LG_QC_MT_REQ_SEL" , "$lang|$userid|" + param1)
    }

    fun onSearch_dataGrdTool(p_ma_req_m_pk :String , p_ma_process_pk :String )
    {
        val v_option :String = "4"
        val v_date_from :String = ConvertYYYYMMDD(txtDateFrom.text.toString())
        val v_date_to :String = ConvertYYYYMMDD(txtDateTo.text.toString())
        v_ma_req_m_pk = p_ma_req_m_pk
        val v_ma_process_pk :String = p_ma_process_pk

        var param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk+ "|" + processType
        onLoadData( "dataGrdTool_data" ,  "LG_QC_MT_REQ_SEL" , "$lang|$userid|" + param1)

    }


    fun  ConvertYYYYMMDD(date :String) :String
    {
        if(date == " " || date.isEmpty())
        {
            return ""
        }
        var date_1 :String = date.toString().replace("/","")
        var result :String = date_1.substring(4,8) +  date_1.substring(2,4) + date_1.substring(0,2)
        return result
    }

    fun onGetHashMapkey(p_type :Int , p_hashmap :HashMap<String , String> , p_txtId :AutoCompleteTextView  ) :String
    {
        /*
         p_type = 0  : search theo ma code de lay key
         p_type = 1  : search toan bo ma theo textview de lay key
        * */
        for(key in p_hashmap.keys){
            //using hashMap.get() function to fetch the values
            when (p_type) {
               0 ->
               {
                   if( p_hashmap.get(key).toString().split("*")[0] ==  p_txtId.text.toString()  )
                   {
                       return  key.toString()
                   }
                   if( p_hashmap.get(key).toString() ==  p_txtId.text.toString()  )
                   {
                       return  key.toString()
                   }
               }
                1->
                {
                    if( p_hashmap.get(key).toString() ==  p_txtId.text.toString()  )
                    {
                        return  key.toString()
                    }
                    if( p_hashmap.get(key).toString().split("*")[0] ==  p_txtId.text.toString()  )
                    {
                        return  key.toString()
                    }
                }
            }
        }
        if(p_txtId.text.toString() != "" && p_txtId.text.toString() != " ")
        {
            return "-1"
        }
        return ""
    }
    fun onSave_Master(p_action :String)
    {
                Thread(Runnable {

            handler.post(Runnable {
                //BaseActivity().showProgressBar(getString(R.string.save) + " " + getString(R.string.and) + " " + getString(R.string.update_images))
                BaseActivity().showProgressBar(getString(R.string.save) + " " + getString(R.string.and) + " " + getString(R.string.update_images))
            })

                    var P_ACTION          :String = p_action
                    var P_MA_REQ_M_PK     :String =v_ma_req_m_pk
                    var P_ASSET_PK        :String =onGetHashMapkey(0 , AssetList , txtReqAsset  )
                    var P_ASSET_CODE      :String =""
                    var P_ASSET_NAME      :String =""
                    var P_STATUS          :String ="1"
                    var P_STATUSCODE      :String ="1"
                    var P_MA_PROCESS_PK   :String = lstProcess.value()
                    var P_MA_PROCESS_CODE :String =""
                    var P_MA_PROCESS_NAME :String =""
                    var P_REQ_EMP_PK      :String =onGetHashMapkey(0 , EmpList , txtReqEmp  )
                    var P_REQ_EMP_CODE    :String =""
                    var P_REQ_EMP_NAME    :String =""
                    var P_CHARGER_PK      :String =onGetHashMapkey(0 , EmpList , txtCharger  )
                    var P_CHARGER_CODE    :String =""
                    var P_CHARGER_NAME    :String =""
                    var P_REQDATE         :String =ConvertYYYYMMDD(txtReqDate.text.toString()).toString()
                    var P_REQ_TYPE        :String =lstReqType.value()
                    var P_REQ_DESC        :String =txtDesc.text.toString()
                    var P_REQ_NO          :String =txtReqNo.text.toString()
                    var P_MA_DATE         :String =ConvertYYYYMMDD(txtMaDate.text.toString()).toString()
                    var P_MA_AMOUNT       :String =txtAmount.text.toString()
                    var P_RESPONSE        :String =txtDesc2.text.toString()
                    var P_PROCESS_TYPE    :String = processType
                    var P_SOURCE_TYPE     :String = sourceType

                    var P_START_DATE      :String = ConvertYYYYMMDD(txtStartDate.text.toString()).toString()
                    var P_START_TIME      :String =txtStartTime.text.toString()
                    var P_END_DATE        :String =ConvertYYYYMMDD(txtEndDate.text.toString()).toString()
                    var P_END_TIME        :String =txtEndTime.text.toString()

                    var P_CRT_BY          :String ="$userid"


                    var param :String =P_ACTION          + "|" +
                            P_MA_REQ_M_PK     + "|" +
                            P_ASSET_PK        + "|" +
                            P_ASSET_CODE      + "|" +
                            P_ASSET_NAME      + "|" +
                            P_STATUS          + "|" +
                            P_STATUSCODE      + "|" +
                            P_MA_PROCESS_PK   + "|" +
                            P_MA_PROCESS_CODE + "|" +
                            P_MA_PROCESS_NAME + "|" +
                            P_REQ_EMP_PK      + "|" +
                            P_REQ_EMP_CODE    + "|" +
                            P_REQ_EMP_NAME    + "|" +
                            P_CHARGER_PK      + "|" +
                            P_CHARGER_CODE    + "|" +
                            P_CHARGER_NAME    + "|" +
                            P_REQDATE         + "|" +
                            P_REQ_TYPE        + "|" +
                            P_REQ_DESC        + "|" +
                            P_REQ_NO          + "|" +
                            P_MA_DATE         + "|" +
                            P_MA_AMOUNT       + "|" +
                            P_RESPONSE        + "|" +
                            P_PROCESS_TYPE        + "|" +
                            P_SOURCE_TYPE        + "|" +
                            P_START_DATE        + "|" +
                            P_START_TIME        + "|" +
                            P_END_DATE        + "|" +
                            P_END_TIME        + "|" +
                            P_CRT_BY

                    val resultmaster = BaseActivity().getWebService().GetDataTableArg(
                        "LG_QC_MT_REQ_UPD",param
                    )


                    if (resultmaster.isError){
                        handler.post(Runnable {
                            BaseActivity().hideProgressBar()
                            Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
                        })
//                        return@Runnable
                    }
                    else
                    {
                        BaseActivity().hideProgressBar()
                        v_ma_req_m_pk = resultmaster.data[0][0].toString()
                        if(v_ma_req_m_pk != "" && 1==2 )
                        {
                            if(P_ACTION != "SUBMIT" && P_ACTION != "DELETE" ) {
                                if (PATHIMAGE.startsWith("content://") && v_ma_req_m_pk != "0") {
                                    onUpdatePic(PATHIMAGE , "P","LG_QC_MT_REQ_UPD_IMG"
                                        ,"INSERT"
                                        ,"P"
                                        ,v_ma_req_m_pk
                                        ,PATHIMAGE
                                        ,""
                                        ,"$userid"
                                    )

                                    onUpdatePic(PATHIMAGE , "F","LG_QC_MT_REQ_UPD_IMG"
                                        ,"UPDATE"
                                        ,"F"
                                        ,v_ma_req_m_pk
                                        ,PATHIMAGE
                                        ,""
                                        ,"$userid")
                                }
                               onUpdTask()
                               onUpdTool()

                            }
                            PATHIMAGE = ""
                        }
                        if(P_ACTION != "DELETE"  )
                        {
                            onSearch_Master(v_ma_req_m_pk)
                            onSearch_dataGrdTool(v_ma_req_m_pk , lstProcess.value() )
                            onSearch_dataGrdTask(v_ma_req_m_pk , lstProcess.value() )
                        }
                        else
                        {

//                            dataGridView.removeRow(cur_pos)
//                            v_ma_req_m_pk = ""
//                           // dataGridView.removeRow(cur_pos)
//                            cur_pos = 0
//                            actionMaster = ""
//                            v_ma_req_m_pk = ""
//                            txtReqNo.setText("")
//

//                            txtReqDate.text = SetCurrentDate()
//                            txtReqAsset.setText("")
//                            txtReqEmp.setText("")
//                            txtDesc.setText("")
//                            txtMaDate.text = SetCurrentDate()
//                            txtCharger.setText("")
//                            txtAmount.setText("")
//                            txtDesc2.setText("")
//                            txtStatus.text=""
//                            listImage.clear()
//                            dataGrdTask.clearAll()
//                            dataGrdTool.clearAll()
//                            txtAsset.clearFocus()
//                            txtReqEmp.clearFocus()
//                            txtCharger.clearFocus()
//                            txtStartDate.text = ""
//                            txtStartTime.text = ""
//                            txtEndDate.text = ""
//                            txtEndTime.text = ""
                        }
                    }


            handler.post(Runnable {

                BaseActivity().hideProgressBar()
                    })

        }).start()
    }


    fun onUpdTask()
    {

        var P_ACTION         :String = ""
        var P_MA_REQ_TASK_PK :String = ""
        var P_MA_REQ_M_PK    :String = ""
        var P_SEQ            :String = ""
        var P_MA_TASK_PK     :String = ""
        var P_MA_PROCESS_PK  :String = ""
        var P_MA_TASK_TYPE   :String = ""
        var P_CRT_BY         :String = "$userid"
        var parameter :String = ""

        for (i:Int in 0..dataGrdTask.Rows().size-1){

            var lstrow = dataGrdTask.Rows()
            var row = lstrow.get(i).getCell()

            P_ACTION         = "UPDATE"
            if(row[G_TASK_PK].toString() == "0")
            {
                P_ACTION         = "INSERT"
            }
            else
            {
                if(row[G_TASK_P_DELETE].toString() == "1")
                {
                    P_ACTION         = "DELETE"
                }
            }

            P_MA_REQ_TASK_PK = row[G_TASK_PK].toString()
            P_MA_REQ_M_PK    = v_ma_req_m_pk
            P_SEQ            = row[G_TASK_SEQ].toString()
            P_MA_TASK_PK     = row[G_TASK_TLG_MA_TASK_PK].toString()
            P_MA_PROCESS_PK  = row[G_TASK_TLG_MA_PROCESS_PK].toString()
            P_MA_TASK_TYPE   = row[G_TASK_MA_TASK_TYPE].toString()
            P_CRT_BY         = "$userid"

            parameter = P_ACTION         + "|" +
                    P_MA_REQ_TASK_PK + "|" +
                    P_MA_REQ_M_PK    + "|" +
                    P_SEQ            + "|" +
                    P_MA_TASK_PK     + "|" +
                    P_MA_PROCESS_PK  + "|" +
                    P_MA_TASK_TYPE   + "|" +
                    P_CRT_BY
            val resultmaster = BaseActivity().getWebService().GetDataTableArg(
                "LG_QC_MT_REQ_UPD_2",parameter
            )

            if (resultmaster.isError){
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                    Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
                })
            }else{
                if(P_ACTION  == "DELETE")
                {
                    dataGrdTask.removeRow(i)
                }
            }


//            if (resultmaster.isError){
//                handler.post(Runnable {
//                    BaseActivity().hideProgressBar()
//                    Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
//                })
//            }
//            else
//            {
//                if(P_ACTION  == "DELETE")
//                {
//                    dataGrdTask.removeRow(i)
//                }
//            }
        }
    }


    fun onUpdTool()
    {

        var P_ACTION         :String = ""
        var P_MA_REQ_CONS_PK :String = ""
        var P_MA_REQ_M_PK    :String = ""
        var P_SEQ            :String = ""
        var P_PART_PK        :String = ""
        var P_QTY            :String = ""
        var P_MA_PROCESS_PK  :String = ""
        var P_CRT_BY         :String = "$userid"
        var parameter :String = ""

        for (i:Int in 0..dataGrdTool.Rows().size-1){

            var lstrow = dataGrdTool.Rows()
            var row = lstrow.get(i).getCell()

            P_ACTION         = "UPDATE"
            if(row[G_TOOL_PK].toString() == "0")
            {
                P_ACTION         = "INSERT"
            }
            else
            {
                if(row[G_TOOL_P_DELETE].toString() == "1")
                {
                    P_ACTION         = "DELETE"
                }
            }

            P_MA_REQ_CONS_PK =  row[G_TOOL_PK].toString()
            P_MA_REQ_M_PK    =  v_ma_req_m_pk
            P_SEQ            =  row[G_TOOL_SEQ].toString()
            P_PART_PK        =  row[G_TOOL_PART_PK].toString()
            P_QTY            =  row[G_TOOL_QTY].toString()
            P_MA_PROCESS_PK  =  row[G_TOOL_TLG_MA_PROCESS_PK].toString()
            P_CRT_BY         = "$userid"

            parameter = P_ACTION         	+ "|" +
                    P_MA_REQ_CONS_PK    + "|" +
                    P_MA_REQ_M_PK       + "|" +
                    P_SEQ               + "|" +
                    P_PART_PK           + "|" +
                    P_QTY               + "|" +
                    P_MA_PROCESS_PK     + "|" +
                    P_CRT_BY
            val resultmaster = BaseActivity().getWebService().GetDataTableArg(
                "LG_QC_MT_REQ_UPD_3",parameter
            )

            if(P_ACTION  == "DELETE")
            {
                dataGrdTool.removeRow(i)
            }

            if (resultmaster.isError){
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                    Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
                })
            }
            else
            {
            }
        }
    }

    private fun SetPreviousDate() : String {
        val car: Calendar = Calendar.getInstance()
        car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }

    private fun SetCurrentDate() : String {
        val car: Calendar = Calendar.getInstance()
        //car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }

    fun showImageDialogURI(uri: Uri){
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


        val input = ImageView(context)

        input.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        input.setImageURI(uri)

        // input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)


        val content = LinearLayout(context)

        content.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
//        if (input.width > input.height)
//            content.rotation = 90F

        content.addView(input)
        dialog.setContentView(content)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    fun showImageDialog(bitmap: Bitmap,pkMoldStop:String){
        try {
            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


            val input = ImageView(context)

            input.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            input.setImageBitmap(bitmap)

            //input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)

            val content = LinearLayout(context)

            content.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            content.addView(input)
            dialog.setContentView(content)
            dialog.setCanceledOnTouchOutside(true)
            dialog.show()

        }catch (e: Exception){
            Log.d("Error " , e.message.toString())
        }
    }


    fun onLoadData( id :String , procedure :String , param :String){

        Thread(Runnable {
            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.loading))
            })
            val result = BaseActivity().getWebService().GetDataTableArg(
                procedure,
                param
            )

            if (result.isError) {
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                    Toast.makeText(BaseActivity().baseContext, result.message, Toast.LENGTH_LONG).show()
                })
            } else {
                when (id) {
                    "dataGrdTask_data" -> {
                        dataGrdTask.setData(result.data)
                    }
                    "dataGrdTool_data" -> {
                        dataGrdTool.setData(result.data)
                    }
                    "dataGridView_data" ->
                    {
                        dataGridView.setData(result.data)
                    }
                }
            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun onLoadList( id :String , procedure :String , param :String) = Thread(Runnable {
        handler.post(Runnable {
            BaseActivity().hideProgressBar()
            BaseActivity().showProgressBar(getString(R.string.loading))
        })
        val result = BaseActivity().getWebService().GetDataTableArg(
            procedure,
            param
        )

        if (result.isError) {
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                Toast.makeText(BaseActivity().baseContext, result.message, Toast.LENGTH_LONG).show()
            })
        } else {
            handler.post(Runnable {
                when (id) {
                    "lstGroup" ->
                    {
                        lstGroup.setData(result.data, 1, 0)
                    }
                    "lstStatus" ->
                    {
                        lstStatus.setData(result.data, 1, 0)
                    }
                    "dataGridView" ->
                    {
                        dataGridView.EndInit()
                        dataGridView.clearColumn()
                        result.data.forEach {
                            var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
                            var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
                            var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
                            dataGridView.addColumn(it[0],size,type,align )
                        }
                        dataGridView.EndInit()
                    }
                    "AssetList" ->
                    {
                        result.data.forEach {
                            AssetList.put(it[0].toString() , it[1].toString())
                        }

                        for(key in AssetList.keys) {
                            assetLst.add(AssetList[key].toString())
                        }

                    }
                    "empLst" ->
                    {
                        result.data.forEach {
                            EmpList.put(it[0].toString() , it[1].toString())
                        }

                        for(key in EmpList.keys) {
                            empLst.add(EmpList[key].toString())
                        }

                    }

                    "lstProcess" ->
                    {
                        lstProcess.setData(result.data, 1, 0)
                    }
                    "lstReqType" ->
                    {
                        lstReqType.setData(result.data, 1, 0)
                    }
                    "dataGrdTask" ->
                    {
                        dataGrdTask.EndInit()
                        dataGrdTask.clearColumn()
                        result.data.forEach {
                            var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
                            var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
                            var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
                            dataGrdTask.addColumn(it[0],size,type,align )
                        }
                        dataGrdTask.EndInit()
                    }
                    "dataGrdTool" ->
                    {
                        dataGrdTool.EndInit()
                        dataGrdTool.clearColumn()
                        result.data.forEach {
                            var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
                            var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
                            var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
                            dataGrdTool.addColumn(it[0],size,type,align )
                        }
                        dataGrdTool.EndInit()
                    }
                    "Master_data" ->
                    {
                        result.data.forEach {
                            txtStatus.setText(it[G_M_STATUS].toString())
                            txtReqNo.setText(it[G_M_REQ_NO].toString())
                            txtReqDate.text = it[G_M_REQ_DATE].toString()

                            txtReqAsset.setText(it[G_M_ASSET_CODE].toString() )
                            txtReqEmp.setText(it[G_M_REQ_EMP_ID].toString())

                            lstProcess.setSelectItem(it[G_M_TLG_MA_PROCESS_PK].toInt())
                            lstReqType.setSelectItem(it[G_M_REQ_TYPE].toInt())

                            txtDesc.setText(it[G_M_REQ_DESC].toString())
                            txtMaDate.setText(it[G_M_MA_DATE].toString())
                            txtCharger.setText(it[G_M_CHARGER_ID].toString())

                            txtAmount.setText(it[G_M_MA_AMOUNT].toString())
                            txtDesc2.setText(it[G_M_RESPONSE].toString())

                            txtStartDate.text = it[G_M_START_DATE].toString()
                            txtStartTime.text = it[G_M_START_TIME].toString()
                            txtEndDate.text = it[G_M_END_DATE].toString()
                            txtEndTime.text = it[G_M_END_TIME].toString()


                        }
                    }
                    "listImage" -> {
                        listImage.setData(result.data)
                    }
                }
            })
        }
        handler.post(Runnable {
            BaseActivity().hideProgressBar()
        })
    }).start()

    fun onLoadMaster(p_master_pk :String )
    {

        var id :String =  "Master_data"
        var v_option :String = "2"
        val v_date_from :String = ConvertYYYYMMDD(txtDateFrom.text.toString())
        val v_date_to :String = ConvertYYYYMMDD(txtDateTo.text.toString())
        v_ma_req_m_pk = p_master_pk
        val v_ma_process_pk :String = ""

        var param1 =  v_option + "|" + lstGroup.value() + "|" + txtAsset.text + "|" + lstStatus.value() + "|" + txtReqNo_M.text  + "|" + v_date_from + "|" + v_date_to + "|" + v_ma_req_m_pk  + "|" + v_ma_process_pk+ "|" + processType
        val result = BaseActivity().getWebService().GetDataTableArg(
            "LG_QC_MT_REQ_SEL",
            "$lang|$userid|" + param1
        )

        if (result.isError) {
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                Toast.makeText(BaseActivity().baseContext, result.message, Toast.LENGTH_LONG).show()
            })
        } else {
            result.data.forEach {
                txtStatus.setText(it[G_M_STATUS].toString())
                txtReqNo.setText(it[G_M_REQ_NO].toString())
                txtReqDate.text = it[G_M_REQ_DATE].toString()
                txtReqAsset.setText(it[G_M_ASSET_CODE].toString() )
                txtReqEmp.setText(it[G_M_REQ_EMP_ID].toString())

                lstProcess.setSelectItem(it[G_M_TLG_MA_PROCESS_PK].toInt())
                lstReqType.setSelectItem(it[G_M_REQ_TYPE].toInt())

                txtDesc.setText(it[G_M_REQ_DESC].toString())
                txtMaDate.setText(it[G_M_MA_DATE].toString())
                txtCharger.setText(it[G_M_CHARGER_ID].toString())

                txtAmount.setText(it[G_M_MA_AMOUNT].toString())
                txtDesc2.setText(it[G_M_RESPONSE].toString())

                txtStartDate.text = it[G_M_START_DATE].toString()
                txtStartTime.text = it[G_M_START_TIME].toString()
                txtEndDate.text = it[G_M_END_DATE].toString()
                txtEndTime.text = it[G_M_END_TIME].toString()

            }
        }
    }
    fun View.hideKeyboard() {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }
}