package com.genuwin.mobile.gw

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.ContextCompat
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import java.io.ByteArrayOutputStream
import java.util.*
import kotlin.collections.ArrayList


class QCMoldStop() : BaseFragment()  {

    lateinit var txtDateFrom : TextView
    lateinit var txtDateTo : TextView
    lateinit var txtMold : EditText
    lateinit var lstTeam : Combobox
    lateinit var lstLine : Combobox
    lateinit var lstShift : Combobox
    lateinit var lstStopType : Combobox
    lateinit var btnSearch : Button
    lateinit var btnDateFrom : Button
    lateinit var btnDateTo : Button
    lateinit var btnSave : Button
    lateinit var dataGridView : DataGridView
    lateinit var dataGridView2 : DataGridView
    lateinit var btnOpenFile : Button



    var MOLD_STOP_PK :String = ""
    /*grid 2*/
    val    G2_STOP_NAME                         : Int = 0
    val    G2_DATE_IN                           : Int = 1
    val    G2_TIME_IN                        	: Int = 2
    val    G2_DATE_OUT                          : Int = 3
    val    G2_TIME_OUT                          : Int = 4
    val    G2_DATE_TARGET                       : Int = 5
    val    G2_TIME_TARGET                       : Int = 6
    val    G2_CAUSE                             : Int = 7
    val    G2_P_DELETE                          : Int = 8
    val    G2_PIC_IMAGE                         : Int = 9
    val    G2_TLG_PR_PROD_MOLD_STOP_PK          : Int = 10
    val    G2_STOP_CODE                         : Int = 11
    val    G2_TLG_MA_PROCESS_PK                 : Int = 12
    val    G2_PROCESS_ID                        : Int = 13
    val    G2_NONE_WORK_TYPE                    : Int = 14
    val    G2_TLG_MA_TASK_PK                    : Int = 15
    val    G2_TLG_PR_PROD_PLAN_PK               : Int = 16
    val    G2_IMG_FULL                          : Int = 17
    val    G2_SERVER_URL                        : Int = 18
    var cur_pos :Int = -1
    var cur_pos_g2 :Int = -1



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_qc_mold_stop,container,false)
        txtDateFrom = root.findViewById(R.id.txtDateFrom)
        txtDateTo = root.findViewById(R.id.txtDateTo)
        txtMold = root.findViewById(R.id.txtMold)
        lstTeam = root.findViewById(R.id.lstTeam)
        lstLine = root.findViewById(R.id.lstLine)
        lstShift = root.findViewById(R.id.lstShift)
        lstStopType = root.findViewById(R.id.lstStopType)
        btnSearch = root.findViewById(R.id.btnSearch)
        btnDateFrom = root.findViewById(R.id.btnDateFrom)
        btnDateTo = root.findViewById(R.id.btnDateTo)
        btnSave = root.findViewById(R.id.btnSave)
        dataGridView = root.findViewById(R.id.dataGridView)
        dataGridView2 = root.findViewById(R.id.dataGridView2)
        txtDateFrom.text = SetPreviousDate()
        txtDateTo.text = SetCurrentDate()
        btnOpenFile = root.findViewById(R.id.btnOpenFile)


        dataGridView.widthWithpercent = true
        dataGridView.mutilSelect=false /*chi highligh dong dang chon*/
        dataGridView.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
        )
        dataGridView.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {

                if(col == 5){
                    doMappingMold(position,col,dataGridView.Columns()[col].text.toString())
                }else {
                    cur_pos = position
                    doLoadData2(position)
                }

                return true
            }
        }


        dataGridView2.widthWithpercent = true
        dataGridView2.mutilSelect=false /*chi highligh dong dang chon*/
        dataGridView2.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(70  , requireContext())
        )




        dataGridView2.itemClickListener = object : DataGridView.HandlerItemClick() {

            override fun onClickCell(position: Int, col: Int): Boolean {
                cur_pos_g2 = position
                if(col == G2_DATE_IN || col == G2_DATE_OUT || col == G2_DATE_TARGET) {
                    var date =  Utils().getStringToDate(txtDateFrom.text.toString().replace("/",""))
                    val yearc = date.get(Calendar.YEAR)
                    val monthc = date.get(Calendar.MONTH)
                    val dayc = date.get(Calendar.DAY_OF_MONTH)

                    BaseActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
                        BaseActivity.callBackDatePickerDialog {
                        override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                            var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
                            dataGridView2.setDataCell(col ,position, date )
                        }
                    })
                }else if (col == G2_TIME_IN || col == G2_TIME_OUT || col == G2_TIME_TARGET){
                    BaseActivity().showDateTimeDialog(callback = object :
                        BaseActivity.callBackTimePickerDialog {
                        override fun callBack(hourOfDay: Int, minute: Int) {
                            var time: String  =  if (hourOfDay < 10 ){ "0" + (hourOfDay ) }else  {"" + (hourOfDay) }  + ":" +  if(minute < 9 ) { "0" + (minute + 1) }else { ""+ (minute +1)}
                            dataGridView2.setDataCell(col ,position, time )
                        }
                    })
                }
                else if (col == G2_P_DELETE){
                    dataGridView2.deleteRow(position , dataGridView2.getDataCell(position,col))
                }
                else if (col == G2_PIC_IMAGE){

                    if(dataGridView2.getDataCell(position,G2_PIC_IMAGE).length < 10 )
                    {
                        BaseActivity().openCamera(position)
                    }
                    else
                    {

                        BaseActivity().confirmDialogPic("Choise Option","Please choise option" , object :
                            BaseActivity.ConfirmDialogCallBack {
                            override fun callBack(bool: Boolean) {
                                if (!bool){
                                    if(dataGridView2.getDataCell(position,G2_PIC_IMAGE).startsWith("content://") )
                                    {
                                        showImageDialogURI(dataGridView2.getDataCell(position,G2_PIC_IMAGE))
                                    }
                                    else
                                    {
                                        val imageBytes =
                                            Base64.decode(dataGridView2.getDataCell(position,G2_PIC_IMAGE), 0)
                                        val image =
                                            BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
                                        showImageDialog(image,dataGridView2.getDataCell(position,G2_IMG_FULL).toString())
                                    }

                                }
                                else
                                {
                                    BaseActivity().openCamera(position)
                                }
                            }
                        })
                    }

                }
                return true
            }
        }



        //dataGridView2.row_backgroud_select =

        btnSearch.setOnClickListener(View.OnClickListener {
            onSearch()
        })

        btnOpenFile.setOnClickListener(View.OnClickListener {

            if(cur_pos_g2 != -1 )
            {


                var server_url = dataGridView2.getDataCell(cur_pos_g2,G2_SERVER_URL)

                if(!server_url.equals(' '))
                {

                    val i = Intent(Intent.ACTION_VIEW, Uri.parse(BaseActivity().getLinkService() + "/"  + server_url))
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    i.setPackage("com.android.chrome")
                    try {
                        context!!.startActivity(i)
                    } catch (e: ActivityNotFoundException) {
                        Toast.makeText(context, "unable to open chrome", Toast.LENGTH_SHORT).show()
                        i.setPackage(null)
                        context!!.startActivity(i)
                    }
                }

            }


        })

        btnDateFrom.setOnClickListener(View.OnClickListener {

            var date =  Utils().getStringToDate(txtDateFrom.text.toString().replace("/",""))
            val yearc = date.get(Calendar.YEAR)
            val monthc = date.get(Calendar.MONTH)
            val dayc = date.get(Calendar.DAY_OF_MONTH)

            BaseActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
                BaseActivity.callBackDatePickerDialog {
                override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                    var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
                    txtDateFrom.text = date
                }
            })
        })

        btnDateTo.setOnClickListener(View.OnClickListener {
            var date =  Utils().getStringToDate(txtDateTo.text.toString().replace("/",""))
            val yearc = date.get(Calendar.YEAR)
            val monthc = date.get(Calendar.MONTH)
            val dayc = date.get(Calendar.DAY_OF_MONTH)

            BaseActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
                BaseActivity.callBackDatePickerDialog {
                override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
                    var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
                    txtDateTo.text = date
                }
            })
        })




        btnSave.setOnClickListener(View.OnClickListener {
            doInsertTask()
        })

        lstTeam.listener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                doLoadLine()
            }

        }

        doLoadFirst()

        return root
    }


    fun  onSearch() = Thread(Runnable {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })
        val result = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_ms0070",
            "$lang|$userid|"
                    + lstTeam.value()
                    + "|" +lstLine.value()
                    + "|" + Utils().getStringddMMyyyyToyyyyMMdd(txtDateFrom.text.toString().replace("/",""))
                    + "|" +  Utils().getStringddMMyyyyToyyyyMMdd(txtDateTo.text.toString().replace("/",""))
                    + "|"+ lstShift.value()
                    +  "|" + lstStopType.value()
                    + "|"+ txtMold.text
        )

        if(result.isError){

            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                dataGridView.setData(result.data)
            })
        }





        handler.post(Runnable {
            BaseActivity().hideProgressBar()
        })

    }).start()

    fun doMappingMold(pos: Int,col: Int, text: String) = Thread(Runnable {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })



        try{
            var p_plan_dt =  dataGridView.getDataCell(pos,8)
            var p_line_pk =  dataGridView.getDataCell(pos,9)
            var p_shift =  dataGridView.getDataCell(pos,2)
            var p_item_pk =  dataGridView.getDataCell(pos,10)
            var p_mold_pk =  dataGridView.getDataCell(pos,11)


            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_ms0070_mold_list",
                "$lang|$userid|" + p_item_pk
            )

            if(result.isError){
                handler.post(Runnable {
                    Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
                    BaseActivity().hideProgressBar()
                })

            }else{
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()

                })
                val list: MutableList<String> = ArrayList()

                result.data.forEachIndexed { index, strings ->
                    //for(z  in 0 until 1000){
                    list.add (strings[1])
                    // }

                }

                handler.post(Runnable {
                    BaseActivity().listDialogingleChoic( text,list.toTypedArray(),  object :
                        BaseActivity.ListDialogCallBack {
                        override fun callBack(it: Int) {
                            mappingMold(pos,col,p_line_pk,result.data[it][0])

                            // doLoadMachine( )
                        }
                    })


                })



            }
        }catch (ex: Exception){
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }


    }).start()



    override fun onCameraResult(requestCode: Int, pathImage: String) {
        // super.onCameraResult(requestCode, pathImage)
        Thread(Runnable {

            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.save) + " " + getString(R.string.and) + " " + getString(R.string.update_images))
            })



            dataGridView2.setDataCell(G2_PIC_IMAGE,requestCode,pathImage)
            btnSave.callOnClick()
//        dataGridView2.setDataCell(G2_PIC_IMAGE,requestCode,pathImage)

            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()


    }



    fun  mappingMold(pos: Int,col: Int,p_pk:String , mold_pk:String)= Thread(Runnable {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })


        val result = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_ms0070_mapping_mold",
            "$p_pk|$mold_pk|$userid"
        )

        if(result.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
                BaseActivity().hideProgressBar()
            })

        }else{
            handler.post(Runnable {
                dataGridView.setDataCell(col,pos,mold_pk)
            })

        }



        handler.post(Runnable {
            BaseActivity().hideProgressBar()
        })

    }).start()


    fun doLoadData2(pos:Int)= Thread(Runnable {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        try{
            var p_wi_plan_pk = dataGridView.getDataCell(pos,0)
            var p_process_id = lstStopType.value()

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_ms0070_3",
                "$lang|$userid|" + p_wi_plan_pk + "|" + p_process_id
            )

            if(result.isError){
                handler.post(Runnable {
                    Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
                })
            }else{

                handler.post(Runnable {
                    dataGridView2.setData(result.data)
                })
            }
        }catch (ex: Exception){

        }
        handler.post(Runnable {
            BaseActivity().hideProgressBar()
        })

    }).start()

    fun doLoadLine() =  Thread(Runnable {
        val resultLine = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_ms0070_line",
            "$lang|$userid|"+ lstTeam.value()
        )

        if(resultLine.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultLine.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstLine.setData(resultLine.data,1,0)
            })
        }
    }).start()

    fun doLoadFirst() = Thread(Runnable {



        val result = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_ms0070_line_layout",
            "$lang|$userid"
        )

        if(result.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstTeam.setData(result.data,1,0)
            })
        }


        val resultShift  = BaseActivity().getWebService().GetDataTableArg("lg_mposv2_ms0070_shift","$lang|$userid")

        if(resultShift.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultShift.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstShift.setData(resultShift.data,1,0)
            })
        }

        val resultModeType  = BaseActivity().getWebService().GetDataTableArg("lg_mposv2_ms0070_stop_type","$lang|$userid")

        if(resultModeType.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultModeType.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstStopType.setData(resultModeType.data,1,0)
            })
        }


        val resultLayour1  = BaseActivity().getWebService().GetDataTableArg("lg_mposv2_ms0070_layout1","$lang|$userid")

        if(resultLayour1.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultLayour1.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {

                dataGridView.EndInit()
                dataGridView.clearColumn()
                resultLayour1.data.forEach {
                    var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
                    var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
                    var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
                    dataGridView.addColumn(it[0],size,type,align )
                }

                dataGridView.EndInit()


            })
        }

        val resultLayour2  = BaseActivity().getWebService().GetDataTableArg("lg_mposv2_ms0070_layout2","$lang|$userid")

        if(resultLayour2.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultLayour2.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {

                dataGridView2.EndInit()
                dataGridView2.clearColumn()
                resultLayour2.data.forEach {
                    var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
                    var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
                    var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
                    dataGridView2.addColumn(it[0],size,type,align )
                }

                dataGridView2.EndInit()

            })
        }

    }).start()


    private fun SetPreviousDate() : String {
        val car: Calendar = Calendar.getInstance()
        car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }

    private fun SetCurrentDate() : String {
        val car: Calendar = Calendar.getInstance()
        //car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }
    fun showImageDialogURI(contentURL: String ){
        var uri = Uri.parse(contentURL)
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


        val input = ImageView(context)

        input.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        input.setImageURI(uri)

        val content = LinearLayout(context)

        content.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        content.addView(input)
        dialog.setContentView(content)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }

    fun OnInsertPic(p_mold_stop_pk :String , p_url :String , p_img_pk :String)
    {
        if(p_url.startsWith("content://") )
        {
            onUpdatePic2(p_url.toString() , "F" ,  "LG_UPD_mposv2_ms0070_4"
                ,"UPDATE"
                ,"F"
                ,p_mold_stop_pk
                ,p_img_pk
                ,""
                ,"$userid"
            )
            onUpdatePic2(p_url.toString() , "P" ,  "LG_UPD_mposv2_ms0070_4"
                ,"UPDATE"
                ,"P"
                ,p_mold_stop_pk
                ,p_img_pk
                ,""
                ,"$userid"
            )
        }


    }


    fun doInsertTask(){

        Thread(Runnable {

            handler.post(Runnable {
//                BaseActivity().showProgressBar(getString(R.string.save) + " " + getString(R.string.and) + " " + getString(R.string.update_images))
                BaseActivity().showProgressBar(getString(R.string.save) + " " + getString(R.string.and) + " " + getString(R.string.update_images))
            })


            var userid = BaseActivity().getSharedPreferences().getString("login_user", "")

            var data :String
            data =  dataGridView2.getDataDLL(userid.toString())
            var parameter :String = ""
            var array_param :Int = 0;
            val schar = arrayOf("|*!!*","*!!*")
            val col_img = 10 // dinh nghia cot chua img content
            val col_img_full = 18 // dinh nghia cot chua img content


            var v_mold_stop_pk :String = "0"
            var imgPath :String = ""
            var param :String = ""
            var v_img_pk :String = ""

            if(data.length > 0)
            {
                array_param = data.split(schar[0].toString() , schar[1].toString()).size - 1

                for( i in 0 .. array_param )
                {

                    parameter = data.split(schar[0].toString() , schar[1].toString())[i]

                    if(parameter != "")
                    {

                        param = ""
                        for(j:Int in 0 ..parameter.split("|").size -1 )
                        {

                            if(j == col_img )
                            {
                                imgPath = parameter.split("|")[j].toString()
                            }

                            if( j == col_img || j == col_img_full )
                            {
                                if( param == "")
                                {
                                    param = "0"
                                }
                                else
                                {
                                    param = param + "|" + "0"
                                }
                            }
                            else
                            {
                                if( param == "")
                                {
                                    param = parameter.split("|")[j]
                                }
                                else
                                {
                                    param = param + "|" + parameter.split("|")[j]
                                }

                            }
                            if(j == G2_IMG_FULL + 1)
                            {
                                v_img_pk =  parameter.split("|")[j]
                            }

                        }


                        val resultmaster = BaseActivity().getWebService().GetDataTableArg(
                            "LG_UPD_mposv2_ms0070_3",param
                        )
                        if (resultmaster.isError){
                            handler.post(Runnable {
                                BaseActivity().hideProgressBar()
                                Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
                            })
                        }else{

                            MOLD_STOP_PK = resultmaster.data[0][0]
                            Log.d("LTNL " , MOLD_STOP_PK)

                            if(imgPath.startsWith("content://") && MOLD_STOP_PK != "0" )
                            {
                                OnInsertPic(MOLD_STOP_PK , imgPath , v_img_pk)
                            }
                            doLoadDataGridTask(cur_pos)
                        }
                    }
                }

            }


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun onUpdatePic2(pathImage: String , type :String , procedure :String
                     ,p_0    :String?
                     ,p_1    :String?
                     ,p_2    :String?
                     ,p_3    :String?
                     ,P_4    :String?
                     ,P_5    :String?
    ){
        if (pathImage.startsWith("content://")){
            var uri = Uri.parse(pathImage.toString())

            var  image = MediaStore.Images.Media.getBitmap(BaseActivity().contentResolver
                , uri)

            var img :ByteArray? = null;

            if(type == "F")
            {
                var byteArrayOutputStream =  ByteArrayOutputStream()
                image.compress(Bitmap.CompressFormat.PNG, 4, byteArrayOutputStream)
                var byteArrayFull = byteArrayOutputStream .toByteArray()
                img = byteArrayFull;

            }

            if(type == "P")
            {
                var imageThumb = Bitmap.createScaledBitmap(image,100,100,false)
                var byteArrayOutputStream2 =  ByteArrayOutputStream()
                imageThumb.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream2)
                var byteArrayThumb = byteArrayOutputStream2 .toByteArray()
                img = byteArrayThumb;
            }




            var key = Utils().dateyyyyMMddhhmmss + Random().nextInt(100).toString()

            var resultImag = BaseActivity().getWebService().UploadImageAndThumb(
                procedure,
                img ,
                p_0 + "|" + p_1 + "|" + p_2 + "|" + p_3 + "|" + P_4 + "|" + P_5
            )

            if (resultImag.isError){
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                    Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
                })
            }else{

            }

//            if (resultImag.isError) {

                //Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()

               // BaseActivity().hideProgressBar()
               // Toast.makeText(BaseActivity().baseContext,resultImag.message, Toast.LENGTH_LONG).show()
//            }

        }
    }

    fun showImageDialog(bitmap: Bitmap,pkImagePK:String){
        try {
            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)


            val input = ImageView(context)

            input.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )
            //input.setImageBitmap(bitmap)
            doLOadFullSizeImage(input,pkImagePK)

            //input.minimumHeight =  if(metrics.widthPixels > metrics.heightPixels  ) (metrics.heightPixels - 200 )  else (metrics.heightPixels - 200)

            val content = LinearLayout(context)

            content.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )

            content.addView(input)
            dialog.setContentView(content)
            dialog.setCanceledOnTouchOutside(true)
            dialog.show()



        }catch (e: Exception){
            Log.d("Error " , e.message.toString())
        }
    }

    fun doLoadDataGridTask(pos:Int){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })
        var p_wi_plan_pk = dataGridView.getDataCell(pos,0)
        var p_process_id = lstStopType.value()

        val result = BaseActivity().getWebService().GetDataTableArg(
            "lg_mposv2_ms0070_3",
            "$lang|$userid|" + p_wi_plan_pk + "|" + p_process_id
        )

        if(result.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,result.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                dataGridView2.setData(result.data)
            })
        }
    }

    fun doLOadFullSizeImage(imageView: ImageView,pkImage:String) = Thread(Runnable {

        val resultmaster = BaseActivity().getWebService().GetDataTableArg(
            "LG_SEL_MS0070_3_IMG_FULL",
            pkImage
        )


        if (resultmaster.isError){
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
                Toast.makeText(BaseActivity().baseContext,resultmaster.message, Toast.LENGTH_LONG).show()
            })

        }else{
            try {
                val imageBytes =
                    Base64.decode(resultmaster.data[0][1], 0)
                val image =
                    BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                handler.post(Runnable {
                    imageView.setImageBitmap(image)
                })
            }catch (e: java.lang.Exception){}
            //TODO
        }


    }).start()
    fun View.hideKeyboard() {
        val inputManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }
}