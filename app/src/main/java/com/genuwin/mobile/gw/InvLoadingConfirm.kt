package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView


class InvLoadingConfirm : BaseFragment() {

    private val TYPE_CLEAR = 1
    private val TYPE_SELECT = 2
    private val TYPE_LOAD = 3
    private val TYPE_MAKESLIP = 4
    private val TYPE_APPROVE = 5

    lateinit var lrl1: LinearLayout
    lateinit var lrl2: LinearLayout


    lateinit var btn_clear: Button
    lateinit var btnSelectAll: Button
    lateinit var btnLoad: Button
    lateinit var btnMakeSlip: Button
    lateinit var btnApprove: Button

    lateinit var edit_Bc1: EditText
    lateinit var edit_reqNo: EditText
    lateinit var edit_reqQty: EditText
    lateinit var edit_reqBal: EditText


    lateinit var txtError: TextView
    lateinit var txtTotalBot: TextView
    lateinit var txtSlipNo: TextView

    lateinit var dataGridViewPre: DataGridView
    lateinit var dataGridViewData: DataGridView

    private var reqPK = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(
            R.layout.fragment_inv_loading_confirm,
            container,
            false
        ) as LinearLayout
        lrl1 = rootView.findViewById(R.id.lrl1) as LinearLayout
        lrl2 = rootView.findViewById(R.id.lrl2) as LinearLayout


        dataGridViewPre = rootView.findViewById(R.id.dataGridViewPre)
        dataGridViewPre.isSelectedEnabled = true

        dataGridViewPre.BeginInit()
        dataGridViewPre.addColumn("PK", 0)
        dataGridViewPre.addColumn("Seq", 40)
        dataGridViewPre.addColumn("BARCODE", 120)
        dataGridViewPre.addColumn("Ref No", 200)
        dataGridViewPre.addColumn("Item Code", 200)
        dataGridViewPre.addColumn("Item Name", 200)
        dataGridViewPre.addColumn("Req Qty", 70)
        dataGridViewPre.addColumn("Req UOM", 70)
        dataGridViewPre.addColumn("Qty", 70)
        dataGridViewPre.addColumn("UOM", 70)
        dataGridViewPre.addColumn("Ref 1 Qty", 80)
        dataGridViewPre.addColumn("Ref 1 UOM", 80)
        dataGridViewPre.addColumn("Ref 2 Qty", 80)
        dataGridViewPre.addColumn("Ref 2 UOM", 80)
        dataGridViewPre.addColumn("Lot No", 70)
        dataGridViewPre.addColumn("PO NO", 200)
        dataGridViewPre.addColumn("COLOR", 200)

        dataGridViewPre.addColumn("WH", 300)
        dataGridViewPre.addColumn("LOC", 100)


        dataGridViewPre.EndInit()

        dataGridViewData = rootView.findViewById(R.id.dataGridView2)
        dataGridViewData.isSelectedEnabled = false

        dataGridViewData.BeginInit()
        dataGridViewData.addColumn("PK", 0)
        dataGridViewData.addColumn("Seq", 40)
        dataGridViewData.addColumn("BARCODE", 120)
        dataGridViewData.addColumn("Ref No", 200)
        dataGridViewData.addColumn("Item Code", 200)
        dataGridViewData.addColumn("Item Name", 200)
        dataGridViewData.addColumn("Req Qty", 70)
        dataGridViewData.addColumn("Req UOM", 70)
        dataGridViewData.addColumn("Qty", 70)
        dataGridViewData.addColumn("UOM", 70)
        dataGridViewData.addColumn("Ref 1 Qty", 80)
        dataGridViewData.addColumn("Ref 1 UOM", 80)
        dataGridViewData.addColumn("Ref 2 Qty", 80)
        dataGridViewData.addColumn("Ref 2 UOM", 80)
        dataGridViewData.addColumn("Lot No", 70)
        dataGridViewData.addColumn("PO NO", 200)
        dataGridViewData.addColumn("COLOR", 200)

        dataGridViewData.addColumn("WH", 300)
        dataGridViewData.addColumn("LOC", 100)


        dataGridViewData.EndInit()




        txtError = rootView.findViewById(R.id.txtError) as TextView
        txtTotalBot = rootView.findViewById(R.id.txtTotalBot) as TextView
        txtSlipNo = rootView.findViewById(R.id.txtSlipNo) as TextView


        edit_Bc1 = rootView.findViewById(R.id.edit_Bc1) as EditText
        edit_reqNo = rootView.findViewById(R.id.edit_reqNo) as EditText
        edit_reqQty = rootView.findViewById(R.id.edit_reqQty) as EditText
        edit_reqBal = rootView.findViewById(R.id.edit_reqBal) as EditText

        btn_clear = rootView.findViewById(R.id.btn_clear) as Button
        btnSelectAll = rootView.findViewById(R.id.btnSelectAll) as Button
        btnLoad = rootView.findViewById(R.id.btnLoad) as Button
        btnMakeSlip = rootView.findViewById(R.id.btnMakeSlip) as Button
        btnApprove = rootView.findViewById(R.id.btnApprove) as Button


        btn_clear.setOnClickListener(View.OnClickListener {
            //val title = "Confirm clear..."
            //val mess = " Are you sure you want to Clear All? "
            alertDialogYN(getString(R.string.confirm), getString(R.string.clear), TYPE_CLEAR)
        })

        btnSelectAll.setOnClickListener(View.OnClickListener {
            // val title = "Select All"
            // val mess = " Are you sure you want to Select All? "
            alertDialogYN(getString(R.string.confirm), getString(R.string.select_all), TYPE_SELECT)
        })

        btnLoad.setOnClickListener(View.OnClickListener {
            // val title = "Select All"
            // val mess = " Are you sure you want to Select All? "
            alertDialogYN(getString(R.string.confirm), getString(R.string.loading), TYPE_LOAD)
        })


        btnMakeSlip.setOnClickListener(View.OnClickListener {

            alertDialogYN(getString(R.string.confirm), btnMakeSlip.text.toString(), TYPE_MAKESLIP)
        })

        edit_Bc1.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    if (edit_Bc1.text.toString().trim { it <= ' ' } == "") {
                        txtError.text = "Pls Scan barcode!"
                        edit_Bc1.requestFocus()
                    } else {
                        OnSaveData(edit_Bc1.text.toString().trim { it <= ' ' })
                        edit_Bc1.text.clear()
                        edit_Bc1.requestFocus()
                    }
                }
                return@OnKeyListener true //important for event onKeyDown
            }
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                edit_Bc1.text.clear()
                edit_Bc1.requestFocus()
            }
            false
        })

        onClear()
        return rootView
    }

    fun alertDialogYN(title: String, msg: String, type: Int) {
        BaseActivity().confirmDialog(title, msg, object :
            BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
                callbackDialogYN(type)

            }
        })
    }

    private fun callbackDialogYN(type: Int) {
        when (type) {
            TYPE_CLEAR -> onClear()
            TYPE_SELECT -> onSelectAll()
            TYPE_LOAD -> onLoading()
            TYPE_MAKESLIP -> onMakeSlip()
        }
    }

    fun onClear() {
        reqPK = ""
        lrl1.visibility = View.VISIBLE
        lrl2.visibility = View.GONE

        txtError.text = ""

        edit_Bc1.setText("")
        edit_reqNo.setText("")
        edit_reqQty.setText("")
        edit_reqBal.setText("")


        btn_clear.isEnabled = true
        btnSelectAll.isEnabled = false
        btnLoad.isEnabled = false
        btnMakeSlip.isEnabled = false
        btnApprove.isEnabled = false

        dataGridViewPre.clearAll()
        dataGridViewData.clearAll()
    }

    fun onSelectAll() {
        dataGridViewPre.selectAll()
    }

    fun onLoading() {
        dataGridViewData.clearAll()
        dataGridViewPre.Rows().forEach { it ->
            if (it.isSelectMode == true) {
                dataGridViewData.addRow(it)
            }

        }

        dataGridViewData.Rows().forEachIndexed { index, it ->
            dataGridViewData.Rows().get(index).getCell()[1].value = (index + 1).toString()
        }
        handler.post(Runnable {
            btnSelectAll.isEnabled = false
            btnLoad.isEnabled = false
            btnMakeSlip.isEnabled = true
            lrl2.visibility = View.VISIBLE
            lrl1.visibility = View.GONE
        })

        doLoadInfoRequest()
    }




    fun OnSaveData(bc: String) {
        val str_scan = bc.toUpperCase()
        val l_length: Int = bc.length
        val str_req_no = bc.substring(1, l_length)

        if (reqPK === "") {
            if (str_scan.indexOf("S") == 0) {

                getSlipNoInfo(str_req_no)


            } else {
                onClear()
                txtError.text = "Slip No '$str_req_no' not exist !!!"
            }
        } else {
            var flag = false
            dataGridViewPre.Rows().forEachIndexed { index, it ->
                if (it.getCell().get(2).value == str_scan) {
                    dataGridViewPre.selectRow(index, true)
                    flag = true
                    return@forEachIndexed
                }
            }

            if (!flag)
                txtError.text = "Pls scan REQ CODE!!!"
        }
    }

    fun getSlipNoInfo(slipNo: String) {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_get_gd_loading_req",
                slipNo
            )

            if (result.isError) {
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })
            } else {
                reqPK = result.data[0][0] // TLG_GD_REQ_M_PK
                handler.post(Runnable {

                    edit_reqNo.setText(result.data[0][1])
                    edit_reqBal.setText(result.data[0][4])
                    edit_reqQty.setText(result.data[0][2])
                })


                val result2 = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_get_gd_loading_req_d",
                    reqPK
                )

                if (result2.isError) {
                    handler.post(Runnable {
                        BaseActivity().showToast(result2.message)
                    })
                } else {


                    handler.post(Runnable {
                        dataGridViewPre.setData(result2.data)
                        btnSelectAll.isEnabled = true
                        btnLoad.isEnabled = true
                    })


                }


            }


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()


    }

    fun doLoadInfoRequest() {
//        handler.post(Runnable {
//            BaseActivity().showProgressBar(getString(R.string.loading))
//        })
//
//        Thread(Runnable {
//            Thread.sleep(1000)
//
//
////            val result = BaseActivity().getWebService().GetDataTableArg(
////                "lg_mposv2_get_gd_loading_makeslip",
////                reqPK
////            )
////
////            if (result.isError) {
////                BaseActivity().showToast(result.message)
////            } else {
////
////
////                handler.post(Runnable {
////
////                    txtSlipNo.setText(result.data[0][0])
////                })
////
////
////            }
//
//
//            handler.post(Runnable {
//                BaseActivity().hideProgressBar()
//            })
//        }).start()
    }

    fun onMakeSlip() {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            var pkList = ""
            var key = ""
            dataGridViewData.Rows().forEachIndexed { index, it ->
                pkList = pkList + key + it.getCell().get(0)
                key = ","
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_loading_makeslip",
                reqPK + "|" + pkList + "|" + userid
            )

            if (result.isError) {
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            } else {


                handler.post(Runnable {

                    txtSlipNo.setText(result.data[0][0])
                    btnMakeSlip.isEnabled = false
                })


            }


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

}