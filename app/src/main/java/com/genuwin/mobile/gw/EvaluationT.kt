package com.genuwin.mobile.gw

import android.database.Cursor
import android.os.Bundle
import android.os.SystemClock
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView
import kotlinx.android.synthetic.main.fragment_inv_eva_cj.*


// this support eva CJ.....
class EvaluationT : BaseFragment() {

    lateinit var txtNewBin: TextView
    lateinit var txtBC: TextView
    lateinit var txtStatus: TextView
    lateinit var txtStatusSys: TextView
    lateinit var btnClear: Button
    lateinit var btnAll: Button
    lateinit var btnNotexist: Button

    lateinit var dgvBCScan: DataGridView
    lateinit var dgvBCHis: DataGridView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var root = inflater.inflate(R.layout.fragment_inv_eva_cj, container, false) as LinearLayout
        txtNewBin = root.findViewById(R.id.txtNewBin)
        txtBC = root.findViewById(R.id.txtBC)
        txtStatus = root.findViewById(R.id.txtStatus)
        txtStatusSys = root.findViewById(R.id.txtStatusSys)
        btnClear = root.findViewById(R.id.btnClear)
        btnAll = root.findViewById(R.id.btnAll)
        btnNotexist = root.findViewById(R.id.btnNotexist)
        dgvBCScan = root.findViewById(R.id.dgvBCScan)
        dgvBCHis = root.findViewById(R.id.dgvBCHis)

        dgvBCScan.widthWithpercent = true
        dgvBCScan.BeginInit()
        dgvBCScan.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45, requireContext())
        )

        dgvBCScan.addColumn(
            getString(R.string.seq), DataGridView.ViewMath()
                .convertDpToPixelInt(30.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCScan.addColumn(
            getString(R.string.barcode), DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCScan.addColumn(
            getString(R.string.scan_time), DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )

        dgvBCScan.EndInit()


        dgvBCHis.BeginInit()
        dgvBCHis.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45, requireContext())
        )

        dgvBCHis.addColumn(
            getString(R.string.seq), DataGridView.ViewMath()
                .convertDpToPixelInt(30.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "STATUS", DataGridView.ViewMath()
                .convertDpToPixelInt(60.0.toFloat(), BaseActivity().baseContext)
        )

        dgvBCHis.addColumn(
            "BIN", DataGridView.ViewMath()
                .convertDpToPixelInt(60.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            getString(R.string.barcode), DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "PO_NO", DataGridView.ViewMath()
                .convertDpToPixelInt(100.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "COLOR", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "LOT", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "ROLL", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "QTY", DataGridView.ViewMath()
                .convertDpToPixelInt(40.0.toFloat(), BaseActivity().baseContext)
        )
        dgvBCHis.addColumn(
            "ITEM", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), BaseActivity().baseContext)
        )

        dgvBCHis.EndInit()

        createSQLine()



        txtNewBin.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                handler.post(Runnable {
                    txtBC.requestFocus()
                })

                return@OnKeyListener true
            } else if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                handler.post(Runnable {
                    txtNewBin.text = ""
                    txtNewBin.requestFocus()
                })

                return@OnKeyListener true
            }
            false
        })
        txtBC.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                val textBC = txtBC.text.toString().trim()

                if (textBC.length > 1) {
                    onSaveBarcode(textBC)
                } else {
                    handler.post(Runnable {
                        txtStatus.text = "Pls Scan barcode!"
                    })
                }


                handler.post(Runnable {
                    txtBC.text = ""
                    txtBC.requestFocus()
                })
                return@OnKeyListener true
            } else if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                handler.post(Runnable {
                    txtBC.text = ""
                    txtBC.requestFocus()
                })

                return@OnKeyListener true
            }
            false
        })


        btnClear.setOnClickListener(View.OnClickListener {
            onClear()
        })
        btnNotexist.setOnClickListener(View.OnClickListener {
            onShowScanHis("001")
        })
        btnAll.setOnClickListener(View.OnClickListener {
            onShowScanHis()
        })
        OnShowScanLog()
        onShowScanHis()

        doLoadFirst()
        return root
    }

    fun doLoadFirst() {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_M010_GET_WH_USER",
                userpk + "|wh_ord"
            )

            if (result.isError) {
                BaseActivity().showToast(result.message)
            } else {
                handler.post(Runnable {
                    cbbwarehouse.setData(result.getData(), 1, 0)
                })
            }


//            val resulLayout = BaseActivity().getWebService().GetDataTableArg(
//                "lg_mposv2_qc3_layout",
//                lang
//            )
//
//
//
//            if(resulLayout.isError){
//                handler.post(Runnable {
//                    BaseActivity().showToast(resulLayout.message)
//                })
//            }else{
//
//                handler.post(Runnable {
//                    dataview.EndInit()
//                    dataview.clearColumn()
//                    resulLayout.data.forEach {
//                        var size = try{ it[1].toInt() } catch (ex: java.lang.Exception){10}
//                        var type = try{ it[2].toString() } catch (ex: java.lang.Exception){"0"}
//                        var align = try{ it[3].toString() } catch (ex: java.lang.Exception){"0"}
//                        dataview.addColumn(it[0],size,type,align )
//                    }
//
//                    dataview.EndInit()
//                })
//            }


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }



    override fun doProcess(time: String) {
        doStart()

    }

    var onUpdateServer = false
    fun doStart() {
        if (onUpdateServer) return

        Thread(Runnable {
            onUpdateServer = true
            Thread.sleep(1000)
            var dp = BaseActivity().getSQLiteDatabase()

            var cursor = dp.rawQuery(
                "select PK, ITEM_BC,TR_WH_IN_PK,LOC_ID  from EVALUATIONBC where DEL_IF=0  and sent_yn = 'N' order by PK asc",
                null
            )


            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var pk = cursor.getString(cursor.getColumnIndex("PK"))
                        var item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"))
                        var wh_in = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"))
                        var loc_id = cursor.getString(cursor.getColumnIndex("LOC_ID"))

                        val arr = arrayOf(
                            pk,
                            item_bc,
                            wh_in,
                            loc_id
                        )

                        array.add(arr)



                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            dp.close()


            array.forEachIndexed { index, strings ->
                val result = BaseActivity().getWebService().GetListDataTableArg(
                    "LG_MPOS_UPL_EVALUATION2",
                    strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + strings[3] + "|" + BaseActivity().deviceID + "|" + userid,
                    1
                )

                if (result.isError) {

                } else {
                    val data = result.getData(0)
                    if (data.totalrows == 1) {

                        var recode = data.records[0]

                        val item_bc: String =  recode.get("item_bc").toString() //ITEM_BC
                        val item_code: String = recode.get("item_code").toString() //ITEM_CODE
                        val item_name: String = recode.get("item_name").toString() //ITEM_NAME
                        val tr_qty: Float = recode.get("qty").toString().toFloat() //QTY
                        val lot_no: String =  recode.get("lot_no").toString() //LOT_NO
                        val label_pk: Int = recode.get("label_pk").toString() .toInt() //LABEL_PK
                        val item_pk: Int = recode.get("item_pk").toString() .toInt() //TLG_IT_ITEM_PK
                        val _status = recode.get("status").toString() //status
                        val _slip_no: String = recode.get("slip_no").toString() //slip no
                        val _income_date: String = recode.get("income_dt").toString() //income date
                        val _charger: String =  recode.get("crt_by").toString() //charger
                        val _supplier_name: String =  recode.get("wh_name").toString() //Supplier
                        val _supplier_pk: String = recode.get("wh_pk").toString() //Supplier_PK
                        val _line_name: String = recode.get("line_name").toString() //po_no
                        val _unit_price: Float = recode.get("line_pk").toString() .toFloat() //unit_price
                        val _uom: String = recode.get("uom").toString() //UOM
                        val _tlg_po_po_d_pk: String = recode.get("po_d_pk").toString() //tlg_po_po_d_pk
                        val id: Int = recode.get("inv_tr_pk").toString().toInt()
                        val _count_child: String = recode.get("count_child").toString()

                        val send_time = Utils().getYMDHHmmss()

                        var sql = "";
                        if (_status.equals("001") )  {
                            sql = "UPDATE EVALUATIONBC set STATUS='" + _status + "',SENT_YN = 'Y', SENT_TIME = '" + send_time + "' where PK = " + id
                        } else {
                            sql = "UPDATE EVALUATIONBC set STATUS='" + _status + "',SENT_YN = 'Y', SENT_TIME = '" + send_time +
                                    "', PO_NO = '" + _line_name  + "', COLOR = '" + _slip_no + "', LOT = '" + lot_no + "', ROLL = '" + _supplier_name + "', QTY = '" + tr_qty+ "', ITEM = '" + item_name +
                                   "'  where PK = " + id
                        }
                        if (sql.trim().length > 0) {
                            var dp = BaseActivity().getSQLiteDatabase()
                            dp.execSQL(sql)

                            dp.close()
                        }
                    }
                }

                OnShowScanLog()
                onShowScanHis()
            }


            onUpdateServer = false
        }).start()


    }

    fun onSaveBarcode(barcode: String) {


        if (isExistBarcode(barcode)) {
            handler.post(Runnable {
                txtStatus.text = "Barcode exist in database!"
            })
        } else {
            var scan_date = Utils().getyyyyMMdd()
            var scan_time = Utils().getYMDHHmmss()
            //if cbbwarehouse.size() > 0
            var dp = BaseActivity().getSQLiteDatabase()

            dp.execSQL(
                "INSERT INTO EVALUATIONBC(ITEM_BC,TR_WH_IN_PK,TR_WH_IN_NAME,LOC_ID,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS) "
                        + "VALUES('"
                        + barcode + "',"
                        + cbbwarehouse.value() + ",'"
                        + cbbwarehouse.text() + "','"
                        + txtNewBin.text + "','"
                        + scan_date + "','"
                        + scan_time + "','"
                        + "N" + "','"
                        + "" + "');"
            );
            dp.close()
            handler.post(Runnable {
                txtStatus.text = "Add success!"
            })

            OnShowScanLog()

        }

    }

    fun isExistBarcode(l_bc_item: String): Boolean {
        var flag = false
        var dp = BaseActivity().getSQLiteDatabase()
        try {

            val countQuery =
                "SELECT PK FROM EVALUATIONBC where  del_if=0 and ITEM_BC ='$l_bc_item' "
            val cursor: Cursor = dp.rawQuery(countQuery, null)
            if (cursor != null && cursor.moveToFirst()) {
                flag = true
            }
            cursor.close()
        } catch (ex: Exception) {

        } finally {

        }
        dp.close()
        return flag
    }

    fun createSQLine() {
        val CREATE_INV_TR = ("CREATE TABLE IF NOT EXISTS EVALUATIONBC ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "LOC_ID INTEGER, "
                + "ITEM_BC INTEGER, "
                + "PO_NO TEXT, "
                + "COLOR TEXT, "
                + "LOT TEXT, "
                + "ROLL TEXT, "
                + "QTY TEXT, "
                + "ITEM TEXT, "
                + "TR_WH_IN_PK TEXT, "
                + "TR_WH_IN_NAME TEXT, "
                + "STATUS TEXT, "
                + "SCAN_DATE TEXT, "
                + "SCAN_TIME TEXT, "
                + "SENT_YN TEXT, "
                + "SENT_TIME TEXT,"
                + "DEL_IF INTEGER default 0 )")
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL(CREATE_INV_TR)
        dp.close()
    }

    fun onClear() {



        BaseActivity().confirmDialog("WAITING>>>>","DO YOU WANT TO CLEAR!!!" , object :
            BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
                var dp = BaseActivity().getSQLiteDatabase()
                dp.execSQL("DELETE FROM EVALUATIONBC   ")
                dp.close()

            }
        })
    }

    fun onShowScanHis(status : String = "ALL" ) {
       var addWhere  =   if(status == "ALL") {
            " and 1 = 1"
       }else {
           " and  status = '" + status +"' "
       }
        var dp = BaseActivity().getSQLiteDatabase()

        var cursor = dp.rawQuery(
            "SELECT PK,STATUS,LOC_ID,ITEM_BC,PO_NO, COLOR, LOT, ROLL,QTY,  ITEM FROM EVALUATIONBC where   SENT_YN='Y' " + addWhere +" order by PK desc",
            null
        )

        handler.post(Runnable {
            dgvBCHis.clearAll()
        })

        var count: Int = cursor.count
        if (cursor != null && cursor.moveToFirst()) {
            var i = 1
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    val arr = arrayOf(
                        i.toString(),
                        cursor.getString(cursor.getColumnIndex("STATUS")),
                        cursor.getString(cursor.getColumnIndex("LOC_ID")),
                        cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                        cursor.getString(cursor.getColumnIndex("PO_NO")),
                        cursor.getString(cursor.getColumnIndex("COLOR")),
                        cursor.getString(cursor.getColumnIndex("LOT")),
                        cursor.getString(cursor.getColumnIndex("ROLL")),
                        cursor.getString(cursor.getColumnIndex("QTY")),
                        cursor.getString(cursor.getColumnIndex("ITEM"))
                    )
                    handler.post(Runnable {
                        dgvBCHis.addRow(
                            arr
                        )
                    })
                    i++
                    cursor.moveToNext();
                }


            }
        }
        cursor.close()
        dp.close()
    }

    fun OnShowScanLog() {
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(
            "SELECT PK,ITEM_BC,SCAN_TIME FROM EVALUATIONBC where   SENT_YN='N' order by PK desc",
            null
        )
        handler.post(Runnable {
            dgvBCScan.clearAll()
        })

        var count: Int = cursor.count
        if (cursor != null && cursor.moveToFirst()) {
            var i = 1
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    val arr = arrayOf(
                        i.toString(),
                        cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                        cursor.getString(cursor.getColumnIndex("SCAN_TIME"))
                    )
                    handler.post(Runnable {
                        dgvBCScan.addRow(
                            arr
                        )
                    })
                    i++
                    cursor.moveToNext();
                }


            }
        }
        cursor.close()
        dp.close()
    }


}