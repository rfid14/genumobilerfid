package com.genuwin.mobile.gw

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.lamvuanh.lib.CircularImageView

class Home : BaseFragment(){

    lateinit var imageUser: CircularImageView
    lateinit var txtUserName: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_home,container,false)

        imageUser = view.findViewById(R.id.userImage)
        txtUserName = view.findViewById(R.id.userName)


        var userID = this.BaseActivity().getSharedPreferences().getString(
            "login_username",
            "NA"
        )

        txtUserName.text = userID



        var userImage = this.BaseActivity().getSharedPreferences().getString(
            "login_image",
            ""
        )

        try {
            if(!userImage.isNullOrEmpty() ) {
                val imageBytes = Base64.decode(userImage, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                if(image != null)
                    imageUser.setImageBitmap(image)
            }
        }catch (ex : Exception){

        }
        return view
    }

}