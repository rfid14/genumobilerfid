package com.genuwin.mobile.gw

import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView


class InvTransferEntry  : BaseFragment(){

    lateinit var grdData1 : DataGridView
    lateinit var grdData2 : DataGridView
    lateinit var grdData3 : DataGridView

    lateinit var txtError : TextView
    lateinit var edit_Bc1 : EditText
    lateinit var lstType : Combobox

    lateinit var lstWH_Out : Combobox
    lateinit var lstLocOut : Combobox
    lateinit var lstWH_In : Combobox

    lateinit var lstLocIn : Combobox


    lateinit var txtTotal : TextView
    lateinit var txtSent : TextView
    lateinit var txtRemain : TextView
    lateinit var txtTime : TextView
    lateinit var txtTotalBot : TextView
    lateinit var txtTotalQtyBot2 : TextView

    lateinit var btnNewRequest : Button
    lateinit var btnDelete : Button
    lateinit var btnMakeSlip : Button
    lateinit var btnApprove : Button
    lateinit var btnUpdate : Button



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(
            R.layout.fragment_inv_transfer_entry,
            container,
            false
        ) as LinearLayout
        grdData1 = rootView.findViewById(R.id.grdData1)

        grdData1.BeginInit()
        grdData1.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("SCAN TIME")
            )
        )

        grdData1.EndInit()
        grdData2 = rootView.findViewById(R.id.grdData2)

        grdData2.BeginInit()
        grdData2.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("Item Code", 200),
                DataGridView.Column("STATUS", 100),
                DataGridView.Column("Messages ", 400),
                DataGridView.Column("SLIP NO", 200),
                DataGridView.Column("Lot No", 100),
                DataGridView.Column("Income Date", 150),
                DataGridView.Column("Charger Name", 200),
                DataGridView.Column("WH Name", 150),
                DataGridView.Column("Line Name", 100)

            )
        )

        grdData2.EndInit()

        grdData2.itemCountListener = object : DataGridView.ItemCountListener() {
            override fun onChange(`object`: Int) {
                txtTotalBot.text = "$`object`"
            }
        }

        grdData3 = rootView.findViewById(R.id.grdData3)

        grdData3.BeginInit()
        grdData3.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),

                DataGridView.Column("Item Code", 200),
                DataGridView.Column("TOTAL", 100),
                DataGridView.Column("QTY", 100),
                //DataGridView.Column("UOM", 100),
                DataGridView.Column("SLIP NO", 200),
                DataGridView.Column("LOT NO", 100),
                DataGridView.Column("PO NO", 100),
                DataGridView.Column("W/H OUT", 250),
                DataGridView.Column("LOC OUT", 100),
                DataGridView.Column("W/H IN", 250),
                DataGridView.Column("LOC IN", 100),
                DataGridView.Column("WH_OUT_PK", 0),
                DataGridView.Column("WH_IN_PK", 0),
                DataGridView.Column("KEY", 0)
            )
        )

        grdData3.EndInit()
        grdData3.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                slipNoOnClick(position)
                return true
            }
        }


        txtError = rootView.findViewById(R.id.txtError)
        edit_Bc1 = rootView.findViewById(R.id.edit_Bc1)
        lstType = rootView.findViewById(R.id.lstType)
        lstType.listener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {
                    TODO("Not yet implemented")
                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    LoadOutWH()
                }

            }




        lstWH_Out = rootView.findViewById(R.id.lstWH_Out)
        lstWH_Out.listener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                LoadLocationOut()
            }

        }
        lstLocOut = rootView.findViewById(R.id.lstLocOut)
        lstWH_In = rootView.findViewById(R.id.lstWH_In)
        lstWH_In.listener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                LoadLocationIn()
            }

        }
        lstLocIn = rootView.findViewById(R.id.lstLocIn)
        txtTotalBot = rootView.findViewById(R.id.txtTotalBot)
        txtTotalQtyBot2 = rootView.findViewById(R.id.txtTotalQtyBot2)



        edit_Bc1.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                val textBC = edit_Bc1.text.toString().trim()

                if (textBC.length > 1) {
                    onSaveBarcode(textBC.toUpperCase())
                } else {
                    handler.post(Runnable {
                        txtError.text = "Pls Scan barcode!"
                    })
                }


                handler.post(Runnable {
                    edit_Bc1.text.clear()
                    edit_Bc1.requestFocus()
                })
                return@OnKeyListener true
            } else if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                handler.post(Runnable {
                    edit_Bc1.text.clear()
                    edit_Bc1.requestFocus()
                })

                return@OnKeyListener true
            }
            false
        })



        txtTotal = rootView.findViewById(R.id.txtTotal)
        txtSent = rootView.findViewById(R.id.txtSent)
        txtRemain = rootView.findViewById(R.id.txtRemain)
        txtTime = rootView.findViewById(R.id.txtTime)
        val sync_frequency =
            BaseActivity().getSharedPreferences().getString("sync_frequency", "5")
        if (sync_frequency != null) {
            handler.post(Runnable {
                txtTime.setText(sync_frequency)
            })
        }



        btnApprove = rootView.findViewById(R.id.btnApprove)
        btnApprove.setOnClickListener {
            if(grdData1.Rows().size > 0 ){
                handler.post{
                    BaseActivity().showToast("Waiting data send validate finish !!!")
                }
                return@setOnClickListener
            }

            BaseActivity().confirmDialog(
                "Confirm Approve...",
                "Are you sure you want to Approve",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                              ProcessApprove()
                        }

                    }
                }
            )
        }

        btnMakeSlip = rootView.findViewById(R.id.btnMakeSlip)
        btnMakeSlip.setOnClickListener {
            if(grdData1.Rows().size > 0 ){
                handler.post{
                    BaseActivity().showToast("Waiting data send validate finish !!!")
                }
                return@setOnClickListener
            }

            BaseActivity().confirmDialog(
                "Confirm Make Slip...",
                "Are you sure you want to Make Slip",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                            ProcessMakeSlip()
                        }

                    }
                }
            )
        }
        btnDelete = rootView.findViewById(R.id.btnDelete)
        btnDelete.setOnClickListener {
            BaseActivity().confirmDialog(
                "Confirm Delete...",
                "Are you sure you want to delete all",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                            onDelete()
                        }

                    }
                }
            )
        }

        btnUpdate = rootView.findViewById(R.id.btnUpdate)
        btnUpdate.setOnClickListener {
            handler.post {
                OnShowScanLog()
                OnShowScanIn()
                OnShowScanAccept()
            }
        }
        btnNewRequest = rootView.findViewById(R.id.btnNewRequest)
        btnNewRequest.setOnClickListener(View.OnClickListener {
            onClearReq()
            OnShowScanLog()
            OnShowScanIn()
            OnShowScanAccept()
        })


          createSQLine()
        //TODO OnShowSlIpNO()
        OnShowScanLog()
        OnShowScanIn()
        OnShowScanAccept()
        LoadTransferType()


        return rootView
    }

    override fun doProcess(time: String) {
        doStart()
        handler.post(Runnable {
            txtTime.setText(time)
        })
    }


    fun onClearReq(){
//        handler.post {
//            reqPK = ""
//            edit_reqNo.text = ""
//            edit_reqBal.text = ""
//            edit_reqQty.text = ""
//            edit_Bc2.text = ""
//        }
        //TODO Clear
    }

    fun LoadTransferType(){

        Thread {
            Thread.sleep(500)
            handler.post {
                BaseActivity().showProgressBar("Loading Type")
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_GET_TRANS_TYPE",
                userpk + "|SASY3000"
            )

            if (result.isError) {

            } else {
                handler.post {
                    lstType.setData(result.data, 1, 0)
                }

            }

            handler.post {
                BaseActivity().hideProgressBar()
            }


        }.start()

    }

    fun LoadOutWH(){
        Thread{
            Thread.sleep(500)
            handler.post {
                BaseActivity().showProgressBar("Loading WH")
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_M010_GET_WH_OUT_USER",
                userpk + "|wh_ord_4|" + lstType.value()
            )

            if (result.isError) {

            } else {
                handler.post {
                    lstWH_Out.setData(result.data, 1, 0)
                }

            }

            handler.post {
                BaseActivity().hideProgressBar()
            }
        }.start()
    }

    fun LoadLocationOut(){
        Thread{
            Thread.sleep(500)
            handler.post {
                BaseActivity().showProgressBar("Loading Location Out/WH In ")
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_GET_LOC",
                lstWH_Out.value()
            )

            if (result.isError) {

            } else {
                handler.post {
                    lstLocOut.setData(result.data, 1, 0)
                }

            }

            val result2 = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_M010_GET_WH_IN_USER",
                userpk + "|" + lstWH_Out.value() + "|wh_ord_3|" + lstType.value()
            )

            if (result2.isError) {

            } else {
                handler.post {
                    lstWH_In.setData(result2.data, 1, 0)
                }

            }

            handler.post {
                BaseActivity().hideProgressBar()
            }
        }.start()
    }

    fun LoadLocationIn(){
        Thread{
            Thread.sleep(500)
            handler.post {
                BaseActivity().showProgressBar("Loading Location In")
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_MPOS_GET_LOC_IN",
                lstWH_In.value() + "|" + lstWH_Out.value() + "|" + lstLocOut.value()
            )

            if (result.isError) {

            } else {
                handler.post {
                    lstLocIn.setData(result.data, 1, 0)
                }

            }


            handler.post {
                BaseActivity().hideProgressBar()
            }
        }.start()
    }

    fun onSaveBarcode(str_scan: String){
        if (lstWH_Out.value().equals("0")) {
            handler.post{
                BaseActivity().showToast("Please check Out WH!");

                txtError.setText("Please check Out WH!");

            }


            return
        }
        if (lstWH_In.value().equals("0")) {
            handler.post{
                BaseActivity().showToast("Please check In WH!");

                txtError.setText("Please check In WH!");

            }


            return
        }
        val isExist: Boolean = isExistBarcode(str_scan) // check barcode exist in data
        if (isExist) // exist data
        {
            BaseActivity().alertRingMedia()

            handler.post(Runnable {
                txtError.setText("Barcode exist in database!")
            })
            return
        } else {
            var scan_date = Utils().getyyyyMMdd()
            var scan_time = Utils().getYMDHHmmss()

            var dp = BaseActivity().getSQLiteDatabase()

            dp.execSQL(
                "INSERT INTO " + this::class.simpleName + "(ITEM_BC,TR_WH_OUT_PK,TR_WH_OUT_NAME,TR_WH_IN_PK,TR_WH_IN_NAME,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS,BC_TYPE,TR_TYPE," +
                        "TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_OUT_NAME,TLG_IN_WHLOC_IN_PK,TLG_IN_WHLOC_IN_NAME) "
                        + "VALUES('"
                        + str_scan + "',"
                        + lstWH_Out.value() + ",'"
                        + lstWH_Out.text() + "',"
                        + lstWH_In.value() + ",'"
                        + lstWH_In.text() + "','"
                        + scan_date + "','"
                        + scan_time + "','"
                        + "N" + "','"
                        + "-" + "','"
                        + lstType.value() + "','"
                        + '0' + "','"
                        + lstLocOut.value() + "','"
                        + lstLocOut.text() + "','"
                        + lstLocIn.value() + "','"
                        + lstLocIn.text() + "');"
            );
            dp.close()
            handler.post {
                txtError.setText("Add success!")
                OnShowScanLog()
            }
        }


    }

    fun isExistBarcode(l_bc_item: String): Boolean {
        var flag = false
        var dp = BaseActivity().getSQLiteDatabase()
        try {
            //String countQuery = "SELECT PK FROM INV_TR where tr_type='"+type+"'  AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            //String countQuery = "SELECT PK FROM INV_TR where  tr_type='"+type+"' AND (status='000' or status=' ') and del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            val countQuery =
                "SELECT PK FROM " + this::class.simpleName + " where  (status='000' or status='-')  and ITEM_BC ='$l_bc_item' and SENT_YN <> 'W'"
            val cursor: Cursor? = dp.rawQuery(countQuery, null)
            if (cursor != null && cursor.moveToFirst()) {
                flag = true
            }
            if (cursor != null) {
                cursor.close()
            }
        } catch (ex: java.lang.Exception) {
        } finally {
            dp.close()
        }
        return flag
    }

    private fun slipNoOnClick(it: Int) {
        //TODO code here
        try{
            val slip_no = grdData3.Rows().get(it).getCell()[4]
            val lot_no = grdData3.Rows().get(it).getCell()[5]
            val wh_out_pk = grdData3.Rows().get(it).getCell()[11]
            val vh_in_pk = grdData3.Rows().get(it).getCell()[12]

            val sql = "SELECT PK,ITEM_BC,ITEM_CODE,ROLL,TR_LOT_NO,COLOR,status FROM  " + this::class.simpleName.toString() + "  where" +
                    "  SENT_YN='Y' and SLIP_NO = '"+ slip_no +"' and TR_LOT_NO = '"+ lot_no +"' and (status='000' or status='-') and TR_WH_IN_PK = '"+vh_in_pk  +"' and TR_WH_OUT_PK = '"+wh_out_pk  +"'  order by ROLL ";

            val tablenm = this::class.simpleName.toString()
            val intent = Intent(BaseActivity().baseContext, DetailActivity::class.java).apply {
                putExtra(BaseDetailFragment.ARG_FRAMENT_NAME, "InvTransferDetail") // DUNG TAM
                putExtra(BaseDetailFragment.ARG_FRAMENT_TITLE, "")
                putExtra(BaseDetailFragment.ARG_ITEM_ID_M, slip_no.value)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D, lot_no.value)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D2, tablenm)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_DATA, sql)

            }
            BaseActivity().startActivity(intent)

        }catch (ex: Exception){

        }


    }

    var onUpdateServer = false
    fun doStart(){
        if (onUpdateServer) return

        Thread(Runnable {
            onUpdateServer = true

            Log.d("TEST" , "ID " + + this.id)

            Thread.sleep(3000)
            var dp = BaseActivity().getSQLiteDatabase()

            var cursor = dp.rawQuery(
                "select PK, ITEM_BC,TR_WH_OUT_PK,TR_WH_IN_PK,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK  from " + this::class.simpleName + " where DEL_IF=0  and sent_yn = 'N' order by PK asc",
                null
            )


            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var pk = cursor.getString(cursor.getColumnIndex("PK"))
                        var item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"))
                        var tr_wh_out_pk = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"))
                        var tr_wh_in_pk = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"))
                        var tr_whloc_out_pk =
                            cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_PK"))
                        var tr_whloc_in_pk =
                            cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_PK"))

                        val arr = arrayOf(
                            pk,
                            item_bc,
                            tr_wh_out_pk,
                            tr_wh_in_pk,
                            tr_whloc_out_pk,
                            tr_whloc_in_pk
                        )

                        array.add(arr)



                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            dp.close()


            array.forEachIndexed { index, strings ->
                Thread.sleep(1000)

                val result = BaseActivity().getWebService().GetListDataTableArg(
                    "lg_mposv2_upl_stock_transfer",
                    strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + strings[3] + "|" + strings[4] + "|" + strings[5] + "|" + BaseActivity().deviceID + "|" + userid + "|" + lang,
                    1
                )

                Log.d("TEST" + this.id, strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + strings[3] + "|" + strings[4] + "|" + strings[5] + "|" + BaseActivity().deviceID + "|" + userid + "|" + lang)



                if (result.isError) {
                    handler.post{
                        BaseActivity().showToast(result.message)
                    }
                } else {
                    val dataRows = result.getData(0)
                    if (dataRows.records.size >= 1) {

                        var data = dataRows.records[0]

                        try {

                            val item_bc: String = data.get("item_bc").toString() //a[i][0];//ITEM_BC

                            val item_code: String = data.get("item_code").toString() //ITEM_CODE

                            val item_name: String = data.get("item_name").toString() //ITEM_NAME

                            val tr_qty: Float = data.get("qty").toString().toFloat() //QTY

                            val lot_no: String = data.get("lot_no").toString() //LOT_NO

                            val label_pk: Int = data.get("label_pk").toString().toInt() //LABEL_PK

                            val item_pk: Int =
                                data.get("item_pk").toString().toInt() //TLG_IT_ITEM_PK

                            val _status: String = data.get("status").toString() //status

                            val _slip_no: String = data.get("slip_no").toString() //slip no

                            val _income_date: String =
                                data.get("income_dt").toString() //income date

                            val _charger: String = data.get("crt_by").toString() //CRT_BY

                            val _wh_out_name: String = data.get("wh_name").toString() //WH_NAME

                            val _wh_out_pk: String = data.get("wh_pk").toString() //WH_PK

                            val _line_name: String = data.get("line_name").toString() //po_no

                            val _line_pk: String = data.get("line_pk").toString()
                            val _uom: String = data.get("uom").toString() //UOM

                            val saleorder_d_pk: String = data.get("saleorder_d_pk").toString()
                            val saleorder_m_pk: String = data.get("saleorder_m_pk").toString()
                            val po_no: String = data.get("po_no").toString()
                            val id: Int = data.get("inv_tr_pk").toString().toInt()
                            val whLoc_pk: String = data.get("loc_pk").toString()
                            val req_m_pk: String = data.get("req_m_pk").toString()
                            val req_d_pk: String = data.get("req_d_pk").toString()
                            val req_no: String = data.get("req_no").toString()
                            val wi_pk: String = data.get("wi_plan_pk").toString()
                            val grade: String = data.get("grade").toString()
                            val status_msg = data.get("status_msg") ///STATUS MSG
                            val roll_no = data.get("roll_no") /// ROLL_NO
                            val color = data.get("color") /// color
                            var scan_time = Utils().getYMDHHmmss()
                            val sql =
                                ("UPDATE " + this::class.simpleName + " set STATUS='" + _status
                                        + "',SENT_YN = 'Y', SENT_TIME = '" + scan_time
                                        + "', TLG_POP_LABEL_PK = '" + label_pk
                                        + "', ITEM_CODE = '" + item_code
                                        + "', ITEM_NAME = '" + item_name
                                        + "', TR_QTY = " + tr_qty
                                        + ", TR_ITEM_PK = " + item_pk //                        + ", TR_WH_OUT_PK = '" + _wh_out_pk
                                        + ",CHARGER='" + _charger
                                        + "',INCOME_DATE='" + _income_date
                                        + "', TR_LOT_NO='" + lot_no
                                        + "', SLIP_NO = '" + _slip_no //                        + "', TR_WH_OUT_NAME = '" + _wh_out_name
                                        + "', UOM = '" + _uom
                                        + "', PO_NO = '" + po_no //                        + "', TR_LINE_PK = '" + _line_pk
                                       // + "', TLG_IN_WHLOC_OUT_PK = '" + whLoc_pk //  + "', TLG_PO_PO_D_PK = '" + _tlg_po_po_d_pk
                                        + "', TLG_SA_SALEORDER_D_PK = '" + saleorder_d_pk
                                        + "', TLG_SA_SALEORDER_M_PK = '" + saleorder_m_pk
                                        + "', TLG_GD_REQ_M_PK = '" + req_m_pk
                                        + "', TLG_GD_REQ_D_PK = '" + req_d_pk
                                        + "', WI_PLAN_PK = '" + wi_pk
                                        + "', GRADE = '" + grade
                                        + "', REQ_NO = '" + req_no
                                        + "', STATUS_MSG = '" + status_msg
                                        + "', ROLL = '" + roll_no + "', grade = '" + grade + "', COLOR = '" + color
                                        + "'  where PK = " + id)
                            val sql1 =
                                "UPDATE " + this::class.simpleName + " set REQ_BAL = CAST(ROUND((CAST(REQ_BAL AS REAL)-" + tr_qty + "),2) AS TEXT), SCANNED = CAST(ROUND((CAST(SCANNED AS REAL)+" + tr_qty + "),2) AS TEXT) " +
                                        " WHERE REQ_NO = '" + req_no + "' AND TR_ITEM_PK = " + item_pk + " AND ITEM_BC IS NULL  AND TLG_GD_REQ_D_PK='" + req_d_pk + "'"

                            var db = BaseActivity().getSQLiteDatabase()
                            db.execSQL(sql)
                            //db.execSQL(sql1)
                            db.close()


                        } catch (exxx: Exception) {
                            handler.post{
                                BaseActivity().showToast(exxx.message.toString())
                            }
                        }
                    }
                }

                handler.post{
                    OnShowScanLog()
                    OnShowScanIn()
                    OnShowScanAccept()
                }

            }


            onUpdateServer = false
        }).start()
    }

    fun ProcessMakeSlip() {
        val unique_id =  Utils().dateyyyyMMddhhmmss
        try {


            var sql = " select  TR_ITEM_PK,TR_QTY, UOM, TR_LOT_NO, TR_WH_OUT_PK, TR_WH_IN_PK,UNIT_PRICE,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY  " +
                    " from " +
                    " (select TR_ITEM_PK, COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO,UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY " +
                    " FROM " + this::class.simpleName +"  WHERE DEL_IF = 0  AND STATUS='000' and SLIP_NO IN ('-','') " +
                    " GROUP BY TR_ITEM_PK,TR_LOT_NO, UNIT_PRICE, UOM, TR_WH_OUT_PK,TR_WH_IN_PK,BC_TYPE,TLG_IN_WHLOC_OUT_PK,TLG_IN_WHLOC_IN_PK,EXECKEY  )";


            var db = BaseActivity().getSQLiteDatabase()
            var cursor = db.rawQuery(sql, null)

            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var TR_ITEM_PK = cursor.getString(cursor.getColumnIndex("TR_ITEM_PK"))
                        var TR_QTY = cursor.getString(cursor.getColumnIndex("TR_QTY"))
                        var UOM = cursor.getString(cursor.getColumnIndex("UOM"))
                        var TR_LOT_NO = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"))
                        var TR_WH_OUT_PK = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"))
                        var TR_WH_IN_PK = cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK"))
                        var UNIT_PRICE = cursor.getString(cursor.getColumnIndex("UNIT_PRICE"))
                        var BC_TYPE = cursor.getString(cursor.getColumnIndex("BC_TYPE"))
                        var TLG_IN_WHLOC_OUT_PK = cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_PK"))
                        var TLG_IN_WHLOC_IN_PK = cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_PK"))
                        var EXECKEY = cursor.getString(cursor.getColumnIndex("EXECKEY"))

                        val arr = arrayOf(
                            TR_ITEM_PK,
                            TR_QTY,
                            UOM,
                            TR_LOT_NO,
                            TR_WH_OUT_PK,
                            TR_WH_IN_PK,
                            UNIT_PRICE,
                            BC_TYPE,
                            TLG_IN_WHLOC_OUT_PK,
                            TLG_IN_WHLOC_IN_PK,
                            EXECKEY
                        )
                        array.add(arr)
                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            db.close()

            Thread {
                Thread.sleep(500)
                handler.post({
                    BaseActivity().showProgressBar("processing....")
                })


                array.forEachIndexed { index, strings ->

                    var execkey: String = strings[10]
                    if (execkey == null || execkey == "") {
                        val ex_item_pk: Int = Integer.valueOf(strings[0])
                        val wh_pk  = strings[4]
                        val in_wh_pk: String = strings[5]
                        val lot_no = strings[3]
                        val bc_type = strings[7]

                        var db2 = BaseActivity().getSQLiteDatabase()
                        val sql2 = "UPDATE " + this::class.simpleName + " set EXECKEY='" + unique_id + "' " +
                                "  where TR_ITEM_PK = " + ex_item_pk + " and" +
                                "        TR_WH_OUT_PK = '" + wh_pk + "' and" +
                                "        TR_WH_IN_PK = '" + in_wh_pk + "' and" +
                                "        TR_LOT_NO = '" + lot_no + "' and" +
                                "        BC_TYPE = '" + bc_type.toString() + "' and" +
                                "      (EXECKEY is null or EXECKEY=' ' )"
                        db2.execSQL(sql2)
                        db2.close()



                        execkey = unique_id





                    }

                    var para = strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + strings[3] + "|" + strings[4] + "|" + strings[5] + "|" + strings[6] + "|" + strings[7] + "|" + strings[8] + "|" + strings[9] + "|" + execkey + "|" + userid + "|" + userpk + "|" + BaseActivity().deviceID
                    val result = BaseActivity().getWebService().GetDataTableArg(
                        "lg_mposv2_pro_trans_mslip",
                        para
                    )

                    if (result.isError) {
                        handler.post{
                            BaseActivity().showToast(result.message)
                        }
                    } else {
                        val dataRows = result.getData()
                        if (dataRows.size == 1) {
                            var data = dataRows[0]

                            var db3 = BaseActivity().getSQLiteDatabase()
                          val sql = "UPDATE  " + this::class.simpleName + "   set SLIP_NO= '" +  data[4] + "'  " +
                                    " where SLIP_NO IN('-','') and TR_ITEM_PK=" + data[0] + " " +
                                    " and TR_WH_OUT_PK= '" + data[1] + "' and TR_WH_IN_PK='" + data[2] + "' and TR_LOT_NO='" + data[3] + "'"
                            db3.execSQL(sql)
                            db3.close()
                        }

                    }


                }
                handler.post {
                    BaseActivity().hideProgressBar()
                    onClearReq()
                    OnShowScanLog()
                    OnShowScanIn()
                    OnShowScanAccept()
                }

            }.start()


        } catch (ex: java.lang.Exception) {
            handler.post{
                BaseActivity().showToast("onMakeSlip :" + ex.message)
            }

        }
    }

    fun ProcessApprove(){
        return
        val unique_id =  Utils().dateyyyyMMddhhmmss
        try {


            var sql = "select SLIP_NO   from " + this::class.simpleName + "  where del_if=0 and STATUS='000' and SLIP_NO NOT IN('-','')  and   PROCESS_TYPE is null  GROUP BY   SLIP_NO "


            var db = BaseActivity().getSQLiteDatabase()
            var cursor = db.rawQuery(sql, null)

            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var slip_no = cursor.getString(cursor.getColumnIndex("SLIP_NO"))

                        val arr = arrayOf(
                            slip_no
                        )
                        array.add(arr)
                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            db.close()

            Thread {
                Thread.sleep(1000)
                handler.post({
                    BaseActivity().showProgressBar("processing....")
                })


                array.forEachIndexed { index, strings ->



                    val result = BaseActivity().getWebService().GetDataTableArg(
                        "",
                        strings[0] + "|" + userpk + "|" + userid
                    )

                    if (result.isError) {
                        handler.post{
                            BaseActivity().showToast(result.message)
                        }
                    } else {
                        val dataRows = result.getData()
                        if (dataRows.size == 1) {
                            var data = dataRows[0]

                            var db3 = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "UPDATE " + InvPreparation::class.simpleName + " set PROCESS_TYPE= 'Y'  where  SLIP_NO = '" + data[0] +"'"
                            db3.execSQL(sql)
                            db3.close()
                        }

                    }


                }
                handler.post {
                    BaseActivity().hideProgressBar()
                }
                onClearReq()
                OnShowScanLog()
                OnShowScanIn()
                OnShowScanAccept()
            }.start()


        } catch (ex: java.lang.Exception) {
            handler.post{
                BaseActivity().showToast("onMakeSlip :" + ex.message)
            }

        }

    }

    fun onDelete(){
        var dp = BaseActivity().getSQLiteDatabase()

        var cursor = dp.rawQuery(
            "select PK, ITEM_BC from " + this::class.simpleName + "  where DEL_IF=0   ",
            null
        )

        var array: MutableList<Array<String>> = ArrayList()

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {

                    var pk = cursor.getString(cursor.getColumnIndex("PK"))
                    var item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"))

                    val arr = arrayOf(
                        pk,
                        item_bc
                    )
                    array.add(arr)
                    cursor.moveToNext();
                }


            }
        }
        cursor.close()
        dp.close()





        Thread {
            Thread.sleep(500)
            handler.post({
                BaseActivity().showProgressBar("DELETING.....")
            })

            array.forEachIndexed { index, strings ->
                val result = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_del_transfer",
                    strings[1] + "|" + BaseActivity().deviceID + "|" + userid
                )

                if (result.isError) {

                } else {
                    val dataRows = result.getData()
                    if (dataRows.size == 1) {

                        var data = dataRows[0]

                        try {

                            var db = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "DELETE FROM " + this::class.simpleName + "  where PK = " + strings[0]
                            db.execSQL(sql)
                            db.close()
                        } catch (exxx: Exception) {

                        }
                    }
                }

            }
            handler.post({
                BaseActivity().hideProgressBar()
            })
            onClearReq()
            OnShowScanLog()
            OnShowScanIn()
            OnShowScanAccept()


        }.start()



    }

    fun OnShowScanLog() {
        try {

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                "SELECT PK,ITEM_BC,SCAN_TIME FROM  " + this::class.simpleName + "  where  SENT_YN='N' order by PK desc ",
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData1.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(
                            i.toString(),

                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("SCAN_TIME"))
                        )
                        handler.post(Runnable {
                            grdData1.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }


            val sql2 =   " select pk,TR_QTY " +
                    " FROM " + this::class.simpleName  +
                    " WHERE DEL_IF = 0 "+
                    " AND SENT_YN <> 'W' " ;
            val cursorC = dp.rawQuery(sql2, null);
            val tt = cursorC.getCount();
            handler.post(Runnable {
                txtTotal.setText(tt.toString())
            })

            cursorC.close()

            val sql3 =   " select pk,TR_QTY " +
                    " FROM " + this::class.simpleName  +
                    " WHERE DEL_IF = 0 "+
                    " AND SENT_YN = 'Y'" ;

            val cursorS = dp.rawQuery(sql3, null);
            val ttY = cursorS.getCount()
            cursorS.close()

            handler.post(Runnable {
                txtSent.setText(ttY.toString())
                txtRemain.setText((tt - ttY).toString())
            })

            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }

    fun OnShowScanIn(){
        var scan_date = Utils().getyyyyMMdd()


        try {

            val sql = "select T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.STATUS_MSG , T2.GD_SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM " + this::class.simpleName + " t2 " +
                    " WHERE DEL_IF = 0   AND T2.STATUS NOT IN('000', '-')  " +
                    " ORDER BY pk ";

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                sql,
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData2.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(


                            cursor.getString(cursor.getColumnIndex("PK")),
                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                            cursor.getString(cursor.getColumnIndex("STATUS")),
                            cursor.getString(cursor.getColumnIndex("STATUS_MSG")),
                            cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")),
                            cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                            cursor.getString(cursor.getColumnIndex("INCOME_DATE")),
                            cursor.getString(cursor.getColumnIndex("CHARGER")),
                            cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")),
                            cursor.getString(cursor.getColumnIndex("LINE_NAME"))


                        )
                        handler.post(Runnable {
                            grdData2.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }




            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }

    fun  OnShowScanAccept(){
        try {
            var  sql2 =" select TR_ITEM_PK, ITEM_CODE,COUNT(*) TOTAL, SUM(TR_QTY) as TR_QTY, TR_LOT_NO, SLIP_NO,PO_NO,DOFING_NO,TR_WH_IN_PK,TR_WH_IN_NAME,TR_WH_OUT_NAME,TR_WH_OUT_PK,BC_TYPE,EXECKEY,TLG_IN_WHLOC_OUT_NAME,TLG_IN_WHLOC_IN_NAME  " +
                    "                     FROM " + this::class.simpleName + "  WHERE DEL_IF = 0  AND SENT_YN = 'Y'  AND STATUS IN('OK', '000') "+
                    "                     GROUP BY TR_ITEM_PK,ITEM_CODE,TR_LOT_NO, SLIP_NO,TR_WH_IN_PK,TR_WH_OUT_NAME,TR_WH_IN_NAME,TR_WH_OUT_PK,BC_TYPE,PO_NO,DOFING_NO,EXECKEY  " +
                    "                     ORDER BY EXECKEY desc,TR_ITEM_PK   ;"

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                sql2,
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData3.clearAll()
            })


            var i = 1
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    val arr = arrayOf(
                        i.toString(),


                        cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                        cursor.getString(cursor.getColumnIndex("TOTAL")),
                        cursor.getString(cursor.getColumnIndex("TR_QTY")),
                        cursor.getString(cursor.getColumnIndex("SLIP_NO")),
                        cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                        cursor.getString(cursor.getColumnIndex("PO_NO")),
                        cursor.getString(cursor.getColumnIndex("TR_WH_IN_NAME")),
                        cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_IN_NAME")),
                        cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")),
                        cursor.getString(cursor.getColumnIndex("TLG_IN_WHLOC_OUT_NAME")),

                        cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK")),
                        cursor.getString(cursor.getColumnIndex("TR_WH_IN_PK")),
                        cursor.getString(cursor.getColumnIndex("EXECKEY"))

                    )
                    handler.post {
                        grdData3.addRow(
                            arr
                        )
                    }
                    i++
                    cursor.moveToNext();
                }


            }


            if (cursor != null) {
                cursor.close()
            }


            var sql = "select pk,TR_QTY FROM " + this::class.simpleName + "   WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND STATUS='000'"



            val cursor2: Cursor?  = dp.rawQuery(
                sql,
                null
            )

            var _qty = 0f
            if (cursor2 != null && cursor2.moveToFirst()) {
                while (cursor2.isAfterLast() == false) {
                    _qty = _qty + cursor2.getString(cursor2.getColumnIndex("TR_QTY")).toFloat()
                    cursor2.moveToNext();
                }


            }
            handler.post({
                txtTotalQtyBot2.setText(_qty.toString())
            })

            if (cursor2 != null) {
                cursor2.close()
            }

            dp.close()
        } catch (ex: java.lang.Exception) {
            handler.post({
                BaseActivity().showToast("GridScanIn: " + ex.message)
            })

            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {


        }
    }

    fun createSQLine(){
        val CREATE_INV_TR = ("CREATE TABLE IF NOT EXISTS  " + this::class.java.simpleName  + " ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "TLG_POP_LABEL_PK INTEGER, "
                + "TR_ITEM_PK INTEGER, "
                + "TR_DATE TEXT, "
                + "ITEM_CODE TEXT, "
                + "ITEM_NAME TEXT, "
                + "ITEM_BC TEXT, "
                + "TR_QTY REAL, "
                + "TR_LOT_NO TEXT, "
                + "UNIT_PRICE REAL, "
                + "UOM TEXT, "
                + "ITEM_TYPE TEXT, "
                + "TR_LINE_PK TEXT, "
                + "LINE_NAME TEXT, "
                + "TLG_IN_WHLOC_OUT_PK TEXT, "
                + "TLG_IN_WHLOC_OUT_NAME TEXT, "
                + "TLG_IN_WHLOC_IN_PK TEXT, "
                + "TLG_IN_WHLOC_IN_NAME TEXT, "
                + "TR_WH_IN_PK TEXT, "
                + "TR_WH_IN_NAME TEXT, "
                + "TR_WH_OUT_PK TEXT, "
                + "TR_WH_OUT_NAME TEXT, "
                + "TR_TYPE TEXT, "
                + "STATUS TEXT, "
                + "TLG_GD_REQ_M_PK TEXT, "
                + "TLG_GD_REQ_D_PK TEXT, "
                + "TLG_SA_SALEORDER_D_PK TEXT, " // SO From Pk ExchangeProduct
                + "TO_SA_SALEORDER_D_PK TEXT, "
                + "SLIP_NO TEXT,"
                + "TLG_PO_PO_D_PK TEXT, "
                + "PO_NO TEXT, "
                + "CUST_PK TEXT, "
                + "CUST_NAME TEXT, "
                + "SLIP_TYPE TEXT, "
                + "SUPPLIER_PK TEXT, " // SO To Pk ExchangeProduct
                + "SUPPLIER_NAME TEXT, "
                + "TR_DELI_QTY REAL, "
                + "ITEM_BC_CHILD TEXT, "
                + "GD_SLIP_NO TEXT, "
                + "SCAN_DATE TEXT, "
                + "SCAN_TIME TEXT, "
                + "SENT_YN TEXT, "
                + "SENT_TIME TEXT,"
                + "PARENT_PK TEXT, " // location in PK StockTransfer
                + "CHILD_PK TEXT, "
                + "BC_TYPE TEXT, "
                + "REMARKS TEXT,"
                + "CHARGER TEXT,"
                + "INCOME_DATE TEXT, "
                + "TLG_POP_INV_TR_PK TEXT, "
                + "MAPPING_YN TEXT,"
                + "EXECKEY TEXT,"
                + "TRANS_PK TEXT,"
                + "TRANS_NAME TEXT,"
                + "SLIP_PK TEXT,"
                + "SLIP_NAME TEXT,"
                + "TO_ITEM_PK TEXT,"
                + "TO_UOM TEXT,"
                + "TO_LOTNO TEXT,"
                + "TO_UOM_QTY TEXT,"
                + "PROD_D_PK TEXT,"
                + "REQ_NO TEXT,"
                + "REQ_QTY TEXT,"
                + "REQ_BAL TEXT,"
                + "SCANNED TEXT,"
                + "MANUAL TEXT,"
                + "SO_NO TEXT,"
                + "WI_PLAN_PK TEXT,"
                + "TR_PARENT_ITEM_PK TEXT,"
                + "PROCESS_TYPE TEXT,"
                + "TLG_SA_SALEORDER_M_PK TEXT, " //+ "TR_LOC_ID TEXT, "        //location out pk StockTransfer
                //+ "LOC_NAME TEXT, "         //location out name StockTransfer
                //+ "TR_WAREHOUSE_PK INTEGER, "
                //+ "WH_NAME TEXT, "
                //+ "QC_REQ_NO TEXT,"
                //+ "QC_SLIP_NO TEXT,"
                //+ "QC_CUSTOMER TEXT,"
                //+ "QC_ITEM TEXT,"               // location in name StockTransfer
                //+ "QC_DATE TEXT,"
                //+ "QC_INSPECTOR TEXT,"
                //+ "QC_CHECKED TEXT,"
                //+ "QC_APPROVE TEXT,"
                //+ "QC_JEDGMENT TEXT,"
                // for maping offline kolonbd
                + "GRADE TEXT,"
                + "WEIGHT TEXT,"
                + "GROSS_WEIGHT TEXT,"
                + "DOFING_NO TEXT,"
                + "PARENT_QTY TEXT,"
                + "EVAL_QTY TEXT,"
                + "EVAL_NUM_CHILD TEXT,"
                + "COLOR TEXT,"
                + "STATUS_MSG TEXT,"
                + "ROLL TEXT,"
                + "DEL_IF INTEGER default 0 )")

        var dp = BaseActivity().getSQLiteDatabase()
         dp.execSQL("DROP TABLE IF EXISTS " + this::class.simpleName + " ");
        dp.execSQL(CREATE_INV_TR)
        dp.close()
    }
}