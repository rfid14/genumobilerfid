package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import java.util.*

class QCMoldReplace(): BaseFragment() {

    lateinit var txtDateFrom : TextView
    lateinit var txtDateTo : TextView
    lateinit var txtMold : EditText
    lateinit var lstTeam : Combobox
    lateinit var lstShift : Combobox
    lateinit var lstStopType : Combobox
    lateinit var btnSearch : Button
    lateinit var btnDateFrom : Button
    lateinit var btnDateTo : Button
    lateinit var btnNewRow : Button
    lateinit var dataGridView : DataGridView
    lateinit var dataGridView2 : DataGridView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_qc_mold_rep,container,false)
        txtDateFrom = root.findViewById(R.id.txtDateFrom)
        txtDateTo = root.findViewById(R.id.txtDateTo)
        txtMold = root.findViewById(R.id.txtMold)
        lstTeam = root.findViewById(R.id.lstTeam)
        lstShift = root.findViewById(R.id.lstShift)
        lstStopType = root.findViewById(R.id.lstStopType)
        btnSearch = root.findViewById(R.id.btnSearch)
        btnDateFrom = root.findViewById(R.id.btnDateFrom)
        btnDateTo = root.findViewById(R.id.btnDateTo)
        btnNewRow = root.findViewById(R.id.btnNewRow)
        dataGridView = root.findViewById(R.id.dataGridView)
        dataGridView2 = root.findViewById(R.id.dataGridView2)
        txtDateFrom.text = SetPreviousDate()
        txtDateTo.text = SetCurrentDate()

        dataGridView.widthWithpercent = true
        dataGridView.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
        )

//        dataGridView.itemClickListener = object : DataGridView.HandlerItemClick(){
//            override fun onClickCell(position: Int, col: Int):Boolean {
//
//                doLoadData2()
//                return true
//            }
//
//        }
//
        dataGridView2.widthWithpercent = true
        dataGridView2.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
            DataGridView.ViewMath().convertDpToPixelInt(45  , requireContext())
        )
//
//
//        btnSearch.setOnClickListener(View.OnClickListener {
//            onSearch()
//        })
//
//        btnDateFrom.setOnClickListener(View.OnClickListener {
//
//            var date =  Utils().getStringToDate(txtDateFrom.text.toString().replace("/",""))
//            val yearc = date.get(Calendar.YEAR)
//            val monthc = date.get(Calendar.MONTH)
//            val dayc = date.get(Calendar.DAY_OF_MONTH)
//
//            MainActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
//                MainActivity.callBackDatePickerDialog {
//                override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
//                    var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
//                    txtDateFrom.text = date
//                }
//            })
//        })
//
//        btnDateTo.setOnClickListener(View.OnClickListener {
//            var date =  Utils().getStringToDate(txtDateTo.text.toString().replace("/",""))
//            val yearc = date.get(Calendar.YEAR)
//            val monthc = date.get(Calendar.MONTH)
//            val dayc = date.get(Calendar.DAY_OF_MONTH)
//
//            MainActivity().showDatePickerDialog(yearc,monthc-1,dayc, callback = object :
//                MainActivity.callBackDatePickerDialog {
//                override fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int) {
//                    var date: String  =  if (dayOfMonth < 10 ){ "0" + (dayOfMonth ) }else  {"" + (dayOfMonth) }  + "/" +  if(monthOfYear < 9 ) { "0" + (monthOfYear + 1) }else { ""+ (monthOfYear +1)} + "/" + year;
//                    txtDateTo.text = date
//                }
//            })
//        })
//
//        btnNewRow.setOnClickListener(View.OnClickListener {
//            MainActivity().showProgressBar("LAM CAI GI DO DI")
//        })
//
//        doLoadFirst()

        return root
    }


    private fun SetPreviousDate() : String {
        val car: Calendar = Calendar.getInstance()
        car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }

    private fun SetCurrentDate() : String {
        val car: Calendar = Calendar.getInstance()
        //car.add(Calendar.DATE, -1)
        var p_from_year = car.get(Calendar.YEAR)
        var p_from_month = car.get(Calendar.MONTH)
        var p_from_day = car.get(Calendar.DAY_OF_MONTH)

        var date: String  =  if (p_from_day < 10 ){ "0" + (p_from_day ) }else  {"" + (p_from_day) }  + "/" +  if(p_from_month < 9 ) { "0" + (p_from_month + 1) }else { ""+ (p_from_month +1)} + "/" + p_from_year;
        return date
    }
}