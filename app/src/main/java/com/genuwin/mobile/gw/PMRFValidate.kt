package com.genuwin.mobile.gw

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.MainActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView
import device.common.DecodeResult
import device.common.DecodeStateCallback
import device.common.ScanConst
import device.sdk.ScanManager
import kotlinx.android.synthetic.main.fragment_inv_preparation_mutli.*
import org.json.JSONObject
import kotlin.math.log


class PMRFValidate: BaseFragment() {


    var mScanner: ScanManager? = null
    var mDecodeResult: DecodeResult? = null
    private var mBackupResultType = ScanConst.ResultType.DCD_RESULT_COPYPASTE

    var mScanResultReceiver =
        object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                Log.v("vvv","onReceive")
                Log.v("vvv",mScanner.toString())
                Log.v("vvv",intent.toString())

                if (mScanner != null) {
                    try {
                        if (intent != null) {
                            if (ScanConst.INTENT_USERMSG == intent.action) {
                                if (mDecodeResult != null) {
                                    mScanner!!.aDecodeGetResult(mDecodeResult!!.recycle())
                                    doProcessBarcode(mDecodeResult.toString())

                                    Log.v("vvv",mDecodeResult.toString())
                                }
                            } else if (ScanConst.INTENT_EVENT == intent.action) {
                                val result =
                                    intent.getBooleanExtra(
                                        ScanConst.EXTRA_EVENT_DECODE_RESULT,
                                        false
                                    )
                                val decodeBytesLength =
                                    intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_LENGTH, 0)
                                val decodeBytesValue =
                                    intent.getByteArrayExtra(ScanConst.EXTRA_EVENT_DECODE_VALUE)
                                val decodeValue = decodeBytesValue!!.toString()
                                val decodeLength = decodeValue.length
                                val symbolName =
                                    intent.getStringExtra(ScanConst.EXTRA_EVENT_SYMBOL_NAME)
                                val symbolId =
                                    intent.getByteExtra(ScanConst.EXTRA_EVENT_SYMBOL_ID, 0.toByte())
                                val symbolType =
                                    intent.getIntExtra(ScanConst.EXTRA_EVENT_SYMBOL_TYPE, 0)
                                val letter =
                                    intent.getByteExtra(
                                        ScanConst.EXTRA_EVENT_DECODE_LETTER,
                                        0.toByte()
                                    )
                                val modifier =
                                    intent.getByteExtra(
                                        ScanConst.EXTRA_EVENT_DECODE_MODIFIER,
                                        0.toByte()
                                    )
                                val decodingTime =
                                    intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_TIME, 0)
                                val TAG = "ARIC TEST"
                                Log.d(TAG, "1. result: $result")
                                Log.d(TAG, "2. bytes length: $decodeBytesLength")
                                Log.d(TAG, "3. bytes value: $decodeBytesValue")
                                Log.d(TAG, "4. decoding length: $decodeLength")
                                Log.d(TAG, "5. decoding value: $decodeValue")
                                Log.d(TAG, "6. symbol name: $symbolName")
                                Log.d(TAG, "7. symbol id: $symbolId")
                                Log.d(TAG, "8. symbol type: $symbolType")
                                Log.d(TAG, "9. decoding letter: $letter")
                                Log.d(TAG, "10.decoding modifier: $modifier")
                                Log.d(TAG, "11.decoding time: $decodingTime")
                            }
                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }



    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private val mStateCallback: DecodeStateCallback =
        object : DecodeStateCallback(mHandler) {
            override fun onChangedState(state: Int) {
                when (state) {
                    ScanConst.STATE_ON, ScanConst.STATE_TURNING_ON -> {}
                    ScanConst.STATE_OFF, ScanConst.STATE_TURNING_OFF -> {}
                }
            }
        }

    private fun initScanner() {
        mScanner = ScanManager()
        mDecodeResult = DecodeResult()
        Log.v("vvv","initScanner")
        if (mScanner != null) {
            mScanner!!.aRegisterDecodeStateCallback(mStateCallback)
            mBackupResultType = mScanner!!.aDecodeGetResultType()
            mScanner!!.aDecodeSetResultType(ScanConst.ResultType.DCD_RESULT_USERMSG)
            mScanner?.aDecodeSetTriggerMode(ScanConst.TriggerMode.DCD_TRIGGER_MODE_ONESHOT)

            val filter = IntentFilter()
            filter.addAction(ScanConst.INTENT_USERMSG)
            filter.addAction(ScanConst.INTENT_EVENT)
            BaseActivity().baseContext.registerReceiver(mScanResultReceiver, filter)
        }
    }




    lateinit var dataview: DataGridView
    lateinit var btnStart: Button
    lateinit var btnClear: Button
    lateinit var btnMapping: Button

    lateinit var txtCountBC: TextView
    lateinit var txtWI: TextView
    lateinit var seekbar_Discrete: SeekBar

    var isSide: Boolean = false
    var isScan: Boolean = false
    var isProcess: Boolean = false

    var side_barcode: String = ""
    var side_pk: String = ""
    var side_name: String = ""
    var side_row: String = ""
    var side_qty: String = ""

    class Lot_No{
        var lot_no:String
        var qty:String
        var item_pk: String
        var item_name: String
        constructor(lotNo: String, Qty:String, itemPk:String, itemName:String){
            lot_no = lotNo
            qty = Qty
            item_pk = itemPk
            item_name = itemName

        }
    }


    var side_lotno : MutableList<Lot_No> = mutableListOf()


    class Barcode{
        var barcode:String
        var item_code:String
        var item_name: String
        var lot_no: String
        var pk_side: String
        constructor(item_bc: String, itemCode:String, itemName:String, lotNo:String, sidePK:String){
            barcode = item_bc
            item_code = itemCode
            item_name = itemName
            lot_no = lotNo
            pk_side = sidePK

        }
    }

    var ListBarrcode : MutableList<Barcode> = mutableListOf()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_rfid_rfvalidate,container,false) as LinearLayout

        dataview = root.findViewById(R.id.dataview)

        dataview.BeginInit()

        dataview.addColumn( getString(R.string.barcode), 120)
        dataview.addColumn( getString(R.string.item_code), 120)
        dataview.addColumn( getString(R.string.lot_no), 100)
        dataview.addColumn( getString(R.string.status), 100)
        dataview.addColumn( getString(R.string.item_name), 200)

        dataview.EndInit()

        btnStart = root.findViewById(R.id.btnStart)
        btnStart.setOnClickListener {
            btnStartClick()
        }

        btnClear = root.findViewById(R.id.btnClear)
        btnClear.setOnClickListener {
            btnClearClick()
        }

        btnMapping = root.findViewById(R.id.btnMapping)
        btnMapping.setOnClickListener {
            btnMappingClick()
        }

        txtCountBC = root.findViewById(R.id.txtCountBC)
        txtWI = root.findViewById(R.id.txtWI)
        seekbar_Discrete = root.findViewById(R.id.seekbar_Discrete)
        seekbar_Discrete.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {


            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {


                if (seekBar != null) {
//                    BaseActivity().showToast(seekBar.progress.toString()+ " POWER")
                    (BaseActivity() as MainActivity).setPower(seekBar.progress* -1 )
                }
            }

        })

        seekbar_Discrete.progress =  (BaseActivity() as MainActivity).getPower() * -1



        initScanner()
        return root
    }

    private fun doProcessBarcode(barcode: String) {
        if (barcode.equals("READ_FAIL")) {
            return
        }

        if (isProcess) return
        isProcess = true
        handler.post(Runnable { BaseActivity().showProgressBar(getString(R.string.loading)) })
        Thread(
            Runnable {
                Thread.sleep(1000)

                if (barcode.startsWith("L")) {
                    var flag  = false
                    val bc = barcode.substring(1)
                    val result =
                        BaseActivity()
                            .getWebService()
                            .GetDataTableArg("lg_sel_rfid_rfvalidate", bc)


                    if (result.isError) {
                        handler.post(Runnable { BaseActivity().showToast(result.message) })
                    } else {

                        if (result.data.size == 1) {

                            side_barcode = barcode;
                            side_pk = bc;
                            side_name =  result.data[0][1]
                            side_row = result.data[0][2]
                            side_qty = result.data[0][3]

                            flag = true;


                            handler.post {
                                txtWI.text = side_name

                            }


                            if(flag){

                                var flag2  = false
                                val result2 =
                                    BaseActivity()
                                        .getWebService()
                                        .GetDataTableArg("lg_sel_rfid_rfvalidate_1", bc)

                                Log.v("lg_sel_rfid_rfvalidate_1",result2.message)
                                if (result2.isError) {

                                    handler.post(Runnable { BaseActivity().showToast(result2.message) })
                                    flag2 = true
                                } else {
                                    for (a in result2.data){
                                        side_lotno.add(Lot_No(a[0],a[1],a[2],a[3]))
                                    }
                                }

                                val result3 =
                                    BaseActivity()
                                        .getWebService()
                                        .GetDataTableArg("lg_sel_rfid_rfvalidate_3", bc)
//                                Log.v("lg_sel_rfid_rfvalidate_3",result3.message)

                                if (result3.isError) {
                                    handler.post(Runnable { BaseActivity().showToast(result3.message) })
                                    flag2 = true
                                } else {
                                    for (a in result3.data){
                                        ListBarrcode.add(Barcode(a[0],a[1],a[2],a[3],bc))
                                    }
                                }
                                val result4 =
                                    BaseActivity()
                                        .getWebService()
                                        .GetDataTableArg("lg_sel_rfid_rfvalidate_4", bc)

//                                    Log.v("lg_sel_rfid_rfvalidate_4",result4.message)
                                if (result4.isError) {
                                    handler.post(Runnable { BaseActivity().showToast(result4.message) })
                                    flag2 = true

                                } else {
                                    for (a in result4.data){
                                        ListBarrcode.add(Barcode(a[0],a[1],a[2],a[3],bc))
                                    }
                                }
                                if(!flag2) {
                                    isSide = true;
                                    isScan = false;
                                }
                            }

                        }
                    }
                } else {

                }

                handler.post(Runnable { BaseActivity().hideProgressBar() })

                isProcess = false
            }
        )
            .start()
    }

    private fun btnStartClick() {
        if(isProcess) return

        if(!isSide){
            mScanner?.aDecodeSetTriggerOn(1)
        }else{

            (BaseActivity() as MainActivity) .RfidControl()
        }
    }

    override fun onHanldEvent(code: Int): Boolean {
        if(!isSide){
            mScanner?.aDecodeSetTriggerOn(1)
            return true
        }

        return  false
    }

    override fun onDataResult(type: Int, data: String) {
        Log.v("vvv", "onDataResult")
        if( type == 1){
            if(isProcess)
                return

            if(!isSide)
                return



            for( e in dataview.Rows()){
                if( e.getCell()[0].value.equals(data))
                    return

            }
            val indexBarcode = getIndexBarcode(data)
//          val indexName =   getIndexItemCode(item_name)


            if(indexBarcode == -1) {
                dataview.addRow(DataGridView.Row(arrayOf(data, "-","-","Incorrect",""), Color.RED))
            }else{
                val bc = ListBarrcode[indexBarcode]
                dataview.addRow( arrayOf(data, bc.item_code,bc.lot_no,"",bc.item_name))
            }

//            ==============================================

//            var check: Boolean = false;
//
//            if (indexBarcode != -1){
//                check = true;
//            }
//            if (indexName == -1){
//                check = false;
//            }
//
//            if(check){
//              val bc = ListBarrcode[indexBarcode]
//                dataview.addRow( arrayOf(data, bc.item_code,bc.lot_no,"",bc.item_name))
//            }else{
//                val bc = ListBarrcode[indexBarcode]
//                dataview.addRow(DataGridView.Row(arrayOf(data, bc.item_code,bc.lot_no,"Incorrect",""), Color.RED))
//
//            }



//            ==============================================


            txtCountBC.text = dataview.Rows().count().toString()
        }else if(type == 2){
            BaseActivity().runOnUiThread {
                if( data.equals("0")){
                    btnStart.text = getText(R.string.start)
                    btnClear.isEnabled = true
                    btnMapping.isEnabled = true
                    seekbar_Discrete.isEnabled = true
                }else{
                    btnStart.text = getText(R.string.stop)
                    btnClear.isEnabled = false
                    btnMapping.isEnabled = false
                    seekbar_Discrete.isEnabled = false
                }
            }

        }


        //super.onDataResult(type, data)
    }

    private fun getIndexItemCode(itemName: String): Int {
        for (i in 0 until side_lotno.size) {
            if (side_lotno[i].item_name.equals(itemName)) {
                return i
            }
        }
        return -1
    }
//    _________________________________________________________
    private fun getIndexBarcode(barcode: String): Int {
        for (i in 0 until ListBarrcode.size) {
            if (ListBarrcode[i].barcode.equals(barcode)) {
                return i
            }
        }
        return -1
    }

    private fun btnClearClick() {
        this.BaseActivity().confirmDialog(getString(R.string.prompt_ismodified),getString(R.string.clear) , object :
            BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
                if (bool){
                    doClear()

                }
            }
        })
    }

    private fun doClear() {
        side_barcode = ""
        side_pk = ""
        side_name = ""
        side_row = ""
        side_qty = ""

        isSide = false
        side_lotno.clear()
        ListBarrcode.clear()
        handler.post {
            txtWI.text = ""
            txtCountBC.text = ""
            dataview.clearAll()
        }

    }

    private fun btnMappingClick() {
        this.BaseActivity().confirmDialog(getString(R.string.prompt_ismodified),getString(R.string.submit) , object :
            BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
                if (bool){
                    doSubbmit()

                }
            }
        })
    }

    fun doSubbmit(){
        if(isProcess)
            return
        isProcess = true
        Thread(Runnable {
            Thread.sleep(500)
            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.loading))
            })

            var key: String = ""
            val listSB  = arrayOf("","","","","","","","","","","","")
            var iCount = 0
            var indexList = 0
            for( e in dataview.Rows()){
                if(e.getCell()[1].value == "-"){
                    listSB[indexList] = listSB[indexList]  +key +e.getCell()[0].value
                    key = ","

                    iCount++
                    if (listSB[indexList].length > 3700)
                    {

                        indexList++

                        key = "";

                    }
                }
            }

            var endPara: String = ""
            var data: String = ""
            for (i in 0..11) {
                data = if (i < listSB.size) {
                    data + endPara + listSB[i].toString()
                } else {
                    data + endPara + ""
                }
                endPara = "|"

                Log.v("listSB", listSB[i].toString())
            }


            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_sel_rfid_rfvalidate_2",
                side_pk + "|" + data + "|" + userid
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{

                for ((indexCol, a) in result.data.withIndex()){

                    for (r in dataview.Rows()){
                        if (r.getCell()[0].value.equals(a[0])){
//                            handler.post {

                                if(r.getCell()[3].value.length > 2){
//                                    r.setCellData(3, r.getCell()[3].value +"-"+a[3])
//                                    r.setCellData(1, a[1])
//                                    r.setCellData(2, a[2])
                                    dataview.setDataCell(1,indexCol, a[1])
                                    dataview.setDataCell(2,indexCol, a[2])
                                    dataview.setDataCell(4,indexCol, a[4])
                                }else{
//                                    r.setCellData(3, a[3])
                                    dataview.setDataCell(3,indexCol, a[3])
                                }
//                                ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//                                if(r.getCell()[1].value.equals("-")){
//                                    Log.d("vvv", "doSubbmit: ")
//                                    r.setCellData(1, a[1])
//                                    r.setCellData(2, a[2])
//                                }else{
//                                    r.setCellData(3, a[3])
//                                }

                            }
//                        }
                    }
                }





            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })

            isProcess = false
        }).start()
    }
}