package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.MainActivity
import com.genuwin.mobile.R
import com.lamvuanh.lib.DataGridView
import device.common.DecodeResult

import device.sdk.ScanManager
import device.common.ScanConst

import android.content.Intent

import android.content.BroadcastReceiver
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import java.lang.Exception
import device.common.DecodeStateCallback
import android.content.IntentFilter
import com.genuwin.mobile.base.BaseActivity
import com.google.android.material.textfield.TextInputEditText






class PMRCDevice: BaseFragment() {
    lateinit var dataview: DataGridView
    lateinit var btnStart: Button
    lateinit var btnClear: Button
    lateinit var btnMapping: Button

    lateinit var txtCountBC: TextView
    lateinit var txtBarcode: TextView
    lateinit var etxWeight: TextInputEditText

    var mScanner: ScanManager? = null
    var mDecodeResult: DecodeResult? = null
    private var mBackupResultType = ScanConst.ResultType.DCD_RESULT_COPYPASTE

    var mScanResultReceiver  = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {

        if (mScanner != null) {
            try {
                if (intent != null) {
                    if (ScanConst.INTENT_USERMSG == intent.action) {
                        if (mDecodeResult != null) {
                            mScanner!!.aDecodeGetResult(mDecodeResult!!.recycle())
                            doProcessBarcode(mDecodeResult.toString())

                        }

                    } else if (ScanConst.INTENT_EVENT == intent.action) {
                        val result =
                            intent.getBooleanExtra(ScanConst.EXTRA_EVENT_DECODE_RESULT, false)
                        val decodeBytesLength =
                            intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_LENGTH, 0)
                        val decodeBytesValue =
                            intent.getByteArrayExtra(ScanConst.EXTRA_EVENT_DECODE_VALUE)
                        val decodeValue = decodeBytesValue!!.toString()
                        val decodeLength = decodeValue.length
                        val symbolName = intent.getStringExtra(ScanConst.EXTRA_EVENT_SYMBOL_NAME)
                        val symbolId = intent.getByteExtra(
                            ScanConst.EXTRA_EVENT_SYMBOL_ID,
                            0.toByte()
                        )
                        val symbolType = intent.getIntExtra(ScanConst.EXTRA_EVENT_SYMBOL_TYPE, 0)
                        val letter = intent.getByteExtra(
                            ScanConst.EXTRA_EVENT_DECODE_LETTER,
                            0.toByte()
                        )
                        val modifier = intent.getByteExtra(
                            ScanConst.EXTRA_EVENT_DECODE_MODIFIER,
                            0.toByte()
                        )
                        val decodingTime = intent.getIntExtra(ScanConst.EXTRA_EVENT_DECODE_TIME, 0)
                        val TAG = "ARIC TEST"
                        Log.d(TAG, "1. result: $result")
                        Log.d(TAG, "2. bytes length: $decodeBytesLength")
                        Log.d(TAG, "3. bytes value: $decodeBytesValue")
                        Log.d(TAG, "4. decoding length: $decodeLength")
                        Log.d(TAG, "5. decoding value: $decodeValue")
                        Log.d(TAG, "6. symbol name: $symbolName")
                        Log.d(TAG, "7. symbol id: $symbolId")
                        Log.d(TAG, "8. symbol type: $symbolType")
                        Log.d(TAG, "9. decoding letter: $letter")
                        Log.d(TAG, "10.decoding modifier: $modifier")
                        Log.d(TAG, "11.decoding time: $decodingTime")

                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        }
    }



    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private val mStateCallback: DecodeStateCallback = object : DecodeStateCallback(mHandler) {
        override fun onChangedState(state: Int) {
            when (state) {
                ScanConst.STATE_ON, ScanConst.STATE_TURNING_ON ->
                {

                }
                ScanConst.STATE_OFF, ScanConst.STATE_TURNING_OFF -> {


                }
            }
        }
    }

    private fun initScanner() {

        mScanner = ScanManager()
        mDecodeResult = DecodeResult()
        if (mScanner != null) {
            mScanner!!.aRegisterDecodeStateCallback(mStateCallback)
            mBackupResultType = mScanner!!.aDecodeGetResultType()
            mScanner!!.aDecodeSetResultType(ScanConst.ResultType.DCD_RESULT_USERMSG)
            mScanner?.aDecodeSetTriggerMode(ScanConst.TriggerMode.DCD_TRIGGER_MODE_ONESHOT)


        }
    }







    var pallet_bc : String =""
    var pallet_pk : String =""
    var item_pk : String =""
    var pallet_item_name : String =""
    var pallet_item_code : String =""
    var pallet_qty : String =""
    var pallet_net_weigth : String =""
    var pallet_remark : String =""
    var pallet_lot_no : String =""
    var pallet_dof_no : String =""
    var pallet_grade : String =""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_rfid_rcdevide,container,false) as LinearLayout

        dataview = root.findViewById(R.id.dataview)

        dataview.BeginInit()

        dataview.addColumn( getString(R.string.barcode), 6)
        dataview.addColumn( getString(R.string.status), 4)
        dataview.widthWithpercent  = true
        dataview.EndInit()

        btnStart = root.findViewById(R.id.btnStart)
        btnStart.setOnClickListener {
            btnStartClick()
        }

        btnClear = root.findViewById(R.id.btnClear)
        btnClear.setOnClickListener {
            btnClearClick()
        }

        btnMapping = root.findViewById(R.id.btnMapping)
        btnMapping.setOnClickListener {
            btnMappingClick()
        }

        txtCountBC = root.findViewById(R.id.txtCountBC)
        txtBarcode = root.findViewById(R.id.txtBarcode)
        etxWeight = root.findViewById(R.id.etxWeight)



        val filter = IntentFilter()
        filter.addAction(ScanConst.INTENT_USERMSG)
        filter.addAction(ScanConst.INTENT_EVENT)
        BaseActivity().baseContext.registerReceiver(mScanResultReceiver, filter)



        initScanner()


        return root
    }

    fun  btnStartClick(){
        if(isProcess) return
        (BaseActivity() as MainActivity) .RfidControl()
    }

    fun btnClearClick(){
        handler.post {
            dataview.clearAll()

            pallet_bc =""
            pallet_pk =""
            item_pk =""
            pallet_item_name  =""
            pallet_item_code =""
            pallet_qty  =""
            pallet_net_weigth  =""
            pallet_remark =""
            pallet_lot_no =""
            pallet_dof_no  =""
            pallet_grade =""
            txtBarcode.text =""
            txtCountBC.text = "0"
        }
    }

    var isProcess = false

    private fun doProcessBarcode(barcode: String) {
        BaseActivity().runOnUiThread {
        if(barcode.equals("READ_FAIL")){
            return@runOnUiThread
        }

        if(isProcess)
            return@runOnUiThread
        isProcess = true
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_sel_rfid_rfdevide",
                barcode
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{

                if(result.data.size== 1) {

                    pallet_bc = barcode;
                    pallet_pk = result.data[0][0]
                    item_pk = result.data[0][1]
                    pallet_item_name = result.data[0][2]
                    pallet_item_code = result.data[0][3]
                    pallet_qty = result.data[0][4]
                    pallet_net_weigth = result.data[0][5]
                    pallet_remark = result.data[0][6]
                    pallet_lot_no = result.data[0][7]
                    pallet_dof_no = result.data[0][8]
                    pallet_grade = result.data[0][9]
                    handler.post {
                        txtBarcode.text = pallet_bc


                    }
                }


            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })


            isProcess = false
        }).start()
        }

    }

    fun btnMappingClick() {
        if (isProcess)
            return

        if (etxWeight.text.toString().isNullOrBlank()){
            BaseActivity().showToast("Pls check weight value!")
            return;
        }

        var weight = etxWeight.text.toString().toFloat()
        if (weight <= 0 || weight >= pallet_net_weigth.toFloat()) {
            BaseActivity().showToast("Pls check weight value!")
            return;
        }

        this.BaseActivity().confirmDialog(
            getString(R.string.prompt_ismodified),
            getString(R.string.prompt_do_you_make_devide),
            object :
                BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {
                    if (bool) {
                        doMakeDevide(weight)

                    }
                }
            })
    }
    fun doMakeDevide(weight1: Float ){
        isProcess = true

        Thread(Runnable {

            Thread.sleep(500)
            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.loading))
            })



            var lstBC = "";

            for( e in dataview.Rows()){

                    lstBC = lstBC +","+e.getCell()[0].value


            }

            val P_EXECKEY: String = System.currentTimeMillis().toString()
            val qty: Int = pallet_qty.toInt() - dataview.Rows().size
            val weight = pallet_net_weigth.toFloat() - weight1
            val qty1: Int = dataview.Rows().size
            //weight1

            val para: String =
                pallet_pk + "|" + item_pk + "|" + P_EXECKEY + "|" + pallet_grade + "|" + qty.toString() + "|" + weight.toString() + "|" + qty1.toString() + "|" + weight1.toString() + "|" + pallet_remark + "|" + pallet_lot_no + "|" + pallet_dof_no + "|" + lstBC + "|" + userid


            val result = BaseActivity().getWebService().TableReadOpenString(
                "lg_upd_rfid_rfdevide",
                para
            )


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })

            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{

                handler.post(Runnable {
                    BaseActivity().confirmDialog("",result.data.toString(),object : BaseActivity.ConfirmDialogCallBack {
                        override fun callBack(bool: Boolean) {

                        }} )
                })


            }



            isProcess = false
        }).start()

    }

    override fun onHanldEvent(code: Int): Boolean {
        if(pallet_bc.isNullOrBlank()){
            mScanner?.aDecodeSetTriggerOn(1)
            return true
        }

        return  false
    }

    override fun onDataResult(type: Int, data: String) {
        if( type == 1){
            if(isProcess)
                return

            if (pallet_bc.isNullOrBlank()) {
                (BaseActivity() as MainActivity ) .stopRfidScan()

                mScanner?.aDecodeSetTriggerOn(1)
                return
            }

            for( e in dataview.Rows()){
                if( e.getCell()[0].value.equals(data))
                    return

            }
            dataview.addRow( arrayOf(data,"-"))
            txtCountBC.text = dataview.Rows().count().toString()
        }else if(type == 2){
            BaseActivity().runOnUiThread {
                if( data.equals("0")){
                    btnStart.text = getText(R.string.start)
                    btnClear.isEnabled = true
                    btnMapping.isEnabled = true
                }else{
                    btnStart.text = getText(R.string.stop)
                    btnClear.isEnabled = false
                    btnMapping.isEnabled = false
                }
            }

        }


        //super.onDataResult(type, data)
    }
}