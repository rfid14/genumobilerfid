package com.genuwin.mobile.gw

import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.DataGridView
import com.lamvuanh.lib.HorizontaBlockView

class InvPreparationDetail : BaseDetailFragment() {
    lateinit var grdData2 : DataGridView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_inv_preparation_detail,container,false) as LinearLayout
        grdData2 = root.findViewById(R.id.grdData2)

        grdData2.BeginInit()
        grdData2.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("Item Code", 250),
                DataGridView.Column("Roll ID", 100),
                DataGridView.Column("Lot No", 100),
                DataGridView.Column("Color", 400)

            )
        )

        grdData2.EndInit()
        doLoadFirst()
        return root
    }

    fun doLoadFirst(){
        try {

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                "SELECT PK,ITEM_BC,ITEM_CODE,ROLL,TR_LOT_NO,COLOR FROM  " + id_d2 + "  where  SENT_YN='Y' and SLIP_NO = '"+ id_d +"'  and GD_SLIP_NO = '"+ id_m +"' and (status='000' or status=' ')  order by ROLL  ",
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData2.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(
                            i.toString(),

                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                            cursor.getString(cursor.getColumnIndex("ROLL")),
                            cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                            cursor.getString(cursor.getColumnIndex("COLOR"))
                        )
                        handler.post(Runnable {
                            grdData2.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }



            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }
}