package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import com.lamvuanh.lib.DataGridView.*


class AssetEntry() : BaseFragment(){

    lateinit var linearLayoutListView: DataGridView
    lateinit var lstDeparment: Combobox
    lateinit var lstItem: Combobox
    lateinit var lstLocation: Combobox
    lateinit var lstStatus: Combobox
    lateinit var btnLoad: Button
    lateinit var txtText: EditText



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_asset_emtry,container,false)
        linearLayoutListView = view.findViewById(R.id.dataListView)

        //TODO add String to String.xml
        linearLayoutListView.addColumn(Column(getString(R.string.pk), 0))
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.code),
                ViewMath().convertDpToPixelInt(200  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.name),
                ViewMath().convertDpToPixelInt(200  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.status),
                ViewMath().convertDpToPixelInt(100  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.item_name),
                ViewMath().convertDpToPixelInt(100  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.item_type),
                ViewMath().convertDpToPixelInt(100  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.issue),
                ViewMath().convertDpToPixelInt(50  , requireContext()),
                CELL_TYPE.TYPE_TEXT,
                TEXT_ALIGN.TEXT_CENTER
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.item_bc),
                ViewMath().convertDpToPixelInt(150  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.amount),
                ViewMath().convertDpToPixelInt(150  , requireContext()),
                        CELL_TYPE.TYPE_TEXT,
                TEXT_ALIGN.TEXT_RIGHT
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.buy_date),
                ViewMath().convertDpToPixelInt(100  , requireContext())
            )
        )


        linearLayoutListView.addColumn(
            Column(
                getString(R.string.description),
                ViewMath().convertDpToPixelInt(300  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.size),
                ViewMath().convertDpToPixelInt(50  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.deparment),
                ViewMath().convertDpToPixelInt(300  , requireContext())
            )
        )
        linearLayoutListView.addColumn(
            Column(
                getString(R.string.location),
                ViewMath().convertDpToPixelInt(300  , requireContext())
            )
        )


        linearLayoutListView.setBorderCellDrawable(R.drawable.border_primary_cell)
        linearLayoutListView.setHeaderStyle(
            ContextCompat.getColor(requireContext(), R.color.white),
            ContextCompat.getColor(requireContext(), R.color.genwin_color1),
             ViewMath().convertDpToPixelInt(45  , requireContext())
        )


        linearLayoutListView.itemClickListener = object :HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int): Boolean {
               var value = linearLayoutListView.getDataCell(position,6)
              if(value   == "N"){

                  BaseActivity().confirmDialog("Issue", "Do you want to issue barcode ?", object :
                      BaseActivity.ConfirmDialogCallBack {
                      override fun callBack(bool: Boolean) {
                          if (bool){
                              doIssueBC(linearLayoutListView.getDataCell(position,0), position)
                          }
                      }
                  })

              }
                return true
            }

        }

        lstDeparment = view.findViewById(R.id.deptype)
        lstLocation = view.findViewById(R.id.locationtype)
        lstItem = view.findViewById(R.id.itemType)
        lstStatus = view.findViewById(R.id.statusType)
        txtText = view.findViewById(R.id.txtText)
        btnLoad = view.findViewById(R.id.btnLoad)
        btnLoad.setOnClickListener(View.OnClickListener {
            searchData(txtText.text.toString())
        })





        doLoadFirst()
        return view
    }

    fun  doLoadFirst() =  Thread(Runnable {


        val resultDep = BaseActivity().getWebService().GetDataTableArg(
            "LG_ASSET_GET_DEPARMENT",
             userid
        )

        if(resultDep.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultDep.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstDeparment.setData(resultDep.data,1,0)
            })
        }




        val resultITEM = BaseActivity().getWebService().GetDataTableArg(
            "LG_ASSET_GET_ITEM",
            userid
        )
        if(resultITEM.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultITEM.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstItem.setData(resultITEM.data,1,0)
            })
        }


        val resultLocation = BaseActivity().getWebService().GetDataTableArg(
            "LG_ASSET_GET_LOCATION",
            userid
        )

        if(resultLocation.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultLocation.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstLocation.setData(resultLocation.data,1,0)
            })
        }

        val resultStatus = BaseActivity().getWebService().GetDataTableArg(
            "LG_ASSET_GET_STATUS",
            userid
        )

        if(resultStatus.isError){
            handler.post(Runnable {
                Toast.makeText(BaseActivity().baseContext,resultStatus.message, Toast.LENGTH_LONG).show()
            })
        }else{

            handler.post(Runnable {
                lstStatus.setData(resultStatus.data,1,0)
            })
        }




    }).start()


    private fun searchData(query: String) {

        Thread(Runnable {




            val resultDep = BaseActivity().getWebService().GetDataTableArg(
                "LG_ASSET_SEARCH_V2",
                query.trim()  + "|" + lstItem.value() + "|" + lstDeparment.value() + "|" + lstLocation.value() + "|" + lstStatus.value()
            )


            if(resultDep.isError){
                handler.post(Runnable {
                    Toast.makeText(BaseActivity().baseContext,resultDep.message, Toast.LENGTH_LONG).show()
                })
            }else{

                handler.post(Runnable {
                    linearLayoutListView.setData(resultDep.data)
                })
            }
        }).start()
    }


    fun doIssueBC(pk1: String, pos: Int) {

        Thread(Runnable {
            var userid = BaseActivity().getSharedPreferences().getString("login_user", "")

            val result = BaseActivity().getWebService().GetDataTableArg("LG_ASSET_ISSUE_LABEL", "$pk1|$userid")
            if (result.isError) {

                handler.post {
                    Toast.makeText(context, result.getMessage(), Toast.LENGTH_LONG)
                        .show()
                }
            } else {

                handler.post {
                    try {
                        linearLayoutListView.setDataCell(6, pos, "Y")
                        linearLayoutListView.setDataCell(
                            7,
                            pos,
                            result.data[0][7].toString()
                        )
                    } catch (ex: Exception) {
                    }
                }
            }

        }).start()
    }
}