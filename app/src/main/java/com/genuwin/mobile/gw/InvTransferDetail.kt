package com.genuwin.mobile.gw

import android.database.Cursor
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.DataGridView

class InvTransferDetail : BaseDetailFragment() {
    lateinit var grdData2 : DataGridView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_inv_preparation_detail,container,false) as LinearLayout
        grdData2 = root.findViewById(R.id.grdData2)

        grdData2.BeginInit()
        grdData2.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("Item Code", 250),
                DataGridView.Column("Roll ID", 100),
                DataGridView.Column("Lot No", 100),
                DataGridView.Column("Color", 400)

            )
        )

        grdData2.EndInit()
        doLoadFirst()
        return root
    }

    fun doLoadFirst(){
        try {

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                data,
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData2.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(
                            i.toString(),

                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                            cursor.getString(cursor.getColumnIndex("ROLL")),
                            cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                            cursor.getString(cursor.getColumnIndex("COLOR"))
                        )
                        handler.post(Runnable {
                            grdData2.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }



            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }
}