package com.genuwin.mobile.gw

import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.os.SystemClock
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.DataGridView

class InvPreparationMulti: BaseFragment()  {
    lateinit var grdData1 : DataGridView
    lateinit var grdData2 : DataGridView
    lateinit var grdData3 : DataGridView

    lateinit var txtError : TextView
    lateinit var edit_Bc1 : EditText
    lateinit var edit_Bc2 : TextView


    lateinit var txtTotal : TextView
    lateinit var txtSent : TextView
    lateinit var txtRemain : TextView
    lateinit var txtTime : TextView
    lateinit var txtTotalBot : TextView
    lateinit var txtTotalQtyBot2 : TextView

    lateinit var btnNewRequest : Button
    lateinit var btnDelete : Button
    lateinit var btnMakeSlip : Button
    lateinit var btnApprove : Button

    lateinit var btnUpdate : Button



   // var reqPK = ""


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView = inflater.inflate(
            R.layout.fragment_inv_preparation_mutli,
            container,
            false
        ) as LinearLayout
        grdData1 = rootView.findViewById(R.id.grdData1)

        grdData1.BeginInit()
        grdData1.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("Request", 150),
                DataGridView.Column("SCAN TIME")
            )
        )

        grdData1.EndInit()
        grdData2 = rootView.findViewById(R.id.grdData2)

        grdData2.BeginInit()
        grdData2.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 150),
                DataGridView.Column("Item Code", 200),
                DataGridView.Column("STATUS", 100),
                DataGridView.Column("Messages ", 400),
                DataGridView.Column("SLIP NO", 200),
                DataGridView.Column("Lot No", 100),
                DataGridView.Column("Income Date", 150),
                DataGridView.Column("Charger Name", 200),
                DataGridView.Column("WH Name", 150),
                DataGridView.Column("Line Name", 100)

            )
        )

        grdData2.EndInit()

        grdData2.itemCountListener = object : DataGridView.ItemCountListener() {
            override fun onChange(`object`: Int) {
                txtTotalBot.text = "$`object`"
            }
        }

        grdData3 = rootView.findViewById(R.id.grdData3)

        grdData3.BeginInit()
        grdData3.addColumns(
            arrayOf(
                DataGridView.Column("Seq", 40),

                DataGridView.Column("Item Code", 200),
                DataGridView.Column("TOTAL", 100),
                DataGridView.Column("QTY", 100),
                DataGridView.Column("UOM", 100),
                DataGridView.Column("SLIP NO", 100),
                DataGridView.Column("Request No", 200)
            )
        )

        grdData3.EndInit()
        grdData3.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                slipNoOnClick(position)
                return true
            }
        }


        txtError = rootView.findViewById(R.id.txtError)
        edit_Bc1 = rootView.findViewById(R.id.edit_Bc1)
        edit_Bc2 = rootView.findViewById(R.id.edit_Bc2)

        txtTotalBot = rootView.findViewById(R.id.txtTotalBot)
        txtTotalQtyBot2 = rootView.findViewById(R.id.txtTotalQtyBot2)



        edit_Bc1.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                //Perform Code
                val textBC = edit_Bc1.text.toString().trim()

                if (textBC.length > 1) {
                    onSaveBarcode(textBC)
                } else {
                    handler.post(Runnable {
                        txtError.text = "Pls Scan barcode!"
                    })
                }


                handler.post(Runnable {
                    edit_Bc1.text.clear()
                    edit_Bc1.requestFocus()
                })
                return@OnKeyListener true
            } else if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                handler.post(Runnable {
                    edit_Bc1.text.clear()
                    edit_Bc1.requestFocus()
                })

                return@OnKeyListener true
            }
            false
        })



        txtTotal = rootView.findViewById(R.id.txtTotal)
        txtSent = rootView.findViewById(R.id.txtSent)
        txtRemain = rootView.findViewById(R.id.txtRemain)
        txtTime = rootView.findViewById(R.id.txtTime)
        val sync_frequency =
            BaseActivity().getSharedPreferences().getString("sync_frequency", "5")
        if (sync_frequency != null) {
            handler.post(Runnable {
                txtTime.setText(sync_frequency)
            })
        }



        btnApprove = rootView.findViewById(R.id.btnApprove)
        btnApprove.setOnClickListener {
            if(grdData1.Rows().size > 0 ){
                handler.post{
                    BaseActivity().showToast("Waiting data send validate finish !!!")
                }
                return@setOnClickListener
            }

            BaseActivity().confirmDialog(
                "Confirm Approve...",
                "Are you sure you want to Approve",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                            ProcessApprove()
                        }

                    }
                }
            )
        }
        btnMakeSlip = rootView.findViewById(R.id.btnMakeSlip)
        btnMakeSlip.setOnClickListener {
            if(grdData1.Rows().size > 0 ){
                handler.post{
                    BaseActivity().showToast("Waiting data send validate finish !!!")
                }
                return@setOnClickListener
            }

            BaseActivity().confirmDialog(
                "Confirm Make Slip...",
                "Are you sure you want to Make Slip",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                            ProcessMakeSlip()
                        }

                    }
                }
            )
        }
        btnDelete = rootView.findViewById(R.id.btnDelete)
        btnDelete.setOnClickListener {
            BaseActivity().confirmDialog(
                "Confirm Delete...",
                "Are you sure you want to delete all",
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        // callbackDialogYN(type)
                        if (bool) {
                            onDelete()
                        }

                    }
                }
            )
        }
        btnNewRequest = rootView.findViewById(R.id.btnNewRequest)
        btnNewRequest.setOnClickListener(View.OnClickListener {
           //TODO show request
            showAllRequest()
        })

        btnUpdate = rootView.findViewById(R.id.btnUpdate)
        btnUpdate.setOnClickListener {
            handler.post {
                OnShowScanLog()
                OnShowScanIn()
                OnShowScanAccept()
            }
        }


        createSQLine()
        OnShowSlIpNO()
        OnShowScanLog()
        OnShowScanIn()
        OnShowScanAccept()



        return rootView
    }

    private fun slipNoOnClick(it: Int) {
        //TODO code here
        try{
            val slip_no = grdData3.Rows().get(it).getCell()[5]
            val gd_slip_no = grdData3.Rows().get(it).getCell()[6]
            val tablenm = this::class.simpleName.toString()
            val intent = Intent(BaseActivity().baseContext, DetailActivity::class.java).apply {
                putExtra(BaseDetailFragment.ARG_FRAMENT_NAME, "InvPreparationDetail")
                putExtra(BaseDetailFragment.ARG_FRAMENT_TITLE, "")
                putExtra(BaseDetailFragment.ARG_ITEM_ID_M, gd_slip_no.value)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D, slip_no.value)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_D2, tablenm)
                putExtra(BaseDetailFragment.ARG_ITEM_ID_DATA, "")

            }
            BaseActivity().startActivity(intent)

        }catch (ex: Exception){

        }


    }

    override fun doProcess(time: String) {
        doStart()
        handler.post(Runnable {
            txtTime.setText(time)
        })
    }



    fun showAllRequest(){
        var  sql2 = "   SELECT GD_SLIP_NO   FROM  " + this::class.simpleName +  "_REQ    WHERE DEL_IF = 0      GROUP BY GD_SLIP_NO  "

        var dp = BaseActivity().getSQLiteDatabase()
        val cursor: Cursor?  = dp.rawQuery(
            sql2,
            null
        )



        var lstSlipNo = ""

        if (cursor != null && cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
                lstSlipNo += "," +  cursor.getString(cursor.getColumnIndex("GD_SLIP_NO"))
                cursor.moveToNext();
            }
        }


        if (cursor != null) {
            cursor.close()
        }
        dp.close()

        if(lstSlipNo.isNotEmpty()){
            lstSlipNo = lstSlipNo.substring(1);
        }

        BaseActivity().confirmDialog("Requests",lstSlipNo,object : BaseActivity.ConfirmDialogCallBack {
            override fun callBack(bool: Boolean) {
            }
        })
    }

    fun onSaveBarcode(bc: String){
        val str_scan: String = bc.toUpperCase()
        val l_length: Int = str_scan.length
        val str_req_no: String = str_scan.substring(1, l_length)
        try {

            if (str_scan.indexOf("S") == 0) {
                    loadReq(str_req_no)

            } else {
                handler.post(Runnable {
                    edit_Bc2.setText(str_scan)
                })
                val isExist: Boolean = isExistBarcode(str_scan) // check barcode exist in data
                if (isExist) // exist data
                {
                    BaseActivity().alertRingMedia()

                    handler.post(Runnable {
                        txtError.setText("Barcode exist in database!")
                    })
                    return
                } else {


                    if (str_scan.length > 20) {
                        handler.post(Runnable {

                            txtError.setText("Barcode has length more 20 char !")
                        })
                        return
                    }
                    if (OnSave(str_scan)) {
                        handler.post(Runnable {
                            txtError.setText("Save success.")
                        })

                        OnShowScanLog()
                    }
                }
            }
        } catch (ex: Exception) {
            //save data error write to file log
            BaseActivity().showToast("Save Error: $ex")
        }
    }

    fun loadReq(str_req_no :String){
        Thread(Runnable {

            Thread.sleep(200)
            handler.post({
                BaseActivity().showProgressBar("Loading request....")
            })

            var  sql2 = "   SELECT GD_SLIP_NO   FROM  " + this::class.simpleName +  "_REQ    WHERE DEL_IF = 0      GROUP BY GD_SLIP_NO  "

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                sql2,
                null
            )



            var lstSlipNo = ""

            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    lstSlipNo += "," +  cursor.getString(cursor.getColumnIndex("GD_SLIP_NO"))
                    cursor.moveToNext();
                }
            }


            if (cursor != null) {
                cursor.close()
            }
            dp.close()

            if(lstSlipNo.isNotEmpty()){
                lstSlipNo = lstSlipNo.substring(1);
            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_get_gd_pre_req_d",
                str_req_no + '|' + lstSlipNo
            )

            if (result.isError) {
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })


            } else {
                val dataRows = result.getData()
                dataRows.forEach { it ->
                    it
                    try {

                        var db = BaseActivity().getSQLiteDatabase()
                        db.execSQL(
                            "INSERT INTO  " + this::class.simpleName + "_REQ  (ITEM_BC,GD_SLIP_NO) "
                                    + "VALUES('"
                                    + it[1] + "','"
                                    + it[0] + "');"
                        )
                        db.close()
                    } catch (exxx: Exception) {

                    }
                }
            }
            handler.post({
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    fun OnSave(bc: String): Boolean {
        var kq = false

        try {
            var scan_date = Utils().getyyyyMMdd()
            var scan_time = Utils().getYMDHHmmss()

            var dp = BaseActivity().getSQLiteDatabase()

            var  sql2 = "   SELECT GD_SLIP_NO, ITEM_BC   FROM  " + this::class.simpleName +  "_REQ  WHERE ITEM_BC =   '" + bc + "' "


            val cursor: Cursor?  = dp.rawQuery(
                sql2,
                null
            )



            var lstSlipNo = ""
            var barcode = ""

            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {

                    lstSlipNo  =   cursor.getString(cursor.getColumnIndex("GD_SLIP_NO"))
                    barcode = cursor.getString(cursor.getColumnIndex("ITEM_BC"))
                    cursor.moveToNext()
                }
            }
            if (cursor != null) {
                cursor.close()
            }

            if(lstSlipNo.isEmpty()){
                handler.post {
                    BaseActivity().showToast("This barcode don't have in Req. ")
                    txtError.setText("This barcode don't have in Req.")
                }
                kq = false
            }else{
                dp.execSQL(
                    "INSERT INTO " + this::class.simpleName + " (ITEM_BC,GD_SLIP_NO,SCAN_DATE,SCAN_TIME,SENT_YN,STATUS) "
                            + "VALUES('"
                            + bc + "','"
                            + lstSlipNo + "','"
                            + scan_date + "','"
                            + scan_time + "','"
                            + "N" + "','"
                            + " " + "');"
                )

                kq = true
            }
            dp.close()



        } catch (ex: java.lang.Exception) {
            handler.post {
                txtError.setText("Save Error!!!")
            }
            kq = false
        }
        return kq
    }

    fun isExistBarcode(l_bc_item: String): Boolean {
        var flag = false
        var dp = BaseActivity().getSQLiteDatabase()
        try {
            //String countQuery = "SELECT PK FROM INV_TR where tr_type='"+type+"'  AND del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            //String countQuery = "SELECT PK FROM INV_TR where  tr_type='"+type+"' AND (status='000' or status=' ') and del_if=0 and ITEM_BC ='"+ l_bc_item + "' ";
            val countQuery =
                "SELECT PK FROM " + this::class.simpleName + " where  (status='000' or status=' ')  and ITEM_BC ='$l_bc_item' and SENT_YN <> 'W'"
            val cursor: Cursor? = dp.rawQuery(countQuery, null)
            if (cursor != null && cursor.moveToFirst()) {
                flag = true
            }
            if (cursor != null) {
                cursor.close()
            }
        } catch (ex: java.lang.Exception) {
        } finally {
            dp.close()
        }
        return flag
    }

    fun OnShowSlIpNO(){

    }

    fun OnShowScanLog() {
        try {

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                "SELECT PK,ITEM_BC,GD_SLIP_NO,SCAN_TIME FROM  " + this::class.simpleName + "  where  SENT_YN='N' order by PK desc ",
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData1.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(
                            i.toString(),

                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")),
                            cursor.getString(cursor.getColumnIndex("SCAN_TIME"))
                        )
                        handler.post(Runnable {
                            grdData1.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }


            val sql2 =   " select pk,QTY " +
                    " FROM " + this::class.simpleName  +
                    " WHERE DEL_IF = 0 "+
                    " AND SENT_YN <> 'W' " ;
            val cursorC = dp.rawQuery(sql2, null);
            val tt = cursorC.getCount();
            handler.post(Runnable {
                txtTotal.setText(tt.toString())
            })

            cursorC.close()

            val sql3 =   " select pk,QTY " +
                    " FROM " + this::class.simpleName  +
                    " WHERE DEL_IF = 0 "+
                    " AND SENT_YN = 'Y'" ;

            val cursorS = dp.rawQuery(sql3, null);
            val ttY = cursorS.getCount()
            cursorS.close()

            handler.post(Runnable {
                txtSent.setText(ttY.toString())
                txtRemain.setText((tt - ttY).toString())
            })

            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }

    fun OnShowScanIn(){
        var scan_date = Utils().getyyyyMMdd()


        try {

            val sql = "select T2.PK, T2.ITEM_BC, T2.ITEM_CODE,  T2.STATUS, T2.STATUS_MSG , T2.GD_SLIP_NO, T2.TR_LOT_NO, T2.INCOME_DATE, T2.CHARGER, T2.TR_WH_OUT_NAME, T2.LINE_NAME, T2.TLG_POP_INV_TR_PK " +
                    " FROM " + this::class.simpleName + " t2 " +
                    " WHERE DEL_IF = 0   AND T2.STATUS NOT IN('000', ' ')  " +
                    " ORDER BY pk ";

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                sql,
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData2.clearAll()
            })


            if (cursor != null && cursor.moveToFirst()) {
                var i = 1
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        val arr = arrayOf(


                            cursor.getString(cursor.getColumnIndex("PK")),
                            cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                            cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                            cursor.getString(cursor.getColumnIndex("STATUS")),
                            cursor.getString(cursor.getColumnIndex("STATUS_MSG")),
                            cursor.getString(cursor.getColumnIndex("GD_SLIP_NO")),
                            cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                            cursor.getString(cursor.getColumnIndex("INCOME_DATE")),
                            cursor.getString(cursor.getColumnIndex("CHARGER")),
                            cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")),
                            cursor.getString(cursor.getColumnIndex("LINE_NAME"))


                        )
                        handler.post(Runnable {
                            grdData2.addRow(
                                arr
                            )
                        })
                        i++
                        cursor.moveToNext();
                    }


                }
            }
            if (cursor != null) {
                cursor.close()
            }




            dp.close()
        } catch (ex: java.lang.Exception) {
            ex.message?.let { BaseActivity().showToast(it) }
        } finally {

        }
    }

    fun OnShowScanAccept(){
        try {
            var  sql2 = "   SELECT ITEM_NAME, ITEM_CODE, COUNT(*) TOTAL, SUM(QTY) as QTY, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK,SLIP_NO, GD_SLIP_NO,TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY " +
                        "   FROM  " + this::class.simpleName +
                        "    WHERE DEL_IF = 0   AND SENT_YN = 'Y' AND STATUS='000' " +
                        "    GROUP BY ITEM_NAME, ITEM_CODE, UOM, TR_ITEM_PK, TLG_GD_REQ_M_PK, SLIP_NO, GD_SLIP_NO, TLG_SA_SALEORDER_D_PK,TR_WH_OUT_PK,EXECKEY" +
                        "    ORDER BY EXECKEY desc  "

            var dp = BaseActivity().getSQLiteDatabase()
            val cursor: Cursor?  = dp.rawQuery(
                sql2,
                null
            ) // TLG_LABEL
            handler.post(Runnable {
                grdData3.clearAll()
            })


            var i = 1
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    val arr = arrayOf(
                        i.toString(),

                        cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                        cursor.getString(cursor.getColumnIndex("TOTAL")),
                        cursor.getString(cursor.getColumnIndex("QTY")),
                        cursor.getString(cursor.getColumnIndex("UOM")),
                        cursor.getString(cursor.getColumnIndex("SLIP_NO")),
                        cursor.getString(cursor.getColumnIndex("GD_SLIP_NO"))
                        // cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")),
                        // cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK"))
                        //  cursor.getString(cursor.getColumnIndex("CHARGER")),
                        // cursor.getString(cursor.getColumnIndex("TR_WH_OUT_NAME")),
                        //cursor.getString(cursor.getColumnIndex("LINE_NAME"))
                    )
                    handler.post {
                        grdData3.addRow(
                            arr
                        )
                    }
                    i++
                    cursor.moveToNext();
                }


            }


            if (cursor != null) {
                cursor.close()
            }


            var sql  = " select pk,QTY " +
                        " FROM  " + this::class.simpleName   +
                        " WHERE DEL_IF = 0 "+
                        " AND SENT_YN = 'Y' AND STATUS='000'"



            val cursor2: Cursor?  = dp.rawQuery(
                sql,
                null
            )

            var _qty = 0f
            if (cursor2 != null && cursor2.moveToFirst()) {
                while (cursor2.isAfterLast() == false) {
                    _qty = _qty + cursor2.getString(cursor2.getColumnIndex("QTY")).toFloat()
                    cursor2.moveToNext();
                }


            }
            handler.post({
                txtTotalQtyBot2.setText(_qty.toString())
            })

            if (cursor2 != null) {
                cursor2.close()
            }

            dp.close()
        } catch (ex: java.lang.Exception) {
            handler.post({
                BaseActivity().showToast("GridScanIn: " + ex.message)
            })

            //Log.e("OnShowScanAccept Error: -->", ex.getMessage());
        } finally {


        }
    }

    fun onDelete(){
        var dp = BaseActivity().getSQLiteDatabase()

        var cursor = dp.rawQuery(
            "select PK, ITEM_BC from " + this::class.simpleName + "  where DEL_IF=0   ",
            null
        )

        var array: MutableList<Array<String>> = ArrayList()

        if (cursor != null && cursor.moveToFirst()) {
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {

                    var pk = cursor.getString(cursor.getColumnIndex("PK"))
                    var item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"))

                    val arr = arrayOf(
                        pk,
                        item_bc
                    )
                    array.add(arr)
                    cursor.moveToNext();
                }


            }
        }
        cursor.close()
        dp.close()





        Thread {
            Thread.sleep(1000)
            handler.post{
                BaseActivity().showProgressBar("DELETING.....")
            }

            array.forEachIndexed { index, strings ->
                val result = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_del_pre",
                    strings[1] + "|" + BaseActivity().deviceID + "|" + userid
                )

                if (result.isError) {

                } else {
                    val dataRows = result.getData()
                    if (dataRows.size == 1) {

                        var data = dataRows[0]

                        try {


                            //val item_bc = data[0] // ITEM BARCODE


                            var db = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "DELETE FROM " + this::class.simpleName + "  where PK = " + strings[0]
                            db.execSQL(sql)
                            db.close()
                        } catch (exxx: Exception) {

                        }
                    }
                }

            }


            handler.post({
                var db2 = BaseActivity().getSQLiteDatabase()
                db2.execSQL("DELETE FROM " + this::class.simpleName + "_REQ ")

                db2.close()
                BaseActivity().hideProgressBar()
            })

            OnShowScanLog()
            OnShowScanIn()
            OnShowScanAccept()


        }.start()



    }

    fun ProcessMakeSlip() {
        val unique_id =  Utils().dateyyyyMMddhhmmss
        try {


            var sql =
                    " SELECT TR_ITEM_PK, TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,QTY, UOM,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                            " FROM " +
                            "       (   " +
                            "           SELECT TR_ITEM_PK,  SUM(QTY) QTY, UOM,  TLG_GD_REQ_M_PK,TLG_SA_SALEORDER_D_PK,TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK,EXECKEY " +
                            "           FROM " + this::class.simpleName +
                            "           WHERE DEL_IF = 0  AND SENT_YN = 'Y' AND STATUS='000' AND SLIP_NO IN('-','') " +
                            "           GROUP BY   TR_ITEM_PK, UOM, TLG_GD_REQ_M_PK, TLG_SA_SALEORDER_D_PK, TR_LOT_NO,TR_WH_OUT_PK,TLG_GD_REQ_D_PK, EXECKEY" +
                            "       )"


            var db = BaseActivity().getSQLiteDatabase()
            var cursor = db.rawQuery(sql, null)

            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var item_pk = cursor.getString(cursor.getColumnIndex("TR_ITEM_PK"))
                        var req_m = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_M_PK"))
                        var sale_d_pk = cursor.getString(cursor.getColumnIndex("TLG_SA_SALEORDER_D_PK"))
                        var qty = cursor.getString(cursor.getColumnIndex("QTY"))
                        var uon = cursor.getString(cursor.getColumnIndex("UOM"))
                        var lot_no = cursor.getString(cursor.getColumnIndex("TR_LOT_NO"))
                        var TR_WH_OUT_PK = cursor.getString(cursor.getColumnIndex("TR_WH_OUT_PK"))
                        var req_d = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK"))
                        var EXECKEY = cursor.getString(cursor.getColumnIndex("EXECKEY"))

                        val arr = arrayOf(
                            item_pk,
                            req_m,
                            sale_d_pk,
                            qty,
                            uon,
                            lot_no,
                            req_d,
                            EXECKEY
                        )
                        array.add(arr)
                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            db.close()

            Thread {
                Thread.sleep(1000)
                handler.post({
                    BaseActivity().showProgressBar("processing....")
                })


                array.forEachIndexed { index, strings ->

                    var execkey: String = strings[7]
                    if (execkey == null || execkey == "") {
                        val ex_item_pk: Int = Integer.valueOf(strings[0])
                        val ex_req_m_pk: Int = Integer.valueOf(strings[1])
                        val ex_lot_no: String = strings[5]
                        val ex_sale_order_d_pk: Int = Integer.valueOf(strings[2])

                        var db2 = BaseActivity().getSQLiteDatabase()
                        val sql2 =
                            "UPDATE " + this::class.simpleName + " set EXECKEY='" + unique_id + "' " +
                                    "  where TR_ITEM_PK = " + ex_item_pk + " and" +
                                    "        TLG_GD_REQ_M_PK = '" + ex_req_m_pk + "' and" +
                                    "        TLG_SA_SALEORDER_D_PK = '" + ex_sale_order_d_pk + "' and" +
                                    "        TR_LOT_NO = '" + ex_lot_no + "' and" +
                                    "      (EXECKEY is null or EXECKEY=' ' )"
                        db2.execSQL(sql2)
                        db2.close()
                        execkey = unique_id





                    }

                    val result = BaseActivity().getWebService().GetDataTableArg(
                        "lg_mposv2_pro_prepa_mslip",
                        strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + strings[3] + "|" + strings[4] + "|" + strings[5] + "|" + strings[6] + "|" + execkey + "|" + userid + "|" + userpk + "|" + BaseActivity().deviceID
                    )

                    if (result.isError) {
                        handler.post{
                            BaseActivity().showToast(result.message)
                        }
                    } else {
                        val dataRows = result.getData()
                        if (dataRows.size == 1) {
                            var data = dataRows[0]

                            var db3 = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "UPDATE " + this::class.simpleName + " set SLIP_NO= '" + data[1] + "'  where  SLIP_NO IN('-','') and TR_ITEM_PK=" +data[0]+ " and TLG_GD_REQ_M_PK= '" +  data[2] + "'"
                            db3.execSQL(sql)
                            db3.close()
                        }

                    }


                }
                handler.post {
                    BaseActivity().hideProgressBar()
                }

                OnShowScanLog()
                OnShowScanIn()
                OnShowScanAccept()
            }.start()


        } catch (ex: java.lang.Exception) {
            handler.post{
                BaseActivity().showToast("onMakeSlip :" + ex.message)
            }

        }
    }

    fun ProcessApprove(){

        val unique_id =  Utils().dateyyyyMMddhhmmss
        try {


            var sql = "select SLIP_NO   from " + this::class.simpleName + "  where del_if=0 and STATUS='000' and SLIP_NO NOT IN('-','')  and   PROCESS_TYPE is null  GROUP BY   SLIP_NO "


            var db = BaseActivity().getSQLiteDatabase()
            var cursor = db.rawQuery(sql, null)

            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var slip_no = cursor.getString(cursor.getColumnIndex("SLIP_NO"))

                        val arr = arrayOf(
                            slip_no
                        )
                        array.add(arr)
                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            db.close()

            Thread {
                Thread.sleep(1000)
                handler.post({
                    BaseActivity().showProgressBar("processing....")
                })


                array.forEachIndexed { index, strings ->



                    val result = BaseActivity().getWebService().GetDataTableArg(
                        "lg_mpos_pro_pre_approve",
                        strings[0] + "|" + userpk + "|" + userid
                    )

                    if (result.isError) {
                        handler.post{
                            BaseActivity().showToast(result.message)
                        }
                    } else {
                        val dataRows = result.getData()
                        if (dataRows.size == 1) {
                            var data = dataRows[0]

                            var db3 = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "UPDATE " + this::class.simpleName + " set PROCESS_TYPE= 'Y'  where  SLIP_NO = '" + data[0] +"'"
                            db3.execSQL(sql)
                            db3.close()
                        }

                    }


                }
                handler.post {
                    BaseActivity().hideProgressBar()
                }

                OnShowScanLog()
                OnShowScanIn()
                OnShowScanAccept()
            }.start()


        } catch (ex: java.lang.Exception) {
            handler.post{
                BaseActivity().showToast("onMakeSlip :" + ex.message)
            }

        }

    }

    var onUpdateServer = false
    fun doStart(){
        if (onUpdateServer) return

        Thread(Runnable {
            onUpdateServer = true
            Thread.sleep(1000)
            var dp = BaseActivity().getSQLiteDatabase()

            var cursor = dp.rawQuery(
                "select PK, ITEM_BC,GD_SLIP_NO  from " + this::class.simpleName + "  where DEL_IF=0  and sent_yn = 'N' order by PK ",
                null
            )


            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var pk = cursor.getString(cursor.getColumnIndex("PK"))
                        var item_bc = cursor.getString(cursor.getColumnIndex("ITEM_BC"))
                        var g_slip_no = cursor.getString(cursor.getColumnIndex("GD_SLIP_NO"))

                        val arr = arrayOf(
                            pk,
                            item_bc,
                            g_slip_no
                        )

                        array.add(arr)



                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            dp.close()


            array.forEachIndexed { index, strings ->
                val result = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_upl_prepara",
                    strings[0] + "|" + strings[1] + "|" + strings[2] + "|" + BaseActivity().deviceID + "|" + userid + "|" + lang
                )

                Thread.sleep(500)

                if (result.isError) {
                    handler.post{
                        BaseActivity().showToast(result.message)
                    }
                } else {
                    val dataRows = result.getData()
                    if (dataRows.size == 1) {

                        var data = dataRows[0]

                        try {


                            val slipNO = data[0] //SLIP_NO

                            val tlg_gd_req_d_pk = data[1] //TLG_GD_REQ_D_PK

                            val req_item_pk = data[2].toInt() //REQ_ITEM_PK

                            val req_qty = data[3].toFloat() //REQ_QTY

                            val out_wh_pk = data[4] //OUT_WH_PK

                            val tlg_pop_lable_pk = data[5].toInt() //TLG_POP_LABEL_PK

                            val tlg_it_item_pk = data[6].toInt() //tlg_it_item_pk

                            val item_bc = data[7] //item_bc

                            val label_uom = data[8] //label_uom

                            val yymmdd = data[9] //yymmdd

                            val item_code = data[10] //item_code

                            val item_name = data[11] //item_name

                            val item_type = data[12] //item_type

                            val label_qty = data[13].toFloat() //label_qty

                            val status = data[14] //status

                            val cust_pk = data[15] //TCO_BUSPARTNER_PK

                            val tlg_sa_saleorder_d_pk = data[16] //TLG_SA_SALEORDER_D_PK

                            val lotNO = data[17] //lot_no

                            val tlg_gd_req_m_pk = data[18] //TLG_GD_REQ_M_PK

                            val income_date = data[19] //INCOME_DATE

                            val wh_name = data[20] //WAREHOUSE

                            val charger = data[21] //CHARGER

                            val pk = data[22].toInt() ///PK return

                            val status_msg = data[23] ///STATUS MSG
                            val roll_no = data[24] /// ROLL_NO
                            val grade = data[25] /// grade
                            val color = data[26] /// color


                            // String _parent_pk = dt.records.get(i).get("parent_pk").toString();//PARENT_PK

                            // String _parent_pk = dt.records.get(i).get("parent_pk").toString();//PARENT_PK
                            var db = BaseActivity().getSQLiteDatabase()
                            val sql =
                                "UPDATE " + this::class.simpleName + " set STATUS='" + status + "', STATUS_MSG='" + status_msg + "', SENT_YN = 'Y', SENT_TIME = '" + Utils().getYMDHHmmss() + "', TLG_POP_LABEL_PK = " + tlg_pop_lable_pk + ", ITEM_CODE = '" +
                                        item_code + "', ITEM_NAME = '" + item_name + "', QTY = " + req_qty + ",TLG_GD_REQ_M_PK='" + tlg_gd_req_m_pk + "', TLG_GD_REQ_D_PK = '" + tlg_gd_req_d_pk + "', TR_ITEM_PK = " +
                                        tlg_it_item_pk + ", TR_WH_OUT_PK = '" + out_wh_pk + "',TR_WH_OUT_NAME='" + wh_name + "',CHARGER='" + charger + "',PO_NO='" + income_date + "',TR_DATE='" + yymmdd + "', TR_LOT_NO='" + lotNO + "',UOM = '" +
                                        label_uom + "',TLG_SA_SALEORDER_D_PK='" + tlg_sa_saleorder_d_pk + "',CUST_PK='" + cust_pk + "', SLIP_NO = '" + slipNO + "', ROLL = '" + roll_no + "', grade = '" + grade + "', color = '" + color + "'    where PK = " + pk
                            db.execSQL(sql)
                            db.close()
                        } catch (exxx: Exception) {
                            handler.post{
                                BaseActivity().showToast(exxx.message.toString())
                            }
                        }
                    }
                }

                handler.post {
                    OnShowScanLog()
                    OnShowScanIn()
                    OnShowScanAccept()
                }

            }


            onUpdateServer = false
        }).start()
    }

    fun createSQLine() {
        val CREATE_INV_TR = ("CREATE TABLE IF NOT EXISTS " + this::class.simpleName +  " ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "LOC_ID INTEGER, "
                + "ITEM_BC INTEGER, "
                + "PO_NO TEXT, "
                + "SLIP_NO TEXT, "
                + "GD_SLIP_NO TEXT, "
                + "COLOR TEXT, "
                + "LOT TEXT, "
                + "ROLL TEXT, "
                + "QTY TEXT, "
                + "ITEM TEXT, "
                + "ITEM_CODE TEXT, "
                + "ITEM_NAME TEXT, "
                + "UOM TEXT, "
                + "TR_LOT_NO TEXT, "
                + "TR_WH_IN_PK TEXT, "
                + "TR_WH_OUT_PK TEXT, "
                + "TR_WH_IN_NAME TEXT, "
                + "TR_WH_OUT_NAME TEXT, "
                + "LINE_NAME TEXT, "
                + "TR_ITEM_PK TEXT, "
                + "TLG_POP_INV_TR_PK TEXT, "
                + "TLG_POP_LABEL_PK TEXT, "
                + "TLG_GD_REQ_M_PK TEXT, "
                + "TLG_GD_REQ_D_PK TEXT, "
                + "TLG_SA_SALEORDER_D_PK TEXT, "
                + "CUST_PK TEXT, "
                + "TR_DATE TEXT, "
                + "STATUS TEXT, "
                + "STATUS_MSG TEXT, "
                + "INCOME_DATE TEXT, "
                + "SCAN_DATE TEXT, "
                + "SCAN_TIME TEXT, "
                + "SENT_YN TEXT, "
                + "CHARGER TEXT, "
                + "SENT_TIME TEXT,"
                + "grade TEXT,"
                + "EXECKEY TEXT,"
                + "PROCESS_TYPE TEXT,"
                + "DEL_IF INTEGER default 0 )")

        val CREATE_REQ_TR = ("CREATE TABLE IF NOT EXISTS " + this::class.simpleName +  "_REQ ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "ITEM_BC INTEGER, "
                + "GD_SLIP_NO TEXT, "
                + "DEL_IF INTEGER default 0 )")
        var dp = BaseActivity().getSQLiteDatabase()
//         dp.execSQL("DROP TABLE IF EXISTS " + this::class.simpleName + " ");
//        dp.execSQL("DROP TABLE IF EXISTS " + this::class.simpleName + "_REQ ");
        dp.execSQL(CREATE_INV_TR)
        dp.execSQL(CREATE_REQ_TR)
        dp.close()
    }


}