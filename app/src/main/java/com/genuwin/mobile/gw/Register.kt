package com.genuwin.mobile.gw

import android.R.attr
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.LinearLayout
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.Utils
import kotlinx.android.synthetic.main.activity_register.*

import android.R.attr.bitmap
import android.content.Intent
import android.graphics.Bitmap

import com.google.zxing.qrcode.QRCodeReader
import java.io.ByteArrayOutputStream
import com.google.zxing.common.HybridBinarizer

import android.graphics.BitmapFactory
import android.os.Environment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.base.BaseDetailFragment
import com.google.zxing.*
import com.google.zxing.ChecksumException

import com.google.zxing.MultiFormatReader

import com.google.zxing.BinaryBitmap

import com.google.zxing.RGBLuminanceSource

import com.google.zxing.LuminanceSource
import com.lamvuanh.lib.DataGridView
import java.io.File


class Register : BaseFragment(){
    lateinit var btnCamera: ImageButton
    lateinit var btnImage: ImageButton
    lateinit var grdData2 : DataGridView

    private val MIN_CLICK_INTERVAL: Long = 600

    private var mLastClickTime: Long = 0
    var isViewClicked = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.activity_register,container,false) as LinearLayout
//        btnCamera = root.findViewById(R.id.btnCamera)
//        btnCamera.setOnClickListener{
//            val currentClickTime = SystemClock.uptimeMillis()
//            val elapsedTime = currentClickTime - mLastClickTime
//
//            mLastClickTime = currentClickTime
//
//            if (elapsedTime <= MIN_CLICK_INTERVAL) {
//                isViewClicked = false
//                BaseActivity().openPickImage(BaseActivity().PICK_IMAGE)
//
//            }else{
//                if (!isViewClicked) {
//                    isViewClicked = true
//                    startTimer()
//                }
//            }
//        }
//
//        btnImage  = root.findViewById(R.id.btnImage)
        grdData2 = root.findViewById(R.id.grdData2)
        grdData2.BeginInit()
        grdData2.widthWithpercent = true
        grdData2.addColumns(
            arrayOf(
                DataGridView.Column("PK", 0),
                DataGridView.Column("VIS_NAME", 300),
                DataGridView.Column("COM_NAME", 100 ),
                DataGridView.Column("COVID", 0)


            )
        )

        grdData2.EndInit()

        grdData2.addRow(arrayOf("1","1","ARIC","") )

        grdData2.itemClickListener = object : DataGridView.HandlerItemClick(){
            override fun onClickCell(position: Int, col: Int):Boolean {
                handler.post {
                    slipNoOnClick(position)
                }

                return true
            }
        }
        doLoadFirst()
        return root
    }

    fun doLoadFirst(){
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LIST_VISITOR_NOCACHE",
                lang + "|" + userid
            )

            if (result.isError){
                BaseActivity().showToast(result.message)
            }else{
                handler.post(Runnable {
                    grdData2.setData(result.getData())
                })
            }



            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    private var lastIndex : Int = -1

    private fun slipNoOnClick(it: Int) {
        lastIndex = it
        //TODO code here
        try{
            //val slip_no = grdData2.Rows().get(it).getCell()[3].value.toString()


                val currentClickTime = SystemClock.uptimeMillis()
                val elapsedTime = currentClickTime - mLastClickTime

                mLastClickTime = currentClickTime

                if (elapsedTime <= MIN_CLICK_INTERVAL) {
                    isViewClicked = false
                    BaseActivity().openPickImage(BaseActivity().PICK_IMAGE)

                }else{
                    if (!isViewClicked) {
                        isViewClicked = true
                        startTimer()
                    }
                }



        }catch (ex: Exception){

        }


    }

    private fun startTimer() {
        handler.postDelayed(Runnable { onSingleClick() }, MIN_CLICK_INTERVAL)
    }

    fun onSingleClick(){
        if(isViewClicked)
            BaseActivity().openCamera(BaseActivity().CAMERA_IMAGE)
    }

    override fun onCameraResult(requestCode: Int, pathImage: String) {
        if (requestCode == BaseActivity().CAMERA_IMAGE || requestCode == BaseActivity().PICK_IMAGE ) {

            if (lastIndex == -1){
                return;
            }



            var list = listOf<String> (
                "0",
                "",
                pathImage
            )
            var uri = Uri.parse(pathImage)


            val bmOptions = BitmapFactory.Options()
            val image = BitmapFactory.decodeFile(pathImage, bmOptions)



            var slip_no = "";

            if (grdData2.Rows().get(lastIndex).getCell()[3].value != null  ){
                slip_no = grdData2.Rows().get(lastIndex).getCell()[3].value.toString()
            }


            var flag = ""
            if(slip_no.length < 5){
                slip_no = readQRImage(image)
                slip_no?.let { grdData2.Rows().get(lastIndex).setCellData(3, it) }
                flag ="Covid"
            }






            val userPK = grdData2.Rows().get(lastIndex).getCell()[0].value.toString()

            var dp = BaseActivity().getSystemSQLiteDatabase()


            var para1 =  "|"    + "|"+ userPK + "|" + slip_no.replace('|','/') + "||" + userid

            var sql1 =
                "INSERT INTO PROCEDURE_LAZY(IMG_URI,PARA,TITLE,PROCEDURE,DEL_IF,PROCESS_YN,IMG_THUMB)  VALUES('" + pathImage + "','" + para1 + "','','" + "LG_QC_MT_REQ_UPD_IMG" + "'," + "0,'" + "N" + "','N');"

            try {
                dp.execSQL(
                    sql1
                )
                dp.close()

            } catch (exx: java.lang.Exception) {
                BaseActivity().showToast(exx.message.toString())
            }




        }
    }
    fun readQRImage(bMap: Bitmap): String {
        var contents: String? = null
        val intArray = IntArray(bMap.width * bMap.height)
        //copy pixel data from the Bitmap into the 'intArray' array
        bMap.getPixels(intArray, 0, bMap.width, 0, 0, bMap.width, bMap.height)
        val source: LuminanceSource = RGBLuminanceSource(bMap.width, bMap.height, intArray)
        val bitmap = BinaryBitmap(HybridBinarizer(source))
        val reader: Reader = MultiFormatReader() // use this otherwise ChecksumException
        try {
            val result = reader.decode(bitmap)
            contents = result.text
            //byte[] rawBytes = result.getRawBytes();
            //BarcodeFormat format = result.getBarcodeFormat();
            //ResultPoint[] points = result.getResultPoints();
        } catch (e: NotFoundException) {
            e.printStackTrace()
        } catch (e: ChecksumException) {
            e.printStackTrace()
        } catch (e: FormatException) {
            e.printStackTrace()
        }
        return contents.toString()
    }
}