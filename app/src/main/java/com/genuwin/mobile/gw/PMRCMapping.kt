package com.genuwin.mobile.gw

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.MainActivity
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseDetailFragment
import com.lamvuanh.lib.DataGridView

class PMRCMapping : BaseFragment() {

    lateinit var dataview: DataGridView
    lateinit var btnStart: Button
    lateinit var btnClear: Button
    lateinit var btnMapping: Button

    lateinit var txtCountBC: TextView
    lateinit var txtWI: TextView



     var wi_pk: String =""
     var item_pk: String =""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var root = inflater.inflate(R.layout.fragment_rfid_rcmapping,container,false) as LinearLayout

        dataview = root.findViewById(R.id.dataview)

        dataview.BeginInit()

        dataview.addColumn( getString(R.string.barcode), 6)
        dataview.addColumn( getString(R.string.status), 4)
        dataview.widthWithpercent  = true
        dataview.EndInit()

        btnStart = root.findViewById(R.id.btnStart)
        btnStart.setOnClickListener {
            btnStartClick()
        }

        btnClear = root.findViewById(R.id.btnClear)
        btnClear.setOnClickListener {
            btnClearClick()
        }

        btnMapping = root.findViewById(R.id.btnMapping)
        btnMapping.setOnClickListener {
            btnMappingClick()
        }

        txtCountBC = root.findViewById(R.id.txtCountBC)
        txtWI = root.findViewById(R.id.txtWI)



        handler.postDelayed(Runnable { openWI() }, 200)
        return root
    }

    fun  btnStartClick(){
        if(isProcess) return
        (BaseActivity() as MainActivity ) .RfidControl()
    }

    fun btnClearClick(){
        handler.post {
            dataview.clearAll()
            wi_pk =""
            txtWI.text =""
            txtCountBC.text = "0"
        }
    }

    var isProcess = false
    fun btnMappingClick(){
        if(isProcess)
            return
        isProcess = true
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)



            var lstBC = "";

            for( e in dataview.Rows()){
                if(e.getCell()[1].value == "-")
                    lstBC = lstBC +","+e.getCell()[0].value


            }

            val result = BaseActivity().getWebService().GetDataTableArg(
                "LG_UPD_POP_RFID",
                lstBC+ "|" + wi_pk + "|" + item_pk + "|" +userid
            )




            if (result.isError){
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })

            }else{
                handler.post {
                    dataview.BeginInit()
                    for( e in dataview.Rows()){
                        if(e.getCell()[1].value == "-")
                            e.setCellData(1,"Mapping")


                    }

                    dataview.EndInit()


                }

            }
            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })


            isProcess = false
        }).start()

    }

    var open_wi: Boolean = false
     fun openWI(){
        if (open_wi )
            return

        open_wi = true
        val intent = Intent(BaseActivity().baseContext, DetailActivity::class.java).apply {
            putExtra(BaseDetailFragment.ARG_FRAMENT_NAME, "PMWI")
            putExtra(BaseDetailFragment.ARG_FRAMENT_TITLE, getString(R.string.wi))
            putExtra(BaseDetailFragment.ARG_ITEM_ID_M, "85")
         

        }
        BaseActivity().startActivityForResult(intent,997)

    }

    override fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(requestCode == 997){

            try {
                if (data == null){
                    wi_pk = ""
                    item_pk = ""
                    handler.post {
                        txtWI.text = ""
                    }
                }else{
                    wi_pk = data?.getExtras()?.get("PK").toString()
                    item_pk = data?.getExtras()?.get("ITEM_PK").toString()
                    handler.post {
                        txtWI.text = data?.getExtras()?.get("WI_NO").toString()
                    }
                }

            }catch (ex: Exception){

            }


            open_wi = false
        }

    }



    override fun onDataResult(type: Int, data: String) {
        if( type == 1){
            if(isProcess)
                return

            if (wi_pk.isBlank()) {
                (BaseActivity() as MainActivity ) .stopRfidScan()
                openWI()
                return
            }

            for( e in dataview.Rows()){
                if( e.getCell()[0].value.equals(data))
                    return

            }
            dataview.addRow( arrayOf(data,"-"))
            txtCountBC.text = dataview.Rows().count().toString()
        }else if(type == 2){
            BaseActivity().runOnUiThread {
                if( data.equals("0")){
                    btnStart.text = getText(R.string.start)
                    btnClear.isEnabled = true
                    btnMapping.isEnabled = true
                }else{
                    btnStart.text = getText(R.string.stop)
                    btnClear.isEnabled = false
                    btnMapping.isEnabled = false
                }
            }

        }


        //super.onDataResult(type, data)
    }
}