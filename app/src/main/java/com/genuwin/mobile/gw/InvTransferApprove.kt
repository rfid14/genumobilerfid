package com.genuwin.mobile.gw

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView
import com.lamvuanh.lib.DataGridView.ItemCountListener


class InvTransferApprove : BaseFragment() {

    lateinit var lstData: DataGridView
    lateinit var editBC: EditText
    lateinit var txtRepNo: TextView
    lateinit var txtWHIn: TextView
    lateinit var txtError: TextView
    lateinit var txtTotalBot: TextView
    lateinit var txtLocIn: TextView
    lateinit var txtCurrentBC: TextView
    lateinit var btnApprove: TextView
    lateinit var btnClear: Button
    lateinit var btnSetLoc: Button
    lateinit var btnSelectAll: Button

    private var onConnectServer = false


    private var req_pk = ""
    private var wh_pk = ""
    private var loc_pk = ""

    var lstLoc: Array<Array<String>> = emptyArray<Array<String>>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var rootView = inflater.inflate(
            R.layout.fragment_inv_transfer_approve,
            container,
            false
        ) as LinearLayout
        txtTotalBot = rootView.findViewById<View>(R.id.txtTotalBot) as TextView


        lstData = rootView.findViewById<View>(R.id.listData) as DataGridView

        lstData.BeginInit()
        lstData.addColumns(
            arrayOf(
                DataGridView.Column("PK", 0),
                DataGridView.Column("M_PK", 0),
                DataGridView.Column("Seq", 40),
                DataGridView.Column("Item BC", 120),
                DataGridView.Column("Loc PK", 0),
                DataGridView.Column("Loc", 100),
                DataGridView.Column("Lot No", 70),
                DataGridView.Column("Roll ID", 70),
                DataGridView.Column("Grade", 70),
                DataGridView.Column("Qty", 70),
                DataGridView.Column("Qty UOM", 70),
                DataGridView.Column("Length QTY", 100),
                DataGridView.Column("UOM", 70),
                DataGridView.Column("PO NO", 200),
                DataGridView.Column("COLOR", 200),
                DataGridView.Column("ITEM CODE", 200),
                DataGridView.Column("ITEM NAME", 250)
            )
        )

        lstData.EndInit()


        lstData.itemClickListener = object : DataGridView.HandlerItemClick() {
            override fun onClickCell(position: Int, col: Int): Boolean {

                //  OnLoadBC(dateview.getDataCell(position,0))
                return false
            }
        }

        lstData.itemCountListener = object : ItemCountListener() {
            override fun onChange(`object`: Int) {
                txtTotalBot.text = "Total: $`object`"
            }
        }


        lstData.isSelectedEnabled = true

        txtRepNo = rootView.findViewById<View>(R.id.txt_reqNo) as TextView
        txtError = rootView.findViewById<View>(R.id.txtError) as TextView
        txtWHIn = rootView.findViewById<View>(R.id.txtWHIn) as TextView
        txtLocIn = rootView.findViewById<View>(R.id.txtLocIn) as TextView
        txtCurrentBC = rootView.findViewById<View>(R.id.txtCurrentBC) as TextView


        btnSetLoc =
            rootView.findViewById<View>(R.id.btnSetLoc) as Button

        btnSetLoc.setOnClickListener { setLoc() }
        btnSelectAll =
            rootView.findViewById<View>(R.id.btnSelectAll) as Button
        btnSelectAll.setOnClickListener { onSelectAll() }


        btnClear = rootView.findViewById<View>(R.id.btnClear) as Button
        btnClear.setOnClickListener {
            BaseActivity().confirmDialog(
                getString(R.string.confirm),
                getString(R.string.clear),
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        Clear()

                    }
                })
        }

        btnApprove =
            rootView.findViewById<View>(R.id.btnApprove) as Button
        btnApprove.setOnClickListener {
            BaseActivity().confirmDialog(
                getString(R.string.confirm),
                getString(R.string.confirm),
                object :
                    BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        doConfirm()

                    }
                })

        }



        editBC = rootView.findViewById<View>(R.id.editBC) as EditText

        editBC.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER) {
                if (event.action === KeyEvent.ACTION_DOWN) {
                    OnSaveBC()
                    editBC.setText("")
                    editBC.requestFocus()
                }
                return@OnKeyListener true
            }
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                editBC.clearFocus()
                Thread.interrupted()
            }
            false
        })

        txtError.hint = "Pls Scan Req No!!!"

        return rootView
    }

    fun setLoc() {
        if (loc_pk.isEmpty()) {
            handler.post { Runnable { txtError.text = "Pls scan Location!!!" }  }
        } else {
            handler.post(Runnable {
                for (i in lstData.Rows().indices) {
                    if (lstData.Rows()[i].isSelectMode) {
                        lstData.setDataCell(4,i,loc_pk)
                        lstData.setDataCell(5,i, txtLocIn.text.toString())
//                        lstData.Rows()[i].setCellData(4, loc_pk)
//                        lstData.Rows()[i].setCellData(5, txtLocIn.text.toString())
                    }
                }

            })


        }
    }

    fun OnSaveBC() {
        txtError.text = ""

        val bc = editBC.text.toString()
        if (bc.length == 0) {
            txtError.text = "Pls scan bc!!!"
            return
        }




        if (onConnectServer) {
            txtError.text = "Pls waiting...."
            return
        }

        handler.post { txtCurrentBC.text = bc }


        var bcSlip = true
        val repno = txtRepNo.text.toString()


        if (repno.length > 0) {
            bcSlip = false
        }


        if (bcSlip) {
            // txtError.text = "Loading Data....."
            getInfoReqNo(bc)
        } else {
            if (bc.startsWith("L")) { //LOCALION
                for (i in 0 until lstLoc.size) {
                    if (bc.toUpperCase() == lstLoc.get(i).get(2).toUpperCase()) {
                        loc_pk = lstLoc.get(i).get(0)
                        handler.post {
                            txtError.text = "Pls scan barcode!!! "
                            txtLocIn.text = lstLoc.get(i).get(1).toString()
                        }
                        return
                    }
                }
                handler.post { txtError.text = "Location does not exist " }
            } else {
                if (loc_pk !== "") {
                    var flag = false
                    for (i in lstData.Rows().indices) {
                        if (lstData.Rows()[i].getCell()[3].toString()
                                .trim { it <= ' ' } == bc.trim { it <= ' ' }
                        ) {
                            flag = true
                            lstData.Rows()[i].setCellData(4, loc_pk)
                            lstData.Rows()[i].setCellData(5, txtLocIn.text.toString())
                            //lstData.Rows().get(i).setSelectMode(true);
                            break
                        }
                    }
                    if (!flag) {
                        handler.post { txtError.text = "BC does not exist in req" }
                    }
                } else {
                    handler.post { txtError.text = "Pls scan location!!! " }
                }
            }


            //CHECK LOC
        }
    }

    fun getInfoReqNo(repno: String) {
        handler.post(Runnable {
            BaseActivity().showProgressBar(getString(R.string.loading))
        })

        Thread(Runnable {
            Thread.sleep(1000)


            val result = BaseActivity().getWebService().GetDataTableArg(
                "lg_mposv2_sel_trans_in_req",
                repno
            )

            if (result.isError) {
                handler.post(Runnable {
                    BaseActivity().showToast(result.message)
                })
            } else {

                if (result.data.size == 0) {
                    handler.post(Runnable {
                        txtError.text = "This Slip No does not exist "
                    })

                } else {

                    req_pk = result.data[0][0].toString()
                    wh_pk = result.data[0][2].toString()


                    handler.post {
                        txtRepNo.text = result.data[0][1].toString()
                        txtWHIn.text = result.data[0][3].toString()
                        //todo Add more col info
                    }
                }


            }

            if (!req_pk.isNullOrEmpty()) {
                val result2 = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_sel_trans_in_req_d",
                    req_pk
                )

                if (result2.isError) {
                    handler.post(Runnable {
                        BaseActivity().showToast(result2.message)
                    })
                } else {


                    handler.post {
                        lstData.clearAll()
                        lstData.setData(result2.data)
                    }


                }

                val result3 = BaseActivity().getWebService().GetDataTableArg(
                    "lg_mposv2_get_loc",
                    wh_pk
                )

                if (result3.isError) {
                    handler.post(Runnable {
                        BaseActivity().showToast(result3.message)
                    })
                } else {

                    lstLoc = result3.data


                }


            }


            handler.post(Runnable {
                BaseActivity().hideProgressBar()
            })
        }).start()
    }

    private fun Clear() {
        req_pk = ""
        wh_pk = ""
        loc_pk = ""
        handler.post {
            txtRepNo.text = ""
            txtError.text = ""
            txtWHIn.text = ""
            txtLocIn.text = ""
            txtCurrentBC.text = ""
            txtError.text = ""
            lstData.clearAll()
        }
    }

    fun onSelectAll() {
        lstData.selectAll()
    }

    fun doConfirm() {
        var flag = false
        lstData.Rows().forEachIndexed { index, row ->
            if (row.getCell()[4].toString().isNullOrEmpty()) {
                BaseActivity().showToast("Pls set Location")
                flag = true
                return@forEachIndexed
            }
        }

        if (!flag) {
            handler.post(Runnable {
                BaseActivity().showProgressBar(getString(R.string.loading))
            })

            Thread(Runnable {
                Thread.sleep(1000)

                try {
                    var data = ""
                    var key = ""
                    for (i in lstData.Rows().indices) {
                        val pk_d = lstData.Rows()[i].getCell()[0].toString()
                        val pk_loc = lstData.Rows()[i].getCell()[4].toString()
                        data = "$data$key$pk_d-$pk_loc"
                        key = ","
                    }


                    val result = BaseActivity().getWebService().TableReadOpenString(
                        "lg_mposv2_upd_trans_in_req",
                        "$req_pk|$data|$userid"
                    )

                    if (result.isError) {
                        handler.post(Runnable {
                            BaseActivity().showToast(result.message)
                        })
                    } else {


                            handler.post { txtError.text = result.data }




                    }
                } catch (ex: Exception) {
                    handler.post { txtError.text = ex.message }
                }

                handler.post(Runnable {
                    BaseActivity().hideProgressBar()
                })
            }).start()
        }
    }
}