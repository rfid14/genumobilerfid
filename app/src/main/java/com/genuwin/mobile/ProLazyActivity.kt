package com.genuwin.mobile

import android.os.Bundle
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView




class ProLazyActivity : BaseActivity(){
    lateinit var dataView: DataGridView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prolazy)

        var sys: ImageView = findViewById(R.id.btnReload)
        sys.setOnClickListener {
            onSys()
        }
        var clear: ImageView = findViewById(R.id.btnClear)
        clear.setOnClickListener{
            onClear()
        }
        var clearAll: ImageView = findViewById(R.id.btnClearAll)
        clearAll.setOnClickListener{
            onClearAll()
        }
        dataView = findViewById(R.id.dataGridView)

        dataView.BeginInit()



        dataView.addColumn(
            "PK", DataGridView.ViewMath()
                .convertDpToPixelInt(60.0.toFloat(), this)
        )

        dataView.addColumn(
            DataGridView.Column(
            "IMG", DataGridView.ViewMath()
                .convertDpToPixelInt(100.0.toFloat(), this),
            DataGridView.CELL_TYPE.TYPE_IMAGEVIEW)
        )

        dataView.addColumn(
            "THUMB_YN", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), this)
        )

        dataView.addColumn(
            "TITLE", DataGridView.ViewMath()
                .convertDpToPixelInt(200.0.toFloat(), this)
        )
        dataView.addColumn(
            "PROCESS_YN", DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(), this)
        )


        dataView.EndInit()

        onSys()
    }


    fun onSys(){
        var dp = getSystemSQLiteDatabase()
        var cursor = dp.rawQuery(
            " select PK, IMG_URI,PROCEDURE,PARA,TITLE,IMG_THUMB,PROCESS_YN  from PROCEDURE_LAZY where DEL_IF = 0 order by PK desc ;  ",
            null
        )
        handler.post(Runnable {
            dataView.clearAll()
        })

        var count: Int = cursor.count
        if (cursor != null && cursor.moveToFirst()) {
            var i = 1
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    val arr = arrayOf(
                        cursor.getString(cursor.getColumnIndex("PK")),
                        cursor.getString(cursor.getColumnIndex("IMG_URI")),
                        cursor.getString(cursor.getColumnIndex("IMG_THUMB")),
                        cursor.getString(cursor.getColumnIndex("TITLE")),
                        cursor.getString(cursor.getColumnIndex("PROCESS_YN"))
                    )
                    handler.post(Runnable {
                        dataView.addRow(
                            arr
                        )
                    })
                    i++
                    cursor.moveToNext();
                }


            }
        }
        cursor.close()
        dp.close()
    }

    fun onClear(){
        val sql =
            "DELETE FROM  PROCEDURE_LAZY   where PROCESS_YN = 'Y'"
        var dp = getSystemSQLiteDatabase()
        dp.execSQL(sql)
        dp.close()

        onSys()
    }

    fun onClearAll(){
        val sql =
            "DELETE FROM  PROCEDURE_LAZY  "
        var dp = getSystemSQLiteDatabase()
        dp.execSQL(sql)
        dp.close()


        val folder = getExternalFilesDir(null)
        if (folder != null) {
            if (folder.isDirectory)
                if (folder != null) {
                    for (child in folder.listFiles())
                        child.delete()
                }
        }


        onSys()
    }
}