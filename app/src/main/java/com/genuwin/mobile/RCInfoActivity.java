package com.genuwin.mobile;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.lamvuanh.lib.DataGridView;

public class RCInfoActivity extends AppCompatActivity {

    DataGridView dataview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rcinfo);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.

//            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("R/C Info");
        }


        dataview = findViewById(R.id.dataview);

        dataview.BeginInit();

        dataview.addColumn( getString(R.string.barcode), 120);
        dataview.addColumn( getString(R.string.item_code), 120);
        dataview.addColumn( getString(R.string.lot_no), 100);
        dataview.addColumn( getString(R.string.status), 100);
        dataview.addColumn( getString(R.string.item_name), 200);

        dataview.EndInit();
    }
}