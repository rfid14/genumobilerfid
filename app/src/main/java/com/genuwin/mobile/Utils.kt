package com.genuwin.mobile

import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    fun md5(s: String): String {
        try { // Create MD5 Hash
            val digest = MessageDigest.getInstance("MD5")
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()
            // Create Hex String
            val hexString = StringBuffer()
            for (i in messageDigest.indices) hexString.append(
                Integer.toHexString(
                    0xFF and messageDigest[i].toInt()
                )
            )
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    fun md5B(s: String): String {
        val MD5 = "MD5"
        try { // Create MD5 Hash
            val digest = MessageDigest
                .getInstance(MD5)
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()
            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2) h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }

    val dateyyyyMMdd: String
        get() {
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("yyyyMMdd")
            return df.format(c.time)
        }

    val dateIncline: String
        get() {
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("dd/MM/yyyy")
            return df.format(c.time)
        }

    val date_ddMMyy: String
        get() {
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("dd/MM/yy")
            return df.format(c.time)
        }

    val dateyyyyMMddhhmmss: String
        get() {
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("yyyyMMddhhmmss")
            return df.format(c.time)
        }

    val dateYMDHHmmss: String
        get() {
            val c = Calendar.getInstance()
            val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            return df.format(c.time)
        }

    fun getDatePrevious(dtime: Int): String {
        val curDate = Calendar.getInstance()
        curDate.add(Calendar.DATE, -dtime)
        val df = SimpleDateFormat("yyyyMMdd")
        return df.format(curDate.time)
    }

    //Format String yyyymmdd to yyyy-mm-dd
    fun getDateFormat(date: String): String {
        var result = ""
        for (i in 0 until date.length) {
            result += date[i]
            if (i == 3 || i == 5) result += "-"
        }
        return result
    }

    fun getFormatYYYYMMDD(yymmdd: String): String {
        try {
            val dt = SimpleDateFormat("yyyyMMdd")
            var date: Date? = null
            try {
                date = dt.parse(yymmdd)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val dt1 = SimpleDateFormat("yyyy/MM/dd")
            return dt1.format(date)
        } catch (e: Exception) {
            Log.e("getFormatYYYYMMDD: ", e.message.toString())
        }
        return yymmdd
    }


    fun getFormatDDMMYYYY(yymmdd: String): String {
        try {
            val dt = SimpleDateFormat("yyyyMMdd")
            var date: Date? = null
            try {
                date = dt.parse(yymmdd)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val dt1 = SimpleDateFormat("dd/MM/yyyy")
            return dt1.format(date)
        } catch (e: Exception) {
            Log.e("getFormatYYYYMMDD: ", e.message.toString())
        }
        return yymmdd
    }

    fun getFormatDDMMYYYY2(yymmdd: String): String {
        try {
            val dt = SimpleDateFormat("dd/MM/yyyy")
            var date: Date? = null
            try {
                date = dt.parse(yymmdd)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            val dt1 = SimpleDateFormat("yyyyMMdd")
            return dt1.format(date)
        } catch (e: Exception) {
            Log.e("getFormatYYYYMMDD: ", e.message.toString())
        }
        return yymmdd
    }


    fun getStringToDate(ddMMyyyy: String): Calendar {
        var date = Calendar.getInstance()
        if (ddMMyyyy.length != 8) {
            return date;
        }
        try {
            var dd = ddMMyyyy.substring(0, 2)
            var MM = ddMMyyyy.substring(2, 4)
            var yyyy = ddMMyyyy.substring(4, 8)


            date.set(Calendar.YEAR, yyyy.toInt())
            date.set(Calendar.MONTH, MM.toInt())
            date.set(Calendar.DAY_OF_MONTH, dd.toInt())

        } catch (ex: java.lang.Exception) {
            return date;
        }


        return date
    }

    fun getStringToDate2(yyyyMMdd: String): Calendar {
        var date = Calendar.getInstance()
        if (yyyyMMdd.length != 8) {
            return date;
        }
        try {
            var yyyy = yyyyMMdd.substring(0, 4)
            var MM = yyyyMMdd.substring(4, 6)
            var dd = yyyyMMdd.substring(6, 8)


            date.set(Calendar.YEAR, yyyy.toInt())
            date.set(Calendar.MONTH, MM.toInt()-1)
            date.set(Calendar.DAY_OF_MONTH, dd.toInt())

        } catch (ex: java.lang.Exception) {
            return date;
        }


        return date
    }

    fun getStringddMMyyyyToyyyyMMdd(ddMMyyyy: String): String {
        if (ddMMyyyy.length != 8) {
            return ddMMyyyy;
        }

        var dd = ddMMyyyy.substring(0, 2)
        var MM = ddMMyyyy.substring(2, 4)
        var yyyy = ddMMyyyy.substring(4, 8)






        return yyyy + MM + dd
    }

    fun getyyyyMMdd(): String {
        var c = Calendar.getInstance()
        var df = SimpleDateFormat("yyyyMMdd")
        return df.format(c.getTime())
    }

    fun getYMDHHmmss(): String? {
        var c = Calendar.getInstance()
        var df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return df.format(c.getTime())
    }
}