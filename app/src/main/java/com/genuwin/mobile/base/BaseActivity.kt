package com.genuwin.mobile.base

import android.Manifest
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.*
import android.content.pm.ActivityInfo
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.DialogFragment
import androidx.preference.PreferenceManager
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.ManagePermissions
import com.genuwin.mobile.R
import com.genuwin.mobile.Service.WebService
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import androidx.core.app.ActivityCompat.startActivityForResult

import android.content.Intent
import android.util.Log


open class BaseActivity : AppCompatActivity() {
    var handler: Handler = Handler(Looper.getMainLooper())

    val PICK_IMAGE = 9998
    val CAMERA_IMAGE  = 9999

    lateinit var client_id: String
    var MINDEX : Int = -1
    var MINDEXFUNCTIONID : String = "" /*thuan.dang 20200509 get function id*/
    var FUNC_SEP_ID :String = ""  /*thuan.dang 20200509 dung de phan biet 2 menu cung 1function id */
    var MENU_NAME :String = "" /*thuan.dang 20200509 add */


    lateinit var deviceID: String
    var cusFragment: BaseFragment? = null



    val PermissionsRequestCode = 300
    public lateinit var managePermissions: ManagePermissions

   // private lateinit var webService: WebService
    fun getWebService(): WebService {
//            val type_server =   getSharedPreferences().getBoolean("login_type_sever", false) ;
//            val dbServerPublic  = getSharedPreferences().getString(
//                "server_address_public",
//                getString(R.string.pref_server_address_default)
//            )
//
//            val dbServer = getSharedPreferences().getString(
//                "server_address",
//                getString(R.string.pref_server_address_default)
//            )
//            val dbName = getSharedPreferences().getString(
//                "server_company",
//                getString(R.string.pref_server_company_default)
//            )
            val dbData = getSharedPreferences().getString(
                "server_database",
                getString(R.string.pref_server_database_default)
            )
            val dbUser = getSharedPreferences().getString(
                "server_username",
                getString(R.string.pref_server_username_default)
            )
            val dbPass = dbUser + "2"

            val url = getLinkService()+ "gwWebservice.asmx"
            var webService = WebService(
                url,
                dbData,
                dbUser,
                dbPass
            )

        return webService
    }

    fun getLinkService(): String {


            val type_server =   getSharedPreferences().getBoolean("login_type_sever", false) ;
            val dbServerPublic  = getSharedPreferences().getString(
                "server_address_public",
                getString(R.string.pref_server_address_default)
            )

            val dbServer = getSharedPreferences().getString(
                "server_address",
                getString(R.string.pref_server_address_default)
            )
            val dbName = getSharedPreferences().getString(
                "server_company",
                getString(R.string.pref_server_company_default)
            )
//            val dbData = getSharedPreferences().getString(
//                "server_database",
//                getString(R.string.pref_server_database_default)
//            )
//            val dbUser = getSharedPreferences().getString(
//                "server_username",
//                getString(R.string.pref_server_username_default)
//            )
//            val dbPass = dbUser + "2"

//        ====================================================================================================
            val url = if (type_server) {
                "http://$dbServerPublic/$dbName/"
            }else{
                "http://$dbServer/$dbName/"
            }


//        val url ="http://test.gasp.genuwinsolution.com/kolonbd/";


        return url
    }

    private lateinit var sharedPrefs: SharedPreferences
    fun defaultPreference(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)

    fun getSharedPreferences(): SharedPreferences {
        if (!::sharedPrefs.isInitialized) {
            sharedPrefs = defaultPreference(this.baseContext)
        }
        return sharedPrefs
    }

    fun getDefaultPackageName():String {
        return "com.genuwin.mobile"
    }



    fun  getSystemSQLiteDatabase() : SQLiteDatabase {

            var  dbsystem = openOrCreateDatabase("gwsystem", MODE_PRIVATE, null)
            try {
                var config = Class.forName( getDefaultPackageName() + ".gw." + "Config").newInstance() as BaseConfig
                config.doConfig(dbsystem)
            } catch (ex: java.lang.Exception) {

            }



        return dbsystem
    }




    fun  getSQLiteDatabase() : SQLiteDatabase {

            var data_name =  if(!::client_id.isInitialized ||  client_id.isNullOrEmpty()){
                "gasp";
            }else{
                client_id
            }

            var  db = openOrCreateDatabase(data_name, MODE_PRIVATE, null)


            try {
                var config = Class.forName(getDefaultPackageName() + "." + client_id + "." + "Config").newInstance() as BaseConfig
                config.doConfig(db)
            } catch (ex: java.lang.Exception) {

            }

            return db

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var client_key = getSharedPreferences().getString(
            "client_key",
            getString(R.string.pref_server_company_default)
        )

        if (client_key != null) {

            if (client_key == "pms"){
                client_id = "gw"
            }else{
                client_id = client_key
            }

        } else {
            client_id = "gw"
        }



        val scale = resources.configuration.fontScale

        //showToast(scale.toString())


    }






    interface ConfirmDialogCallBack{
        fun callBack(bool: Boolean) {}
    }
    fun confirmDialog(title: String, body: String, dialogCallback: ConfirmDialogCallBack){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(body)

        builder.setPositiveButton(
            R.string.yes,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(true)
                dialog.cancel()
            })
        builder.setNegativeButton(
            R.string.no,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(false)
                dialog.cancel()
            })

        builder.show()
    }

    fun confirmDialogPic(title: String, body: String, dialogCallback: ConfirmDialogCallBack){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(body)

        builder.setPositiveButton(
            R.string.update_images,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(true)
                dialog.cancel()
            })
        builder.setNegativeButton(
            R.string.show_images,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(false)
                dialog.cancel()
            })

        builder.show()
    }

    fun confirmDialogPicMaster(title: String, body: String, dialogCallback: ConfirmDialogCallBack){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setMessage(body)

        builder.setPositiveButton(
            R.string.delete_images,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(true)
                dialog.cancel()
            })
        builder.setNegativeButton(
            R.string.show_images,
            DialogInterface.OnClickListener { dialog, _ ->
                dialogCallback.callBack(false)
                dialog.cancel()
            })

        builder.show()
    }

    interface ListDialogCallBack{
        fun callBack(it: Int) {}
    }

    fun listDialogingleChoic(
        title: String,
        array: Array<String>,
        dialogCallback: ListDialogCallBack
    ){
        lateinit var dialog: AlertDialog
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
        builder.setSingleChoiceItems(array, -1, { _, which ->

            dialogCallback.callBack(which)
            dialog.dismiss()

        })

        dialog = builder.create()

        // Finally, display the alert dialog
        dialog.show()
    }

    fun lockOrientation(bool: Boolean) {

        handler.post(Runnable {
            if (bool) {
                requestedOrientation = getScreenOrientation()
                //resources.configuration.orientation = getScreenOrientation()

            } else {
                //resources.configuration.orientation = Configuration.ORIENTATION_UNDEFINED
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            }
        })
    }

    var isSetOrientation: Boolean = false
    fun setOrientation(int: Int) {
        isSetOrientation = true
        handler.post(Runnable {
            requestedOrientation = int
            //resources.configuration.orientation = int
        })
    }

    fun getScreenOrientation(): Int {

        var displayMetrics =  baseContext.resources.displayMetrics
        return if (displayMetrics.widthPixels == displayMetrics.heightPixels) {
            ActivityInfo.SCREEN_ORIENTATION_LOCKED
        } else {
            if (displayMetrics.widthPixels  < displayMetrics.heightPixels) {
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }
        }
    }

    lateinit var progressBar: AlertDialog
    lateinit var progressBarBodyText: TextView

    fun showProgressBar(title: String) {
        var isCreateNew  = false

        if (!::progressBar.isInitialized)
        {
            isCreateNew = true

        }else if(!progressBar.isShowing){
            progressBar.dismiss()
            isCreateNew = true
        }
        if(isCreateNew){

            var progressBarBuilder = AlertDialog.Builder(this)
            var factory = getLayoutInflater()
            var view = factory.inflate(R.layout.layout_loading_dialog, null)
            view.findViewById<TextView>(R.id.dialog_text).text = title
            progressBarBodyText = view.findViewById(R.id.dialog_body)
            progressBarBuilder.setView(view)
            progressBarBuilder.setCancelable(false)
            var progressBarT = progressBarBuilder.create()

            try {

                progressBarT.show()
                progressBar = progressBarT
            }catch (e: Exception){

            }


        }else {
            progressBarBodyText.text = title;
        }


    }

    fun hideProgressBar() {
        if (::progressBar.isInitialized)
        {

            //  progressBar.hide()
            try {
                progressBar.setCanceledOnTouchOutside(true)
                progressBar.dismiss()

            }catch (e: java.lang.Exception){}
        }


    }


    interface callBackDatePickerDialog{
        fun callBack(year: Int, monthOfYear: Int, dayOfMonth: Int){}
    }

    fun showDatePickerDialog(callback: callBackDatePickerDialog){
        val c = Calendar.getInstance()
        val yearc = c.get(Calendar.YEAR)
        val monthc = c.get(Calendar.MONTH)
        val dayc = c.get(Calendar.DAY_OF_MONTH)
        showDatePickerDialog(yearc, monthc, dayc, callback)
    }

    fun showDatePickerDialog(
        yeart: Int,
        monthOfYeart: Int,
        dayt: Int,
        callback: callBackDatePickerDialog
    ){
        val dpd = DatePickerDialog(
            this,
            DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                callback.callBack(year, monthOfYear, dayOfMonth)
            },
            yeart,
            monthOfYeart,
            dayt
        )

            dpd.show()

    }

    interface callBackTimePickerDialog{
        fun callBack(hourOfDay: Int, minute: Int){}
    }

    fun showDateTimeDialog(callback: callBackTimePickerDialog){

        val c = Calendar.getInstance()
        val hourOfDayc = c.get(Calendar.HOUR_OF_DAY)
        val minutec = c.get(Calendar.MINUTE)

        val tpd = TimePickerDialog(
            this,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                callback.callBack(hourOfDay, minute)
            },
            hourOfDayc,
            minutec,
            false
        );
        tpd.show()

    }

    fun showTimerPickerDialog(
        yeart: Int,
        monthOfYeart: Int,
        dayt: Int,
        callback: callBackDatePickerDialog
    ){
       val test = DialogFragment()

    }


    fun showToast(message: String) {
        handler.post {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

    }

    fun getNewDialog(): Dialog{
        val dialog = Dialog(this)
        return dialog
    }
/*thuan.dang 20200514 add : hide keyboard when click other position*/
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    fun openCamera(code: Int){
        val list = listOf<String>(
            Manifest.permission.CAMERA
        )

        var managePermissions = ManagePermissions(this, list, CAMERA_IMAGE, code, true)
        managePermissions.checkPermissions()


    }

    fun openPickImage(code: Int){
        val list = listOf<String>(
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        var managePermissions = ManagePermissions(this, list, PICK_IMAGE, code, true)
        managePermissions.checkPermissions()
    }

    var imageUri : Uri? = null
    fun Permissionsalreadygranted(code: Int, requestCode: Int) {
        if( code == PICK_IMAGE) {

            var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)


            startActivityForResult(intent, requestCode)


        }else  if( code == CAMERA_IMAGE) {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            cameraIntent.putExtra(MediaStore.EXTRA_SIZE_LIMIT,"1048576")
            startActivityForResult(cameraIntent, CAMERA_IMAGE)


//            var    values = ContentValues()
//            values.put(MediaStore.Images.Media.TITLE, "GENUWIN_CAPTURE")
//            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera")
//            this.imageUri = contentResolver.insert(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
//            )
//            var intent =  Intent(MediaStore.ACTION_IMAGE_CAPTURE)
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)
//
//            startActivityForResult(intent, requestCode)
            }else if(code == PermissionsRequestCode){
                recreate()
            }


    }




    fun onChangeStatus() {
        cusFragment?.onChangeStatus()
    }

    open fun onUpdateCountProLazy(){

    }

    fun alertRingMedia() {
        try{
            MediaPlayer.create(this, R.raw.msbeep01).start()
        }catch (ex:java.lang.Exception){

        }


    }






}
