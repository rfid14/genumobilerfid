package com.genuwin.android.base

interface CallBackListener {
    fun callBack(obj: String)
    fun requestProgressDialog(title: String, body:String)
    fun requestProgressDialogDismiss()
}
