package com.genuwin.mobile.base

import android.os.Bundle
import com.genuwin.android.base.BaseFragment

open class BaseDetailFragment : BaseFragment() {
    companion object {

        const val ARG_FRAMENT_NAME = "frament_name"
        const val ARG_FRAMENT_TITLE = "frament_title"
        const val ARG_ITEM_ID_D = "item_id_d"
        const val ARG_ITEM_ID_M = "item_id_m"
        const val ARG_ITEM_ID_D2 = "item_id_d2"
        const val ARG_ITEM_ID_DATA = "item_id_data"


    }


    var id_d = "0"
    var id_d2 = "0"
    var id_m = "0"
    var data = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        arguments?.let {
           // if (it.containsKey(ARG_ITEM_ID_D)) {
                // Load the dummy content specified by the fragment
                // arguments. In a real-world scenario, use a Loader
                // to load content from a content provider.
                id_d = it.getString(ARG_ITEM_ID_D).toString()
                id_d2 = it.getString(ARG_ITEM_ID_D2).toString()
                id_m = it.getString(ARG_ITEM_ID_M).toString()
                data = it.getString(ARG_ITEM_ID_DATA).toString()


            //}
        }
    }
}