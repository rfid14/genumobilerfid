package com.genuwin.mobile.base

import android.database.sqlite.SQLiteDatabase

open class BaseConfig {
    open fun doConfig(db: SQLiteDatabase) {}
}