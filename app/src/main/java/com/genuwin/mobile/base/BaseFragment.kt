package com.genuwin.android.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.genuwin.mobile.DetailActivity
import com.genuwin.mobile.MainActivity
import com.genuwin.mobile.base.BaseActivity


open class BaseFragment : Fragment() {
    var ErrorList = ""


    protected var handler: Handler = Handler()
    lateinit var functionActivity: MainActivity
    lateinit var detailActivity: DetailActivity
    lateinit var callBackListener: CallBackListener

    lateinit var lang :String
    lateinit var userid : String
    lateinit var userpk : String


    var isModified : Boolean = false
    var isStop : Boolean = false


    fun BaseActivity(): BaseActivity {
        if(::detailActivity.isInitialized)
           return  detailActivity
        if(::functionActivity.isInitialized)
            return  functionActivity
        if (activity != null)
            return  activity as BaseActivity


        return  com.genuwin.mobile.base.BaseActivity()
    }

    val classeNameStr: String  get() = this.toString()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity){
            functionActivity = context
        }else if (context is DetailActivity) {
            detailActivity = context
        }
    }


    override fun  onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        setRetainInstance(true)
        lang = BaseActivity().getSharedPreferences().getString("lang", "en").toString()
        userid = BaseActivity().getSharedPreferences().getString("login_user", "").toString()
        userpk = BaseActivity().getSharedPreferences().getString("login_user_pk", "").toString()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)





        translateChildren(view)
        handler.postDelayed(updateDataToServer, (10000).toLong())
    }

    fun translateChildren(v: View) {

        if(v is ViewGroup){
            for (i in 0 until v.childCount) {
                translateChildren(v.getChildAt(i))

            }
        }else{
            if(v.tag != null){
                var tag_children = v.tag.toString()
                if (tag_children.startsWith("key_")) {
                    if(v is TextView ) {


                            handler.post(Runnable {
                                val newTrans = getLangWithKey(tag_children)
                                if (!newTrans.isNullOrEmpty())
                                    v.text = newTrans
                            })
                    }else if (v is Button){
                        handler.post(Runnable {
                            val newTrans = getLangWithKey(tag_children)
                            if (!newTrans.isNullOrEmpty())
                                v.text = newTrans
                        })
                    }
                }
            }
        }

    }

    private fun getLangWithKey(tagChildren: String): CharSequence? {

        var db = BaseActivity().getSystemSQLiteDatabase()
        var cursor =  db.rawQuery(
            "SELECT  TEXT_VALUE  FROM PACK_LANG2 where KEY_TITLE = '$tagChildren'",
            null
        )
        var str  =""
        if (cursor != null && cursor.moveToFirst())
        {
            str = cursor.getString(cursor.getColumnIndex("TEXT_VALUE"))
        }
        cursor.close()
        db.close()
        return str.trim();

    }

    open fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {}
    open fun onDataResult(type: Int, data: String ){}
    open fun onCameraResult(requestCode: Int, pathImage: String) {}
    open fun onChangeStatus() {}
    open fun doProcess(time:String){}



    fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {


        return false
    }

    val updateDataToServer: Runnable = object : Runnable {
        override fun run() {
            val sync_frequency =
                BaseActivity().getSharedPreferences().getString("sync_frequency", "5")


            if(isStop)
                return
            doProcess(sync_frequency.toString())
            SystemClock.sleep(100)


            if (sync_frequency != null) {
                handler.postDelayed(this, (sync_frequency.toInt() * 1000).toLong())
            }

        }
    }


    fun close(){
        isStop = true
        handler.removeCallbacks(updateDataToServer)
        handler.removeCallbacksAndMessages(null)
        this.onDestroy()
    }

    open fun onHanldEvent(code: Int): Boolean {
        return false

    }

}