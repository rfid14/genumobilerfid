package com.genuwin.mobile

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import java.util.*

class SplashActivity : AppCompatActivity() {


    val appID: String = "PRO150KOTLINOFARIC"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(baseContext)

        var lang = sharedPreferences.getString("lang", "en").toString().trim()
        language(lang)


        Handler(Looper.getMainLooper()).postDelayed({
            showPhoneStatePermission()
        }, 1000)

    }

    fun language(language: String) {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val resources = resources
        val configuration = resources.configuration
        configuration.locale = locale
        resources.updateConfiguration(configuration, resources.displayMetrics)

    }

    private fun showPhoneStatePermission() {
        val permissionCheck = ContextCompat.checkSelfPermission(
            this, Manifest.permission.INTERNET
        )
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.INTERNET
                )
            ) {

                showExplanation(
                    "Permission Needed",
                    "Rationale",
                    Manifest.permission.INTERNET,
                    1
                )
            } else {
                requestPermission(Manifest.permission.INTERNET, 1)

            }
        } else {
            var sharedPreferences =
                androidx.preference.PreferenceManager.getDefaultSharedPreferences(baseContext)
            var sharedPreferencesEdit: SharedPreferences.Editor = sharedPreferences.edit()


            sharedPreferencesEdit.putString("appid", appID)
            sharedPreferencesEdit.commit()
            sharedPreferencesEdit.apply()

           // var server_address_public = sharedPreferences.getString("server_address_public", "").toString().trim()
            var server_address = sharedPreferences.getString("server_address", "").toString().trim()
            var server_company = sharedPreferences.getString("server_company", "").toString().trim()
            var server_database =
                sharedPreferences.getString("server_database", "").toString().trim()
            var server_username =
                sharedPreferences.getString("server_username", "").toString().trim()


            //var userid = sharedPreferences.getString("login_user", "").toString().trim()
           // var userpass = sharedPreferences.getString("login_pass", "").toString().trim()


            // dbname , dbuser , dbpass
            // IF DON'T HAVE DOMAIN VALUE CLEAR data info and goto Setting


            if (server_address.isEmpty() || server_company.isEmpty() || server_database.isEmpty() || server_username.isEmpty()) { // first for IT
                sharedPreferencesEdit.putString("server_address_public", "")
                sharedPreferencesEdit.putString("server_address", "")
                sharedPreferencesEdit.putString("server_company", "")
                sharedPreferencesEdit.putString("server_database", "")
                sharedPreferencesEdit.putString("server_username", "")
                sharedPreferencesEdit.commit()
                sharedPreferencesEdit.apply()


                showSettingActivity()


            } else {
                startActivity(Intent(this, LoginActivity::class.java)) // first for user

                finish()
            }


            // close splash activity

        }
    }

    fun showSettingActivity() {
        val intent = Intent(this, SettingsActivity::class.java)
        startActivityForResult(intent, 2) // Activity is started with requestCode 2
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 2) {
            showPhoneStatePermission()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1 -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(baseContext, "Permission Granted!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(baseContext, "Permission Denied!", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun showExplanation(
        title: String,
        message: String,
        permission: String,
        permissionRequestCode: Int
    ) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok,
                DialogInterface.OnClickListener { _, _ ->
                    requestPermission(
                        permission,
                        permissionRequestCode
                    )
                })
        builder.create().show()
    }

    private fun requestPermission(permissionName: String, permissionRequestCode: Int) {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(permissionName), permissionRequestCode
        )
    }

    private fun setupPermissions() {
        val permission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.INTERNET
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {

            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.INTERNET),
            101
        )
    }
}
