package com.genuwin.mobile

import android.Manifest
import android.app.AlertDialog
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.core.view.MenuCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.FragmentTransaction
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.gw.Home
import com.google.android.material.navigation.NavigationView
import com.lamvuanh.lib.CircularImageView
import device.common.rfid.*
import device.sdk.RFIDManager
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {



    lateinit var toolbar: Toolbar
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView
    lateinit var menu: Menu
    lateinit var imageUser: CircularImageView
    lateinit var txtUserName: TextView
    lateinit var idActionLogout: ImageButton
    lateinit var item: MenuItem



    var fragmentID: Int = 0



    lateinit var menuGroup: Array<Array<String>>
    lateinit var menuItems: Array<Array<String>>









    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)






        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, 0, 0
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        menu = navView.menu
        fragmentID = R.id.content_function
        var headerLayout = navView.getHeaderView(0)

        imageUser = headerLayout.findViewById(R.id.imageViewUser)
        txtUserName = headerLayout.findViewById(R.id.txtUserName)
        idActionLogout = headerLayout.findViewById(R.id.id_action_logout)

        idActionLogout.setOnClickListener(View.OnClickListener {
            handler.post(Runnable {
                if(this.cusFragment != null){
                    this.cusFragment!!.close()
                    this.cusFragment = null
                }
                handler.removeCallbacksAndMessages(null)
                startActivity(Intent(this, LoginActivity::class.java))
                finish()
            })
        })

        var userName = getSharedPreferences().getString(
            "login_username",
            "Genuwin User"
        )
        txtUserName.text = userName

        var userImage = getSharedPreferences().getString(
            "login_image",
            ""
        )

        try {
            if(!userImage.isNullOrEmpty() ) {
                val imageBytes = Base64.decode(userImage, 0)
                val image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)

                imageUser.setImageBitmap(image)
            }
        }catch (ex: Exception){

        }

        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(fragmentID, Home())
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()

        doLoadMenu()


        val list = listOf<String>(
             Manifest.permission.CAMERA,
            Manifest.permission.BLUETOOTH,
             Manifest.permission.BLUETOOTH_ADMIN,
                 Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
         Manifest.permission. ACCESS_WIFI_STATE,
            Manifest.permission.READ_CALENDAR,
          Manifest.permission. WRITE_CALENDAR,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission. SYSTEM_ALERT_WINDOW
        )

        // Initialize a new instance of ManagePermissions class


        try{

                deviceID = getDeviceIMEI().toString().toUpperCase()


        }catch (ex: java.lang.Exception){
            managePermissions = ManagePermissions(this, list, PermissionsRequestCode, 1, true)
            managePermissions.checkPermissions()

        }

       // deviceID = "XXXXXXXXXXX";

        getSystemSQLiteDatabase().close()
        handler.postDelayed(updateDataToServer, (10000).toLong())
        onUpdateCountProLazy()


        paramOfInvent = ParamOfInvent()
        modeOfInvent = ModeOfInvent()
        txCycle = TxCycle()

        rfidManager = RFIDManager.getInstance();
        rfidManager!!.RegisterRFIDCallback(mRFIDCallback)

        customIntentConfig = CustomIntentConfig()

        customIntentConfig!!.action = CUSTOM_INTENT
        customIntentConfig!!.category = "RFID"
        customIntentConfig!!.extraRfidData = "DATA"
        rfidManager!!.SetCustomIntentConfig(customIntentConfig)

        var filter = IntentFilter(BluetoothDevice.ACTION_ACL_DISCONNECTED)
        registerReceiver(broadcastReceiver, filter)

        filter = IntentFilter(RFIDConst.ResultType.INTENT_EVENT)
        registerReceiver(broadcastReceiver, filter)

        filter = IntentFilter(CUSTOM_INTENT)
        filter.addCategory(customIntentConfig!!.category)
        registerReceiver(broadcastReceiver, filter)

        //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        //Check EXTERNAL_STORAGE_PERMISSION
        if (!checkPermission()) requestPermission()
        //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//        Log.v("qqq", "onCreate")

        connectRFID()


    }
    //    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    override fun onDestroy() {
        super.onDestroy()
//        Log.v("qqq", "onDestroy")
//        rfidManager!!.UnregisterRFIDCallback(mRFIDCallback)
    }

    override fun onStop() {
        super.onStop()
//        Log.v("qqq", "onStop")
    }

    private fun checkPermission(): Boolean {
        val permissionResult =
            ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (permissionResult == PackageManager.PERMISSION_GRANTED) {
            true
        } else false
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            0
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        try {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.finishAffinity(this)
            }
        } catch (e: ArrayIndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

//    +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

     fun connectRFID() {

         deviceConnected = true
         isOpened = true

//        connectedDeviceMacAddress = getSharedPreferences().getString("scaner_address", "00:19:01:72:D9:E7").toString()
//
//        setConnect(connectedDeviceMacAddress,deviceName)
//
//        if (RFIDConst.CommandErr.SUCCESS == rfidManager?.Open(mConnectedDevice) ) {
//            if (mConnectedDevice != DEVICE_BT) {
//                deviceConnected = true
//            }
//            isOpened = true
//            deviceConfigSetting()
//        } else {
//        }
    }

    val updateDataToServer: Runnable = object : Runnable {
        override fun run() {
            doStart()
            SystemClock.sleep(100)

            val sync_frequency =
               getSharedPreferences().getString("sync_frequency", "5")
            if (sync_frequency != null) {
                handler.postDelayed(this, (sync_frequency.toInt() * 1000).toLong())
            }else{
                handler.postDelayed(this, (10000).toLong())
            }
        }
    }

    fun getDeviceIMEI(): String? {
        var deviceUniqueIdentifier = getSharedPreferences().getString("appsecure", "")

        if (null == deviceUniqueIdentifier || deviceUniqueIdentifier.isEmpty()) {
            deviceUniqueIdentifier = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
            }else{
                Utils().dateyyyyMMddhhmmss
            }
            getSharedPreferences().edit().putString("appsecure", deviceUniqueIdentifier).commit()


        }
        return deviceUniqueIdentifier
    }

    var onUpdateServer = false
    fun doStart() {
        if (onUpdateServer) return

        Thread(Runnable {
            onUpdateServer = true
            Thread.sleep(1000)
            var dp = getSystemSQLiteDatabase()

            var cursor = dp.rawQuery(
                " select PK, IMG_URI,PROCEDURE,PARA,TITLE,IMG_THUMB  from PROCEDURE_LAZY where DEL_IF = 0  and PROCESS_YN = 'N' order by PK asc;  ",
                null
            )

            Log.v("Tag", "onUpdateServer");


            var array: MutableList<Array<String>> = ArrayList()

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor != null && cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        var pk = cursor.getString(cursor.getColumnIndex("PK"))
                        var IMG_URI = cursor.getString(cursor.getColumnIndex("IMG_URI"))
                        var PROCEDURE = cursor.getString(cursor.getColumnIndex("PROCEDURE"))
                        var PARA = cursor.getString(cursor.getColumnIndex("PARA"))
                        var title = cursor.getString(cursor.getColumnIndex("TITLE"))
                        var img_thumb = cursor.getString(cursor.getColumnIndex("IMG_THUMB"))

                        val arr = arrayOf(
                            pk,
                            IMG_URI,
                            PROCEDURE,
                            PARA,
                            title,
                            img_thumb
                        )

                        array.add(arr)



                        cursor.moveToNext();
                    }


                }
            }
            cursor.close()
            dp.close()


            array.forEachIndexed { index, strings ->

                var pk = strings[0]
                var img_uri = strings[1]
                var PROCEDURE = strings[2]
                var PARA = strings[3]
                var title = strings[4]
                var img_thumb = strings[5]


                if (img_uri.length > 0) {
                    var uri = Uri.parse(img_uri)


                    try {
                        val image = BitmapFactory.decodeFile(uri.getPath())



                        var byteArrayOutputStream = ByteArrayOutputStream()
                        image.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream)

                        var byteArrayFull = byteArrayOutputStream.toByteArray()



                        var byte = byteArrayFull

                        var resultImag = getWebService().UploadImageAndThumb(
                            PROCEDURE,
                            byte,
                            PARA
                        )

                        if (resultImag.isError) {

                        } else {
                            val sql =
                                "UPDATE PROCEDURE_LAZY set PROCESS_YN='Y'  where PK = " + pk
                            var dp = getSystemSQLiteDatabase()
                            dp.execSQL(sql)
                            dp.close()

                        }
                    } catch (exx: java.lang.Exception) {
                        showToast(exx.message.toString())

                    }


                } else {
                    val result = getWebService().GetListDataTableArg(
                        PROCEDURE,
                        PARA,
                        1
                    )

                    if (result.isError) {

                    } else {
                        val data = result.getData(0)
                        if (data.totalrows == 1) {

                            var recode = data.records[0]
                            //val pk: String =  recode.get("pk").toString() //ITEM_BC
                            val sql = "UPDATE PROCEDURE_UPDATE set PROCESS_YN='Y'  where PK = " + pk

                            var dp = getSystemSQLiteDatabase()
                            dp.execSQL(sql)

                            dp.close()
                        }
                    }
                }
                onUpdateCountProLazy()


            }


            onUpdateServer = false
        }).start()


    }



//    fun getPath( uri:Uri): String? {
//
//        var cursor:Cursor? = null
//        try {
//            var projection = listOf<String>(MediaStore.Images.Media.DATA)
//             cursor = getContentResolver().query(uri, projection.toTypedArray(), null, null, null)
//
//           var column_index = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
//            cursor?.moveToFirst()
//            return column_index?.let { cursor?.getString(it) }
//        }catch (e: java.lang.Exception){
//            return ""
//        }
//    }

    lateinit var currentPhotoPath: String
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        } else {
            TODO("VERSION.SDK_INT < FROYO")
        }
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if( requestCode == 900){
            connectRFID()
            return
        }

      if(resultCode == RESULT_OK) {


          if (cusFragment != null) {
              if(requestCode == CAMERA_IMAGE ){


                  val bp: Bitmap = data?.getExtras()?.get("data") as Bitmap


                  val timeStamp: String = "GENUWIN_"+ SimpleDateFormat("yyyyMMdd_HHmmss").format(Date()) + ".png"
                  val folder = getExternalFilesDir(null)

                  val dest = File(folder, timeStamp)

                  try {
                      val out = FileOutputStream(dest)
                      bp.compress(Bitmap.CompressFormat.PNG, 90, out)
                      out.flush()
                      out.close()

                      cusFragment!!.onCameraResult(requestCode, dest.absolutePath)
                  } catch (e: java.lang.Exception) {
                      e.printStackTrace()
                  }


                  return
              }else if(requestCode == PICK_IMAGE){
                  val selectedImage = data!!.data
                 // val a = selectedImage.toString()
                  val filePathColumn =
                      arrayOf(MediaStore.Images.Media.DATA)

                  val cursor: Cursor? = contentResolver.query(
                      selectedImage!!,
                      filePathColumn, null, null, null
                  )
                  cursor?.moveToFirst()

                  val columnIndex: Int = cursor?.getColumnIndex(filePathColumn[0]) ?:  -1
                  val picturePath: String = cursor?.getString(columnIndex) ?: ""
                  cursor?.close()

                  val bmOptions = BitmapFactory.Options()
                  val image = BitmapFactory.decodeFile(picturePath, bmOptions)

                  val timeStamp: String = "GENUWIN_"+ SimpleDateFormat("yyyyMMdd_HHmmss").format(Date()) + ".png"
                  val folder = getExternalFilesDir(null)

                  val dest = File(folder, timeStamp)

                  try {
                      val out = FileOutputStream(dest)
                      image.compress(Bitmap.CompressFormat.PNG, 90, out)
                      out.flush()
                      out.close()

                      cusFragment!!.onCameraResult(requestCode, dest.absolutePath)
                  } catch (e: java.lang.Exception) {
                      e.printStackTrace()
                  }



              }else if (data != null) {
                  cusFragment?.onResult(requestCode, resultCode, data)
              } else {

//                       var  thumbnail = MediaStore.Images.Media.getBitmap(
//                            getContentResolver(), imageUri)

              }
          }

      }else{

              cusFragment?.onResult(requestCode, resultCode, data)

      }

    }





    fun doLoadMenu() {

        Thread(Runnable {


            var appid = getSharedPreferences().getString("appid", "")
            var lang = getSharedPreferences().getString("lang", "en")
            var userid = getSharedPreferences().getString("login_user", "")
            var adminyn = getSharedPreferences().getString("login_adminyn", "N")

            val result = getWebService().GetListDataTableArg(
                "LG_mposv2_GET_AUTHORITY",
                appid + "|" + lang + "|" + userid + "|" + adminyn,
                2
            )

            System.out.println(result)
//            Log.v("vvv", .toString())

            if (result.isError) {

                showToast(result.message)

            }
            else {
                try {
//                    Log.v("vvv", result)

                    menuGroup = result.getDataStringArr(0)
                    menuItems = result.getDataStringArr(1)



                    handler.post(Runnable {
                        menu.clear()




                        menuGroup.forEachIndexed { index, it ->


                            var sub = menu.addSubMenu(index, 0, Menu.NONE, it[2].toString())
//==========================================================================
                            var itemInfo =
                                sub.add(999998, 9999991, Menu.NONE, getString(R.string.rc_info))
                            itemInfo.setIcon(R.drawable.ic_property_managemet)
                            var itemWI =
                                sub.add(999998, 9999992, Menu.NONE, getString(R.string.w_i))
                            itemWI.setIcon(R.drawable.ic_property_managemet)
//==========================================================================
                            menuItems.forEachIndexed { _, it2 ->

                                if (it2[3] == it[0]) {
                                    try {

                                        var id = it2[0].toString().toInt()
                                        var item =
                                            sub.add(index, id, Menu.NONE, it2[4].toString())

                                        try {
                                            try {
                                                var id2 = resources.getIdentifier(
                                                    it[0].toString(),
                                                    "drawable",
                                                    baseContext.packageName
                                                )
                                                if (id2 == 0) {
                                                    id2 = resources.getIdentifier(
                                                        "ic_property_managemet",
                                                        "drawable",
                                                        baseContext.packageName
                                                    )
                                                }
                                                item.setIcon(id2)
                                            } catch (ex: Exception) {
                                            }
                                        } catch (ex: Exception) {
                                        }

                                    } catch (ex1: Exception) {

                                    }
                                }
                            }
                        }





//                        ====================================================================-
                        var sub = menu.addSubMenu(
                            999999,
                            0,
                            Menu.NONE,
                            getString(R.string.setting_function)
                        )
                        var item2 =
                            sub.add(999999, 999999, Menu.NONE, getString(R.string.setting))
                        item2.setIcon(R.drawable.ic_settings_white_24dp)

                        MenuCompat.setGroupDividerEnabled(menu, true)
                    })

                } catch (ex: Exception) {
                }


            }
        }).start()
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        if (item.itemId == 9999991){
            startActivityForResult(Intent(this, RCInfoActivity::class.java),900)
            return true
        }

        else if (item.itemId == 9999992){
            startActivityForResult(Intent(this, ExchangeItemActivity::class.java),900)
            return true
        }

//        ==============================================================

        if (item.itemId == 999999){
            startActivityForResult(Intent(this, SettingsLimitActivity::class.java),900)
            return true
        }

        if (!::menuItems.isInitialized) {

        } else if (menuItems.size == 0) {

        } else {
            var menuId = item.itemId.toString()
            var menuIndex = -1
            menuItems.forEachIndexed { index, it ->
                if (it[0].toString() == menuId) {
                    menuIndex = index
                    MINDEX = index

                    MINDEXFUNCTIONID = it[2].toString() /*thuan.dang 20200509 get function id*/
                    FUNC_SEP_ID = it[7].toString() /*thuan.dang 20200509 dung de phan biet 2 function id cung 1form*/
                    MENU_NAME  =  it[4].toString()
                    return@forEachIndexed
                }
            }

            if (menuIndex == -1) {

            } else {
                if (cusFragment != null ){
                    if (cusFragment!!.isModified) {
                        askBeforeClose(menuIndex)

                    } else {
                        openFrament(menuIndex)
                    }
                } else {
                    openFrament(menuIndex)
                }


            }
        }


        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    fun askBeforeClose(index: Int) {

        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.prompt_ismodified)
        builder.setMessage(R.string.prompt_ismodified_content)

        builder.setPositiveButton(
            R.string.yes,
            DialogInterface.OnClickListener { _, _ ->
                openFrament(index)
            })
        builder.setNegativeButton(
            R.string.no,
            DialogInterface.OnClickListener { dialog, _ -> dialog.cancel() })

        builder.show()
    }



    fun openFrament(index: Int) {


        var fragment: BaseFragment? = null
        val titleFrament = menuItems[index][4]
        val framentClass = menuItems[index][2]
        val orie  =  try {
                                menuItems[index][5]
                            }catch (ex: java.lang.Exception){
                                "1"
                            }
        val orie_m  =  try {
            menuItems[index][6]
        }catch (ex: java.lang.Exception){
            "1"
        }




        try {
            fragment =
                Class.forName(getDefaultPackageName() + "." + client_id + "." + framentClass).newInstance() as BaseFragment
        } catch (ex: java.lang.Exception) {

        }

        if (fragment == null) {

            try {
                fragment =
                    Class.forName(getDefaultPackageName() + "." + "gw" + "." + framentClass).newInstance() as BaseFragment
            } catch (ex: java.lang.Exception) {

            }
        }

        if (fragment == null) {
            handler.post { Runnable {
                Toast.makeText(baseContext, getString(R.string.check_update), Toast.LENGTH_SHORT).show()
            } }
            return
        }

        if(this.cusFragment != null){
            this.cusFragment!!.close()
            this.cusFragment = null

        }


        this.cusFragment = fragment



        val ft: FragmentTransaction = supportFragmentManager.beginTransaction()
        ft.replace(fragmentID, fragment)
        ft.addToBackStack(null);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        ft.commit()

        supportActionBar!!.setTitle(titleFrament)
        title = titleFrament

        val tabletSize = resources.getBoolean(R.bool.isTablet)
        if(!tabletSize){
            handler.post(Runnable {
                if (orie_m == "1")
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                else if (orie_m == "2")
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                else
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

            })
        }else {

            handler.post(Runnable {
                if (orie == "1")
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                else if (orie == "2")
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                else
                    setOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
            })
        }

    }




    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        if (this.cusFragment != null) {
            if (event != null) {
                return cusFragment!!.onKeyUp(keyCode, event)
            }
        }

        return super.onKeyUp(keyCode, event)
    }

    override fun onUpdateCountProLazy (){

        Thread {

            Thread.sleep(500)

            try{
                var dp = getSystemSQLiteDatabase()

                var cursor = dp.rawQuery(
                    " select PK, IMG_URI,PROCEDURE,PARA,TITLE,IMG_THUMB  from PROCEDURE_LAZY where DEL_IF = 0  and PROCESS_YN = 'N' order by PK asc;  ",
                    null
                )

                var c = 0;

                try {
                    var c = cursor.count
                }catch (e: java.lang.Exception){

                }





                if (::item.isInitialized) {
                    handler.post {

                        item.title = c.toString()
                    }
                }

                cursor.close();
                dp.close()
            }catch(ex: Exception) {

            }


        }.start()



    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
         item = menu!!.findItem(R.id.menu1)



        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu1 -> {
                val intent = Intent(this, ProLazyActivity::class.java)
                startActivity(intent)
            }

            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }


    companion object{
        const val MSG_COMMAND_SET_RFID_DEFAULT = 1
        const val MSG_COMMAND_SET_RFID_INVENTORY_PARAM = 2
        const val MSG_COMMAND_SET_RFID_TX_POWER = 3
        const val MSG_COMMAND_SET_RFID_TX_CYCLE = 4
        const val MSG_COMMAND_SET_RFID_PREFIX = 5
        const val MSG_COMMAND_SET_RFID_SUFFIX = 6
        const val MSG_COMMAND_SET_RFID_TX_DATA_FORMAT = 7
        const val MSG_COMMAND_SET_RFID_RESULT_TYPE = 8
        const val MSG_COMMAND_SET_RFID_INVENTORY_MODE = 9



        const val DEVICE_BT = 1
        const val DEVICE_USB = 2
        const val DEVICE_UART = 3
    }


    private val CUSTOM_INTENT = "Custom.Intent.Test"

    private val mConnectedDevice = DEVICE_BT

    private var rfidManager: RFIDManager? = null
    private var paramOfInvent: ParamOfInvent? = null
    private var modeOfInvent: ModeOfInvent? = null
    private var txCycle: TxCycle? = null
    private var customIntentConfig: CustomIntentConfig? = null


    var connectedDeviceMacAddress = "-"
    var deviceName = ""
    var deviceConnected = false
    var isRfidRunning = false
    var isOpened = false

    var fhandler: Handler = object : Handler(Looper.getMainLooper()){
        override fun handleMessage(msg: Message){

            if (rfidManager == null || paramOfInvent == null || modeOfInvent == null || txCycle == null || customIntentConfig == null){
                return
            }

            val iErr: Int
            var iMessage : String =""
            when(msg.what){
                MSG_COMMAND_SET_RFID_DEFAULT -> {
                    iErr = rfidManager!!.SetDefaultConfig()
                    if (iErr != RFIDConst.CommandErr.SUCCESS) {
                        iMessage = "SetDefaultConfig() is failed : $iErr"
                    }
                }
                MSG_COMMAND_SET_RFID_INVENTORY_PARAM -> {

                    paramOfInvent!!.session = 1
                    paramOfInvent!!.q = 5
                    paramOfInvent!!.inventoryFlag = 2

                    iErr = rfidManager!!.SetInventoryParam(paramOfInvent)
                    if (iErr != RFIDConst.CommandErr.SUCCESS) {
                        iMessage ="SetInventoryParam() is failed : $iErr"
                    }
                }
                MSG_COMMAND_SET_RFID_TX_POWER -> {
                    iErr = rfidManager!!.SetTxPower(_power)
                    if (iErr != RFIDConst.CommandErr.SUCCESS) {
                        iMessage = "SetTxPower() is failed : $iErr"
                    }

                    iMessage = iMessage + " / " + "GetTxPower() : " + rfidManager!!.GetTxPower()
                    iMessage = iMessage + " / " + "GetMaxPower() : " + rfidManager!!.GetMaxPower()
                    iMessage = iMessage + " / " + "GetOemInfo() : " + rfidManager!!.GetOemInfo()


                }
                MSG_COMMAND_SET_RFID_TX_CYCLE -> {
                    txCycle!!.onTime = 100
                    txCycle!!.offTime = 10
                    iErr = rfidManager!!.SetTxCycle(txCycle)
                    if (iErr != RFIDConst.CommandErr.SUCCESS) {
                        iMessage ="SetTxCycle() is failed : $iErr"
                    }
                }


                MSG_COMMAND_SET_RFID_PREFIX ->{

                }

                MSG_COMMAND_SET_RFID_SUFFIX -> {
                }

                MSG_COMMAND_SET_RFID_TX_DATA_FORMAT -> {
                    /*
                     * public static final int TX_FORMAT_TAG_DATA = 0;
                     * public static final int TX_FORMAT_PREFIX_TAG_DATA = 1;
                     * public static final int TX_FORMAT_TAG_DATA_SUFFIX = 2;
                     * public static final int TX_FORMAT_PREFIX_TAG_DATA_SUFFIX = 3;
                     */
//                    iErr = rfidManager!!.SetTxDataFormat(RFIDConst.RFIDConfig.TX_FORMAT_TAG_DATA)
                    iErr = rfidManager!!.SetTxDataFormat(RFIDConst.RFIDConfig.DATA_FORMAT_PC_EPC_CRC)
                    if (iErr != RFIDConst.CommandErr.SUCCESS) {
                        iMessage ="SetTxDataFormat() is failed : $iErr"
                    }
                    iMessage += "Data format : " + rfidManager!!.GetDataFormat()

                }

                MSG_COMMAND_SET_RFID_RESULT_TYPE -> {
                    /*
                     * public static final int RFID_RESULT_CALLBACK = 0;
                     * public static final int RFID_RESULT_KBDMSG = 1;
                     * public static final int RFID_RESULT_COPYPASTE = 2;
                     * public static final int RFID_RESULT_USERMSG = 3;
                     * public static final int RFID_RESULT_EVENT = 4;
                     * public static final int RFID_RESULT_CUSTOM_INTENT = 5;
                     */
                    rfidManager!!.SetResultType(RFIDConst.ResultType.RFID_RESULT_CUSTOM_INTENT)
                }
                MSG_COMMAND_SET_RFID_INVENTORY_MODE -> {
                    modeOfInvent!!.single = 0
                    modeOfInvent!!.select = 0
                    modeOfInvent!!.timeout = 0
                    rfidManager!!.SetOperationMode(modeOfInvent)
                    iMessage ="Device name : " + rfidManager!!.GetBtDevice()

                }
            }

            Log.d("ARIC-DEBUG", _power.toString() +"/ " + iMessage)
            return ;

        }
    }
    var mhandler = Handler(Looper.getMainLooper())

//    private val mHandler = Handler { false }


    val mRFIDCallback =object : RFIDCallback(mhandler){
        override fun onNotifyReceivedPacket(recvPacket: RecvPacket?) {
            if( recvPacket == null) return
            if(!isConnected()){
                return;
            }
            Log.d("qqq", "onNotifyReceivedPacket : " + recvPacket!!.RecvString)

            scanData(recvPacket!!.RecvString)
        }

        override fun onNotifyDataWriteFail() {
            //super.onNotifyDataWriteFail()
        }

        override fun onNotifyChangedState(state: Int) {
            when (state) {
                RFIDConst.DeviceState.BT_CONNECTED -> Log.i(
                    "RFID_callbacks",
                    "onNotifyChangedState BT_CONNECTED : [$state]"
                )
                RFIDConst.DeviceState.BT_DISCONNECTED -> Log.i("RFID_callbacks", "onNotifyChangedState BT_CONNECT_FAILED : [$state]")
                RFIDConst.DeviceState.BT_OPENED -> Log.i("RFID_callbacks", "onNotifyChangedState BT_OPENED : [$state]")
                RFIDConst.DeviceState.BT_CLOSED -> Log.i("RFID_callbacks", "onNotifyChangedState BT_CLOSED : [$state]")
                RFIDConst.DeviceState.USB_OPENED -> Log.i("RFID_callbacks", "onNotifyChangedState USB_OPENED : [$state]")
                RFIDConst.DeviceState.USB_CLOSED -> Log.i("RFID_callbacks", "onNotifyChangedState USB_CLOSED : [$state]")
                RFIDConst.DeviceState.UART_OPENED -> Log.i("RFID_callbacks", "onNotifyChangedState UART_OPENED : [$state]")
                RFIDConst.DeviceState.UART_CLOSED -> Log.i("RFID_callbacks", "onNotifyChangedState UART_CLOSED : [$state]")
                RFIDConst.DeviceState.TRIGGER_MODE_RFID -> Log.i("RFID_callbacks", "onNotifyChangedState TRIGGER_MODE_RFID : [$state]")
                RFIDConst.DeviceState.TRIGGER_MODE_SCAN -> Log.i("RFID_callbacks", "onNotifyChangedState TRIGGER_MODE_SCAN : [$state]")
                RFIDConst.DeviceState.TRIGGER_RFID_KEYDOWN -> {
                    Log.i("RFID_callbacks", "onNotifyChangedState TRIGGER_RFID_KEYDOWN : [$state]")
                    startScan()
                }
                RFIDConst.DeviceState.TRIGGER_RFID_KEYUP -> {
                    Log.i("RFID_callbacks", "onNotifyChangedState TRIGGER_RFID_KEYUP : [$state]")
                    stopScan()


                }
                RFIDConst.DeviceState.TRIGGER_SCAN_KEYDOWN -> Log.i(
                    "RFID_callbacks",
                    "onNotifyChangedState TRIGGER_SCAN_KEYDOWN : [$state]"
                )
                RFIDConst.DeviceState.TRIGGER_SCAN_KEYUP -> Log.i("RFID_callbacks", "onNotifyChangedState TRIGGER_SCAN_KEYUP : [$state]")
                RFIDConst.DeviceState.LOW_BATT -> Log.i("RFID_callbacks", "onNotifyChangedState LOW_BATT : [$state]")
                RFIDConst.DeviceState.POWER_OFF -> Log.i("RFID_callbacks", "onNotifyChangedState POWER_OFF : [$state]")
                else -> {}
            }
            //super.onNotifyChangedState(state)
        }
    }

    private val broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            val action = intent.action

            if (BluetoothDevice.ACTION_ACL_DISCONNECTED == action) {
                setDisconnect()

            } else if (RFIDConst.ResultType.INTENT_EVENT == action) {
                scanData(intent.extras!!.getString(RFIDConst.ResultType.EXTRA_EVENT_RFID_DATA))
            } else if (CUSTOM_INTENT == action) {
                scanData(intent.extras!!.getString(customIntentConfig?.extraRfidData))
            }

        }
    }

    var _power:Int  = -22

    fun setPower(power:Int){
        _power = power
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_TX_POWER, 0, 0, null),
            600
        )

    }
    fun getPower():Int{
        return _power
    }

    private fun deviceConfigSetting() {
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_DEFAULT, 0, 0, null),
            200
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(
                MSG_COMMAND_SET_RFID_INVENTORY_PARAM,
                0,
                0,
                null
            ), 400
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_TX_POWER, 0, 0, null),
            600
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_TX_CYCLE, 0, 0, null),
            800
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_PREFIX, 0, 0, null),
            1000
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(MSG_COMMAND_SET_RFID_SUFFIX, 0, 0, null),
            1200
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(
                MSG_COMMAND_SET_RFID_TX_DATA_FORMAT,
                0,
                0,
                null
            ), 1400
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(
                MSG_COMMAND_SET_RFID_RESULT_TYPE,
                0,
                0,
                null
            ), 1600
        )
        fhandler.sendMessageDelayed(
            fhandler.obtainMessage(
                MSG_COMMAND_SET_RFID_INVENTORY_MODE,
                0,
                0,
                null
            ), 1800
        )
    }



    open fun scanData(data: String?){
        Log.v("scanData", data.toString())
        if (data != null ) {

            try{
                val dataCallBack = data.toString().trim().substring(16,28).toUpperCase()
                cusFragment?.onDataResult(1, dataCallBack)

            }catch (ex: Exception){}


        };
    }

    fun isConnected(): Boolean {
        return deviceConnected
    }

    fun setConnect(macAddress: String, deviceName: String?) {
       deviceConnected = true
        connectedDeviceMacAddress = macAddress
        rfidManager?.ConnectBTDevice(macAddress, deviceName)
    }

    fun setDisconnect() {
        deviceConnected = false
        //rfidManager.Close();
        rfidManager?.DisconnectBTDevice()
    }

    fun RfidControl(){
        if (isRfidRunning)
            stopRfidScan()
        else {
            if(!isConnected())
                connectRFID()
            startRfidScan()
        }
    }

    fun stopScan(){
        stopRfidScan()

        cusFragment?.onDataResult(2, "0")
    }

    fun stopRfidScan() {
        if (isRfidRunning) {
            isRfidRunning = false
            rfidManager!!.Stop()
            cusFragment?.onDataResult(2, "0")


        }
    }

    open fun startScan(){

        try {
            if(!cusFragment!!.onHanldEvent(0)){
                startRfidScan()


            }
        }catch (ex: java.lang.Exception){

        }


    }

    private fun startRfidScan() {
        if (rfidManager != null) {
            if (!isRfidRunning) {
                isRfidRunning = true
                //rfidManager.StartInventory_ext(1, 0, 0);
                Log.v("vvv","startRfidScan" )
                rfidManager!!.StartInventory()
                cusFragment?.onDataResult(2, "1")

            }



        }
    }




}
