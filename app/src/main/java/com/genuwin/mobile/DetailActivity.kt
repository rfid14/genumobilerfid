package com.genuwin.mobile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import androidx.appcompat.widget.Toolbar
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.base.BaseActivity
import com.genuwin.mobile.base.BaseDetailFragment

class DetailActivity : BaseActivity(){
    lateinit var toolbar: Toolbar




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_activity_main)

//        toolbar = findViewById(R.id.toolbar)
//        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(false)


        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.

            var frament_name = intent.getStringExtra(BaseDetailFragment.ARG_FRAMENT_NAME)
            //var frament_title = intent.getStringExtra(BaseDetailFragment.ARG_FRAMENT_TITLE)

           // toolbar.setTitle(frament_title)

            try {
                var fragment  =
                    Class.forName( getDefaultPackageName() +"."+ client_id +"."+ frament_name ).newInstance() as BaseFragment


                    fragment.apply {
                        arguments = Bundle().apply {
                            putString(
                                BaseDetailFragment.ARG_ITEM_ID_M,
                                intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_M)
                            )
                            putString(
                                BaseDetailFragment.ARG_ITEM_ID_D,
                                intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_D)
                            )
                            putString(
                                BaseDetailFragment.ARG_ITEM_ID_D2,
                                intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_D2)
                            )
                            putString(
                                BaseDetailFragment.ARG_ITEM_ID_DATA,
                                intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_DATA)
                            )
                        }
                    }
                cusFragment = fragment
                    supportFragmentManager.beginTransaction()
                        .add(R.id.item_detail_container, fragment)
                        .commit()


            } catch (ex: java.lang.Exception) {
                try {
                    var fragment  =
                        Class.forName(getDefaultPackageName() +"."+ "gw" +"."+ frament_name).newInstance() as BaseFragment


                        fragment.apply {
                            arguments = Bundle().apply {
                                putString(
                                    BaseDetailFragment.ARG_ITEM_ID_M,
                                    intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_M)
                                )
                                putString(
                                    BaseDetailFragment.ARG_ITEM_ID_D,
                                    intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_D)
                                )
                                putString(
                                    BaseDetailFragment.ARG_ITEM_ID_D2,
                                    intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_D2)
                                )
                                putString(
                                    BaseDetailFragment.ARG_ITEM_ID_DATA,
                                    intent.getStringExtra(BaseDetailFragment.ARG_ITEM_ID_DATA)
                                )
                            }
                        }

                    cusFragment = fragment
                        supportFragmentManager.beginTransaction()
                            .add(R.id.item_detail_container, fragment)
                            .commit()

                } catch (ex: java.lang.Exception) {

                }
            }



        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK) {


            if (cusFragment != null) {
                if (data != null) {
                    cusFragment!!.onResult(requestCode, resultCode, data)
                } else {

//                       var  thumbnail = MediaStore.Images.Media.getBitmap(
//                            getContentResolver(), imageUri)
                    var str = imageUri.toString()
                    cusFragment!!.onCameraResult(requestCode, str)
                }
            }

        }

    }



    override fun onBackPressed() {
        // your code.
        finish()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            true
        } else super.onKeyDown(keyCode, event)
    }


}