package com.genuwin.mobile;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.lamvuanh.lib.DataGridView;

public class ExchangeItemDataMainActivity extends AppCompatActivity {
    DataGridView dataview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.

//            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("W/I");
        }

        setContentView(R.layout.activity_exchange_item_data_main);

        dataview = findViewById(R.id.dataview);

        dataview.BeginInit();

        dataview.addColumn( getString(R.string.wi_no), 120);
        dataview.addColumn( getString(R.string.line), 120);
        dataview.addColumn( getString(R.string.lot_no), 100);
        dataview.addColumn( getString(R.string.status), 100);
        dataview.addColumn( getString(R.string.item_name), 200);

        dataview.EndInit();
    }


}