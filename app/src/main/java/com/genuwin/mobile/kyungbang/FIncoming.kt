package com.genuwin.mobile.kyungbang


import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.Combobox
import com.lamvuanh.lib.DataGridView
import java.util.*


class FIncoming : BaseFragment() {
    lateinit var bc1: EditText
    lateinit var bc2: EditText
    lateinit var tr_qty: EditText
    lateinit var code: EditText
    lateinit var name: EditText
    lateinit var lotNo: EditText
    lateinit var btnDelete: Button
    lateinit var btnSearch: Button
    lateinit var btnUploadInc: Button
    lateinit var btnSave: Button
    lateinit var txt_error2: TextView
    lateinit var lst_WH: Combobox
    lateinit var lst_line: Combobox
    lateinit var lst_LOC: Combobox
    lateinit var txtTotalLabel: TextView
    lateinit var dateview: DataGridView
    lateinit var ckbClearData: CheckBox
    lateinit var CREATE_TABLE_DUAL: String
    var l_Scan_Type = "1" // incoming
    var p_label_pk: Int = 0
    var p_item_type =""

    var P_TLG_IT_ITEM_PK: Int = 0


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_fincoming, container, false)
        btnDelete = view.findViewById(R.id.btnDelete)
        btnUploadInc = view.findViewById(R.id.btnUploadInc)
        btnSearch = view.findViewById(R.id.btnSearch)
        btnSave = view.findViewById(R.id.btnSave)
        bc1 = view.findViewById(R.id.edit_Bc1)
        bc2 = view.findViewById(R.id.edit_Bc2)
        code = view.findViewById(R.id.edit_Code2)
        name = view.findViewById(R.id.edit_Name)
        lotNo = view.findViewById(R.id.edit_LotNo)
        tr_qty = view.findViewById(R.id.edit_Qty)
        lst_WH = view.findViewById(R.id.lst_WH)
        lst_line = view.findViewById(R.id.lst_line)
        lst_LOC = view.findViewById(R.id.lst_LOC)
        dateview = view.findViewById(R.id.dateview)
        ckbClearData = view.findViewById(R.id.ckbClearData)

        txt_error2 = view.findViewById(R.id.txt_error2)
        txtTotalLabel = view.findViewById(R.id.txtTotalLabel)
        txt_error2.setTextColor(Color.RED)
        txt_error2.textSize = 18.0.toFloat()

        bc1.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event!!.action == KeyEvent.ACTION_DOWN) {

                        if ("" == bc1.text.toString()) {
                            txt_error2.text = "Pls Scan barcode!"
                            bc1.requestFocus()
                        } else {
                            OnSaveData()
                        }
                    }
                    return true //important for event onKeyDown
                }


                return false
            }

        })

        btnDelete.setOnClickListener(View.OnClickListener {
            if(ckbClearData.isChecked){
                ClearAllDate();
            }else {
                DeleteData()
            }
        })

        btnUploadInc.setOnClickListener(View.OnClickListener {
            onClickUploadDataInc()
        })

        btnSearch.setOnClickListener(View.OnClickListener {
            onClickSearch()
        })


        btnSave.setOnClickListener(View.OnClickListener {
           SaveData()
        })

        dateview.BeginInit()

        dateview.addColumn("ITEM_BC",
            DataGridView.ViewMath()
            .convertDpToPixelInt(80.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("TR_QTY", DataGridView.ViewMath()
            .convertDpToPixelInt(50.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("TR_LOT_NO", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("WH_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(100.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("LINE_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("ITEM_CODE", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("ITEM_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.EndInit()


        dateview.itemClickListener = object : DataGridView.HandlerItemClick() {
            override fun onClickCell(position: Int, col: Int): Boolean {

              //  OnLoadBC(dateview.getDataCell(position,0))
                return false
            }
        }


        LoadWH()
        LoadLine()
        LoadLoc()
        OnShowGW()

        bc1.requestFocus()

        return view
    }

    private fun LoadWH() {


        val selectQuery =
            "SELECT  pk,pk||'-'|| wh_id||'-'||wh_name,wh_name FROM tlg_in_warehouse where pk >0 order by  wh_id" // tlg_in_warehouse
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(selectQuery, null)

        val data = Array(cursor.count) { arrayOfNulls<String>(2) }


        // looping through all rows and adding to list
        var index = 0
        if (cursor != null && cursor.moveToFirst()) {
            do {
                data[index][0] = cursor.getString(0).toString()
                data[index][1] = cursor.getString(1).toString()
                index++
            } while (cursor.moveToNext())
        }
        cursor.close()
        dp.close()
        lst_WH.setData(data, 1, 0)
    }

    private fun LoadLine() {
        val selectQuery =
            "SELECT  pk,pk||'-'||line_id||'-'||line_name FROM tlg_pb_line where pk>0 order by line_id"
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(selectQuery, null)

        val data = Array(cursor.count) { arrayOfNulls<String>(2) }


        // looping through all rows and adding to list
        var index = 0
        if (cursor != null && cursor.moveToFirst()) {
            do {
                data[index][0] = cursor.getString(0).toString()
                data[index][1] = cursor.getString(1).toString()
                index++
            } while (cursor.moveToNext())
        }
        cursor.close()
        dp.close()
        lst_line.setData(data, 1, 0)
    }

    private fun LoadLoc() {
        val selectQuery =
            "SELECT  pk,pk||'-'||loc_id||'-'||loc_name FROM tlg_in_whloc where pk>0 order by loc_id"
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(selectQuery, null)

        val data = Array(cursor.count) { arrayOfNulls<String>(2) }


        // looping through all rows and adding to list
        var index = 0
        if (cursor != null && cursor.moveToFirst()) {
            do {
                data[index][0] = cursor.getString(0).toString()
                data[index][1] = cursor.getString(1).toString()
                index++
            } while (cursor.moveToNext())
        }
        cursor.close()
        dp.close()
        lst_LOC.setData(data, 1, 0)
    }


    fun OnSaveData() {


        val str_scan = bc1.text.toString().toUpperCase()

        try {
            if (str_scan == "") {
                txt_error2.text = "Pls Scan barcode!"
                return
            }

            val l_rs: Boolean = OnLoadBC(str_scan)

            if (Check_Exist_PK(str_scan)) // exist data
            {
                bc1.setText("")
                bc1.requestFocus()

                // return;
            } else {
                if (l_rs) // scan barcode,has data then
                {
                    // int cnt=Check_Exist_PK(str_scan);//check barcode exist in
                    if (OnSave()) // scanned save data existed from downoad
                    {
                        txt_error2.text = "insert label success."
                        bc1.text.clear()
                        bc1.requestFocus()
                        OnShowGW()
                    }
                } else {
                    bc2.text = bc1.text // scan barcode not exist then
                    // save barcode here.(can not
                    // yet download or register
                    // barcode)
                    code.setText("")
                    name.setText("")
                    lotNo.setText("")
                    tr_qty.setText("")
                    // txt_error2.setText("add new data sucess!!!");
                    if (OnSave()) // save data new,then update lot no,tr_qty
                    {
                        txt_error2.text = "add new data sucess!!!"
                        bc1.text.clear()
                        bc1.requestFocus()
                        // txt_error2.setText("Barcoce "+str_scan+" not exist.");
                        OnShowGW()
                    }
                }
            }

        } catch (ex: Exception) {
            // save data error write to file log
        }
    }


    fun Check_Exist_PK(l_bc_item: String): Boolean {
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(
            "SELECT PK FROM INV_TR where tr_type='1' and ITEM_BC ='" + l_bc_item + "' ",
            null
        )
        // Check_Exist_PK=cursor.getCount();
        if (cursor != null && cursor.moveToFirst()) {
            var intPk = cursor.getString(cursor.getColumnIndex("PK")).toInt()
            cursor.moveToNext()
            cursor.close()
            dp.close()
            if (intPk > 0)
                return true

        }
        return false

    }


    fun OnLoadBC(l_bc_item: String): Boolean {

        var l_RS = false
        val SQL_SEL_LABEL_REQ =
            ("SELECT  PK AS LABEL_PK, TLG_IT_ITEM_PK, ITEM_BC, LABEL_QTY, LABEL_UOM, YYYYMMDD, ITEM_CODE, ITEM_NAME, ITEM_TYPE, LOT_NO "
                    + " FROM TLG_LABEL "
                    + " WHERE     (ITEM_BC = '"
                    + l_bc_item
                    + "') ") // ='"+l_bc_item+"'
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(SQL_SEL_LABEL_REQ, null)


        if (cursor != null && cursor.moveToFirst()) {
            bc2.setText(cursor.getString(cursor.getColumnIndex("ITEM_BC")))
            code.setText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")))
            name.setText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")))
            tr_qty.setText(cursor.getString(cursor.getColumnIndex("LABEL_QTY")))
            lotNo.setText(cursor.getString(cursor.getColumnIndex("LOT_NO")))
            p_label_pk = cursor.getString(
                cursor
                    .getColumnIndex("LABEL_PK")
            ).toInt()
            P_TLG_IT_ITEM_PK = cursor.getString(
                cursor
                    .getColumnIndex("TLG_IT_ITEM_PK")
            ).toInt()
            p_item_type = cursor.getString(cursor.getColumnIndex("ITEM_TYPE"))
            cursor.moveToNext()
            cursor.close()
            l_RS = true
        }
       dp.close()
        return l_RS
    }

    fun OnSave(): Boolean {
        //db=openOrCreateDatabase("gasp", MODE_PRIVATE, null);
        try {

            var P_ITEM_BC = bc2.text.toString()
            var P_ITEM_CODE = code.text.toString()
            var P_ITEM_NAME = name.text.toString()
            var P_TR_LOT_NO = lotNo.text.toString()
            var P_TR_QTY = tr_qty.text.toString()
            val P_TR_DATE: String
            val cal: Date = Calendar.getInstance().time

            // c.get(Calendar.)
            P_TR_DATE = "" // nho lay sysdate
            val p_wh_pk = lst_WH.value()
            val p_wh_loc_pk = lst_LOC.value()
            val p_lie_pk = lst_line.value()
            var dp = BaseActivity().getSQLiteDatabase()
            dp.execSQL(
                "INSERT INTO INV_TR(TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_LINE_PK, TR_QTY, TR_LOT_NO, ITEM_BC, TR_DATE, TR_ITEM_PK, ITEM_CODE, ITEM_NAME,TR_TYPE,WH_NAME,LINE_NAME,ITEM_TYPE) "
                        + "VALUES('"
                        + p_label_pk
                        + "','"
                        + p_wh_pk
                        + "','"
                        + p_wh_loc_pk
                        + "','"
                        + p_lie_pk
                        + "','"
                        + P_TR_QTY
                        + "','"
                        + P_TR_LOT_NO
                        + "','"
                        + P_ITEM_BC
                        + "','"
                        + P_TR_DATE
                        + "','"
                        + P_TLG_IT_ITEM_PK
                        + "','"
                        + P_ITEM_CODE
                        + "','"
                        + P_ITEM_NAME
                        + "','"
                        + l_Scan_Type
                        + "','"
                        + lst_WH.text()
                        + "','" + lst_line.text() + "','" + p_item_type + "');"
            )
            //bc1.setText("");
            //bc1.requestFocus();
            // txt_error2.setText("Save Error!!!");
            dp.close()
            return true
        } catch (ex: java.lang.Exception) {
            txt_error2.text = "Save Error!!!"
        }
        return true
    }


    fun OnShowGW() // show data gridview
    {
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor1 = dp.rawQuery(
            "SELECT PK FROM INV_TR where TR_TYPE='1' order by PK desc ",
            null
        ) // TLG_LABEL
        var cursor = dp.rawQuery(
            "SELECT ITEM_BC, TR_QTY, TR_LOT_NO,WH_NAME,LINE_NAME,null WH_LOC,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='1' and DEL_IF = 0 order by PK desc ",
            null
        ) // TLG_LABEL
        val countList: Int = cursor1.count
        val count: Int = cursor.count
        val l_total: Int = cursor.count - 1

        handler.post(Runnable {
            txtTotalLabel.text = "Total:$countList Label(s)"
            dateview.clearAll()
        })


        // count la so dong,con so 5 la column,-->so phan tu cua mang =count*5
      //  val vals = arrayOfNulls<String>(count * 7)

        if (cursor != null && cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false)
            {
                val arr= arrayOf(
                    cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                    cursor.getString(cursor.getColumnIndex("TR_QTY")),
                    cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                    cursor.getString(cursor.getColumnIndex("WH_NAME")),
                    cursor.getString(cursor.getColumnIndex("LINE_NAME")),
                    cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                    cursor.getString(cursor.getColumnIndex("ITEM_NAME"))
                )
                handler.post(Runnable {
                    dateview.addRow(
                        arr
                    )
                })
                cursor.moveToNext();
            }


        }
        cursor1.close()
        cursor.close()
        dp.close()
    }

    fun ClearAllDate(){
        BaseActivity().confirmDialog(
            "Confirm Clear All Data...",
            "Are you sure you want to clear ALL item B/C",
            object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {
                    if (bool) {
                        var dp = BaseActivity().getSQLiteDatabase()
                        dp.execSQL("DELETE from INV_TR")
                        dp.close()
                        bc2.text.clear()
                        code.text.clear()
                        name.text.clear()
                        lotNo.text.clear()
                        tr_qty.text.clear()
                        txt_error2.text = "Clear data sucess."
                        OnShowGW()
                        BaseActivity().showToast("Clear success..")
                    }
                }
            })
    }

    fun DeleteData() {


        var P_ITEM_BC = bc2.text.toString()
        if (P_ITEM_BC.length < 1) {
            txt_error2.text = "Please select B/C!:"
        } else {

            BaseActivity().confirmDialog(
                "Confirm Delete...",
                "Are you sure you want to delete item B/C: $P_ITEM_BC ",
                object : BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {
                        if (bool) {
                            OnDeleteBC(P_ITEM_BC)
                            bc2.text.clear()
                            code.text.clear()
                            name.text.clear()
                            lotNo.text.clear()
                            tr_qty.text.clear()
                            txt_error2.text = "Delete data sucess." + P_ITEM_BC.toString()
                            OnShowGW()
                            BaseActivity().showToast("Delete success..")
                        }
                    }
                })


        }
    }

    fun OnDeleteBC(p_pk: String) {
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL("DELETE from INV_TR where ITEM_BC = '$p_pk'")
        dp.close()
    }


    fun onClickUploadDataInc() {


        BaseActivity().confirmDialog(
            "Confirm Upload...",
            "Are you sure you want to upload data to server?",
            object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {
                    if (bool) {
                        onStartUpdate()
                    }

                }
            })


    }


    fun onClickSearch(){
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(
            "SELECT  ITEM_CODE,count(ITEM_CODE) as COUNT_ITEM,SUM(TR_QTY) AS QTY FROM INV_TR where TR_TYPE='1' GROUP BY ITEM_CODE ",
            null
        );// TLG_LABEL
        var count = cursor.count
        var str = ""
        if (cursor != null && cursor.moveToFirst())
        {

            str = "ITEM \t\t\t COUNT  \t\t\t QTY\n----------------------------------------------------------------\n";
            while (cursor.isAfterLast() == false)
            {
                str =
                    str + cursor.getString(cursor.getColumnIndex("ITEM_CODE")) + "\t\t\t\t " + cursor.getString(
                        cursor.getColumnIndex("COUNT_ITEM")
                    ) + "\t\t\t\t\t " + cursor.getString(cursor.getColumnIndex("QTY")) + "\n";
                str = str + "----------------------------------------------------------------\n";
                cursor.moveToNext();
            }

        }else{
            str = "Not found data.";
        }

        cursor.close();
        dp.close();
        handler.post(Runnable {
            BaseActivity().confirmDialog(
                "List Item ...",
                str,
                object : BaseActivity.ConfirmDialogCallBack {
                    override fun callBack(bool: Boolean) {


                    }
                })
        })

    }


    fun SaveData() {
        val bc2 = bc2.text

        if (bc2.isEmpty() ) {
            txt_error2.text = "Please, scan one barcode to update."
            txt_error2.setTextColor(Color.RED)
            txt_error2.textSize = 18.0.toFloat()
            return
        } else {
            BaseActivity().confirmDialog("Confirm update...", "Are you sure you want to update item B/C: $bc2", object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {

                    OnUpdateData()
                    OnShowGW()
                    BaseActivity().showToast("Update success..")

                }
            } )

        }
    }

    fun OnUpdateData() {




        // get data input control to var then update db
        var P_ITEM_BC = bc2.text.toString()
        var P_ITEM_CODE = code.text.toString()
        var P_ITEM_NAME = name.text.toString()
        var P_TR_LOT_NO = lotNo.text.toString()
        var P_TR_QTY = tr_qty.text.toString()
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL(
            "UPDATE INV_TR set TR_QTY='" + P_TR_QTY + "',TR_LOT_NO='"
                    + P_TR_LOT_NO + "'  where pk = (select max(pk) from INV_TR where ITEM_BC ='"
                    + P_ITEM_BC.toString() + "') and TR_TYPE='" + l_Scan_Type + "' "
        )

        txt_error2.text = "Update data sucess."
        dp.close()
    }

    fun onStartUpdate() {

        var dp = BaseActivity().getSQLiteDatabase()

        var cursor2 = dp.rawQuery(
            "select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where TR_TYPE='1' and DEL_IF='0'",
            null
        )


        var USER = BaseActivity().getSharedPreferences().getString("login_user", "")


        Thread(Runnable {
            if (cursor2.moveToFirst()) {




                    handler.post(Runnable {
                        BaseActivity().showProgressBar("Updateing....")
                        txt_error2.text = ""
                    })


            var j = 0
            do {
                // labels.add(cursor.getString(1));
                var para = ""

                for (i in 0 until cursor2.columnCount-1) {
                    if (para.length <= 0) {
                        if (cursor2.getString(i) != null){
                            para += cursor2.getString(i) + "|"
                        } else para += "|"
                    } else {
                        para += if (cursor2.getString(i) != null) cursor2.getString(i) +  "|"   else "|"
                    }
                }
                para += "|$USER"
                j++

                handler.post(Runnable {
                    BaseActivity().showProgressBar(j.toString() + "/" + cursor2.count)
                })


                var result = BaseActivity().getWebService().GetDataTableArg("LG_MPOS_M020_UPLOAD_v2", para)
                if(!result.isError){
                    val para1: List<String> = para.split("|")
                    var id = para1.get(13)

                    val sql =
                        "UPDATE INV_TR set DEL_IF=$id  where PK = $id"
                    dp.execSQL(sql)
                }else{
                    handler.post(Runnable {
                        BaseActivity().showToast(result.message+ "/" +para)
                    })

                }


            } while (cursor2.moveToNext())
            //////////////////////////
                handler.post(Runnable {
                    BaseActivity().hideProgressBar()


                })
                OnShowGW()
        } else {

                handler.post(Runnable {
                    txt_error2.setTextColor(Color.RED)
                    txt_error2.text = "Data is empty!"
                })

        }

        cursor2.close()
       dp.close()
            }).start()

    }






}