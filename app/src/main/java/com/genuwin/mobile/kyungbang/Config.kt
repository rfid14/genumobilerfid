package com.genuwin.mobile.kyungbang

import android.database.sqlite.SQLiteDatabase
import com.genuwin.mobile.base.BaseConfig

class Config (): BaseConfig() {
    override fun doConfig(db: SQLiteDatabase) {
       // super.doConfig(db)
        val CREATE_INV_TR = ("CREATE TABLE IF NOT EXISTS INV_TR ( "
                + "PK INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "TLG_POP_LABEL_PK INTEGER, " + "TR_WAREHOUSE_PK INTEGER, "
                + "TR_LOC_ID TEXT, " + "TR_QTY INTEGER, " + "TR_LOT_NO TEXT, "
                + "TR_TYPE TEXT, " + "ITEM_BC TEXT, " + "TR_DATE TEXT, "
                + "TR_ITEM_PK INTEGER, " + "TR_LINE_PK INTEGER, "
                + "ITEM_CODE TEXT, " + "ITEM_NAME TEXT, " + "ITEM_TYPE TEXT, "
                + "TLG_GD_REQ_D_PK INTEGER, " + "WH_NAME TEXT, "
                + "LOC_NAME TEXT, " + "LINE_NAME TEXT, " + "SLIP_NO TEXT,DEL_IF INTEGER default 0 )")
        db.execSQL(CREATE_INV_TR)

        // CREATE TABLE TLG_LABEL

        // CREATE TABLE TLG_LABEL
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS TLG_LABEL(PK NUMBER, TLG_IT_ITEM_PK NUMBER, ITEM_BC VARCHAR2,"
                    + " LABEL_UOM VARCHAR2, YYYYMMDD VARCHAR2, ITEM_CODE VARCHAR2, ITEM_NAME VARCHAR2, ITEM_TYPE VARCHAR2,"
                    + "LABEL_QTY NUMBER, LOT_NO VARCHAR2);"
        )

        // CREATE TABLE TLG_GD_REQ

        // CREATE TABLE TLG_GD_REQ
        db.execSQL(
            "CREATE TABLE IF NOT EXISTS TLG_GD_REQ(TLG_GD_REQ_M_PK NUMBER, REQ_DATE VARCHAR2, SLIP_NO VARCHAR2"
                    + ", TLG_GD_REQ_D_PK NUMBER, REQ_ITEM_PK NUMBER, REQ_QTY NUMBER,REQ_UOM VARCHAR2, OUT_WH_PK NUMBER, REF_NO VARCHAR2, OVER_RATIO NUMBER);"
        )

        // CREATE TABLE tlg_in_warehouse

        // CREATE TABLE tlg_in_warehouse
        db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_warehouse(PK NUMBER,wh_id VARCHAR2,wh_name VARCHAR2);")

        // CREATE TABLE tlg_in_whloc

        // CREATE TABLE tlg_in_whloc
        db.execSQL("CREATE TABLE IF NOT EXISTS tlg_in_whloc(PK NUMBER, loc_id VARCHAR2, loc_name VARCHAR2);")

        // CREATE TABLE tlg_pb_line

        // CREATE TABLE tlg_pb_line
        db.execSQL("CREATE TABLE IF NOT EXISTS tlg_pb_line(PK NUMBER,line_id VARCHAR2,line_name VARCHAR2);")
    }
}