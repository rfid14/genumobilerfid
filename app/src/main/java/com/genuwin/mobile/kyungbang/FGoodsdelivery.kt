package com.genuwin.mobile.kyungbang


import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.genuwin.android.base.BaseFragment
import com.genuwin.mobile.R
import com.genuwin.mobile.base.BaseActivity
import com.lamvuanh.lib.DataGridView


class FGoodsdelivery(): BaseFragment() {
    lateinit var bc1: EditText
    lateinit var bc2: EditText
    lateinit var code: EditText
    lateinit var name: EditText
    lateinit var lotNo: EditText
    lateinit var btnDelete: Button
    lateinit var btnSearch: Button
    lateinit var btnUploadGood: Button
    lateinit var txt_error: TextView
    lateinit var req_qty: TextView
    lateinit var edit_Qty: TextView
    lateinit var reqBal2: TextView
    lateinit var reqNo: TextView
    lateinit var wh_Pk : TextView
    lateinit var wh_name : TextView
    lateinit var txtTotalLabel : TextView


    lateinit var dateview: DataGridView

    var P_ITEM_BC =  ""
    var P_ITEM_CODE =""
    var P_ITEM_NAME= ""
    var P_TR_LOT_NO= ""
    var P_TR_QTY =""
    var P_SLIP_NO= ""
    var P_TR_ITEM_PK= ""
    var P_REQ_D_PK = ""
    var P_WH_PK = ""
    var P_WH_NAME= ""

    var lbl_SlipNo1_Tag  =""
    var _Scan_Type = "3" //gd request


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_fgoodsdelivery, container, false)

        btnDelete = view.findViewById(R.id.btn_delete);
        btnSearch = view.findViewById(R.id.btnSearch);
        btnUploadGood = view.findViewById(R.id.btnUploadGood);

        bc1 =view.findViewById(R.id.edit_Bc1);
        txt_error=view.findViewById(R.id.txt_error);
        edit_Qty=view.findViewById(R.id.edit_Qty);

        bc2 =view.findViewById(R.id.edit_Bc2);
        reqNo =view.findViewById(R.id.edit_reqNo);
        code =view.findViewById(R.id.edit_Code);
        name =view.findViewById(R.id.edit_Name);
        lotNo =view.findViewById(R.id.edit_LotNo);
        req_qty =view.findViewById(R.id.edit_reqBal1);
        reqBal2 =view.findViewById(R.id.edit_ReqBal2);
        wh_Pk = view.findViewById(R.id.txt_WH_PK)
        wh_name = view.findViewById(R.id.txt_WH_NAME)
        txtTotalLabel = view.findViewById(R.id.txtTotalLabel)

        dateview = view.findViewById(R.id.dateview)

        bc1.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (event!!.action == KeyEvent.ACTION_DOWN) {

                        if ("" == bc1.text.toString()) {
                            txt_error.text = "Pls Scan barcode!"
                            bc1.requestFocus()
                        } else {
                            OnSaveData()
                        }
                    }
                    return true //important for event onKeyDown
                }


                return false
            }

        })

        dateview.BeginInit()

        dateview.addColumn("ITEM_BC",
            DataGridView.ViewMath()
                .convertDpToPixelInt(80.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("TR_QTY", DataGridView.ViewMath()
            .convertDpToPixelInt(50.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("TR_LOT_NO", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("WH_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(100.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("LINE_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("ITEM_CODE", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.addColumn("ITEM_NAME", DataGridView.ViewMath()
            .convertDpToPixelInt(70.0.toFloat(),BaseActivity().baseContext))
        dateview.EndInit()

        dateview.itemClickListener = object : DataGridView.HandlerItemClick() {
            override fun onClickCell(position: Int, col: Int): Boolean {

                bc2.setText(dateview.getDataCell(position,0))
                return true
            }
        }

        btnUploadGood.setOnClickListener(View.OnClickListener {
            onClickUploadDataGoods()
        })

        btnSearch.setOnClickListener(View.OnClickListener {
            onClickSearch()
        })

        btnDelete.setOnClickListener(View.OnClickListener {
            Delete()
        })



        bc1.findFocus();

        OnShowGW()

        return view
    }


    fun OnSaveData() {

        txt_error.setTextColor(Color.RED)
        txt_error.setTextSize(16.0.toFloat())
        if (bc1.text.toString() == "") {
            txt_error.setText("Pls Scan barcode!")
            return
            //bc1.requestFocus();
        }
        val str_scan = bc1.text.toString().toUpperCase()
        val l_lenght = bc1.text.toString().length
        val str_req_no = bc1.text.toString().substring(1, l_lenght)


        try {
            if (str_scan == "") {
                txt_error.setText("Pls Scan barcode!")

                return
            }
            //scan slip no
            if (str_scan.indexOf("S") == 0) {
                //System.out.println("str_req_no " +str_req_no);
                val l_HaveData: Boolean = OnLoadSlipNo(str_req_no)
                //System.out.println("slip no 33:"+l_HaveData);
                if (!l_HaveData) {
                    txt_error.setText("Slip No is not found!")
                    // bc1.getText().clear();
                    bc1.setText("")
                    bc1.requestFocus()
                    lbl_SlipNo1_Tag = ""
                    return
                } else {
                    //bc1.setText("");
                    bc1.text.clear()

                    txt_error.setText("")
                    //ClearControl();
                    return
                }
            }
            //scan already slip no
            if (lbl_SlipNo1_Tag.equals("")) {
                txt_error.setText("Pls Scan Slip No!")
                bc1.text.clear()

                bc1.requestFocus()

                return
            }


            val l_rs: Boolean = OnLoadBC(str_scan, lbl_SlipNo1_Tag)
            if (l_rs) //scan barcode,has data then
            {
                val cnt: Int = Check_Exist_PK(str_scan) //check barcode exist in data base
                if (cnt > 0) //exist data
                {
                    txt_error.setText("Please,scan barcode other.So barcode $str_scan existed!!")
                    bc1.text.clear()
                    bc1.requestFocus()
                } else  //save data scanned
                {
                    if (OnSave()) {
                        txt_error.setText("Save success.")
                        bc1.text.clear()
                        bc1.requestFocus()
                        //OnShowGW_Header();
                        OnShowGW()
                    }
                }
            } else  //barcode error,or barcode not yet download to mobile
            {
                txt_error.setText("Item Bacode $str_scan has not exist.")
                //bc1.getText().clear();
                bc1.setText("")
                bc1.requestFocus()
            }
        } catch (ex: Exception) {
            //save data error write to file log
        }
    }

    fun OnLoadBC(l_bc_item: String, l_slip_no: String): Boolean {
        var l_RS = false
        var dp = BaseActivity().getSQLiteDatabase()

        val SQL_SEL_LABEL_REQ =
            "SELECT A.ITEM_BC,A.ITEM_CODE, A.ITEM_NAME, A.ITEM_TYPE, A.LOT_NO, A.LABEL_QTY, A.PK AS LABEL_PK, A.TLG_IT_ITEM_PK AS TR_ITEM_PK, A.LABEL_UOM, B.SLIP_NO," +
                    "B.REQ_QTY, B.OUT_WH_PK,B.TLG_GD_REQ_D_PK, B.TLG_GD_REQ_M_PK" +
                    " FROM TLG_LABEL AS A INNER JOIN" +
                    " TLG_GD_REQ AS B ON A.TLG_IT_ITEM_PK = B.REQ_ITEM_PK" +
                    " WHERE     (A.ITEM_BC = '" + l_bc_item + "') AND (B.SLIP_NO = '" + l_slip_no + "') " //='"+l_bc_item+"'

        var cursor = dp.rawQuery(SQL_SEL_LABEL_REQ, null)

        val i = 0

        if (cursor != null && cursor.moveToFirst()) {
            bc2.setText(cursor.getString(cursor.getColumnIndex("ITEM_BC")))
            code.setText(cursor.getString(cursor.getColumnIndex("ITEM_CODE")))
            name.setText(cursor.getString(cursor.getColumnIndex("ITEM_NAME")))
            edit_Qty.setText(cursor.getString(cursor.getColumnIndex("LABEL_QTY")))
            lotNo.setText(cursor.getString(cursor.getColumnIndex("LOT_NO")))
            req_qty.setText(cursor.getString(cursor.getColumnIndex("REQ_QTY")))
            reqBal2.setText(cursor.getString(cursor.getColumnIndex("TR_ITEM_PK")))
            P_REQ_D_PK = cursor.getString(cursor.getColumnIndex("TLG_GD_REQ_D_PK"))
            wh_Pk.setText(cursor.getString(cursor.getColumnIndex("OUT_WH_PK")))
            val pkwh = wh_Pk.text.toString().toInt()
            val ListWH = GetDataWH(pkwh)
            //System.out.println("ware house name:"+ListWH.get(0));
            if (ListWH != null) {
                wh_name.text = ListWH.get(0)
            }
            cursor.moveToNext()
            cursor.close()
            l_RS = true
        }
        dp.close()
        return l_RS
    }

    fun GetDataWH(p_pk: Int): List<String>? {

        val labels: MutableList<String> = ArrayList()
        // Select All Query tlg_in_warehouse
        val selectQuery =
            "SELECT  pk,wh_id||'-'||wh_name,wh_name FROM tlg_in_warehouse where pk=$p_pk order by  wh_id" // tlg_in_warehouse
        // db = this.getReadableDatabase();
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor != null && cursor.moveToFirst()) {
            do {
                labels.add(cursor.getString(2))
            } while (cursor.moveToNext())
        }
        // closing connection
        cursor.close()
        dp.close()
        return labels
    }

    fun Check_Exist_PK(l_bc_item: String): Int {


        val countQuery =
            "SELECT PK FROM INV_TR where tr_type='3' and ITEM_BC ='$l_bc_item' " //='"+l_bc_item+"'
        var Check_Exist_PK = 0
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(countQuery, null)
        //Check_Exist_PK=cursor.getCount();
        if (cursor != null && cursor.moveToFirst()) {
            Check_Exist_PK = cursor.getString(cursor.getColumnIndex("PK")).toInt()
            cursor.moveToNext()
            cursor.close()
            return Check_Exist_PK
        }
        dp.close()
        return Check_Exist_PK
    }

    private fun OnLoadSlipNo(str_scan_req: String): Boolean {
        var l_RS = false

        try {

            val SQL_SEL_GD_REQ =
                "SELECT DISTINCT TLG_GD_REQ_M_PK, SLIP_NO, SUM(REQ_QTY) AS REQ_QTY,OVER_RATIO FROM TLG_GD_REQ WHERE SLIP_NO ='$str_scan_req' GROUP BY TLG_GD_REQ_M_PK, SLIP_NO,OVER_RATIO  " //='"+l_bc_item+"'
            var dp = BaseActivity().getSQLiteDatabase()
            var cursor = dp.rawQuery(SQL_SEL_GD_REQ, null)
            if (cursor.getCount() < 1) {
                txt_error.setText("No have data! Pls Check download form again!!")
                l_RS = false
            }
            if (cursor != null && cursor.moveToFirst()) {
                reqNo.setText(cursor.getString(cursor.getColumnIndex("SLIP_NO")))
                lbl_SlipNo1_Tag = reqNo.text.toString()
                println("******lbl_SlipNo1_Tag:$lbl_SlipNo1_Tag")
                edit_Qty.setText(cursor.getString(cursor.getColumnIndex("REQ_QTY")))
                cursor.moveToNext()
                cursor.close()
                l_RS = true
            }
            dp.close()
        } catch (ex: java.lang.Exception) {
            l_RS = false
            bc1.text.clear()
            bc1.requestFocus()
        }
        return l_RS
    }


    private fun OnSave(): Boolean {

        try {

            // get data input control to var then insert db
            P_SLIP_NO = reqNo.text.toString()
            P_ITEM_BC = bc2.text.toString()
            P_ITEM_CODE = code.text.toString()
            P_ITEM_NAME = name.text.toString()
            P_TR_LOT_NO = lotNo.text.toString()
            P_TR_QTY = edit_Qty.getText().toString()
            P_TR_ITEM_PK = reqBal2.text.toString()
            P_WH_PK = wh_Pk.text.toString()
            P_WH_NAME = wh_name.text.toString()
            var dp = BaseActivity().getSQLiteDatabase()
            dp.execSQL(
                "INSERT INTO INV_TR(ITEM_BC,ITEM_CODE,ITEM_NAME,TR_LOT_NO,TR_QTY,SLIP_NO,TR_TYPE,TR_ITEM_PK,TLG_GD_REQ_D_PK,TR_WAREHOUSE_PK,WH_NAME) "
                        + "VALUES('"
                        + P_ITEM_BC + "','" +
                        P_ITEM_CODE + "','"
                        + P_ITEM_NAME + "','"
                        + P_TR_LOT_NO + "','"
                        + P_TR_QTY + "','"
                        + P_SLIP_NO + "','"
                        + _Scan_Type + "','"
                        + P_TR_ITEM_PK + "','"
                        + P_REQ_D_PK + "','"
                        + P_WH_PK + "','"
                        + P_WH_NAME
                        + "');"
            )
            //bc1.setText("");
            //bc1.requestFocus();
            //msg_error.setText("Save Error!!!");
            dp.close()
            return true
        } catch (ex: java.lang.Exception) {
            txt_error.setText("Save Error!!!")
            //return false;
        }
        return true
    }

    fun OnShowGW() //show data gridview
    {
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor1 = dp.rawQuery(
            "SELECT PK FROM INV_TR where TR_TYPE='3' order by PK desc ",
            null
        ) // TLG_LABEL
        var cursor = dp.rawQuery(
            "SELECT ITEM_BC, TR_QTY, TR_LOT_NO,WH_NAME,LINE_NAME,ITEM_CODE,ITEM_NAME FROM INV_TR where TR_TYPE='3' and DEL_IF  = 0 order by PK desc ",
            null
        ) // TLG_LABEL
        val l_total: Int = cursor1.getCount()
        val count: Int = cursor.getCount()

        handler.post(Runnable {
            txtTotalLabel.setText("Total :$l_total (Labels.)")
            dateview.clearAll()
        })


        if (cursor != null && cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false)
            {
                val arr= arrayOf(
                    cursor.getString(cursor.getColumnIndex("ITEM_BC")),
                    cursor.getString(cursor.getColumnIndex("TR_QTY")),
                    cursor.getString(cursor.getColumnIndex("TR_LOT_NO")),
                    cursor.getString(cursor.getColumnIndex("WH_NAME")),
                    cursor.getString(cursor.getColumnIndex("LINE_NAME")),
                    cursor.getString(cursor.getColumnIndex("ITEM_CODE")),
                    cursor.getString(cursor.getColumnIndex("ITEM_NAME"))
                )
                handler.post(Runnable {
                        dateview.addRow(
                            arr
                        )
                })
                cursor.moveToNext()
            }
        }
        cursor1.close()
        cursor.close()
        dp.close()
    }


    fun onClickUploadDataGoods() {


        BaseActivity().confirmDialog(
            "Confirm Upload...",
            "Are you sure you want to upload data to server?",
            object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {
                    if (bool) {
                        onStartUpdate()
                    }

                }
            })


    }


    fun onStartUpdate() {


        var dp = BaseActivity().getSQLiteDatabase()
        var cursor2 = dp.rawQuery(
            "select TLG_POP_LABEL_PK, TR_WAREHOUSE_PK,TR_LOC_ID, TR_QTY , TR_LOT_NO, TR_TYPE, ITEM_BC, TR_DATE, TR_ITEM_PK, TR_LINE_PK, ITEM_CODE, ITEM_NAME,ITEM_TYPE,PK,TLG_GD_REQ_D_PK, SLIP_NO  from INV_TR where TR_TYPE = '3' and DEL_IF ='0'",
            null
        )


        var USER = BaseActivity().getSharedPreferences().getString("login_user", "")

        Thread(Runnable {
            if (cursor2.moveToFirst()) {


                handler.post(Runnable {
                    BaseActivity().showProgressBar("Updateing....")
                    txt_error.text = ""
                })

                var j = 0
                do {
                    // labels.add(cursor.getString(1));
                    var para = ""
                    for (i in 0 until cursor2.columnCount-1) {
                        if (para.length <= 0) {
                            if (cursor2.getString(i) != null){
                                para += cursor2.getString(i) + "|"
                            } else para += "|"
                        } else {
                            para += if (cursor2.getString(i) != null) cursor2.getString(i) +  "|"   else "|"
                        }
                    }
                    para += "|$USER"

                    j++

                    handler.post(Runnable {
                        BaseActivity().showProgressBar(j.toString() + "/" + cursor2.count)
                    })



                    var result = BaseActivity().getWebService().GetDataTableArg("LG_MPOS_M020_UPLOAD_v2", para)
                    if(!result.isError){
                        val para1: List<String> = para.split("|")
                        var id = para1.get(13)

                        val sql =
                            "UPDATE INV_TR set DEL_IF=$id  where PK = $id"
                        dp.execSQL(sql)
                    }else{
                        handler.post(Runnable {
                            BaseActivity().showToast(result.message+ "/" +para)
                        })
                    }


                } while (cursor2.moveToNext())
                //////////////////////////


                handler.post(Runnable {
                    BaseActivity().hideProgressBar()


                })
                OnShowGW()
            } else {

                handler.post(Runnable {
                    txt_error.setTextColor(Color.RED)
                    txt_error.text = "Data is empty!"
                })

            }

            cursor2.close()
            dp.close()
        }).start()



    }

    fun onClickSearch(){
        var dp = BaseActivity().getSQLiteDatabase()
        var cursor = dp.rawQuery(
            "SELECT  ITEM_CODE,count(ITEM_CODE) as COUNT_ITEM,SUM(TR_QTY) AS QTY FROM INV_TR where TR_TYPE='3' GROUP BY ITEM_CODE  ",
            null
        );// TLG_LABEL
        var count = cursor.count
        var str = ""
        if (cursor != null && cursor.moveToFirst())
        {
            str = "ITEM \t\t\t COUNT  \t\t\t QTY\n----------------------------------------------------------------\n";
            for (i in 0 until cursor.columnCount-1) {
                str =
                    str + cursor.getString(cursor.getColumnIndex("ITEM_CODE")) + "\t\t\t\t " + cursor.getString(
                        cursor.getColumnIndex("COUNT_ITEM")
                    ) + "\t\t\t\t\t " + cursor.getString(cursor.getColumnIndex("QTY")) + "\n";
                str = str + "----------------------------------------------------------------\n";
                cursor.moveToNext();
            }
        }else{
            str = "Not found data.";
        }

        cursor.close();
        dp.close();

        handler.post(Runnable {
            BaseActivity().confirmDialog("List Item ...", str, object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {


                }
            })
        })


    }

    fun Delete() {

        txt_error.setTextColor(Color.RED)
        txt_error.textSize = 18.0.toFloat()
        var P_ITEM_BC = bc2.text.toString()
        if (P_ITEM_BC.length < 1) {
            handler.post(Runnable {
                BaseActivity().confirmDialog(
                    "Confirm Clear...",
                    "Are you sure you want to clear item B/C",
                    object : BaseActivity.ConfirmDialogCallBack {
                        override fun callBack(bool: Boolean) {
                            if (bool) {
                                OnDeleteAll()
                                bc2.text.clear()
                                code.text.clear()
                                name.text.clear()
                                lotNo.text.clear()
                                edit_Qty.text = ""

                                OnShowGW()
                                BaseActivity().showToast("Clear success..")
                            }
                        }
                    })
            })
        } else {
            handler.post(Runnable {
                BaseActivity().confirmDialog(
                    "Confirm Delete...",
                    "Are you sure you want to delete item B/C: $P_ITEM_BC ",
                    object : BaseActivity.ConfirmDialogCallBack {
                        override fun callBack(bool: Boolean) {
                            if (bool) {
                                OnDeleteBC(P_ITEM_BC)
                                bc2.text.clear()
                                code.text.clear()
                                name.text.clear()
                                lotNo.text.clear()
                                edit_Qty.text = ""
                                txt_error.text = "Delete data sucess." + P_ITEM_BC.toString()
                                OnShowGW()
                                BaseActivity().showToast("Delete success..")
                            }
                        }
                    })
            })

        }
    }

    fun OnDeleteBC(p_pk: String) {
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL("DELETE from INV_TR where ITEM_BC = '$p_pk'")
        dp.close()
    }

    fun OnDeleteAll() {
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL("DELETE from INV_TR where TR_TYPE='3' and DEL_IF = 0")
        dp.close()
    }


    fun SaveData() {
        val bc2 = bc2.text

        if (bc2.isEmpty() ) {
            txt_error.text = "Please, scan one barcode to update."

            return
        } else {
            BaseActivity().confirmDialog("Confirm update...", "Are you sure you want to update item B/C: $bc2", object : BaseActivity.ConfirmDialogCallBack {
                override fun callBack(bool: Boolean) {
                    handler.post(Runnable {
                        OnUpdateData()
                        OnShowGW()
                        BaseActivity().showToast("Update success..")
                    })


                }
            } )

        }
    }

    fun OnUpdateData() {
        val str_scan_bc = bc1.text.toString()
        val P_PK = Check_Exist_PK(str_scan_bc)


        // get data input control to var then update db
        var P_ITEM_BC = bc2.text.toString()
        var P_ITEM_CODE = code.text.toString()
        var P_ITEM_NAME = name.text.toString()
        var P_TR_LOT_NO = lotNo.text.toString()
        var P_TR_QTY = edit_Qty.text.toString()
        var dp = BaseActivity().getSQLiteDatabase()
        dp.execSQL(
            "update INV_TR set TR_QTY='"+P_TR_QTY+"' where pk ='"+P_PK+"'  "
        )

        txt_error.text = "Update data sucess."
        dp.close()
    }




}