package com.lamvuanh.lib

import android.R
import android.content.Context

import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import android.widget.Spinner

class Combobox @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr){


    var spinner: Spinner


    private var hashMapPK: LinkedHashMap<Int,String> = LinkedHashMap()

    lateinit var dataAdapter: ArrayAdapter<String>

    public lateinit  var listener: OnItemSelectedListener



    fun setData(data: Array<Array<String?>>) {
        setData(data, 0, 0)
    }

    fun setData(
        data: Array<Array<String?>>,
        titleCol: Int,
        valueCol: Int
    ) {
        val lstName: MutableList<String> = ArrayList()
        hashMapPK.clear()
        for (i in data.indices) {
            if (titleCol > data[i].size || valueCol > data[i].size) {
            } else {
                lstName.add(data[i][titleCol].toString())
                hashMapPK.put(i, data[i][valueCol].toString())
            }
        }
        if (lstName.size > 0) {
             dataAdapter = ArrayAdapter(
                context,
                R.layout.simple_list_item_1,
                lstName
            )
            dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)

                spinner.adapter = dataAdapter

        } else {
            spinner.adapter = null
        }
    }

    fun clearData(){
        spinner.adapter = null
        hashMapPK.clear()
    }

    fun value(): String {
        if(hashMapPK.size == 0){
            return "";
        }
        var locationPK = ""
        val pkType: String = hashMapPK.get(spinner.selectedItemPosition).toString()
        if (pkType != null) {
            locationPK = pkType.toString()
        }
        return locationPK
    }

    fun text(): String {

        if(hashMapPK.size == 0){
            return "";
        }
        return spinner.selectedItem.toString()

    }

    fun size():Int{
        return hashMapPK.size
    }

    /*thuan.dang 20200503 add lay vi tri cua hashmap , set vi tri i cua key vao trong list
    * dung trong viec lay du lieu database fill vao
    * */
    fun getPosition(value_key :Int) :Int
    {
        var i :Int = 0
        for(key in hashMapPK.keys){
            if( hashMapPK[key].toString() == value_key.toString())
            {

                 return  i;
            }
            i++
        }
        return -1
    }
    fun setSelectItem(value_key :Int)
    {
        var position :Int = getPosition(value_key)

        if( position != -1)
        {
            spinner.setSelection(position)

        }

    }
 /*thuan.dang 20200503 end*/


    init {
        spinner = Spinner(getContext())
        spinner.layoutParams =  LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )


        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if (::listener.isInitialized) {
                    listener.onItemSelected(parent, view, position, id)
                }
            } // to close the onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>) {

            }
        }




        addView(spinner)
    }
}
