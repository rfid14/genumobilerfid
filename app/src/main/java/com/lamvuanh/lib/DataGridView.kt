package com.lamvuanh.lib

import android.R
import android.content.Context
import android.content.res.Resources
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import java.io.InputStream
import kotlin.math.roundToInt


class DataGridView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
    ): LinearLayout(context, attrs, defStyleAttr){


    abstract class HandlerItemClick{
        abstract fun onClickCell(position: Int, col: Int):Boolean
    }
    abstract class CustomCell{
        abstract fun getView(position: Int, col: Int, value: String):View

    }

    abstract class ItemCountListener {
        abstract fun onChange(count: Int)
    }

    private var _context: Context
    private var _rootView: LinearLayout

    private val _column: MutableList<Column>
    private val _row: MutableList<Row>

    val border_color: Int = Color.WHITE
    var backgroud_header_color: Int = Color.GRAY

    var isHeaderTextAlign = false
    var test_id = 0
    var widthWithpercent = false
    var isSelectedEnabled = true

    var text_header_color: Int = Color.WHITE
    var border_cell_id = 0
//    val text_cell_color: Int = Color.BLACK
    val text_cell_color: Int = Color.parseColor("#42FF00")
    val row_backgroud_1: Int = Color.WHITE
    val row_backgroud_2: Int = Color.parseColor("#F5F5F5") //Color.WHITE;
    var row_backgroud_select: Int = Color.LTGRAY
    var mutilSelect :Boolean = true

    var header_height = 70
    private var min_row_height = 45
    var max_row_height = 0
    private var auto_resie  = false

    private var rootViewWidth = 1
    private val header: LinearLayout
    private val listView: ListView
    private val adapter: ListViewAdapter

    private var cur_count = 0
    lateinit var itemCountListener: ItemCountListener
    lateinit var itemClickListener: HandlerItemClick
    lateinit var customCell : CustomCell

    private var mLastClickTime: Long = 0
    private var mLastClickPos = 0

    var _onInit = false


    enum class TEXT_ALIGN(val value: Int) {
        TEXT_LEFT(0),
        TEXT_RIGHT(1),
        TEXT_CENTER(2);
        companion object {
            fun fromString(value: String) = values().first { it.ordinal == value.toInt() }
        }
    }

    enum class CELL_TYPE(val value: Int) {
        TYPE_TEXT(0),
        TYPE_CHECKBOX(1),
        TYPE_COMBOBOX(2),
        TYPE_IMAGEVIEW(3),
        TYPE_EDITVIEW(4),

        TYPE_CUSTOMVIEW(99);

        companion object {
            fun fromString(value: String) = values().first { it.ordinal == value.toInt() }
        }
    }

    val array_Insert: MutableList<Int> = ArrayList()
    val array_Update: MutableList<Int> = ArrayList()
    var array_Delete: MutableList<Int> = ArrayList()


    init {
        _context = getContext()
        _column = ArrayList()
        _row = ArrayList()
        _rootView = LinearLayout(getContext())
        _rootView.layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT)
        _rootView.orientation = VERTICAL

        val topLine = LinearLayout(getContext())
        topLine.layoutParams = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1)
        topLine.setBackgroundColor(border_color)

        val scrollView = HorizontalScrollView(getContext())
        scrollView.layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        //contect
        val contentLayout = LinearLayout(getContext())
        contentLayout.layoutParams = LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        contentLayout.orientation = VERTICAL
        contentLayout.setPadding(0, 2, 0, 0)
        contentLayout.setBackgroundColor(Color.BLACK)

        header = LinearLayout(getContext())
        header.layoutParams = LayoutParams( ViewGroup.LayoutParams.MATCH_PARENT, header_height)
        header.orientation = HORIZONTAL
        header.setBackgroundColor(backgroud_header_color)
        listView = ListView(getContext())
        listView.setLayoutParams(LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
        contentLayout.addView(header)
        contentLayout.addView(listView)
        scrollView.addView(contentLayout)
        _rootView.addView(topLine)
        _rootView.addView(scrollView)
        addView(_rootView)


        adapter = ListViewAdapter()
        listView.setAdapter(adapter)


        try{
            val drawableResId  = context.resources.getIdentifier(
                "border_primary_cell",
                "drawable",
                context.packageName
            )

            setBorderCellDrawable(drawableResId)
        }
        finally {

        }


        setHeaderStyle(
            Color.WHITE,
            Color.parseColor("#1F4E78"),
            ViewMath().convertDpToPixelInt(40f, context)
        )
        setContentBackgroundColor(Color.WHITE)
        setBackgroundColor(Color.WHITE)
        isHeaderTextAlign = true
        widthWithpercent = false

        makeHeaderRow()
    }



    fun BeginInit() {
        _onInit = true
    }

    fun setWidthwWithpercent(widthwWithpercent: Boolean) {
        if (this.widthWithpercent != widthwWithpercent) {
            this.widthWithpercent = widthwWithpercent
            handler.post( { makeHeaderRow() })
        }
    }
    fun EndInit() {
        _onInit = false
        makeHeaderRow()
    }



    private fun  onCellClick(position: Int,col:Int){
        if (::itemClickListener.isInitialized) {

            if( itemClickListener.onClickCell(position,col)){
                handler.post( {
                    _row[position].isSelectMode = !_row[position].isSelectMode

                    adapter.notifyDataSetChanged()
                })
            }

        }else{
            handler.post( {
                  _row[position].isSelectMode = !_row[position].isSelectMode
                    adapter.notifyDataSetChanged()
            })
        }


    }

    fun setColumnType(col:Int,type:CELL_TYPE){
        if(col < _column.size){
            _column[col].type = type
        }
    }

    fun selectAll(){
        if(isSelectedEnabled) {
            try {
                handler.post( {
                    var checkSelectAll = true
                    _row.forEach({
                        if(it.isSelectMode == false){
                            checkSelectAll = false
                            return@forEach
                        }
                    })

                    if(checkSelectAll == true){
                        _row.forEach({
                            it.isSelectMode = false
                        })
                    }else{
                        _row.forEach({
                            it.isSelectMode = true
                        })
                    }



                    adapter.notifyDataSetChanged()
                })
            } catch (ex: Exception) {
            }
        }
    }


    fun makeHeaderRow() {
        if (_onInit) return

            header.setBackgroundColor(backgroud_header_color)
            header.removeAllViews()
            if (Columns().size == 0) {
                header.layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1
                )
            } else {
                header.layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    header_height
                )
            }
            if (widthWithpercent) {
                var iSum = 0
                for (i in Columns().indices) {
                    iSum += Columns()[i].widthpercent
                }
                if (iSum > 0) {
                    val rate = (rootViewWidth * 1.0 / iSum).toFloat()
                    for (i in Columns().indices) {
                        Columns()[i].width = (Columns()[i].widthpercent * rate).toInt()
                    }
                }
            } else {
                for (i in Columns().indices) {
                    Columns()[i].width = ViewMath().convertDpToPixelInt(
                        Columns()[i].widthpercent.toFloat(),
                        context
                    )
                }
            }
            for (i in Columns().indices) {
                val col = Columns()[i]
                if (col.width != 0) {
                    val contentLayout = LinearLayout(context)
                    contentLayout.layoutParams = LayoutParams(
                        col.width,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    contentLayout.gravity = Gravity.CENTER
                    contentLayout.orientation = HORIZONTAL
                    contentLayout.setPadding(2, 2, 2, 2)
                    val contentLayoutContent = LinearLayout(context)
                    contentLayoutContent.layoutParams = LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    contentLayoutContent.gravity = Gravity.CENTER
                    contentLayoutContent.orientation = HORIZONTAL
                    //contentLayoutContent.setPadding(5, 3, 5, 3);
                    if (col.backgroundcolor != 0) {
                        contentLayoutContent.setBackgroundColor(col.backgroundcolor)
                    }
                    if (border_cell_id != 0) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            contentLayout.setBackgroundDrawable(
                                ContextCompat.getDrawable(
                                    context,
                                    border_cell_id
                                )
                            )
                        } else {
                            contentLayout.background = ContextCompat.getDrawable(
                                context,
                                border_cell_id
                            )
                        }
                    }
                    val textView = TextView(context)
                    textView.layoutParams = LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    textView.text = col.text
                    textView.setTextColor(text_header_color)
                    textView.setPadding(3, 8, 3, 8)
                    if (isHeaderTextAlign) {
                        textView.gravity = Gravity.CENTER
                    } else {
                        when (col.textAlign) {
                            TEXT_ALIGN.TEXT_CENTER -> textView.gravity = Gravity.CENTER
                            TEXT_ALIGN.TEXT_RIGHT -> textView.gravity = Gravity.END
                            else -> textView.gravity = Gravity.START
                        }
                    }
                    contentLayoutContent.addView(textView)
                    contentLayout.addView(contentLayoutContent)
                    header.addView(contentLayout)
                }
            }
            adapter.notifyDataSetChanged()

    }

    fun setHeaderBackgroundColor(color: Int) {
        backgroud_header_color = color
        header.setBackgroundColor(backgroud_header_color)

    }

    fun setHheaderHeight(height: Int) {
        header_height = height
        makeHeaderRow()
    }

    fun setHeaderTextColor(color: Int) {
        text_header_color = color
        makeHeaderRow()
    }

    fun setHeaderStyle(color: Int, backgroundColor: Int, height: Int) {
        text_header_color = color
        header_height = height
        backgroud_header_color = backgroundColor
        header.setBackgroundColor(backgroundColor)
        makeHeaderRow()
    }

    fun setDisplayHeader(boolean: Boolean){
        if (boolean )
            header.visibility = View.VISIBLE
        else
            header.visibility = View.GONE
    }

    fun setContentBackgroundColor(color: Int) {
        listView.setBackgroundColor(color)
    }

    fun setBorderCellDrawable(id: Int) {
        border_cell_id = id
        makeHeaderRow()
    }

    fun Columns(): List<Column> {
        return _column
    }

    fun clearColumn(){
        _column.clear()
    }

    fun addColumn(title: String) {
        _column.add(Column(title))
        makeHeaderRow()
    }

    fun addColumn(title: String, size: Int) {

        _column.add(Column(title,size))
        makeHeaderRow()
    }

    fun addColumn(title: String, size: Int, type: String, textAlign: String) {
        _column.add(Column(title,size, CELL_TYPE.fromString(type), TEXT_ALIGN.fromString(textAlign)))
        makeHeaderRow()
    }

    fun addColumn(col: Column) {
        _column.add(col)
        makeHeaderRow()
    }

    fun addColumns(cols: Array<Column>?) {
        if (cols == null) return
        for (i in cols.indices) _column.add(cols[i])
        makeHeaderRow()
    }

    fun addColumns(title: Array<String>?) {
        if (title == null) return
       // val cols = arrayOfNulls<Column>(title.size)
        for (i in title.indices) {
            _column.add(Column(title[i]))
        }
        makeHeaderRow()
    }

    fun setWidthColumns(sizes: IntArray?) {
        if (sizes == null) return
        //val cols = arrayOfNulls<Column>(sizes.size)
        for (i in sizes.indices) {
            if (i <= _column.size) {
                _column[i].width = ViewMath().convertDpToPixelInt( sizes[i].toFloat(),context)
            }
        }
        makeHeaderRow()
    }

    fun setTextAlignmentColumns(aligns: Array<TEXT_ALIGN>) {
//        if (aligns == null) return
        //val cols =  arrayOfNulls<Column>(aligns.size)
        for (i in aligns.indices) {
            if (i <= _column.size) {
                _column[i].textAlign = aligns[i]
            }
        }
        makeHeaderRow()
    }

    fun Rows(): List<Row> {
        return _row
    }

    fun getDataCell(row: Int, col: Int):String{
        try{
           return _row[row].getCell()[col].value
        }catch (e:java.lang.Exception){

        }
        return "";
    }

    fun setColumnData(pos: Int, data: Array<Array<String>>) {
        if (pos < Columns().size) {
            Columns()[pos].setData(data)
        }
    }

    fun setData(data: Array<Array<String>>) {
        handler.post( {
            _row.clear()
            if (data.size > 0) {
                    for (i in data.indices) {
                        _row.add(Row(data[i]))
                    }
            }

            try {
                handler.post( { adapter.notifyDataSetChanged() })
            } catch (ex: Exception) {
                Log.d("NOTIFY", ex.message.toString())
            }
        })
    }



    fun setDataCell(icol: Int, iRow: Int, value: String) {

        if (iRow < _row.size) if (icol < _row[iRow].getCell().size) {
            if(_row[iRow].getCell()[icol].value !=  value) {
                _row[iRow].setCellData(icol, value)
                _row[iRow].isModified = true
                onArrayDLL(2 , iRow)
            }
        }
        try {
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun setDataCellNotUpdateView(icol: Int, iRow: Int, value: String) {

        if (iRow < _row.size)
            if (icol < _row[iRow].getCell().size) {
                if(_row[iRow].getCell()[icol].value !=  value) {
                    _row[iRow].setCellData(icol, value)
                    _row[iRow].isModified = true
                    onArrayDLL(2 , iRow)
                }
            }

    }

    fun addRow(dataRow: Array<String>) {
        _row.add(Row(dataRow))
        try {
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun addRow(row: Row) {
        _row.add(row)
        try {
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun removeRow(pos: Int) {
        try {
            _row.removeAt(pos)
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun selectRow(pos: Int, select: Boolean){
        try {

            _row.get(pos).isSelectMode =  select
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun selectRow(pos: Int){
        try {

            _row.get(pos).isSelectMode =  ! _row.get(pos).isSelectMode
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }
    }

    fun clearAll() {
        try {
            _row.clear()
            handler.post( { adapter.notifyDataSetChanged() })
        } catch (ex: Exception) {
            Log.d("NOTIFY", ex.message.toString())
        }

    }

    fun  deleteRow(pos : Int , option : String )
    {
        when (option) {
            "0" -> array_Delete.remove(pos);
            "1" -> onArrayDLL(3 , pos)
            else -> { // Note the block
                Log.d("Error : " , "Option input incorrect")
            }
        }
    }

    fun onArrayDLL(option : Int , position: Int)
    {
        /*1.insert , 2.update , 3.delete , 4.clear all array*/
        when (option) {
            1 ->{
                if(array_Insert.indexOf(position) < 0  )
                {
                    array_Insert.add(position);
                    array_Update.remove(position);
                    array_Delete.remove(position)
                }

            }
            2 ->{

                if(array_Update.indexOf(position) < 0 )
                {
                    array_Update.add(position);
                    array_Insert.remove(position);
                    array_Delete.remove(position);
                }
            }
            3 ->{
                array_Delete.add(position);
                array_Insert.remove(position);
                array_Update.remove(position);
            }
            4 ->{
                array_Delete.clear();
                array_Insert.clear();
                array_Update.clear();
            }
            else -> { // Note the block
                Log.d("Error : " , "Option input incorrect")
            }
        }
    }

    fun getDataDLL(userid :String ) :String
    {
        var data = ""
        var action = ""
       try {
           /*INSERT*/

           if(array_Insert.size > 0)
           {
               array_Insert.forEach {
                   action = "*!!*INSERT"
                   if (data == "")
                   {
                       data = action
                   }
                   else
                   {
                       data = data + "|"  + action
                   }
                   for (i in Columns().indices)
                   {
                       data = data + "|" + _row[it].getCell()[i].value
                   }
                   data = data + "|" + userid
               }
           }
           /*UPDATE*/

           if(array_Update.size > 0)
           {
               array_Update.forEach {
                   action = "*!!*UPDATE"

                   if (data == "")
                   {
                       data = action
                   }
                   else
                   {
                       data = data + "|"  + action
                   }
                   for (i in Columns().indices)
                   {
                       data = data + "|" + _row[it].getCell()[i].value

                   }
                   data = data + "|" + userid
               }

           }
            /*DELETE*/
         //  var data_del :String = ""
           if(array_Delete.size > 0)
           {
               array_Delete.forEach {
                   action = "*!!*DELETE"
                   if (data == "")
                   {
                       data = action
                   }
                   else
                   {
                       data = data + "|"  + action
                   }
                   for (i in Columns().indices)
                   {
                       data = data + "|" + _row[it].getCell()[i].value
                   }
                   data = data + "|" + userid
               }
           }
           onArrayDLL(4 , 0)
           return data
       }
       catch (e : Exception)
       {
            Log.d("Get Data Error : " , e.toString())
       }
        return ""
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)


        //Log.d("HEI", listView.measuredHeight.toString())

        if(changed) {
            if (rootViewWidth != _rootView.width) {
                rootViewWidth = _rootView.width
                makeHeaderRow()

            }

            Log.d("HEI", (height - t).toString())

            if(auto_resie) {
                Log.d("HEI", (height - t).toString())
                listView.setLayoutParams(
                    LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                    (height - t) - header_height
                )
                )
            }

        }
    }

    class Column {
        var textAlign: TEXT_ALIGN = TEXT_ALIGN.TEXT_LEFT
        var text: String? = null
        var width = 150
        var widthpercent = 150
        var type:CELL_TYPE  = CELL_TYPE.TYPE_TEXT
        var backgroundcolor = 0

        private val dataKey: LinkedHashMap<Int,String> = LinkedHashMap()
        private val dataName: MutableList<String> =  ArrayList()


        constructor(text: String) : this(text,150)
        constructor(text: String,width: Int) : this(text,width,CELL_TYPE.TYPE_TEXT)
        constructor(text: String,  width: Int, type: CELL_TYPE): this(text,width,type,TEXT_ALIGN.TEXT_LEFT)
        constructor(text: String,  width: Int, type: CELL_TYPE, textAlign: TEXT_ALIGN){
            this.text = text
            this.width = width
            this.widthpercent = width
            this.type = type
            this.textAlign = textAlign

        }

        fun setData(data: Array<Array<String>>) {
            dataKey.clear()
            dataName.clear()
            for (i in data.indices) {
                try {
                    dataName.add(data[i][1])
                    dataKey.put(i, data[i][0])
                } catch (ex: Exception) {
                }
            }
            type = CELL_TYPE.TYPE_COMBOBOX
        }

        fun getDataKey(): LinkedHashMap<Int,String> {
            return dataKey
        }

        fun getDataName(): List<String> {
            return dataName
        }




    }

    class Row(cells: Array<String>, background: Int = 0 ) {
        var isModified = false
        var bClr = 0
        var isSelectMode = false

        var Action = ""
        private var cell: MutableList<Cell> =   ArrayList()


        init {
            cell.clear()
            for (i in cells.indices)
                try {
                    cell.add(Cell(cells[i]))
                }catch (ex: java.lang.Exception){
                    cell.add(Cell(""))
                }

            bClr = background
        }

        fun getCell(): List<Cell> {
            return cell
        }

//        fun setCell(pcells: List<Any?>?) {
//            this.cell = pcells
//        }

        fun setCell(cells: Array<Any>) {
            cell.clear()
            for (i in cells.indices) cell.add(Cell(cells.get(i).toString(),0,0 ))
        }

        fun setCellData(pos: Int, value: String) {
            if (pos < cell.size) {
                cell[pos].value = value

            }
        }

        fun setCellData(pos: Int, value: Cell) {
            if (pos < cell.size) {
                cell.removeAt(pos)
                cell.add(pos, value)
            }
        }


    }

    class Cell {
        var backgroundcolor = 0
        var textcolor = 0
        var value: String = ""
        var allowEdit = true


        constructor(text: String) : this(text,true)

        constructor(text: String, allowEdit: Boolean) : this(text,allowEdit,Color.TRANSPARENT, Color.BLACK)

        constructor(text: String,backgroundcolor: Int, textcolor: Int): this(text,true,backgroundcolor,textcolor)

        constructor(text: String,allowEdit:Boolean, backgroundcolor: Int,textcolor: Int){
            this.value = text
            this.allowEdit = allowEdit
            this.backgroundcolor = backgroundcolor
            this.textcolor = textcolor
        }


        override fun toString(): String {
            return value
        }

        fun toInt(): Int {
            try {
                return value.toInt()
            } catch (ex: Exception) {
            }
            return -1
        }

    }

    private inner class ListViewAdapter : BaseAdapter() {
        override fun notifyDataSetChanged() {
            if (::itemCountListener.isInitialized) if (count != cur_count) {
                cur_count = count
                itemCountListener.onChange(count)
            }
            super.notifyDataSetChanged()

        }


        override fun getCount(): Int {
            return _row.size
        }

        override fun getItem(position: Int): Row? {
             return if ( position < _row.size)
                _row[position]
            else
                null
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
            val rowView = LinearLayout(context)

            if(max_row_height != 0){
                rowView.minimumHeight = max_row_height

                rowView.layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    max_row_height
                )
            }else{
                rowView.minimumHeight = min_row_height
                rowView.layoutParams = LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            }

            rowView.orientation = HORIZONTAL
            val row = getItem(position) ?: return rowView

            if (row.bClr != 0) {
                rowView.setBackgroundColor(row.bClr)
            } else if (row.isSelectMode) {

                /*thuan.dang 20200429 add mutilSelect dung de highligh dong dang chon
                gridview.mutilSelect = false */
                if( !mutilSelect )
                {
                    row.isSelectMode = false
                }
                /*thuan.dang 20200429 end  mutilSelect dung de highligh dong dang chon*/
                rowView.setBackgroundColor(row_backgroud_select)
            }

//            else if (row.isModified) {
//                rowView.setBackgroundColor(Color.RED) // TODO CHANGE HERE
//            }
            else {
                rowView.setBackgroundColor(if (position % 2 == 0) row_backgroud_1 else row_backgroud_2)
            }
            for (j in row.getCell().indices) {
                var col: Column? = null
                if (j < Columns().size) {
                    col = Columns()[j]
                }
                if (col == null) {
                    col = Column("INDEX-$position" )
                }
                if (col.width != 0) {
                    val contentLayout = LinearLayout(context)
                    contentLayout.layoutParams = LayoutParams(
                        col.width,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    contentLayout.gravity = Gravity.CENTER
                    contentLayout.orientation = HORIZONTAL
                    contentLayout.setPadding(2, 2, 2, 2)
                    if (border_cell_id != 0) {
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            contentLayout.setBackgroundDrawable(
                                ContextCompat.getDrawable(
                                    context,
                                    border_cell_id
                                )
                            )
                        } else {
                            contentLayout.background = ContextCompat.getDrawable(
                                context,
                                border_cell_id
                            )
                        }
                    }
                    val cell = row.getCell()[j]
                    val contentLayoutContent = LinearLayout(context)
                    contentLayoutContent.layoutParams = LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                    )
                    contentLayoutContent.gravity = Gravity.CENTER
                    contentLayoutContent.orientation = HORIZONTAL
                    //contentLayoutContent.setPadding(5, 3, 5, 3);
                   // if (cell.backgroundcolor != 0) {
                        contentLayoutContent.setBackgroundColor(cell.backgroundcolor)
                   // }
                    when (col.type) {
                        CELL_TYPE.TYPE_CHECKBOX -> {
                            val checkBox = CheckBox(context)
                            checkBox.layoutParams = LayoutParams(
                                ViewGroup.LayoutParams.WRAP_CONTENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )
                            checkBox.isChecked =  if (row.getCell()[j].toString() == "0") false else true
                            if(!cell.allowEdit){
                                checkBox.isEnabled = false
                            }
                            checkBox.setOnCheckedChangeListener { _, isChecked ->
                                if (isChecked) {
                                    setDataCellNotUpdateView(j,position, "1")
                                }
                                else
                                {
                                    setDataCellNotUpdateView(j,position, "0")
                                }
                                onCellClick(position,j)
                            }
//                            checkBox.setOnClickListener(OnClickListener {
////
////                            })
                            contentLayout.gravity = Gravity.CENTER
                            contentLayout.addView(checkBox)

                        }
                        CELL_TYPE.TYPE_COMBOBOX -> {
                            val spinner = Spinner(context)
                            spinner.layoutParams = LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.MATCH_PARENT
                            )
                            val dataAdapter =
                                ArrayAdapter(
                                    context,
                                    R.layout.simple_list_item_1,
                                    col.getDataName()
                                )
                            dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item)
                            spinner.adapter = dataAdapter
                            var pos = 0
                            while (pos < col.getDataKey().size ) {
                                if (row.getCell()[j].toString().contains(
                                        col.getDataKey().values.toTypedArray().get(
                                            pos
                                        )
                                    )
                                ) {
                                    spinner.setSelection(pos)
                                    break
                                }
                                pos++
                            }
                            if(!cell.allowEdit){
                                spinner.isEnabled = false
                            }
                            contentLayout.addView(spinner)
                        }
                        CELL_TYPE.TYPE_IMAGEVIEW -> {
                            val imageButton = ImageButton(context)
                            imageButton.layoutParams = LayoutParams(
                                ViewGroup.LayoutParams. MATCH_PARENT,
                                100
                            )
                            imageButton.maxHeight = 100
                            imageButton.isDuplicateParentStateEnabled = true
                            imageButton.setOnClickListener({
                                onCellClick(position,j)
                            })

                            if(!cell.allowEdit){
                                imageButton.isEnabled = false
                            }


                            //cell.value ="content://media/external/images/media/201"
                            try {
                                if(!cell.value.isEmpty() && cell.value.length > 10  ) {
                                    if(cell.value.startsWith("content://") || cell.value.startsWith("/")){
                                        val uri = Uri.parse(cell.value)

                                         imageButton.setImageURI(uri)
                                    }else {
                                        val imageBytes = Base64.decode(cell.value, 0)
                                        val image = BitmapFactory.decodeByteArray(
                                            imageBytes,
                                            0,
                                            imageBytes.size
                                        )

                                        imageButton.setImageBitmap(image)
                                    }
                                }else{
                                    imageButton.setImageResource( R.drawable.ic_menu_camera)
                                }
                            }catch (ex : Exception){


                            }
                            contentLayout.addView(imageButton)
                        }
                        CELL_TYPE.TYPE_EDITVIEW -> {
                            val editView = EditText(context)
                            editView.layoutParams = LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )
                            editView.setText(cell.toString() )
                            editView.setTextColor(text_cell_color)
                            editView.setPadding(3, 2, 3, 2)
                            when (col.textAlign) {
                                TEXT_ALIGN.TEXT_CENTER -> editView.gravity = Gravity.CENTER
                                TEXT_ALIGN.TEXT_RIGHT -> editView.gravity = Gravity.END
                                else -> editView.gravity = Gravity.START
                            }
//                            editView.setOnClickListener(OnClickListener {
//                                onCellClick(position,j)
//                            })
                            editView.addTextChangedListener(object : TextWatcher {
                                override fun afterTextChanged(p0: Editable?) {
                                    setDataCellNotUpdateView(j,position, p0.toString())
                                }

                                override fun beforeTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {

                                }

                                override fun onTextChanged(
                                    p0: CharSequence?,
                                    p1: Int,
                                    p2: Int,
                                    p3: Int
                                ) {
                                }

                            })
                            contentLayoutContent.addView(editView)
                            contentLayout.addView(contentLayoutContent)
                        }
                        CELL_TYPE.TYPE_CUSTOMVIEW -> {
                            if(::customCell.isInitialized){
                                val view = customCell.getView(position,j,cell.value)
                                view.setOnClickListener({
                                    onCellClick(position,j)
                                })
                                contentLayout.addView(view)
                            }else{

                                //FOR TEXT
                                val textView = TextView(context)
                                textView.layoutParams = LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT
                                )
                                textView.text = cell.toString()
                                textView.setTextColor(text_cell_color)
                                textView.setPadding(3, 2, 3, 2)
                                when (col.textAlign) {
                                    TEXT_ALIGN.TEXT_CENTER -> textView.gravity = Gravity.CENTER
                                    TEXT_ALIGN.TEXT_RIGHT -> textView.gravity = Gravity.END
                                    else -> textView.gravity = Gravity.START
                                }
                                if (cell.textcolor != 0) {
                                    textView.setTextColor(cell.textcolor)
                                }
//                                textView.setOnClickListener(OnClickListener {
//                                    onCellClick(position,j)
//                                })
                                contentLayoutContent.addView(textView)
                                contentLayout.addView(contentLayoutContent)
                            }
                        }
                        else -> {
                            val textView = TextView(context)
                            textView.layoutParams = LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT
                            )
                            textView.text = cell.toString()
                            textView.setTextColor(text_cell_color)
                            textView.setPadding(3, 2, 3, 2)
                            when (col.textAlign) {
                                TEXT_ALIGN.TEXT_CENTER -> textView.gravity = Gravity.CENTER
                                TEXT_ALIGN.TEXT_RIGHT -> textView.gravity = Gravity.END
                                else -> textView.gravity = Gravity.START
                            }
                            if (cell.textcolor != 0) {
                                textView.setTextColor(cell.textcolor)
                            }
                            textView.setOnClickListener(OnClickListener {
                                onCellClick(position,j)
                            })
                            contentLayoutContent.addView(textView)
                            contentLayout.addView(contentLayoutContent)

                        }
                    }

                    rowView.addView(contentLayout)
                }
            }
            return rowView
        }
    }

    class ViewMath {
        fun convertDpToPixelInt(dp: Float, context: Context): Int {
            return convertDpToPixel(dp, context).roundToInt()
        }

        fun convertDpToPixelInt(dp: Int, context: Context): Int {
            return convertDpToPixel(dp.toFloat(), context).roundToInt()
        }

        private fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources: Resources = context.resources
            val metrics: DisplayMetrics = resources.displayMetrics
            return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        fun convertPixelsToDp(px: Float, context: Context): Float {
            return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }
    }

//    class MyBitmap{
//        fun decodeSampledBitmapFromResource(
//            uri: Uri,
//            reqWidth: Int,
//            reqHeight: Int
//        ): Bitmap {
//            // First decode with inJustDecodeBounds=true to check dimensions
//            return BitmapFactory.Options().run {
//                inJustDecodeBounds = true
//                BitmapFactory.decodeResource(res, resId, this)
//
//                // Calculate inSampleSize
//                inSampleSize = calculateInSampleSize(this, reqWidth, reqHeight)
//
//                // Decode bitmap with inSampleSize set
//                inJustDecodeBounds = false
//
//                BitmapFactory.decodeResource(res, resId, this)
//            }
//        }
//        fun calculateInSampleSize(
//            options: BitmapFactory.Options,
//            reqWidth: Int,
//            reqHeight: Int
//        ): Int {
//// Raw height and width of image
//            val height = options.outHeight
//            val width = options.outWidth
//            var inSampleSize = 1
//            if (height > reqHeight || width > reqWidth) {
//                val halfHeight = height / 3
//                val halfWidth = width / 3
//
//                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
//                // height and width larger than the requested height and width.
//                while (halfHeight / inSampleSize > reqHeight
//                    && halfWidth / inSampleSize > reqWidth
//                ) {
//                    inSampleSize *= 2
//                }
//            }
//            return inSampleSize
//        }
//    }


    // private  OnClickListener clickListener = null;

}
