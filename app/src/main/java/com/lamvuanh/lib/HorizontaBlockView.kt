package com.lamvuanh.lib

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children
import androidx.core.view.get
import com.genuwin.mobile.R
import java.util.*
import kotlin.math.roundToInt


class HorizontaBlockView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
): LinearLayout(context, attrs, defStyleAttr){


    private var _context: Context

    private var selectIndex  = -1
    private val _block: MutableList<Block>
    private var body: LinearLayout

    lateinit var customview:CustomView
    lateinit var handlerClickBlock: HandlerClickBlock
    var withper = 1.5







    init {
        _context = getContext()
        _block = ArrayList()





        val scrollView = HorizontalScrollView(getContext())
        scrollView.layoutParams = LayoutParams(MATCH_PARENT,MATCH_PARENT )
        scrollView.setPadding(10,10,10,10)


        val contentLayout = LinearLayout(getContext())
        contentLayout.layoutParams = LayoutParams( MATCH_PARENT, MATCH_PARENT)
        contentLayout.orientation = VERTICAL
        contentLayout.setPadding(0, 2, 0, 0)
       // contentLayout.setBackgroundColor(Color.BLACK)

        body = LinearLayout(getContext())
        body.layoutParams = LayoutParams( WRAP_CONTENT, MATCH_PARENT)
        body.orientation = HORIZONTAL


        contentLayout.addView(body)
        scrollView.addView(contentLayout)
        addView(scrollView)

    }

    private fun getView(position: Int):View{

        val contentLayout = ConstraintLayout(context)
        contentLayout.layoutParams = LayoutParams(  ((body.height-10) * withper).roundToInt()  , body.height-10)
        contentLayout.setPadding(5, 5, 5, 5)






        var view: View


            if (::customview.isInitialized){
                try {
                    view= customview.getView(position, contentLayout)
                }catch (e: java.lang.Exception){
                    view =  onGetView(position, contentLayout)
                }

            }else {
                view = onGetView(position, contentLayout)

            }
        view.layoutParams = LayoutParams( ((body.height-10) * withper).roundToInt()  , body.height-10)
        handler.post(Runnable {
            contentLayout.addView( view)
        })






        var btn = LinearLayout(context)
        btn.layoutParams = LayoutParams( ((body.height-10) * withper).roundToInt()  , body.height-10)
        btn.setBackgroundColor(Color.TRANSPARENT)
       // btn.setBackgroundColor(Color.RED)
//        var textView  = TextView(context)
//        textView.layoutParams = LayoutParams(  MATCH_PARENT, 2)
//        textView.minWidth = body.height-10
//        textView.text ="                  "
//        btn.addView(textView)

        handler.post(Runnable {
            contentLayout.addView( btn)
        })
            btn.setOnClickListener(View.OnClickListener {

                this.onClickBlock(position)
            })



        return contentLayout
    }

    fun get(index: Int): Array<String>? {
        try {
            return _block.get(index).getData()
        }catch (ex: java.lang.Exception){
            return null
        }

    }

    fun getSelectValue():  Array<String>?{
        try {
            return _block.get(selectIndex).getData()
        }catch (ex: java.lang.Exception){
            return null
        }
    }

    fun add(arr: Array<String>){
        _block.add(Block(arr))
        handler.post(Runnable {
            readerView()
        })
    }

    fun size():Int{
        return _block.size
    }

    fun onGetView(position: Int,viewgroup :View): View{

//        var iView = viewgroup  as LinearLayout
//        var textView = TextView(context)
//        textView.layoutParams = LayoutParams( MATCH_PARENT, MATCH_PARENT)
//        textView.setTextColor( Color.RED )
//
//        try {
//            textView.text = _block.get(position).getData().get(0).toString()
//        }catch (e: java.lang.Exception){}
//
//
//
//
//        val contentLayout = LinearLayout(getContext())
//        Log.d("TAG" ,body.toString())
//        contentLayout.layoutParams = LayoutParams( WRAP_CONTENT, body.height)
//        contentLayout.orientation = VERTICAL
//        contentLayout.setPadding(0, 0, 10, 0)
//        contentLayout.setBackgroundColor(Color.BLUE)
//        contentLayout.minimumWidth = body.height
//
//        contentLayout.addView(textView)
//        iView.addView(contentLayout)



        return viewgroup
    }





    abstract class CustomView{
        abstract fun getView(position: Int,viewgroup :View):View
    }

    abstract class HandlerClickBlock{
        abstract fun onSelectBlockIndex(position: Int, status: Boolean) :Boolean
        abstract fun onSelectBlockIndexed(position: Int, status: Boolean)
    }


   fun onClickBlock (position: Int){

       var cancel:Boolean = if( ::handlerClickBlock.isInitialized){
            handlerClickBlock.onSelectBlockIndex(position, if (selectIndex == position) true else false)
       }else{
           false
       }

       if(cancel) return
       try{
           body.get(selectIndex).setBackgroundColor(Color.TRANSPARENT)
       }catch (e:java.lang.Exception){}

       if(selectIndex == position){
           selectIndex = -1
       }else{
           selectIndex = position
           body.get(selectIndex).setBackgroundColor(Color.YELLOW)
       }

       if(::handlerClickBlock.isInitialized){
           handlerClickBlock.onSelectBlockIndexed(position,if (selectIndex == -1) false else true  )
       }



   }





    fun  readerView(){
        body.removeAllViews()
        _block.forEachIndexed { index, _ ->



            body.addView(getView(index))


        }

        invalidate()
    }

    fun cancelClickable(view:View, value: Boolean){
        if(view is ViewGroup){
            if( (view).childCount > 0 )
            {
                (view).children.forEach {
                    cancelClickable(it,value)
                }
            }
        }

        //view.isClickable = false
        view.isDuplicateParentStateEnabled =value

    }



    fun setData(data: Array<Array<String>>) {
        handler.post(Runnable {
            _block.clear()
            selectIndex =-1
            if (data.size > 0) {
                for (i in data.indices) {
                    _block.add(Block(data[i]))
                }
            }

            try {
                handler.post(Runnable {readerView() })
            } catch (ex: Exception) {
                //Log.d("NOTIFY", ex.message)
            }
        })
    }

    fun clear(){

            _block.clear()
            selectIndex =-1


            try {
                readerView()
            } catch (ex: Exception) {
                //Log.d("NOTIFY", ex.message)
            }
    }

    fun clearSelect(){
        handler.post(Runnable {

            selectIndex =-1
            try {
                readerView()
            } catch (ex: Exception) {
                //Log.d("NOTIFY", ex.message)
            }

        })
    }










    class Block {
        var backgroundcolor = 0
        var textcolor = 0
        var value: Array<String>

        constructor(text: Array<String>) : this(text, Color.WHITE)

        constructor(text: Array<String>,backgroundcolor: Int): this(text,backgroundcolor, Color.BLACK)

        constructor(text: Array<String>, backgroundcolor: Int,textcolor: Int){
            value = text
            this.backgroundcolor = backgroundcolor
            this.textcolor = textcolor
        }


        fun getData(): Array<String> {
            return value
        }



    }



//    class ViewMath {
//        fun convertDpToPixelInt(dp: Float, context: Context): Int {
//            return Math.round(convertDpToPixel(dp, context))
//        }
//
//        fun convertDpToPixel(dp: Float, context: Context): Float {
//            val resources: Resources = context.getResources()
//            val metrics: DisplayMetrics = resources.getDisplayMetrics()
//            return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
//        }
//
//        fun convertPixelsToDp(px: Float, context: Context): Float {
//            return px / (context.getResources().getDisplayMetrics().densityDpi as Float / DisplayMetrics.DENSITY_DEFAULT)
//        }
//    }

    // private  OnClickListener clickListener = null;

}